---
version: "5.6.5"
title: "KDE Plasma 5.6.5, Bugfix Release"
errata:
    link: https://community.kde.org/Plasma/5.6_Errata
    name: 5.6 Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: 506e6d4dd4d5088
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.6.5 announcement](/announcements/plasma-5.6.5).
