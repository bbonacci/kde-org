KDE Project Security Advisory
=============================

Title:          KMail: JavaScript access to local and remote URLs
Risk Rating:    Critical
CVE:            CVE-2016-7967
Platforms:      All
Versions:       kmail 5.3.0
Author:         Andre Heinecke <aheinecke@intevation.de>
Date:           6 October 2016

Overview
========

KMail since version 5.3.0 used a QWebEngine based viewer
that had JavaScript enabled. Since the generated html is executed
in the local file security context by default access to remote and local
URLs was enabled.

Impact
======

An unauthenticated attacker can send out mails with malicious content
with executable JavaScript code that read or write local files and send them
to remote URLs or change the contents of local files in malicious ways. The
code is executed when when viewing HTML the mails.
Combined with CVE-2016-7966 the code could also be executed when viewing
plain text mails.

Workaround
==========

Assuming a version with CVE-2016-7966 fixed a user is protected
from this by only viewing plain text mails.

Solution
========

For KMail apply the following patch:
https://quickgit.kde.org/?p=messagelib.git&a=commitdiff&h=dfc6a86f1b25f1da04b8f1df5320fcdd7085bcc1

Credits
=======

Thanks to Roland Tapken for reporting this issue, Andre Heinecke from
Intevation GmbH for analysing and the problems and reviewing the fix
and Laurent Montel for fixing the issues.
