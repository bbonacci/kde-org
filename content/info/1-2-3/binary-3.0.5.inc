<ul>
<!--   CONECTIVA LINUX -->
<!--
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>:
  (<a href="http://download.kde.org/stable/3.0.5/Conectiva/README">README</a>
  /
  <a href="http://download.kde.org/stable/3.0.5/Conectiva/LEIAME">LEIAME</a>):
  <ul type="disc">
    <li>
      8.0:
      <a href="http://download.kde.org/stable/3.0.5/Conectiva/conectiva/RPMS.kde/">Intel
      i386</a>
    </li>
  </ul>
</li>
-->

<!--  DEBIAN -->
<li><a href="http://www.debian.org">Debian</a>:
    <ul type="disc">
<!--      <li>
         Debian unstable (sid):
         <a href="http://download.kde.org/stable/3.0.5/Debian/sid/">Intel i386</a>
      </li>
-->
      <li>
         Debian stable (woody):
         <a href="http://download.kde.org/stable/3.0.5/Debian/woody/">Intel i386</a>
      </li>
    </ul>
</li>

<!--   FREEBSD -->
<li>
  <a href="http://www.freebsd.org/">FreeBSD</a>
  (<a href="http://download.kde.org/stable/3.0.5/FreeBSD/README">README</a>):
  <ul type="disc">
    <li>4.7:
      <a href="http://download.kde.org/stable/3.0.5/FreeBSD/">4.7-STABLE</a>
   </li>
  </ul>
</li>

<!--   MAC OS X (FINK PROJECT) -->
<!--
<li>
  <a href="http://www.apple.com/macosx/">Mac OS</a> /
  <a href="http://www.opendarwin.org/">OpenDarwin</a>
  (downloads via the <a href="http://fink.sourceforge.net/">Fink</a> project):
  <ul type="disc">
    <li>
      <a href="http://fink.sourceforge.net/news/kde.php">X</a>
    </li>
  </ul>
</li>
-->

<!--   MANDRAKE LINUX -->
<!--
<li>
  <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>:
  <ul type="disc">
    <li>
      9.0:
      <a href="http://download.kde.org/stable/3.0.5/Mandrake/9.0/">Intel
i586</a>,
     <a href="http://download.kde.org/stable/3.0.5/Mandrake/9.0/noarch/">noarch</a>
   directory for i18n packages.
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/stable/3.0.5/Mandrake/8.2/">Intel
     i586</a>,
     <a href="http://download.kde.org/stable/3.0.5/Mandrake/8.2/noarch/">noarch</a>
   directory for i18n packages.
    </li>
  </ul>
</li>
-->

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>:
  <ul type="disc">
    <li>
      8.0: <a href="http://download.kde.org/stable/3.0.5/RedHat/8.0/i386/">Intel i386</a>,
           <a href="http://download.kde.org/stable/3.0.5/RedHat/8.0/noarch/">Language packages</a>
    </li>
  </ul>
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution):
   <ul type="disc">
     <li>
       8.1: <a href="http://download.kde.org/stable/3.0.5/contrib/Slackware/8.1/">Intel i386</a>
     </li>
   </ul>
</li>

<!-- SOLARIS -->
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      9.0: <a href="http://download.kde.org/stable/3.0.5/contrib/Solaris/">SPARC64</a>
    </li>
    <li>
      8.0: <a href="http://download.kde.org/stable/3.0.5/contrib/Solaris/">Intel x86 / SPARC64</a>
    </li>
    <li>
      7.0: <a href="http://download.kde.org/stable/3.0.5/contrib/Solaris/">SPARC64</a>
    </li>
  </ul>
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
   (<a href="http://download.kde.org/stable/3.0.5/SuSE/README">README</a>):
  <ul type="disc">
    <li>
      8.1:
      <a href="http://download.kde.org/stable/3.0.5/SuSE/ix86/8.1/">Intel
      i386</a>
    </li>
   <li>
     8.0:
     <a href="http://download.kde.org/stable/3.0.5/SuSE/ix86/8.0/">Intel
     i386</a>
    </li>
    <li>
      7.3:
      <a href="http://download.kde.org/stable/3.0.5/SuSE/ix86/7.3/">Intel
      i386</a> and
      <a href="http://download.kde.org/stable/3.0.5/SuSE/ppc/7.3/">IBM
      PowerPC</a>
    </li>
  </ul>
</li>

<!-- TURBO LINUX -->
<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
  (<a href="http://download.kde.org/stable/3.0.5/Turbo/Readme.txt">README</a>):
    <ul type="disc">
      <li>
        <a href="http://download.kde.org/stable/3.0.5/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       8.0: 
       <a href="http://download.kde.org/stable/3.0.5/Turbo/8/i586/">Intel i586</a>
      </li>
      <li>
       7.0:
       <a href="http://download.kde.org/stable/3.0.5/Turbo/7/i586/">Intel i586</a>
      </li>
    </ul>
</li>
   
<!--   TRU64 -->
  <!--
<li>
  <a href="http://www.tru64unix.compaq.com/">Tru64</a>
  (<a href="http://download.kde.org/stable/3.0.5/contrib/Tru64/README.Tru64">README</a>):
  <ul type="disc">
    <li>
      4.0d, e, f and g and 5.x:
      <a href="http://download.kde.org/stable/3.0.5/contrib/Tru64/">Compaq Alpha</a>
    </li>
  </ul>
</li>
  -->
  
<!--   YELLOWDOG LINUX -->
<!--
  <a href="http://www.yellowdoglinux.com/">YellowDog</a>:
  <a href="http://download.kde.org/stable/3.0.5/YellowDog">2.2</a>
-->
</ul>
