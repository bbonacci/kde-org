---
qtversion: 5.15.2
date: 2023-06-10
layout: framework
libCount: 83
---


### Baloo

* Use common helper for Property/JSON conversion
* Don't install D-Bus interfaces without BUILD_INDEXER_SERVICE
* [IdTreeDB] Consolidate put/del into common set
* Cleanup some leftover stale code
* [balooshow] Improve display of property and plaintext terms

### KConfigWidgets

* KColorSchemeMenu: Remove accelerator markers from scheme name
* Give KColorSchemeMenu namespace a short description
* Fixup: Pass scheme name - not path - to KColorSchemeManager::indexForScheme
* Split menu creating functionality out of KColorSchemeManager

### KCoreAddons

* use fcntl to fix macOS compile

### KDELibs 4 Support

* kssl: Update for LibreSSL 3.7

### KDocTools

* Add Arabic Support

### KFileMetaData

* Cleanup property name/id mapping test
* Add method to export list of all Property names
* Reduce PropertyInfo construction overhead
* Add benchmark for PropertyInfo instantiation

### KHolidays #

* Significantly speed up HolidayRegion::defaultRegionCode()

### KIconThemes

* KIconTheme: allow to also fallback to Breeze-dark when set through QPA

### KImageFormats

* pcx: multiple fixes (2)
* Avoid unnecessary conversions
* RGB/SGI writer: fix alpha detection and image limit size
* TGA writer: fix alpha detection and performance improvements
* pcx: multiple fixes
* PCX: Fix reading of the extended palette (bug 463951)

### KIO

* Deprecate KIO::AccessManager and related classes
* Enable thumbnail caching if thumbnail directory is on an encrypted volume (bug 443806)
* KdirLister: update symlink dir content on file removal (bug 469254)
* Polish menu before creating platform window

### Kirigami

* ActionTextField: Disable shortcut for invisible and disabled text fields
* BasicListItemTest: Guard against nullable background in ScrollView
* Fix tst_basiclistitem_tooltip
* Make it possible to disable BasicListItem tooltip
* Fix almost all links in the KF5 Kirigami docs
* Fix painting of non-symbolic icons which are fallbacks for symbolic (bug 451538)

### KItemModels

* Preserve numeric sort roles as well

### KNewStuff

* Remove KF5TextWidgets remnants

### KParts

* PartLoader::createPartInstanceForMimeType(): Avoid compiler detected null pointer access

### KTextEditor

* Fix incorrect lineHeight for drag pixmap (bug 468196)

### Prison

* Add EAN13 support
* Factor out code for interfacing with ZXing for barcode generation

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
