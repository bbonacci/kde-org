---
aliases:
- ../../kde-frameworks-5.20.0
date: 2016-03-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### Breeze Icons

- Many new icons
- Add virtualbox mimetype icons and some other missing mimetypes
- Add synaptic and octopi icon support
- Fix cut icon (bug 354061)
- Fix name of audio-headphones.svg (+=d)
- Rating icons with smaller margin (1px)

### Framework Integration

- Remove possible file-name in KDEPlatformFileDialog::setDirectory()
- Don't filter by name if we have mime types

### KActivities

- Remove dependency on Qt5::Widgets
- Remove dependency on KDBusAddons
- Remove dependency on KI18n
- Remove unused includes
- Shell scripts output improved
- Added the data model (ActivitiesModel) showing the activities to the library
- Build only the library by default
- Remove the service and workspace components from the build
- Move the library into src/lib from src/lib/core
- Fix CMake warning
- Fix crash in activities context menu (bug 351485)

### KAuth

- Fix kded5 dead lock when a program using kauth exits

### KConfig

- KConfigIniBackend: Fix expensive detach in lookup

### KCoreAddons

- Fix Kdelibs4 config migration for Windows
- Add API to get Frameworks runtime version info
- KRandom: Don't use up 16K of /dev/urandom to seed rand() (bug 359485)

### KDeclarative

- Don't call null object pointer (bug 347962)

### KDED

- Make it possible to compile with -DQT_NO_CAST_FROM_ASCII

### KDELibs 4 Support

- Fix session management for KApplication based applications (bug 354724)

### KDocTools

- Use unicode characters for callouts

### KFileMetaData

- KFileMetadata can now query and store information about the original email a saved file may have been an attachment of

### KHTML

- Fix cursor updating in view
- Limit string memory use
- KHTML java applet viewer: repair broken DBus call to kpasswdserver

### KI18n

- Use portable import macro for nl_msg_cat_cntr
- Skip looking up . and .. to find the translations for an application
- Restrict _nl_msg_cat_cntr use to GNU gettext implementations
- Add KLocalizedString::languages()
- Place Gettext calls only if catalog has been located

### KIconThemes

- Make sure variable is being initialized

### KInit

- kdeinit: Prefer loading libraries from RUNPATH
- Implement "Qt5 TODO: use QUrl::fromStringList"

### KIO

- Fix KIO app-slave connection breaking if appName contains a '/' (bug 357499)
- Try multiple authentication methods in case of failures
- help: fix mimeType() on get()
- KOpenWithDialog: show mimetype name and comment in "Remember" checkbox text (bug 110146)
- A series of changes to avoid a directory relist after a file rename in more cases (bug 359596)
- http: rename m_iError to m_kioError
- kio_http: read and discard body after a 404 with errorPage=false
- kio_http: fix mimetype determination when URL ends with '/'
- FavIconRequestJob: add accessor hostUrl() so that konqueror can find out what the job was for, in the slot
- FavIconRequestJob: fix job hanging when aborting due to favicon too big
- FavIconRequestJob: fix errorString(), it only had the URL
- KIO::RenameDialog: restore preview support, add date and size labels (bug 356278)
- KIO::RenameDialog: refactor duplicated code
- Fix wrong path-to-QUrl conversions
- Use kf5.kio in the category name to match other categories

### KItemModels

- KLinkItemSelectionModel: Add new default constructor
- KLinkItemSelectionModel: Make the linked selection model settable
- KLinkItemSelectionModel: Handle changes to the selectionModel model
- KLinkItemSelectionModel: Don't store model locally
- KSelectionProxyModel: Fix iteration bug
- Reset KSelectionProxyModel state when needed
- Add a property indicating whether the models form a connected chain
- KModelIndexProxyMapper: Simplify logic of connected check

### KJS

- Limit string memory use

### KNewStuff

- Show a warning if there's an error in the Engine

### Package Framework

- Let KDocTools stay optional on KPackage

### KPeople

- Fix deprecated API usage
- Add actionType to the declarative plugin
- Reverse the filtering logic in PersonsSortFilterProxyModel
- Make the QML example slightly more usable
- Add actionType to the PersonActionsModel

### KService

- Simplify code, reduce pointer dereferences, container-related improvements
- Add kmimeassociations_dumper test program, inspired by bug 359850
- Fix chromium/wine apps not loading on some distributions (bug 213972)

### KTextEditor

- Fix highlighting of all occurences in ReadOnlyPart
- Don't iterate over a QString as if it was a QStringList
- Properly initialize static QMaps
- Prefer toDisplayString(QUrl::PreferLocalFile)
- Support surrogate character sending from input method
- Do not crash on shutdown when text animation is still running

### KWallet Framework

- Make sure KDocTools is looked-up
- Don't pass a negative number to dbus, it asserts in libdbus
- Clean cmake files
- KWallet::openWallet(Synchronous): don't time out after 25 seconds

### KWindowSystem

- support _NET_WM_BYPASS_COMPOSITOR (bug 349910)

### KXMLGUI

- Use non-native Language name as fallback
- Fix session management broken since KF5 / Qt5 (bug 354724)
- Shortcut schemes: support globally installed schemes
- Use Qt's qHash(QKeySequence) when building against Qt 5.6+
- Shortcut schemes: fix bug where two KXMLGUIClients with the same name overwrite each other's scheme file
- kxmlguiwindowtest: add shortcuts dialog, for testing the shortcut schemes editor
- Shortcut schemes: improve usability by changing texts in GUI
- Shortcut schemes: improve scheme list combo (automatic size, don't clear on unknown scheme)
- Shortcut schemes: don't prepend the guiclient name to the filename
- Shortcut schemes: create dir before trying to save a new shortcut scheme
- Shortcut schemes: restore layout margin, it looks very cramped otherwise
- Fix memory leak in KXmlGui startup hook

### Plasma Framework

- IconItem: Don't overwrite source when using QIcon::name()
- ContainmentInterface: Fix use of QRect right() and bottom()
- Remove effectively duplicate code path for handling QPixmaps
- Add API docs for IconItem
- Fix stylesheet (bug 359345)
- Don't wipe window mask on every geometry change when compositing is active and no mask has been set
- Applet: Don't crash on remove panel (bug 345723)
- Theme: Discard pixmap cache when changing theme (bug 359924)
- IconItemTest: Skip when grabToImage fails
- IconItem: Fix changing color of svg icons loaded from icon theme
- Fix svg iconPath resolving in IconItem
- If path is passed, pick the tail (bug 359902)
- Add properties configurationRequired and reason
- Move contextualActionsAboutToShow to Applet
- ScrollViewStyle: Do not use margins of the flickable item
- DataContainer: Fix slot checks before connect/disconnect
- ToolTip: Prevent multiple geometry changes while changing contents
- SvgItem: Don't use Plasma::Theme from rendering thread
- AppletQuickItem: Fix finding own attached layout (bug 358849)
- Smaller expander for the taskbar
- ToolTip: Stop show timer if hideTooltip is called (bug 358894)
- Disable animation of icons in plasma tooltips
- Drop animations from tooltips
- Default theme follows color scheme
- Fix IconItem not loading non-theme icons with name (bug 359388)
- Prefer other containments than desktop in containmentAt()
- WindowThumbnail: Discard glx pixmap in stopRedirecting() (bug 357895)
- Remove the legacy applets filter
- ToolButtonStyle: Don't rely on an outside ID
- Don't assume we find a corona (bug 359026)
- Calendar: Add proper back/forward buttons and a "Today" button (bugs 336124, 348362, 358536)

### Sonnet

- Don't disable language detection just because a language is set
- Disable automatic disabling of automatic spelling by default
- Fix TextBreaks
- Fix Hunspell dictionary search paths missing '/' (bug 359866)
- Add &lt;app dir&gt;/../share/hunspell to dictionary search path

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
