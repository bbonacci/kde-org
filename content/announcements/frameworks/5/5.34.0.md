---
aliases:
- ../../kde-frameworks-5.34.0
date: 2017-05-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### Baloo

- balooctl, baloosearch, balooshow: Fix order of QCoreApplication object creation (bug 378539)

### Breeze Icons

- Add icons for hotspot (https://github.com/KDAB/hotspot)
- Better version control system icons (bug 377380)
- Add plasmate icon (bug 376780)
- Update microphone-sensitivity icons (bug 377012)
- Raise default for 'Panel' icons to 48

### Extra CMake Modules

- Sanitizers: Don't use GCC-like flags for e.g. MSVC
- KDEPackageAppTemplates: documentation improvements
- KDECompilerSettings: Pass -Wvla &amp; -Wdate-time
- Support older qmlplugindump versions
- Introduce ecm_generate_qmltypes
- Allow projects to include the file twice
- Fix rx that matches project names out of the git uri
- Introduce fetch-translations build command
- Use -Wno-gnu-zero-variadic-macro-arguments more

### KActivities

- We are using only Tier 1 frameworks, so move us to Tier 2
- Removed KIO from the deps

### KAuth

- Security fix: verify that whoever is calling us is actually who he says he is

### KConfig

- Fix relativePath calculation in KDesktopFile::locateLocal() (bug 345100)

### KConfigWidgets

- Set the icon for the Donate action
- Relax constraints for processing QGroupBoxes

### KDeclarative

- Don't set ItemHasContents in DropArea
- Don't accept hover events in the DragArea

### KDocTools

- Workaround for MSVC and catalog loading
- Solve a visibility conflict for meinproc5 (bug 379142)
- Quote few other variables with path (avoid issues with spaces)
- Quote few variables with path (avoid issues with spaces)
- Temporarily disable the local doc on Windows
- FindDocBookXML4.cmake, FindDocBookXSL.cmake - search in homebrew installations

### KFileMetaData

- makes KArchive be optional and do not build extractors needing it
- fix duplicated symbols compilation error with mingw on Windows

### KGlobalAccel

- build: Remove KService dependency

### KI18n

- fix basename handling of po files (bug 379116)
- Fix ki18n bootstrapping

### KIconThemes

- Don't even try to create icons with empty sizes

### KIO

- KDirSortFilterProxyModel: bring back natural sorting (bug 343452)
- Fill UDS_CREATION_TIME with the value of st_birthtime on FreeBSD
- http slave: send error page after authorization failure (bug 373323)
- kioexec: delegate upload to a kded module (bug 370532)
- Fix KDirlister Gui Test setting URL scheme twice
- Delete kiod modules on exit
- Generate a moc_predefs.h file for KIOCore (bug 371721)
- kioexec: fix support for --suggestedfilename

### KNewStuff

- Allow multiple categories with the same name
- KNewStuff: Show file's size information in grid delegate
- If an entry's size is known, show it in the list view
- Register and declare KNSCore::EntryInternal::List as a metatype
- Don't fall through the switch. Double entries? No please
- always close the downloaded file after downloading

### KPackage Framework

- Fix include path in KF5PackageMacros.cmake
- Ignore warnings during appdata generation (bug 378529)

### KRunner

- Template: Change toplevel template category to "Plasma"

### KTextEditor

- KAuth integration in document saving - vol. 2
- Fix assertion when applying code folding that changes cursor position
- Use non-deprecated &lt;gui&gt; root element in ui.rc file
- Add scroll-bar-marks also to the built-in search&amp;replace
- KAuth integration in document saving

### KWayland

- Validate surface is valid when sending TextInput leave event

### KWidgetsAddons

- KNewPasswordWidget: don't hide visibility action in plaintext mode (bug 378276)
- KPasswordDialog: don't hide visibility action in plaintext mode (bug 378276)
- Fix KActionSelectorPrivate::insertionIndex()

### KXMLGUI

- kcm_useraccount is dead, long live user_manager
- Reproducible builds: drop version from XMLGUI_COMPILING_OS
- Fix: DOCTYPE name must match root element type
- Fix wrong usage of ANY in kpartgui.dtd
- Use non-deprecated &lt;gui&gt; root element
- API dox fixes: replace 0 with nullptr or remove where not applied

### NetworkManagerQt

- Fix crash when retrieving active connection list (bug 373993)
- Set default value for auto-negotiation based on running NM version

### Oxygen Icons

- Add icon for hotspot (https://github.com/KDAB/hotspot)
- Raise default for 'Panel' icons to 48

### Plasma Framework

- reload icon when usesPlasmaTheme changes
- Install Plasma Components 3 so they can be used
- Introduce units.iconSizeHints.* to provide user-configurable icon size hints (bug 378443)
- [TextFieldStyle] Fix textField is not defined error
- Update the ungrabMouse hack for Qt 5.8
- Guard against Applet not loading AppletInterface (bug 377050)
- Calendar: Use correct language for month and day names
- Generate plugins.qmltypes files for the plugins we install
- if the user did set an implicit size, keep it

### Solid

- Add include that is needed in msys2

### Syntax Highlighting

- Add Arduino extension
- LaTeX: Fix Incorrect termination of iffalse comments (bug 378487)

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
