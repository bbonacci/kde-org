---
qtversion: 5.15.2
date: 2022-10-09
layout: framework
libCount: 83
---


### Breeze Icons

* Add text/javascript symlink

### Extra CMake Modules

* Correctly separate multiple interface compile definitions
* Also resurrect std::auto_ptr on MSVC when using Exiv2
* API dox: document KAPPTEMPLATESDIR & KFILETEMPLATESDIR

### KCalendarCore

* Use dtStart as the default for dtRecurrence

### KCMUtils

* Fix missing kcmutils_proxy_model export in static builds (bug 459123)
* Launch KCMs that are not part of systemsettings in kcmshell5 (bug 458975)

### KConfig

* Only warn about a file being inaccessible if we know which file it is
* Fix size and position restoration on multimonitor setups (bug 458963)
* Warn when accessing an inaccessible config file
* Fix minValue/maxValue for KConfigCompilerSignallingItem

### KConfigWidgets

* add KHamburgerMenu::insertIntoMenuBefore() method

### KContacts

* Make QtQuick trully optional

### KCoreAddons

* Cache portal urls
* Add new bugReportUrl standard metadata property (bug 370195)
* Add support for static builds
* Fix moc configuration of K_PLUGIN_CLASS macro
* Use a non-deprecated notify signal for the KJob::percent property

### KDBusAddons

* Don't assume we have X11 on Windows with Qt 6
* Remove obsolete KDBusConnectionPool from the Qt 6 build
* Fix static compilation on non-X11 systems

### KDeclarative

* KeySequenceItem: Restore the previous value when it fails
* Remove project() calls from subdirs
* KeySequenceItem: Allow detecting when the key sequence is cleared (bug 458799)
* KeySequenceItem: Use the helper's shortcut instead of duplicating them
* KeySequenceItem: Disable clear button when there's no key sequence (bug 458798)
* KeySequenceItem: Cancel sequence recording when pressing the button
* KeySequenceItem: Remove workaround, it was addressed upstream
* KeySequenceItem: Show a cancel button when recording (bug 458796)
* GridDelegate: show tooltip when `toolTip` is empty and title/caption is elided (bug 459052)
* AbstractKCM: make footer top padding optional
* [kquickaddons/configmodule] Make getters const

### KDESU

* Drop setgid in favor of disabling process tracability explicitly

### KDE GUI Addons

* KeySequenceRecorder: Do not emit gotKeySequence when cancelling
* recorder: Allow setting the initial value of currentKeySequence
* recorder: Fix workaround in KDeclarative
* recorder: Do not keep two sequences at the same time (bug 458795)
* recorder: Never request inhibition twice for the same surface or seat (bug 458795)

### KHolidays #

* Add Japanese substitute holiday in 2023

### KI18n

* Add useful info to warning
* Really support :usagetip cue (bug 459283)
* Warn if the domain is empty
* Mark codeLanguage as const
* KCatalog: make setting LANGUAGE env var more robust (bug 444614)

### KIconThemes

* KIconEngine: Use QFileInfo::completeBaseName
* KIconEngine: Return actual icon name of loaded icon (bug 432293)
* Add dedicated kiconloader_p.h header for KIconLoaderPrivate

### KIdleTime

* Add native wayland plugin
* xsyncbasedpoller ctor initialize m_sync_event

### KImageFormats

* pcx: Do not support sequential devices (bug 459541)
* Fix maximum number of channels (testcase added)
* LibRaw_QIODevice::seek() avoid seek on a sequential device
* LibRaw_QIODevice::seek() bounding checks
* Camera RAW images plugin (bug 454208: on my Debian with KF5.96 and the pulgin installed, I see the preview of all my RAW files (ARW included) in Dolphin)
* Enables opening of XCF files with Width and/or Height greater than 32K
* Replace C cast with reinterpret_cast
* avif: adjust for libavif breaking change in YUV<->RGB conversion
* Fix image allocation with Qt 6

### KIO

* DesktopExecParser: Fix parsing of TerminalApplication when it contains args (bug 459408)
* KPropertiesDialog: Split single command entry box into separate exec and args (bug 455252)
* Allow hiding permissions tab
* Port http to workerbase
* Worker template: fix install location
* kcms/webshortcuts: hide from System Settings' main navigation

### Kirigami

* Improve DefaultListItemBackground code
* MnemonicAttached: Make sure to return StyledText-compatible strings for richTextLabel
* CategorizedSettings: ensure space for long words and don't wrap in weird places (bug 458393)
* private/PrivateCardsGridView.qml: Anchor DelegateRycler to an existing item
* ActionsMenu: Properly cleanup the menu
* Restore going back/forward in ColumnView using back/forward mouse buttons (bug 436790)
* Improve settingscomponents/CategorizedSettings.qml code formatting
* do not evaluate strings directly, use string's length instead
* show the title background when the title is aligned to vertical center
* Use new CI templates for static builds
* Add RestoreMode to Binding objects
* Examples: use normal target installation arguments var
* Make link buttons look more like links (bug 459227)
* Remove unused indicateActiveFocus property
* Port pushpopclear example away from Kirigami.Label
* Don't violate min <= max assert in qBound in Qt 6
* FormLayout: Fix confusing argument name in method implementation
* imagecolors: make extracted colors follow WCAG contrast criterion (bug 457861)
* colorutils: add `luminance`
* colorutils: avoid negative L in LabColor
* colorutils: add `colorToXYZ`
* BannerImage: Delay recalculating title size until after anchors have settled
* Always reset m_futureImageData to null after deleteLater (bug 456047)
* OverlaySheet: Fix some mis-indentation and missing return in JavaScript
* fix scrolling calculation
* Use Qt Shader Tools for shadow shaders with Qt 6
* InlineMessage: Make the MouseArea work better with external MouseAreas
* InlineMessage: add hoveredLink property

### KItemModels

* Remove obsolete classes from the Qt 6 build

### KNewStuff

* [kmoretools] Allow analyzing remote folders with Filelight
* Fix search fields resetting text when changing window size (bug 455345)
* Fix circular visibility condition for upload action
* Fix broken layout on dialog footer
* Drop unnecessary Qt6Core5Compat dependency
* Remove remnants of QTextCodec usage

### KNotification

* Add CMake option to build WITHOUT_X11

### KPackage Framework

* Build test KPackage structure as static plugin
* Fix duplicate symbol in static builds (bug 459099)
* Ignore sddmtheme dependencies if we do not have the knsrc file installed (bug 415583)
* Remove unneeded known categories

### KQuickCharts

* Unpack points in the fragment shader of a line chart (bug 449005)
* Adapt shaders to use Qt shader tools for Qt 6
* Adapt to QSGMaterialShader API changes in Qt 6
* Adapt to variant comparison changes in Qt 6

### KRunner

* QueryMatch: Make long docstrings for enum values more readable
* Deprecate QueryMatch::MatchType::InformationalMatch
* Runner templates: fix install location

### KTextEditor

* Macro replaced by factory function
* Small refactor to KateViewInternal::word{Prev,Next}
* Tests for basic cursor movement between words
* Remove shell-like completion handling on TAB
* Allow code completion using the tab key
* clipboardialog: add placerholder label when empty
* Unassign transpose character shortcut
* avoid magic to compute line (bug 450817)
* Enable indent-on-paste for indenttest
* Fix cstyle/indentpaste4 script
* Fix cstyle tests: elsething and fork
* Disable R indenter debug mode
* Don't change indent-on-paste default yet
* Add tests and fixes for R indent-on-paste
* Add tests and fixes for julia indent-on-paste
* Add tests for ruby indent-on-paste
* Add tests and fixes for python indent and indent-on-paste
* Fix python indent script trigger character detection with empty chars
* Disable cstyle.js debug mode
* Add tests and fixes for cstyle indent and indent-on-paste
* Enable 'indent text on paste' by default
* Normalize signatures in mainwindow.cpp
* Plugin template: fix install location

### KWayland

* PlasmaWindowManagement: Avoid unbounded recursion and delay in readData (bug 453124)
* client/plasmashell: fix applet popups displacing other windows (bug 459523)
* client/plasmashell: add fallback for applet popups (bug 459188)
* require PlasmaWaylandProtocols 1.9.0 and bump supported plasmashell version to 8
* ConnectionThread: connect by symbol rather than by name
* ConnectionThread: Move event dispatcing into a separate method
* ConnectionThread: Make sure we consume properly the threads
* Add support for wl_output version 4

### KWindowSystem

* Port QtWinExtra uses for Qt 6
* Remove QWindow::isExposed() check in activateWindow() (bug 458983)

### KXMLGUI

* use same config for position restoration as for position saving

### NetworkManagerQt

* Export NetworkManager namespace
* Guard access to wirelessNetworkInterface in wirelessNetwork (bug 459500)
* Rename interfacesAdded slot in manager to dbusInterfacesAdded

### Plasma Framework

* Don't violate the min <= max assert in qBound in Qt 6
* IconLabel: always enable `fillWidth`
* wallpaperinterface: allow wallpaper plugin to set custom accent color
* Add CMake option to build WITHOUT_X11
* PC3/IconLabel: Simplify Layout.fillWidth expression for label
* ExpandableListItem: make fewer assumptions about the action
* Clear PasswordField with Ctrl+Shift+U (bug 451550)
* Add fade in and out animations to PC3 ToolTip
* Reset devicePixelRatio after loading the cachedBackground of mask
* Always give FrameSvg's mask in logical pixels
* desktoptheme/breeze: update disk.svg (bug 445810)
* Remove shortcut to make corona immutable
* ExpandableListItem: add arrow key navigation support
* PC3/ProgressBar: Fix sizing bugs, reformat code, remove animation hacks (bug 456550)
* Port IconItem to use Qt Shader Tools for Qt 6
* Remove commented out dead slot
* Keep Tooltips the proper type (bug 446375)
* don't remove the shadow on ~Dialog (bug 437920)

### Prison

* Increase quiet zone for qrcode (bug 458222)

### Purpose

* Fix QML warning, properly refer to the view
* Remove unneeded EXPORT_SHARE_VERSION usages

### QQC2StyleBridge

* ToolTip: Don't strip ampersands from HTML entities
* Grammar & typo in README.md
* Also check for controlRoot.size > 0 (bug 458486)
* Revert "also check for controlRoot.size > 0"
* also check for controlRoot.size > 0 (bug 458486)
* ToolTip: use Text.Wrap, not Text.WordWrap (bug 459331)
* ToolButton: Do not layout for the icon when it's not set (bug 459470)
* ProgressBar: Add TODO KF6, since we can't remove the workaround now
* Add fade in/out animations to ToolTips
* SpinBox: Flip left/right padding for styles with indicators on the sides
* SpinBox: Avoid explicit horizontalAlignment in TextField for better RTL
* SpinBox: Remove assignment which is never read
* SpinBox: Reset activeSubControls when neither of indicators are active (bug 459004)
* SpinBox: Let hover events propagate through TextField to the control (bug 459004)
* SpinBox: Reformat QML for better code style and clarity
* SpinBox: AlignLeft by default
* SpinBox: Use implicit size based on the max value
* SpinBox: move onTextEdited to contentItem, check acceptableInput
* SpinBox: update imports

### Solid

* FstabStorageAccess: Trim output from (u)mount
* udisks2: Handle NotAuthorizedCanObtain and NotAuthorizedDismissed

### Syntax Highlighting

* Bash,Zsh: add Dollar Prefix style for simple variable
* Zsh: disable spellcheck
* Zsh: fix Parameter Expansion style for )) in $((...)) and add missing parameter expansion flags
* Bash: fix Parameter Expansion style for )) in $((...))
* prefer Oklab to CIELAB for perceptual color difference in ansi256 mode
* PostgreSQL: Remove # as line comment
* Improve ASN.1 highlighting
* Ruby: add keywords used for refinements to the mixin methods list
* Python: add := operator, built-in functions and special methods
* Crystal Syntax Highlighting Definition: // operator, macros, keywords
* debianchangelog.xml: Set default filename
* bash.xml: The other (more correct) form of multiline comments

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
