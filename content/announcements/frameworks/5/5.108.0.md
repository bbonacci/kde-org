---
qtversion: 5.15.2
date: 2023-07-08
layout: framework
libCount: 83
---


### Baloo

* [Document] Fix parameter name for setParentId
* [WriteTransaction] Avoid false warning when removing empty xattr terms
* [DocumentUrlDB] Remove recursive removal of parent directories
* [AdvancedQueryParser] Correctly parse empty quoted strings
* [UnindexedFileIndexer] Split into multiple transactions
* [FirstRunIndexer] Split initial index run into multiple transactions
* Fix signedness mismatch compiler warning
* [UnindexedFileIndexer] Skip document when BasicIndexingJob fails
* [Transaction] Allow to fully reset the transaction state
* [Transaction] Use unique_ptr for write transaction
* Verify parentId when adding documents
* [DatabaseDbis] Use default member initializers instead of constructor

### Breeze Icons

* Export cmake config so that a app can ensure that breeze-icons is installed

### KArchive

* Don't create subdirectory in toplevel

### KDocTools

* doc: use a more generic Frameworks entity in the example

### KFileMetaData

* Reapply ReplayGain changes

### KDE GUI Addons

* Add an option to disable building the geo: URI scheme handler

### KHolidays #

* Add juneteenth as variable holiday

### KImageFormats

* jxl: add support for libjxl v0.9, drop support for old 0.6.1

### KIO

* KUrlRequester: restore unnamed filter compatibility with Plasma file dialog
* Add KUrlRequester::nameFilters, deprecate KUrlRequester::filter (bug 369542)
* KUrlRequester::setFilter: note special filter syntax, allow unnamed filters (bug 369542)
* KCoreDirLister::nameFilter: revert to non-partial matching again
* filewidgets/kfilewidget: Select and focus filename after canceling overwrite (bug 444515)
* [previewjob] Check whether thumbRootDevice is valid before access (bug 470845)

### Kirigami

* PassiveNotificationsManager: Fix callBack lifetime (bug 470786)
* Use MobileForm.AboutPage
* BasicListItem: Use pressed state for tooltip
* DefautListItemBackground: prioritize user input events when setting background color

### KPackage Framework

* Define json-validate-ignore property in .kde-ci.yml

### KWidgetsAddons

* Remove duplicate row in category selection

### KXMLGUI

* Add a view_redisplay_merge MergeLocal to ui_standards.rc (bug 470848)

### Plasma Framework

* Polish DropMenu before creating platform window

### Syntax Highlighting

* ensure all .json files are valid
* Highlight QML pragma keyword

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
