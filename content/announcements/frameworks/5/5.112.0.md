---
qtversion: 5.15.2
date: 2023-11-12
layout: framework
libCount: 83
---


### Baloo

* [PendingFile] Remove default constructor, METATYPE declaration
* [PendingFile] Remove unused and incorrect setPath method

### Extra CMake Modules

* Rename prefix.sh.cmake to prefix.sh.in

### KActivitiesStats

* ResultSet: expose agent field

### KCalendarCore

* ICalFormat: don't shift all-day invite dates to UTC (bug 421400)

### KConfig

* kconfigwatcher: do not assert absolute paths
* dbussanitizer: do not allow trailing slashes
* dbussanitizer: qassertx to print the path
* notify: don't try to send or receive dbus notifications on absolute paths
* more aggressively sanitize dbus paths

### KCoreAddons

* Fix API docs generation for KPluginMetaDataOption enum values
* Deprecate unused KStringHandler::isUtf8 & KStringHandler::from8Bit

### KGlobalAccel

* Add build option for KF6 coinstallability

### KIO

* KDirModel: Refactor _k_slotClear()
* KDirModel: Replace 'slow' with 'fsType' naming
* KDirModel: Reduce calls to isSlow()
* KDirModel: Limit details fetching for network fs

### Kirigami

* Avatar: Add tests for cyrillic initials
* Add support for cyrillic initials

### KNotification

* Adapt to notification API and permission changes in Android SDK 33 (bug 474643)

### KTextEditor

* Fix selection shrink when indenting (bug 329247)

### NetworkManagerQt

* Fix incorrect signal signature
* Remove incorrect comment
* Listen for both DBus service registration events and interface added events (bug 471870)

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
