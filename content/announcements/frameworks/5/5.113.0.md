---
qtversion: 5.15.2
date: 2023-12-14
layout: framework
libCount: 83
---


### Baloo

* [ExtractorProcess] Handle signal mangling by DrKonqi (bug 421317)
* [ExtractorProcess] Test the various processing states
* [ExtractorProcess] Make the extractor process path a constructor argument
* [BasicIndexingBenchmark] Allow to keep temporary DB
* [QueryTest] Add test case for terms folded to empty strings (bug 407664)
* [DocumentUrlDb] Reduce allocations during path reconstruction
* Shortcut non-matching phrase queries
* [EngineQuery] Remove AND/OR operations
* [QueryParser] Remove it, as it is no longer used
* [SearchStore] Always use TermGenerator instead of QueryParser (bug 412421)
* [Extractor] Do not emit startedIndexingFile for skipped documents (bug 462009)
* Remove dead registerBalooWatcher DBus method
* [FileWatchTest] Check attr changed signal when XAttr is not available
* [FileWatchTest] Replace common boilerplate with RAII
* [KInotify] Fix _k_addWatches helper when hitting descriptor limit
* [KInotify] Simplify dirIter logic, use unique_ptr
* [KInotify] Always add all watches in the event loop
* [KInotify] Silence EventMoveSelf warning when not relevant
* [KInotify] Removed unused and obsolete `available()` method

### Extra CMake Modules

* ECMQtDeclareLoggingCategory: support kdebugsettings files w/ . in basename
* Add Find7Zip, deprecate Find7z, revert Find7z broken Linux support
* Find7z: make it work also on non-Windows systems

### Framework Integration

* Reflect identifier change of oxygen-icons

### KActivitiesStats

* Adapt to renamed activities library repo
* Adapt to move out of Frameworks

### KActivities

* Adapt to move out of Frameworks

### KArchive

* Fix broken bzip2 with new shared-mime-info

### KConfig

* Make KConfigWatcher noop for in-memory configs

### KConfigWidgets

* KRecentFileActions: Fix use after free (bug 476312)

### KCoreAddons

* kdirwatch: don't crash after moving threads (bug 472862)
* kdirwatch: don't leave relative entries dangling
* kdirwatch: expand ref counting system to keep account of public instances
* kdirwatch: always unref d, and unset d from inside d (bug 472862)

### KFileMetaData

* Output generic file types from dump utility
* Check if file path passed to dump utility is a readable file
* [FFmpegExtractor] Bail out on first missing component during find_package (bug 458313)
* Bump minimum Exiv2 version to 0.26
* [TaglibExtractor] Add support for Ogg stream with FLAC (audio/x-flac+ogg) (bug 475352)
* Generate fromName hash table programmatically from PropertyInfo table
* Use proxy class for lowercase property name lookup
* Reduce PropertyInfo::fromName overhead
* [FFMpegExtractor] Support and check video/vnd.avi
* [TaglibExtractor] Adapt code to SMI changes for audio/x-wav -> /vnd.wave

### KIO

* Add KF5 include for KPropertiesDialogPlugin
* Add build option for KF6 coinstallability
* Adaptations for shared-mime-info >= 2.3

### KRunner

* Adapt to plasma-framework being renamed to libplasma
* Deprecate old QueryMatch::setType which will be removed in KF6
* Backport KF6 API to set categoryRelevance for QueryMatch
* Adapt to plasma-framework moving to Plasma

### KService

* Remove warning about empty Exec field from KService::exec() (bug 430157)

### Plasma Framework

* Adapt to renamed activities library repo
* Adjust to KWayland moving to Plasma
* Adapt to kactivities moving to Plasma
* Reflect identifier change of oxygen-icons

### Sonnet

* Use the cmake variables rather than if(TARGET)

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
