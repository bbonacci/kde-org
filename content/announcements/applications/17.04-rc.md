---
aliases:
- ../announce-applications-17.04-rc
date: 2017-04-07
description: KDE Ships Applications 17.04 Release Candidate.
layout: application
release: applications-17.03.90
title: KDE Ships Release Candidate of KDE Applications 17.04
---

April 7, 2017. Today KDE released the release candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/17.04_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

The KDE Applications 17.04 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.