---
aliases:
- ../../fulllog_applications-18.12.1
hidden: true
title: KDE Applications 18.12.1 Full Log Page
type: fulllog
version: 18.12.1
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Ensure StandardDirs::saveDir returns existing dir. <a href='http://commits.kde.org/akonadi/28f2916db1a2ad98b00250eb4d5dd92ca1548c50'>Commit.</a> </li>
<li>Fix SQL syntax error in findOrphanedItems(). <a href='http://commits.kde.org/akonadi/2db662a26c0515ba5b0b11602bdaf59c5cad7726'>Commit.</a> </li>
<li>Handle query prepare errors in QueryBuilder::exec. <a href='http://commits.kde.org/akonadi/3e47fbc309fafc6cfee0caa88452b1ef49c8a76f'>Commit.</a> </li>
<li>Not necessary to check twice. <a href='http://commits.kde.org/akonadi/71e90c85a0ced18d92a0d390f1850567edf8ce1d'>Commit.</a> </li>
<li>Remove not necessary lines. <a href='http://commits.kde.org/akonadi/07e5352de98c1c5ab8c4a70e95ea85bc648e18f5'>Commit.</a> </li>
<li>Allow to sort list of folder. <a href='http://commits.kde.org/akonadi/c80c65fe844351d655720a04306593d762c741ee'>Commit.</a> </li>
<li>Fix mem leak. <a href='http://commits.kde.org/akonadi/59f8b5305971705fddaeab647d61ed349bbbc85c'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Make sure to copy imported file from tmp to localdata directory. <a href='http://commits.kde.org/akregator/b25edb139c93fb32daa4d59ee457a07722cad5a4'>Commit.</a> </li>
<li>Fix Bug 402132 - Make it more obvious that the search didn't find anything. <a href='http://commits.kde.org/akregator/36c8375d8bc44580cadd9dc7478a637ec0677ebf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402132'>#402132</a></li>
<li>Fix Bug 402639 - akregator doesn't know how to deal with "/tmp/mozilla_user0/0ZFsmr25.atom". <a href='http://commits.kde.org/akregator/e7b78b23bc4330a96a4c2605b4700de4897bb1fc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402639'>#402639</a></li>
<li>Don't crash with Qt 5.11+. <a href='http://commits.kde.org/akregator/af43acadd45b15d005a5dbd9985ab019794dcd57'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371511'>#371511</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Use more https in links. <a href='http://commits.kde.org/ark/5946b88a8beabd64d6c52734187aff03731f6f12'>Commit.</a> </li>
</ul>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Hide]</a></h3>
<ul id='ulartikulate' style='display: block'>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/artikulate/1fb6f48f2bd62dea8724b6b3eba2b33fa0132f10'>Commit.</a> </li>
</ul>
<h3><a name='audiocd-kio' href='https://cgit.kde.org/audiocd-kio.git'>audiocd-kio</a> <a href='#audiocd-kio' onclick='toggle("ulaudiocd-kio", this)'>[Hide]</a></h3>
<ul id='ulaudiocd-kio' style='display: block'>
<li>Fix KCM installation. <a href='http://commits.kde.org/audiocd-kio/9fbc804f4184c7ad58f24548893f3ae7fd8f85ec'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Fix logic bug in insertEntryBefore: add missing else branch, corresponding to insertEntry logic. <a href='http://commits.kde.org/cantor/a7a0191c1f048bd77fb4d2e655e272f0274a9c29'>Commit.</a> </li>
<li>[Python2] Fix bug with server, which don't work with multiexpression code (for example "a=3\nb=4"). Also add regression tests for this problem. <a href='http://commits.kde.org/cantor/31e83563a88fb117cc55209c11331a0e63de0983'>Commit.</a> </li>
<li>[R] Fix bug in RServer, when rserver finishs to response after defining new user function. <a href='http://commits.kde.org/cantor/d41d29dc4321209ae685ca579320f8841c4f171c'>Commit.</a> </li>
<li>[Julia] Fix bug with autocompletion for "nested" command, like "Base.Mai". Add test for this and also minor improvments in tests. <a href='http://commits.kde.org/cantor/d0c41311c7a344c8f5c191ec7963c09b05656bde'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix display of image orientation. <a href='http://commits.kde.org/dolphin/8f051a391db543a05c00bc1323634a48d7e6444d'>Commit.</a> </li>
<li>Fix title update when changing active split view. <a href='http://commits.kde.org/dolphin/504db3f702625c63cb304430e60ab21b9411cb96'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402641'>#402641</a></li>
<li>Use https over http for homepage url. <a href='http://commits.kde.org/dolphin/1cde03cdfe66f455e115a373878524b227fadc4e'>Commit.</a> </li>
<li>Add failing test case for bug #402641. <a href='http://commits.kde.org/dolphin/f0e66d34655d5e1eecaed2cab7fd07432aecb714'>Commit.</a> See bug <a href='https://bugs.kde.org/402641'>#402641</a></li>
<li>Use correct icon for the "New Window" menu item. <a href='http://commits.kde.org/dolphin/f6549f08ea0797bcdf5d584ce41005a741db12cb'>Commit.</a> </li>
</ul>
<h3><a name='dolphin-plugins' href='https://cgit.kde.org/dolphin-plugins.git'>dolphin-plugins</a> <a href='#dolphin-plugins' onclick='toggle("uldolphin-plugins", this)'>[Hide]</a></h3>
<ul id='uldolphin-plugins' style='display: block'>
<li>[fileviewgitplugin] Ability to add conflicting files as well. <a href='http://commits.kde.org/dolphin-plugins/7c2a889f23489a18f9bc22e00ccf6cc49e692831'>Commit.</a> </li>
<li>[fileviewgitplugin] Apply Git log to files as well as directories. <a href='http://commits.kde.org/dolphin-plugins/41ba6289f82245ab696073c8717ba61f5f36c041'>Commit.</a> </li>
</ul>
<h3><a name='dragon' href='https://cgit.kde.org/dragon.git'>dragon</a> <a href='#dragon' onclick='toggle("uldragon", this)'>[Hide]</a></h3>
<ul id='uldragon' style='display: block'>
<li>[loadview] Do not allow changing volume on scroll. <a href='http://commits.kde.org/dragon/addaf47d1af218a46777f740bdc9d579a16bdf1f'>Commit.</a> </li>
</ul>
<h3><a name='ffmpegthumbs' href='https://cgit.kde.org/ffmpegthumbs.git'>ffmpegthumbs</a> <a href='#ffmpegthumbs' onclick='toggle("ulffmpegthumbs", this)'>[Hide]</a></h3>
<ul id='ulffmpegthumbs' style='display: block'>
<li>I18n: allow to translate the configuration. <a href='http://commits.kde.org/ffmpegthumbs/1ece967efbfe89ebc39a135079277955f3bc89cd'>Commit.</a> </li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Hide]</a></h3>
<ul id='ulfilelight' style='display: block'>
<li>Use more https in links. <a href='http://commits.kde.org/filelight/60a46b2c150a1dad24fef982992d281cbad21456'>Commit.</a> </li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Ci: enable freebsd platform. <a href='http://commits.kde.org/juk/0bc22b42465eb8883c61684493b07cb9f2a3ee68'>Commit.</a> </li>
<li>Ci: add the template for creating the flatpak. <a href='http://commits.kde.org/juk/3cf1c4111895729f746be9d50210fd6d01122c9c'>Commit.</a> </li>
<li>Ci: it seems recursive include is not supported. <a href='http://commits.kde.org/juk/579b019985fa6d91426077c363431e6716e6ad75'>Commit.</a> </li>
<li>Add the Gitlab CI configuration. <a href='http://commits.kde.org/juk/2555b23d0f3e422d0958ab46273e6c14aae4428c'>Commit.</a> </li>
<li>Restore proper sorting of the list of playlists. <a href='http://commits.kde.org/juk/468a5169347c6e463ae10676e32e9ad89cdbdf74'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402398'>#402398</a></li>
<li>Force sorting on so that special playlist sort to top. <a href='http://commits.kde.org/juk/dc3124e40b736cf7706b52f906326b48fc4ff4c1'>Commit.</a> See bug <a href='https://bugs.kde.org/402398'>#402398</a></li>
<li>Fix the "Show Columns" menu to show correct state on startup. <a href='http://commits.kde.org/juk/f9721878bd2977a66054fe3d2a13d48a0f071bb0'>Commit.</a> </li>
<li>Prevent opening new items from inadvertently editing track metadata on existing items. <a href='http://commits.kde.org/juk/c6afa4c528fa9c738bc369853abd9956033cbd27'>Commit.</a> </li>
</ul>
<h3><a name='kalgebra' href='https://cgit.kde.org/kalgebra.git'>kalgebra</a> <a href='#kalgebra' onclick='toggle("ulkalgebra", this)'>[Hide]</a></h3>
<ul id='ulkalgebra' style='display: block'>
<li>Don't set property twice. <a href='http://commits.kde.org/kalgebra/9e087c493d612b69d625e59610b6baf039b4b17e'>Commit.</a> </li>
<li>Make it easier to copy from the console. <a href='http://commits.kde.org/kalgebra/52a76c7d8db2708f44eb03769910abeda1b440b1'>Commit.</a> </li>
<li>Don't show about:blank when copying the result. <a href='http://commits.kde.org/kalgebra/d24d056cb85f494352c0bf7c6546ab8d34352885'>Commit.</a> </li>
</ul>
<h3><a name='kamoso' href='https://cgit.kde.org/kamoso.git'>kamoso</a> <a href='#kamoso' onclick='toggle("ulkamoso", this)'>[Hide]</a></h3>
<ul id='ulkamoso' style='display: block'>
<li>Fix compilation with -DQT_NO_URL_CAST_FROM_STRING and enable it. <a href='http://commits.kde.org/kamoso/22105f451647cb62dfc6eeb1a5f4a5efc0ffd723'>Commit.</a> </li>
</ul>
<h3><a name='kanagram' href='https://cgit.kde.org/kanagram.git'>kanagram</a> <a href='#kanagram' onclick='toggle("ulkanagram", this)'>[Hide]</a></h3>
<ul id='ulkanagram' style='display: block'>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/kanagram/1c5b561c307254e25faca0476893d5b4087c9034'>Commit.</a> </li>
<li>Use QString() here. <a href='http://commits.kde.org/kanagram/13a3ea04da3e8a42665185680206b44942766182'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>[projectplugin] Correct code index widget show. <a href='http://commits.kde.org/kate/806044f6a9d1afa5b36edcc4b060dc51c847596f'>Commit.</a> </li>
<li>[kateproject tool] Fine tune cppcheck. <a href='http://commits.kde.org/kate/68e44b97d148b47f3a01be52dc63504b08b47d99'>Commit.</a> </li>
<li>[kateproject] Gracefully exit when analyzer is not finished. <a href='http://commits.kde.org/kate/4405f6ee67304ecb49274d606d210f5d7ea94b22'>Commit.</a> </li>
<li>ViewManager: Fix I18n_ARGUMENT_MISSING hint in message box when open a very large file. <a href='http://commits.kde.org/kate/5a9fa2358c0c364b8275a9bc806bdfaf9d362ca5'>Commit.</a> </li>
</ul>
<h3><a name='kcachegrind' href='https://cgit.kde.org/kcachegrind.git'>kcachegrind</a> <a href='#kcachegrind' onclick='toggle("ulkcachegrind", this)'>[Hide]</a></h3>
<ul id='ulkcachegrind' style='display: block'>
<li>Use more https in links. <a href='http://commits.kde.org/kcachegrind/50eda2acc9d3d081932c09e7731443976577948e'>Commit.</a> </li>
</ul>
<h3><a name='kcalc' href='https://cgit.kde.org/kcalc.git'>kcalc</a> <a href='#kcalc' onclick='toggle("ulkcalc", this)'>[Hide]</a></h3>
<ul id='ulkcalc' style='display: block'>
<li>Use more https in links. <a href='http://commits.kde.org/kcalc/fce7249fad90b137413bb7875096ecb2de576015'>Commit.</a> </li>
<li>Fix running the test after requiring ECM 5.46.0. <a href='http://commits.kde.org/kcalc/dc4d541aec8dda17e0505641244f5bbbbb73ea0a'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix empty warning dialog on missing font in project. <a href='http://commits.kde.org/kdenlive/b112192abd33d438840f5631f4fd148431e3d96c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401903'>#401903</a></li>
<li>Fix bin item description cannot be edited if it contains zone subclips. <a href='http://commits.kde.org/kdenlive/822d219c0691f13b657a22860a825a7b76c9b0a9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402817'>#402817</a></li>
<li>Fix screengrab with audio broken. <a href='http://commits.kde.org/kdenlive/0839ebf4c410df0a60d3ac1f3cab8562e0c32351'>Commit.</a> </li>
<li>Move Gain effect to "Audio correction" category. <a href='http://commits.kde.org/kdenlive/a899240c6b15a235e750cad4d68cfcbf833b000e'>Commit.</a> </li>
<li>Ci: enable freebsd build. <a href='http://commits.kde.org/kdenlive/fb0a66b0793a03624cc1eb2b7bb8d2016a2f8df4'>Commit.</a> </li>
<li>QtScript is not used anymore. <a href='http://commits.kde.org/kdenlive/c91c8abb75896c02fcff81376ee8f96a8617992a'>Commit.</a> </li>
<li>Ci: add recipe for gitlab CI. <a href='http://commits.kde.org/kdenlive/3b1c2b8e5b8c45628c484ff376488d6a8e99a1aa'>Commit.</a> </li>
<li>Fix incorrect color theme correction for AppImages. <a href='http://commits.kde.org/kdenlive/540c8f8f9c8ba56b2ef738fdac9f38a6938515fa'>Commit.</a> </li>
<li>Fix color theme lost on AppImage. <a href='http://commits.kde.org/kdenlive/e2b499ff8be36a6102e9f234f2c666eda155bfb3'>Commit.</a> </li>
<li>Update AppData app version. <a href='http://commits.kde.org/kdenlive/a49facdf69d2596abd77996a01e369732586e0a6'>Commit.</a> </li>
<li>Fix bin/melt.exe & libmlt* loading on Windows. <a href='http://commits.kde.org/kdenlive/9b7b423f2acf4f7d2227f2ae70c3752bfb23e04c'>Commit.</a> </li>
<li>Necessary OpenGL headers are provided by Qt. <a href='http://commits.kde.org/kdenlive/2d3be73e68dbf965d4594c2e5c6a4fcfe936b608'>Commit.</a> </li>
<li>Fix keyframes import. <a href='http://commits.kde.org/kdenlive/94d2efb924730a98cfa25879ad85aa37578f46c8'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix send to KDE Connect for older KDE Connect version. <a href='http://commits.kde.org/kdepim-addons/787dbfe2ba5e64089b68544e82195bad89145bcf'>Commit.</a> </li>
<li>Allow to show address on map for EventReservation too. <a href='http://commits.kde.org/kdepim-addons/44f47c5fe75fc16d525dfd995f8b0000d39df244'>Commit.</a> </li>
<li>Disable subscription. <a href='http://commits.kde.org/kdepim-addons/6bf5348ee5f637ebc8ff85dba742b04896bdf93e'>Commit.</a> </li>
<li>Debug-- Use ExtendedSelection. <a href='http://commits.kde.org/kdepim-addons/bca8215f6d793ca6b0169ce9b2c8c9cd8f2b0f41'>Commit.</a> </li>
<li>Fix enable/disable buttons. <a href='http://commits.kde.org/kdepim-addons/63ac168550ac0e8ffd01cbc7872c463aa57d0895'>Commit.</a> </li>
<li>Backport all fix for adblock. <a href='http://commits.kde.org/kdepim-addons/aba8e421a99f8d857119d0eafc53cd35fcf83079'>Commit.</a> </li>
<li>Don't build example in official release. <a href='http://commits.kde.org/kdepim-addons/5d9a9569fd4b9618807fabf210159b432fad3ae9'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Fix sort element. <a href='http://commits.kde.org/kdepim-runtime/48ba6bbafda54a51f4b81ec457d8431888092435'>Commit.</a> </li>
<li>Make sure to delete it when action was done. <a href='http://commits.kde.org/kdepim-runtime/21342dd7d863ef9a31672491db2915c08c772452'>Commit.</a> </li>
</ul>
<h3><a name='kgeography' href='https://cgit.kde.org/kgeography.git'>kgeography</a> <a href='#kgeography' onclick='toggle("ulkgeography", this)'>[Hide]</a></h3>
<ul id='ulkgeography' style='display: block'>
<li>Update Moscow federal city and oblast. <a href='http://commits.kde.org/kgeography/8119d8bb874f5ee01e9fa9044d04d7bda655adc5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/337964'>#337964</a></li>
</ul>
<h3><a name='kget' href='https://cgit.kde.org/kget.git'>kget</a> <a href='#kget' onclick='toggle("ulkget", this)'>[Hide]</a></h3>
<ul id='ulkget' style='display: block'>
<li>Set window icon in code instead of in the desktop file. <a href='http://commits.kde.org/kget/80adf24c1091115e2fa4839650a5708b2d4672c8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400367'>#400367</a></li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Hide]</a></h3>
<ul id='ulkgpg' style='display: block'>
<li>Update screenshots of the handbook. <a href='http://commits.kde.org/kgpg/7c7d2c6225732501ed59d9dfdf32b66dee842b40'>Commit.</a> </li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Hide]</a></h3>
<ul id='ulkig' style='display: block'>
<li>Fix print preview in Kig. <a href='http://commits.kde.org/kig/3f85b75497e229fa030b36708ea189255229aa7c'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Use more https with links. <a href='http://commits.kde.org/kio-extras/97cb44265ee677abba9d43663d57d7e2aa157db8'>Commit.</a> </li>
</ul>
<h3><a name='kiten' href='https://cgit.kde.org/kiten.git'>kiten</a> <a href='#kiten' onclick='toggle("ulkiten", this)'>[Hide]</a></h3>
<ul id='ulkiten' style='display: block'>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/kiten/81fa24e99a94c752120033c4c50f03d6825ef91c'>Commit.</a> </li>
</ul>
<h3><a name='klickety' href='https://cgit.kde.org/klickety.git'>klickety</a> <a href='#klickety' onclick='toggle("ulklickety", this)'>[Hide]</a></h3>
<ul id='ulklickety' style='display: block'>
<li>Fix potential mem leak found by asan. <a href='http://commits.kde.org/klickety/6cc47fa717a606b95775fa036a5fecd7c356163c'>Commit.</a> </li>
</ul>
<h3><a name='kmahjongg' href='https://cgit.kde.org/kmahjongg.git'>kmahjongg</a> <a href='#kmahjongg' onclick='toggle("ulkmahjongg", this)'>[Hide]</a></h3>
<ul id='ulkmahjongg' style='display: block'>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/kmahjongg/4ed92a730fb77ec499e57bd1c7d1d3d553dac4a2'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Unifiedmailboxagent: Stop using std::optional. <a href='http://commits.kde.org/kmail/5171464e88777ae68f7d95c17cea8e04b460c230'>Commit.</a> </li>
<li>Fix Bug 402378 - mailto staement , uncomplete url past to kmail composer. <a href='http://commits.kde.org/kmail/d3ac2f227a860ae895d726a42114d8cc8d8df4ac'>Commit.</a> </li>
<li>Make sure that it doesn't crash here. <a href='http://commits.kde.org/kmail/b23ff8e0a940c4da30fac4adef34526fd4b641bb'>Commit.</a> </li>
<li>Check++. <a href='http://commits.kde.org/kmail/5636ee7a5a789af7697b4577ae29e1e437c4e673'>Commit.</a> </li>
<li>Fix check. <a href='http://commits.kde.org/kmail/837fb1b41a3713d22c8f1ab0d757189241f26637'>Commit.</a> </li>
<li>Fix Bug 401419 - Kmail crashes on trying to open any email. <a href='http://commits.kde.org/kmail/4c7adc353a55cd858be341b04b0f6aeddd7ede70'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401419'>#401419</a></li>
<li>Fix add parentwidget. <a href='http://commits.kde.org/kmail/dc7b33b89de2e52f09cc7f3cb5015679e0779e8f'>Commit.</a> </li>
</ul>
<h3><a name='knotes' href='https://cgit.kde.org/knotes.git'>knotes</a> <a href='#knotes' onclick='toggle("ulknotes", this)'>[Hide]</a></h3>
<ul id='ulknotes' style='display: block'>
<li>Fix Bug 402453 - KNotes help menu is empty. <a href='http://commits.kde.org/knotes/f245faae1ff603227fad8c4679698fdfb4261d87'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402453'>#402453</a></li>
</ul>
<h3><a name='kompare' href='https://cgit.kde.org/kompare.git'>kompare</a> <a href='#kompare' onclick='toggle("ulkompare", this)'>[Hide]</a></h3>
<ul id='ulkompare' style='display: block'>
<li>Use more https in links. <a href='http://commits.kde.org/kompare/4bef3e80307f967ff433dae9de9f5002bb2ec3d0'>Commit.</a> </li>
</ul>
<h3><a name='konqueror' href='https://cgit.kde.org/konqueror.git'>konqueror</a> <a href='#konqueror' onclick='toggle("ulkonqueror", this)'>[Hide]</a></h3>
<ul id='ulkonqueror' style='display: block'>
<li>Fix all plugin ui.rc/metadata to be also installed for webenginepart. <a href='http://commits.kde.org/konqueror/0617d9c8df6f4f35e0d4c1ad3e41ba4f6e60c920'>Commit.</a> </li>
<li>Fix error page to have UTF-8 encoding set as needed. <a href='http://commits.kde.org/konqueror/7e0ccca427c359119eb95e4413b58a7ec117e153'>Commit.</a> </li>
<li>Fix ui.rc files to use non-deprecated <gui> and to match DOCTYPE. <a href='http://commits.kde.org/konqueror/da6ebf36fd501d4c853f72d14ce8df74fb7119bd'>Commit.</a> </li>
<li>Fix WebEngineTextExtension::completeText a bit. <a href='http://commits.kde.org/konqueror/b15a81f9a382a6893875cf95f22c87f62a1d9fb1'>Commit.</a> </li>
<li>Remove unused find_package for Qt5Xml. <a href='http://commits.kde.org/konqueror/614eb3ad97ab96fdf50e33c7a9324aa2672515f0'>Commit.</a> </li>
<li>Fix KF5_MIN_VERSION to match API used by code for some releases. <a href='http://commits.kde.org/konqueror/519fa0c86a9e5b84f42e1997100658a2811752b7'>Commit.</a> </li>
<li>Properly support BUILD_TESTING. <a href='http://commits.kde.org/konqueror/a8015c34b39db5e856c1cc0f1fabff27f26fc07e'>Commit.</a> </li>
<li>Do not duplicated work of KAboutData::setupCommandLine(). <a href='http://commits.kde.org/konqueror/d6be40056aa92e5148012f19e26251998d4a18d0'>Commit.</a> </li>
<li>Use same konqueror.org homepage everywhere. <a href='http://commits.kde.org/konqueror/62a2ddc44781c4957defc09b4c02a23da53cb08f'>Commit.</a> </li>
<li>Remove duplicated cmake_minimum_required from libkonq, was resetting things. <a href='http://commits.kde.org/konqueror/86be8d737be8dc64c45381bc1e77633c626ea318'>Commit.</a> </li>
<li>Remove usage of dead QT_USE_FAST_CONCATENATION. <a href='http://commits.kde.org/konqueror/a9dc95416c0b2b18ff20d8b3bbda7afee2ed8ec3'>Commit.</a> </li>
<li>Remove unneeded moc includes. <a href='http://commits.kde.org/konqueror/3453efb25f1a4e2b773320fe0adfee3b14cc72fd'>Commit.</a> </li>
<li>Use more https in links. <a href='http://commits.kde.org/konqueror/1934038b66115b5f62064acab15077dec8b812a0'>Commit.</a> </li>
<li>Fix typo in include directory. <a href='http://commits.kde.org/konqueror/4749dff2152d5b0c7015e492f3511067357aab54'>Commit.</a> </li>
<li>Fix broken-by-kf5-port installation of some icons. <a href='http://commits.kde.org/konqueror/2d6c467c22dcfab07d440492b003f7174c915419'>Commit.</a> </li>
</ul>
<h3><a name='konquest' href='https://cgit.kde.org/konquest.git'>konquest</a> <a href='#konquest' onclick='toggle("ulkonquest", this)'>[Hide]</a></h3>
<ul id='ulkonquest' style='display: block'>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/konquest/75d7b36dd122cab1f01587d8ed14151702a8436d'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Fix crash in extendSelection. <a href='http://commits.kde.org/konsole/ef3773b8753b7553fc09c8cb020925388b05bc73'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402452'>#402452</a></li>
<li>Fix regression on tab color not reverting back to 'no activity' color. <a href='http://commits.kde.org/konsole/7dfae7a7b526706c3e2f27b54842d487f753db75'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402541'>#402541</a></li>
<li>Don't change alternate scrolling state after calling reset(). <a href='http://commits.kde.org/konsole/b545a812e3bf74157387fd60b61f6e2146723834'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402461'>#402461</a></li>
<li>Fix crash while trying to move tab with only one view. <a href='http://commits.kde.org/konsole/6a759670ac6a8fecd5142dd0d1f79b4b36a22721'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402278'>#402278</a></li>
<li>Fix cursor when anti aliasing is enabled. <a href='http://commits.kde.org/konsole/b48ecb5712037027e6385515c9eced7fabfc3dc3'>Commit.</a> </li>
<li>Fix crash when changing from blinking cursor to block cursor. <a href='http://commits.kde.org/konsole/02a68768c2b091e52035345fa08533885bb06453'>Commit.</a> </li>
<li>Fix drawing box chars, avoid storing and saving state all the time. <a href='http://commits.kde.org/konsole/807ac77061604c2ac7cf84b0a0b29dd949a6c634'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401463'>#401463</a></li>
<li>Restore default tabbar style when custom stylesheet is disabled. <a href='http://commits.kde.org/konsole/18e107bd32a197ba9c82a6dbf81ed1dcbdfcfc96'>Commit.</a> </li>
<li>Fix condition to redraw search result line. <a href='http://commits.kde.org/konsole/c2a08bf35e15da19b1614067ffcd277c6b147352'>Commit.</a> </li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Hide]</a></h3>
<ul id='ulkpat' style='display: block'>
<li>Correct help link for the new versions of KF5 (>52). <a href='http://commits.kde.org/kpat/0fd079dcfd49d4c66bc6483c36fb49f7529373ee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402837'>#402837</a></li>
</ul>
<h3><a name='ksirk' href='https://cgit.kde.org/ksirk.git'>ksirk</a> <a href='#ksirk' onclick='toggle("ulksirk", this)'>[Hide]</a></h3>
<ul id='ulksirk' style='display: block'>
<li>Fix crash when we delete element. <a href='http://commits.kde.org/ksirk/d6cadc396c1e44a5433811f30dacfe0c1477ec1d'>Commit.</a> </li>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/ksirk/a6b16d62be9ba256ec467caa5d6ba934c1afd436'>Commit.</a> </li>
<li>Fix signal/slot. <a href='http://commits.kde.org/ksirk/65a2ddc0e505f1893d1d58d5318d4afee534cf1e'>Commit.</a> </li>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/ksirk/fbd2c0a1657f4ce2bcf075a6fab20550f3a465b5'>Commit.</a> </li>
</ul>
<h3><a name='ktouch' href='https://cgit.kde.org/ktouch.git'>ktouch</a> <a href='#ktouch' onclick='toggle("ulktouch", this)'>[Hide]</a></h3>
<ul id='ulktouch' style='display: block'>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/ktouch/66ad48e3d85e3443668180dcfb0fcc4c2100b561'>Commit.</a> </li>
</ul>
<h3><a name='kturtle' href='https://cgit.kde.org/kturtle.git'>kturtle</a> <a href='#kturtle' onclick='toggle("ulkturtle", this)'>[Hide]</a></h3>
<ul id='ulkturtle' style='display: block'>
<li>Ci: enable freebsd and windows builds. <a href='http://commits.kde.org/kturtle/88a9afbcbfac3ac0fece8a47d997ecff96223204'>Commit.</a> </li>
<li>Ci: add gitlab CI configuration. <a href='http://commits.kde.org/kturtle/69da820020c23415d9f369886b4e842b301cbf94'>Commit.</a> </li>
</ul>
<h3><a name='kwordquiz' href='https://cgit.kde.org/kwordquiz.git'>kwordquiz</a> <a href='#kwordquiz' onclick='toggle("ulkwordquiz", this)'>[Hide]</a></h3>
<ul id='ulkwordquiz' style='display: block'>
<li>Use nullptr/override/explicit. <a href='http://commits.kde.org/kwordquiz/1dbcbdf93efc1dce8624365dad9310fc0ea56cee'>Commit.</a> </li>
<li>Fix dialog layout. <a href='http://commits.kde.org/kwordquiz/37a974a2c8d4996de288d91f6aa79ddd67b74bf4'>Commit.</a> </li>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/kwordquiz/974a20d16546ebcf6df2d425d40930afec9273f4'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Init kcrash so it works with kdeinit https://markmail.org/thread/zv5pheijaze72bzs. <a href='http://commits.kde.org/lokalize/c54e65d077d500d49308435414ff49c21ac63a6c'>Commit.</a> </li>
<li>Fix compilation with -DQT_NO_CAST_TO_ASCII and enable it. <a href='http://commits.kde.org/lokalize/9e2e6f01afbe8cb2751cdd25755c0bbfb0f35a6a'>Commit.</a> </li>
</ul>
<h3><a name='lskat' href='https://cgit.kde.org/lskat.git'>lskat</a> <a href='#lskat' onclick='toggle("ullskat", this)'>[Hide]</a></h3>
<ul id='ullskat' style='display: block'>
<li>Fix tab order in name dialog. <a href='http://commits.kde.org/lskat/a4631ffbd88fcaca36f09781299e68387836eee5'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Clean up. <a href='http://commits.kde.org/mailcommon/60abae2adcff31e427b50347ae8af7ab7075a9ba'>Commit.</a> </li>
<li>Optimization. Don't initialize brokencolor if not necessary. <a href='http://commits.kde.org/mailcommon/09484467ef9256392498e0c5d3edbc84d4364a83'>Commit.</a> </li>
</ul>
<h3><a name='mailimporter' href='https://cgit.kde.org/mailimporter.git'>mailimporter</a> <a href='#mailimporter' onclick='toggle("ulmailimporter", this)'>[Hide]</a></h3>
<ul id='ulmailimporter' style='display: block'>
<li>Add missing include. <a href='http://commits.kde.org/mailimporter/a371c851f32572695217141611066377553fb88b'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Api.longurl.org doesn't work now. Use lengthenurl.info. <a href='http://commits.kde.org/messagelib/699120d0138beb88e1d8646af503f59456c1c725'>Commit.</a> </li>
<li>Inform when we can't expand url. <a href='http://commits.kde.org/messagelib/a5b272e54e931b7c694004360d2de5583a98037f'>Commit.</a> </li>
<li>Don't generate string when it's not necessary. <a href='http://commits.kde.org/messagelib/16281e63e2abc16aba3191af4a5922850bb1f4bd'>Commit.</a> </li>
<li>Fix Bug 402378 - mailto staement , uncomplete url past to kmail composer. <a href='http://commits.kde.org/messagelib/4f1db227b1b393ea18670ee4664972ba663cff04'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402378'>#402378</a></li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Do not crash in files without title-info. <a href='http://commits.kde.org/okular/58038fed18fa3e89b481bb3843f29f45fff4240a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402558'>#402558</a></li>
<li>Fix crash when writing to some forms. <a href='http://commits.kde.org/okular/ba5ec662a238584e66fd91c7fab728400021fcd1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402307'>#402307</a></li>
</ul>
<h3><a name='palapeli' href='https://cgit.kde.org/palapeli.git'>palapeli</a> <a href='#palapeli' onclick='toggle("ulpalapeli", this)'>[Hide]</a></h3>
<ul id='ulpalapeli' style='display: block'>
<li>Fix going into the config dialog sometimes breaking left button. <a href='http://commits.kde.org/palapeli/d144cd3b84446634bb0a1332ec5f9017d651ec6b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402655'>#402655</a></li>
<li>Add missing exec code. <a href='http://commits.kde.org/palapeli/98117d9cad83c2ad9500624fb7179f710d6b10fd'>Commit.</a> </li>
</ul>
<h3><a name='picmi' href='https://cgit.kde.org/picmi.git'>picmi</a> <a href='#picmi' onclick='toggle("ulpicmi", this)'>[Hide]</a></h3>
<ul id='ulpicmi' style='display: block'>
<li>Set project license. <a href='http://commits.kde.org/picmi/0448ba56231cf2aa4de8c4dd20c10c64a7ba3c20'>Commit.</a> </li>
<li>Update picmi screenshot to a variant using breeze styling and the cdn host. <a href='http://commits.kde.org/picmi/fca3f21e71a84375b7ddefc4d93eee5dc0b2f077'>Commit.</a> </li>
</ul>
<h3><a name='rocs' href='https://cgit.kde.org/rocs.git'>rocs</a> <a href='#rocs' onclick='toggle("ulrocs", this)'>[Hide]</a></h3>
<ul id='ulrocs' style='display: block'>
<li>Ci: enable freebsd build. <a href='http://commits.kde.org/rocs/10ab63ae45c1167962d1b0a11de6450e0d21b281'>Commit.</a> </li>
<li>Add gitlab-ci configuration. <a href='http://commits.kde.org/rocs/abbf6e21d97b5f9bd7054923fdd0a7c1871113ac'>Commit.</a> </li>
<li>Make gitignore little less restrictive. <a href='http://commits.kde.org/rocs/e77f4b1533982ace1679a70cda36d4d9d402f9c8'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Use more https in links. <a href='http://commits.kde.org/spectacle/2d4bd435b592b986b18527db26b0a33b72063fb2'>Commit.</a> </li>
</ul>
<h3><a name='step' href='https://cgit.kde.org/step.git'>step</a> <a href='#step' onclick='toggle("ulstep", this)'>[Hide]</a></h3>
<ul id='ulstep' style='display: block'>
<li>Remove traces of KDE4 from the build system. <a href='http://commits.kde.org/step/2c2595222af44503fbd2207e6d8beb48d20b9624'>Commit.</a> </li>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/step/614f623401b5099e03c947ef2900a3bd52b6ae75'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Fix 'Strange multiple duplication of labels on interfaces in component diagram after saving'. <a href='http://commits.kde.org/umbrello/3cc83a5f971b93c1e850eaa3a8d19765d6a17ecd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402722'>#402722</a></li>
<li>Cleanup owner setting of PinPortBase related classes in constructor. <a href='http://commits.kde.org/umbrello/578e053ad83377a166feb5541ed959bbcfd60f3f'>Commit.</a> </li>
<li>In ClassifierWidget fix duplicated common painting in widget interface variant. <a href='http://commits.kde.org/umbrello/40100890db843cd0b94c4c19696241a7314f99d1'>Commit.</a> </li>
<li>Update docbook files to version 4.5 to be compatible with KF5. <a href='http://commits.kde.org/umbrello/626de6de7889b4724394cd47901fae5249ce7ec7'>Commit.</a> </li>
</ul>