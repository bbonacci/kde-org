---
aliases:
- ../../fulllog_applications-15.08.3
hidden: true
title: KDE Applications 15.08.3 Full Log Page
type: fulllog
version: 15.08.3
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix CollectionStatistics reporting wrong numbers for folders with $IGNORED emails. <a href='http://commits.kde.org/akonadi/a09b50e0f957c2606cc92f127447b83c33e91b60'>Commit.</a> </li>
<li>Give the CI a chance to finish this test before the timeout. <a href='http://commits.kde.org/akonadi/8a1ce31114c1a7d2431fb9102d511b84135f87d6'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Fix opening of ISO files. <a href='http://commits.kde.org/ark/b10430b06ef9ea458e50d222518f72e738c21e43'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354344'>#354344</a></li>
<li>Fix opening of compressed tar archives with special characters in the extension. <a href='http://commits.kde.org/ark/9e1607559db2e25aa3d3fecb9fe0dd1ca16dbc01'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/328815'>#328815</a></li>
<li>Libarchiveplugin: Optimize copyFiles(). <a href='http://commits.kde.org/ark/f8891e4d02e89ac309f3df24a02fd00ff9ad5154'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/282388'>#282388</a>. Code review <a href='https://git.reviewboard.kde.org/r/D401'>#D401</a></li>
<li>Do not always expand the first level folders. <a href='http://commits.kde.org/ark/c9a15ff6a446715939b3dc186ac93d588b71484b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/303708'>#303708</a></li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Show]</a></h3>
<ul id='ulcantor' style='display: none'>
<li>By default use octave-cli rather than octave. <a href='http://commits.kde.org/cantor/6a3bc6c39a4cc4c21973f53c82187a0778e2eca4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125646'>#125646</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>Allow home directories with non-local file paths. <a href='http://commits.kde.org/dolphin/5dd5eaf08da4b7d11e53c90096c2ea0e6a19e840'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352743'>#352743</a>. Fixes bug <a href='https://bugs.kde.org/353550'>#353550</a>. Code review <a href='https://git.reviewboard.kde.org/r/125586'>#125586</a></li>
<li>Fix detach tab not working when path contains spaces. <a href='http://commits.kde.org/dolphin/4d6cd761d8987adee511546f644ecf5f357148ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352420'>#352420</a>. Code review <a href='https://git.reviewboard.kde.org/r/125587'>#125587</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Show]</a></h3>
<ul id='ulgwenview' style='display: none'>
<li>Do not scale up images smaller than the thumbnail size. <a href='http://commits.kde.org/gwenview/9cd7aa54e6d702c6e1d20d8554b0924f70c234fe'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124416'>#124416</a></li>
</ul>
<h3><a name='kblocks' href='https://cgit.kde.org/kblocks.git'>kblocks</a> <a href='#kblocks' onclick='toggle("ulkblocks", this)'>[Show]</a></h3>
<ul id='ulkblocks' style='display: none'>
<li>Requirements cleanup. <a href='http://commits.kde.org/kblocks/4de85c28847a60d10c22459ebdabe7f6c25701b2'>Commit.</a> </li>
<li>ToAscii -> toLatin1. <a href='http://commits.kde.org/kblocks/90b1461cc6df7b844b3a67b9bb899c7cad4ce42a'>Commit.</a> </li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Show]</a></h3>
<ul id='ulkde-dev-scripts' style='display: none'>
<li>Draw_lib_dependencies: Fix bug when MAXDEPTH is reached. <a href='http://commits.kde.org/kde-dev-scripts/4779e89269ccaac2985ff008fbfd556b3328a5b8'>Commit.</a> </li>
</ul>
<h3><a name='kde-runtime' href='https://cgit.kde.org/kde-runtime.git'>kde-runtime</a> <a href='#kde-runtime' onclick='toggle("ulkde-runtime", this)'>[Show]</a></h3>
<ul id='ulkde-runtime' style='display: none'>
<li>Make kglobalaccel,khelpcenter,kuiserver bits optional (default ON). <a href='http://commits.kde.org/kde-runtime/be3ee5b9a36e1b7f3c6eab466e33762ae101b3c6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351500'>#351500</a></li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Fix double-emit of result and missing warning when listing hits an inaccessible folder. <a href='http://commits.kde.org/kdelibs/e60e8b96b211290e330c7ff6a3b84b20ab85850d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125613'>#125613</a>. See bug <a href='https://bugs.kde.org/333436'>#333436</a></li>
<li>Docu: recommend uiDelegate() rather than ui(). <a href='http://commits.kde.org/kdelibs/6a0be51bc498ed6280a7c0b9d55de0adcbd1ae13'>Commit.</a> </li>
<li>Backport commit b72fc5e56579035bf987075e16324ef95ef8e3d4. <a href='http://commits.kde.org/kdelibs/4f7ea2f770cf062ef22293fbb21a086f3e0cbfcb'>Commit.</a> </li>
<li>Try to figure out what mime we're getting in CI. <a href='http://commits.kde.org/kdelibs/218aa9c2c2bf9efad203192d6cfc1552733ede89'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Integrate downstream patches. <a href='http://commits.kde.org/kdenlive/1515402dccf892a08a9cb73aa552ddf9c0d505bf'>Commit.</a> </li>
<li>Backport fix transition should adapt duration to clips overlap. <a href='http://commits.kde.org/kdenlive/36d8206b7346f921b59f05875562d4573d6541f0'>Commit.</a> </li>
<li>Fix compiler warnings. <a href='http://commits.kde.org/kdenlive/558e99985a02c111b18eb63e236edff9babef4e5'>Commit.</a> </li>
<li>Fix x265 variable parameter incorrectly set. <a href='http://commits.kde.org/kdenlive/3cfadbe7e3ee78d8a859d699a1b0f46d28346d00'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354077'>#354077</a></li>
<li>Fix crash in custom render profiles & wrong group on new profile. <a href='http://commits.kde.org/kdenlive/9d2cf13bcae7521f87f3e755e65af938182c6c06'>Commit.</a> </li>
<li>Fix crash on render profile with empty file extension. <a href='http://commits.kde.org/kdenlive/60c5cc5c53343990e7a7d42ce35a5bb45a91781e'>Commit.</a> See bug <a href='https://bugs.kde.org/352670'>#352670</a></li>
<li>Fix opening old project files makes some timeline clips invisible. <a href='http://commits.kde.org/kdenlive/31b518b3d8f1b99ecc09481bcbe211a1c3a3ff7e'>Commit.</a> See bug <a href='https://bugs.kde.org/353125'>#353125</a></li>
<li>Remove old unused files. <a href='http://commits.kde.org/kdenlive/29c8669a7efc4a67107c4ed2a2dbae669537f03c'>Commit.</a> </li>
<li>Reorganize effects categories. <a href='http://commits.kde.org/kdenlive/544a99826da2b7dbad66af90749c31d36c6cc842'>Commit.</a> See bug <a href='https://bugs.kde.org/351715'>#351715</a></li>
<li>Make zoom slider zoom in on wheel up event, like Ctrl+wheel in timeline. <a href='http://commits.kde.org/kdenlive/87d7eff6fe8eb877906711b322536c8cb36d3216'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352853'>#352853</a></li>
<li>Fix scopes titlebar not hidden on startup. <a href='http://commits.kde.org/kdenlive/3f2dd7b4e702a569399d3fc94b08632999198d35'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352847'>#352847</a></li>
<li>Rename Project Tree > Project Bin. <a href='http://commits.kde.org/kdenlive/ff3238430fd721ff6e688f9ca3551f2d9dcf8c41'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352815'>#352815</a></li>
<li>Improve clip conversion in DVD Wizard. <a href='http://commits.kde.org/kdenlive/3a8b74df7cfd8773c7a00444cf4280d6684c680d'>Commit.</a> See bug <a href='https://bugs.kde.org/350788'>#350788</a></li>
<li>Allow adding non mpeg files to DVD wizard (it will autoconvert). <a href='http://commits.kde.org/kdenlive/c35a78c75cfdbebf6625813c7b653c6960d77746'>Commit.</a> See bug <a href='https://bugs.kde.org/350788'>#350788</a></li>
<li>Fix CID 1323720, 1323721 (null deref), 1323728 (member init). <a href='http://commits.kde.org/kdenlive/880f0fa282ca74cc581189d22f101ee6b35232f7'>Commit.</a> </li>
<li>Fix CID 1323727 (member init). <a href='http://commits.kde.org/kdenlive/7e1ab36661a7ac9a267b64398cfd3f495b19158f'>Commit.</a> </li>
<li>Fix CID 1323723 (member init). <a href='http://commits.kde.org/kdenlive/52b80db47bd93cb34b1ac1f35302837b79a79577'>Commit.</a> </li>
<li>Fix CID 1323719 (null dereference). <a href='http://commits.kde.org/kdenlive/18443be30c6135a8d40dc73b2cd90cd35f2ec587'>Commit.</a> </li>
<li>Fix CID 1323685 (null dereference). <a href='http://commits.kde.org/kdenlive/89f94913f5b4d4add542fab218afdb0c047f1653'>Commit.</a> </li>
<li>Fix CID 1323680 (div by 0). <a href='http://commits.kde.org/kdenlive/005a26e141a71137eb3397a00b0b66868bd4405e'>Commit.</a> </li>
<li>Fix favorite effects not showing description in info box. <a href='http://commits.kde.org/kdenlive/f3df279514f8110df1173fb7718f817800775545'>Commit.</a> </li>
<li>Make bin search case insensitive. <a href='http://commits.kde.org/kdenlive/37d33708cf292f79c2683d69f7bf78353898f7f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352884'>#352884</a></li>
<li>Fix Effects list context menu. <a href='http://commits.kde.org/kdenlive/5c7b0f375bc62148248a9ab33d7166bedcf8ca07'>Commit.</a> </li>
<li>Fix audio align. <a href='http://commits.kde.org/kdenlive/2cbe9414b71c5b01c61471302396e90ca5fa0150'>Commit.</a> </li>
<li>Fix CID 1323738  (dead code). <a href='http://commits.kde.org/kdenlive/c9feb881360273d05670c38ef0667fe04a0118c7'>Commit.</a> </li>
<li>Fix CID 1323681 (null dereference). <a href='http://commits.kde.org/kdenlive/bdde85c27951e5b45625be00a56ce9cc8ad27935'>Commit.</a> </li>
<li>Fix CID 1323735 (dead code). <a href='http://commits.kde.org/kdenlive/5b46566bdc2694dcb6c724864777950948922599'>Commit.</a> </li>
<li>Fix CID 1323682 (null dereference), 1323725 (unitialized members), 1323737 (dead code). <a href='http://commits.kde.org/kdenlive/0d823bc5fe0228019b21f4cfd12dd5cdc3d31753'>Commit.</a> </li>
<li>Fix CID 1323726 (uninitialized member). <a href='http://commits.kde.org/kdenlive/525ab610ab8986511a035093f88be0fc0257b0cc'>Commit.</a> </li>
<li>Fix CID 1323734 (unused member), 1323676 (logical operator). <a href='http://commits.kde.org/kdenlive/df18d0afaa0d9ae4a619fa4cb0509cf0561702b1'>Commit.</a> </li>
<li>Fix coverity 1323684, 1323688 (null dereference). <a href='http://commits.kde.org/kdenlive/d545b4d9d1033f27876911b41649830b71d9c25b'>Commit.</a> </li>
<li>Fix CID 1323686 (null dereference). <a href='http://commits.kde.org/kdenlive/b85f2131e83b9bc813a00cfe1653efde17d018b0'>Commit.</a> </li>
<li>Fix coverity 1323677 (dead code). <a href='http://commits.kde.org/kdenlive/7962594140eeb2b5317a1a6ff5e45f79bbbcde80'>Commit.</a> </li>
<li>Fix coverity 1323676 (logical operator). <a href='http://commits.kde.org/kdenlive/897fda6724ed0b4e3b6bd3a501ee89bbfc70371a'>Commit.</a> </li>
<li>Fix transition settings not properly restored on some locales. <a href='http://commits.kde.org/kdenlive/523536d9faf64c06ca6e763a9aa77293d86d7718'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354208'>#354208</a></li>
<li>Fix coverity 1228801 (float division). <a href='http://commits.kde.org/kdenlive/84d92dd67a0bccd45f8ad05f377d64a68b4dd036'>Commit.</a> </li>
<li>Fix coverity 1323687 (null dereference). <a href='http://commits.kde.org/kdenlive/351931e159b34d6e607a23b5b67021ed13476b07'>Commit.</a> </li>
<li>CID 1228800 intentional. <a href='http://commits.kde.org/kdenlive/502faea6b8ce3f7b7042d37cfac7adbff09e3d5d'>Commit.</a> </li>
<li>Fix coverity 1323683 (null dereference). <a href='http://commits.kde.org/kdenlive/37c87e274d834669361309112824187f6628d523'>Commit.</a> </li>
<li>Fix coverity 1323689-92 (null dereference). <a href='http://commits.kde.org/kdenlive/827522a752dd8cb44edcfbddaebffa9b4edd6b23'>Commit.</a> </li>
<li>Fix coverity 1323729 (uninitialized member). <a href='http://commits.kde.org/kdenlive/636e47fd1712a44d29b18e1f27af2cd8db4ccaa6'>Commit.</a> </li>
<li>Fix coverity 1228798 (integer division). <a href='http://commits.kde.org/kdenlive/ee19fe09555935b0a53c46da5a88ca07ef796d32'>Commit.</a> </li>
<li>Fix coverity 1228795 (integer / double conversion). <a href='http://commits.kde.org/kdenlive/18f7407f909a7f92a1408f1558fa427acec125f3'>Commit.</a> </li>
<li>Fix coverity 1323736 (dead code). <a href='http://commits.kde.org/kdenlive/e237a8f648231aad284bb5d6a004b842c81e061e'>Commit.</a> </li>
<li>Fix coverity 1323724 (uninitialized member). <a href='http://commits.kde.org/kdenlive/0fee59d4a6549c26e7f8d1648d16bf28ff910e31'>Commit.</a> </li>
<li>Fix coverity 1323730 (uninitialized member). <a href='http://commits.kde.org/kdenlive/cd1ea20987763a1ac53742d014a102fea3a9e9eb'>Commit.</a> </li>
<li>Fix coverity 1323733. <a href='http://commits.kde.org/kdenlive/9072f58c9b559f84d9f80e725adcccfc9cc36332'>Commit.</a> </li>
<li>Fix coverity 1323678 (dead code). <a href='http://commits.kde.org/kdenlive/6fa07f39d90fff044ef3c5ea03ce233d0dc80425'>Commit.</a> </li>
<li>Fix coverity 1323679 (dead code). <a href='http://commits.kde.org/kdenlive/14eb1ff66425903aceee560fe19b02cd0702fdaa'>Commit.</a> </li>
<li>Brace mistake in packporting patch. <a href='http://commits.kde.org/kdenlive/7dc00432d2a76e7f7eceb4448016f5908d88619e'>Commit.</a> </li>
<li>Fix freeze on monitor. <a href='http://commits.kde.org/kdenlive/858a85523f1f9bbd3db5e8398cfebc63ea56c653'>Commit.</a> </li>
<li>Fix GL monitor on Intel GMA. <a href='http://commits.kde.org/kdenlive/27e2808ef56641d67adcc9b662fdc0ca184a6bda'>Commit.</a> </li>
<li>Fix clipping of audio signal view db marks. <a href='http://commits.kde.org/kdenlive/de50989848eecf63cf2247c591d02dd6f33cf383'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125708'>#125708</a></li>
<li>Timeline incorrectly checks track index bounds crash. <a href='http://commits.kde.org/kdenlive/d0ca82c01c9c12c76f110bb654a3fef7aff181b5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125707'>#125707</a></li>
<li>Configuration dialog UI improvement: show action texts assigned to jog buttons without "&"; easier to read; sorts according to user expectations. <a href='http://commits.kde.org/kdenlive/6c9464d4cd99ade8316df61c370f5c5d43818ffb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125548'>#125548</a></li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Fix conversion error in sub-repetition interval from command line. <a href='http://commits.kde.org/kdepim/616bca0e193771b2ff8621b008c407e447af5c64'>Commit.</a> </li>
<li>Re-enable sendmail for email alarms. <a href='http://commits.kde.org/kdepim/2a3f9dd8168a76234e63a09544b5b36e466d1779'>Commit.</a> </li>
<li>Prepare 5.0.3. <a href='http://commits.kde.org/kdepim/fe02727d931610e3062119e3bb25bc9088e5254d'>Commit.</a> </li>
<li>Fix Bug 351395 - Crash when the identity's "dictionary" setting is changed. <a href='http://commits.kde.org/kdepim/7744a9211d819488ba9de6880113e82297acf5bb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351395'>#351395</a></li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>5.0.3. <a href='http://commits.kde.org/kdepim-runtime/edba4888dabc25b30fd5a0b69946c1fa731c01ff'>Commit.</a> </li>
<li>Check calendar ptr before access it in writeFile() in kalarm resource. <a href='http://commits.kde.org/kdepim-runtime/a528dcf98582da1c49163ff93209fd00090eac01'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125747'>#125747</a></li>
<li>Adapt test to change 813e4dfdcf30bed34397fa578d0d1ae0e61ea625. <a href='http://commits.kde.org/kdepim-runtime/5efd08b0f05f74fc1bba4ccb370dbc8182c4602c'>Commit.</a> </li>
</ul>
<h3><a name='kdepimlibs' href='https://cgit.kde.org/kdepimlibs.git'>kdepimlibs</a> <a href='#kdepimlibs' onclick='toggle("ulkdepimlibs", this)'>[Show]</a></h3>
<ul id='ulkdepimlibs' style='display: none'>
<li>Fix license, this needs to be LGPL. <a href='http://commits.kde.org/kdepimlibs/a7831bbdfdf9896eb9518bca1abb94185d81bdac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351752'>#351752</a></li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Show]</a></h3>
<ul id='ulkgpg' style='display: none'>
<li>Bump version number for KDE Applications 15.08.3. <a href='http://commits.kde.org/kgpg/546e02bc04d52867b9227c7f879fa71cc352e6bd'>Commit.</a> </li>
<li>Make KGpgSignUid work with GnuPG >= 2.1. <a href='http://commits.kde.org/kgpg/300cf34cc3cae322d9c8a456b92fceac3c28bdda'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/341535'>#341535</a></li>
<li>Fix GnuPG 2.1 not finding the secret keys in CAFF mode. <a href='http://commits.kde.org/kgpg/10838fa8f856b9dc977647601cb532a32f6e035e'>Commit.</a> See bug <a href='https://bugs.kde.org/341535'>#341535</a></li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Show]</a></h3>
<ul id='ulkig' style='display: none'>
<li>Use mkpath Instead of mkdir to Prevent Cold Start Bug. <a href='http://commits.kde.org/kig/c97e58162255db43e2030a39c79583872efed83c'>Commit.</a> See bug <a href='https://bugs.kde.org/353729'>#353729</a></li>
<li>Fix the Type of the Angle in Degrees Label. <a href='http://commits.kde.org/kig/5e7af604096c7522a516ae181b0b4a60e2fba667'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353743'>#353743</a></li>
<li>Remove the nofork Argument in Kig Call. <a href='http://commits.kde.org/kig/8835cbe4ed5f1f22fbb14fb1a627e2d00ce64089'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353726'>#353726</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Show]</a></h3>
<ul id='ulkio-extras' style='display: none'>
<li>Fixed thumbnail generation for files of type "image/webp". <a href='http://commits.kde.org/kio-extras/b77547ad90467a7f0a37469e9a58417c4ea63b8a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125788'>#125788</a></li>
</ul>
<h3><a name='kiriki' href='https://cgit.kde.org/kiriki.git'>kiriki</a> <a href='#kiriki' onclick='toggle("ulkiriki", this)'>[Show]</a></h3>
<ul id='ulkiriki' style='display: none'>
<li>Revert "Remove unused dependencies.". <a href='http://commits.kde.org/kiriki/daf43b57227ac12d073495276b5a802f291b2f75'>Commit.</a> </li>
<li>Remove unused dependencies. <a href='http://commits.kde.org/kiriki/3508e9f37cc46c05dfabb226f41da4ffda6c89d1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125956'>#125956</a></li>
<li>DocTools for kdoctools_create_handbook. <a href='http://commits.kde.org/kiriki/a304bf00b87b39481199e4ffd33687d04a3d62ab'>Commit.</a> </li>
</ul>
<h3><a name='kmahjongg' href='https://cgit.kde.org/kmahjongg.git'>kmahjongg</a> <a href='#kmahjongg' onclick='toggle("ulkmahjongg", this)'>[Show]</a></h3>
<ul id='ulkmahjongg' style='display: none'>
<li>Save and load game number properly. <a href='http://commits.kde.org/kmahjongg/a2834e03c2e76f8582e17262d2d14cdc0e721b48'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353845'>#353845</a>. Code review <a href='https://git.reviewboard.kde.org/r/125855'>#125855</a></li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Prepare for 15.08.3. <a href='http://commits.kde.org/kopete/738a9722055bb79c0c3fd3461d6e02f42dfd4f61'>Commit.</a> </li>
</ul>
<h3><a name='kruler' href='https://cgit.kde.org/kruler.git'>kruler</a> <a href='#kruler' onclick='toggle("ulkruler", this)'>[Show]</a></h3>
<ul id='ulkruler' style='display: none'>
<li>Fix QCoreApplication::arguments warning. <a href='http://commits.kde.org/kruler/bc1cd3e62d4322ce379aa17facd00be91807d370'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125602'>#125602</a></li>
</ul>
<h3><a name='ktp-accounts-kcm' href='https://cgit.kde.org/ktp-accounts-kcm.git'>ktp-accounts-kcm</a> <a href='#ktp-accounts-kcm' onclick='toggle("ulktp-accounts-kcm", this)'>[Show]</a></h3>
<ul id='ulktp-accounts-kcm' style='display: none'>
<li>[kaccounts] Store the secret in temp variable. <a href='http://commits.kde.org/ktp-accounts-kcm/58cab8b577280452a2b81376240e4004d05c1478'>Commit.</a> </li>
<li>[kaccounts] Emit configUiReady() when config UI is ready. <a href='http://commits.kde.org/ktp-accounts-kcm/2a702004da2484694f3d1418959f387a4f54967c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351785'>#351785</a></li>
</ul>
<h3><a name='libkdegames' href='https://cgit.kde.org/libkdegames.git'>libkdegames</a> <a href='#libkdegames' onclick='toggle("ullibkdegames", this)'>[Show]</a></h3>
<ul id='ullibkdegames' style='display: none'>
<li>Fix KScoreDialog::setConfigGroup documentation. <a href='http://commits.kde.org/libkdegames/983b7e539aa9c4eb72b5dfdfe5e3fd8531d006ec'>Commit.</a> </li>
</ul>
<h3><a name='libkomparediff2' href='https://cgit.kde.org/libkomparediff2.git'>libkomparediff2</a> <a href='#libkomparediff2' onclick='toggle("ullibkomparediff2", this)'>[Show]</a></h3>
<ul id='ullibkomparediff2' style='display: none'>
<li>Fix calling KIO::stat and KIO::mkdir in saveDestination. <a href='http://commits.kde.org/libkomparediff2/73b12ed92aa45e1c67fceddc81ad07524c26e391'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125563'>#125563</a></li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Show]</a></h3>
<ul id='ullokalize' style='display: none'>
<li>Fix eastern language input. <a href='http://commits.kde.org/lokalize/54340befa34216fd36b91015668d6683e47aaf43'>Commit.</a> </li>
<li>BUG: 349427. <a href='http://commits.kde.org/lokalize/33aea9c0793f33f893b00c10ffcd5e1ce0c6b00a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349427'>#349427</a></li>
<li>BUG: 348086. <a href='http://commits.kde.org/lokalize/631535cda53e709d26660094060f0adf0333a1e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348086'>#348086</a></li>
<li>BUG: 354175. <a href='http://commits.kde.org/lokalize/7f31225adffd6d3ad2aac9985b43258e84ea4ff9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354175'>#354175</a></li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Clarify license of the DGML files. <a href='http://commits.kde.org/marble/e96a51255298b0854f299d79a63627c69b4f3ff7'>Commit.</a> </li>
<li>Fix copy and paste oversight. <a href='http://commits.kde.org/marble/f5549d393a65ad3f2fc4e354d376578c3d1993d7'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Save file attachments on left click. <a href='http://commits.kde.org/okular/a675670ddaefc6da554523b6a6dfc8d1e1073e2b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354268'>#354268</a></li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Show]</a></h3>
<ul id='ulprint-manager' style='display: none'>
<li>Guard m_connectedEvents with a mutex. <a href='http://commits.kde.org/print-manager/65d91695a7c6acce4ad24e8fdfea47e811d45323'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix doc of SQLImport::parseColumnConstraints() in relation of 'character set' and 'collate' attribute. <a href='http://commits.kde.org/umbrello/51804eeefc58033e7cf97d92856d56af0dd765b1'>Commit.</a> See bug <a href='https://bugs.kde.org/353668'>#353668</a></li>
<li>Fix bug 'don't generate the code with postgresql option in code generation wizard'. <a href='http://commits.kde.org/umbrello/16c167212abc2206d04c123726afc20e9626efa7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354700'>#354700</a></li>
<li>Don't change focus when documentation is shown. <a href='http://commits.kde.org/umbrello/57c1ed5eb51ecfb07ede64093ffa2967dbeca981'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125781'>#125781</a></li>
</ul>