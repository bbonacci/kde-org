2005-05-24 22:47 +0000 [r417912]  djarvie

	* kalarm/mainwindowbase.h: Add comments describing class

2005-05-26 12:59 +0000 [r418371]  vanrijn

	* kpilot/conduits/memofileconduit/memofile-conduit.cc: - fixing bug
	  106324 - isFirstSync() is true for both copyHHtoPC and
	  copyPCtoHH. memofile conduit only wants to do copyHHtoPC if we're
	  either told to, or we don't have any local memos in the
	  filesystem

2005-05-26 17:06 +0000 [r418453]  vanrijn

	* kpilot/conduits/memofileconduit/memofile-conduit.cc,
	  kpilot/conduits/memofileconduit/memofile-conduit.h,
	  kpilot/lib/pilotLocalDatabase.cc: - further fixes for 106324 -
	  don't do a global delete from database/local database. instead,
	  use model similar to abbrowser-conduit, where we sync first, then
	  remove any extra records from the palm. - fix in
	  pilotLocalDatabase for null Private *d. This was only initialized
	  if record count was greater than 0. This means that if we have an
	  empty database and we try to write a record to it, d will be null
	  and we'll get a crash. This change makes us initialize d in
	  openDatabase() even if there are no records (empty local
	  database). CCBUG:106324

2005-05-26 17:09 +0000 [r418454]  adridg

	* kpilot/lib/pilotDatabase.h: Protect against bad pointers and
	  failed opens

2005-05-26 17:37 +0000 [r418470-418469]  adridg

	* kpilot/lib/testdatabase.cc, kpilot/lib/data (added),
	  kpilot/lib/data/MemoDB.pdb (added): Add test that demonstrates
	  that setCategory() is broken

	* kpilot/lib/pilotDatabase.cc: qstrncpy() is like strlcpy(), not
	  strncpy(), so don't use it like the latter would (and fix
	  critical category-name-mangling bug)

2005-05-26 18:04 +0000 [r418481]  djarvie

	* kalarm/alarmevent.cpp: Make events in the calendar transparent

2005-05-27 13:21 +0000 [r418703]  dgp

	* configure.in.in: Cleaned up headers file checks, removed obsolete
	  checks inherited from kdenetwork. Removes warnings about if_ppp
	  headers during ./configure step.

2005-05-27 17:54 +0000 [r418793]  djarvie

	* kalarm/messagewin.cpp: Hide KMix window if KMix is started by
	  KAlarm

2005-05-27 18:43 +0000 [r418810]  djarvie

	* kalarm/messagewin.cpp: Fix comments

2005-05-28 16:36 +0000 [r419143]  vkrause

	* knode/knnntpclient.cpp: Backport from trunk for KDE 3.4.2: Check
	  the response code after sending LIST OVERVIEW.FMT command. KNode
	  now works again with servers that don't support this command.
	  BUG: 104422, 106390

2005-05-28 18:19 +0000 [r419192]  kloecker

	* kmail/configuredialog.cpp, kmail/configuredialog_p.h: Backport of
	  SVN commit 418874 by kloecker: - Fix bug which inserted all
	  encodings twice into the list of override encoding. - Init
	  widgets in the c'tor and read the current values in load(). -
	  Save empty string instead of 'Auto' for automatic character
	  encoding.

2005-05-30 02:59 +0000 [r419610]  schrot

	* korganizer/korgac/koalarmclient.cpp: backport from trunk: -
	  remember last check on logout CCBUG: 102489

2005-05-30 16:00 +0000 [r419777]  tilladam

	* plugins/kmail/bodypartformatter/text_calendar.cpp: And here...

2005-05-31 13:37 +0000 [r420163]  schrot

	* knode/knarticlefactory.cpp, knode/knarticlefactory.h: non-string
	  backport from trunk: - fix %NAME and %EMAIL macros CCBUG: 22101

2005-06-02 13:21 +0000 [r421198]  dfaure

	* kresources/kolab/kabc/contact.cpp: Backport fix for phone number
	  mapping (contact.cpp 1.8-1.9 in CVS)

2005-06-02 21:57 +0000 [r421367]  kloecker

	* libemailfunctions/email.cpp: Backport of SVN commit 421362 by
	  kloecker: > > - Don't eat local-part only addresses (i.e.
	  distribution list names) when decoding IDNs.

2005-06-03 19:27 +0000 [r421714]  tilladam

	* libkcal/todo.cpp: Completed date safety improvements from trunk.

2005-06-04 11:20 +0000 [r422040]  tilladam

	* kmail/kmfoldertree.cpp: Show the resource folder icons also for
	  folders with unread mail.

2005-06-05 08:56 +0000 [r422360]  tilladam

	* kmail/recipientspicker.cpp: Backport fixes from trunk.

2005-06-06 15:28 +0000 [r422818]  kainhofe

	* kresources/kolab/kcal/task.cpp: Backport from HEAD: Priorities
	  use a range 0-9, so don't reset if >5, but only if >9

2005-06-06 16:40 +0000 [r422835]  tilladam

	* kaddressbook/kabcore.cpp: Backport of the cancel pasting fix from
	  trunk.

2005-06-07 10:13 +0000 [r423044]  tilladam

	* kmail/kmreaderwin.cpp: Backport of the shift-click crash bugfix.

2005-06-07 19:59 +0000 [r423196]  djarvie

	* kalarm/widgets/pushbutton.h, kalarm/widgets/messagebox.h,
	  kalarm/widgets/label.h, kalarm/widgets/spinbox.h,
	  kalarm/widgets/checkbox.h: Apidox fixes

2005-06-08 08:11 +0000 [r423375]  osterfeld

	* akregator/src/myarticle.cpp: Don't use comments count for hash
	  calculation. So articles with comments count aren't updated
	  anymore when the comments count changed (especially Slashdot).
	  BUG: 106345

2005-06-08 11:50 +0000 [r423438]  kainhofe

	* korganizer/koeditorattachments.cpp: Backport from HEAD: requester
	  dialogs without a parent can be a pain, in particular when the
	  main window hides them. After the editor dlg is closed, any
	  requester dlg that is still open will crash korganizer...

2005-06-08 11:59 +0000 [r423443]  tilladam

	* korganizer/freebusymanager.cpp, korganizer/koeditorfreebusy.cpp:
	  fb retrieval reset fix backport.

2005-06-08 21:25 +0000 [r423559-423558]  djarvie

	* kalarm/daemon.cpp: Wait for longer to register with daemon before
	  displaying error message

	* kalarm/prefdlg.cpp: Shift 'Cancel if late' option so as to apply
	  to all types of alarm

2005-06-09 09:36 +0000 [r423663]  dfaure

	* certmanager/lib/ui/keyselectiondialog.cpp: Backport the bugfix
	  part of 423041 by Thomas Zander: Work around bug in Qt where
	  disabling and reenabling the widget that has focus does not
	  properly make it refocus. This solves the problem that you had to
	  click in the search line (even while the cursor wash flashing
	  there) to make it editable.

2005-06-09 12:17 +0000 [r423701]  danimo

	* kontact/src/mainwindow.cpp: backport fix: remove potentially
	  dangerous processEvents()

2005-06-09 12:49 +0000 [r423719]  kainhofe

	* libkcal/calendar.cpp, korganizer/calendarview.cpp: Backport from
	  HEAD: SVN commit 423710 by kainhofe: Don't add the incidence
	  twice when dissociating it from the recurrence. This will lead to
	  duplicates, and is ugly anyway.

2005-06-10 18:23 +0000 [r424081]  djarvie

	* kalarm/widgets/spinbox2.cpp, kalarm/widgets/spinbox2.h: iFix
	  Plastik style 'enabled' indication for time spinbox left-hand
	  buttons

2005-06-10 19:06 +0000 [r424088]  djarvie

	* kalarm/messagewin.cpp: Prevent message windows always being full
	  screen after a big message is shown

2005-06-11 16:41 +0000 [r424364]  tilladam

	* kmail/kmheaders.cpp: Backport of: SVN commit 424363 by tilladam:
	  Don't allow operations to be performed on items which are already
	  being operated on. Namely holding down the Del key was triggering
	  multiple Deletes/Moves for the same message pointer, which for
	  any but the first command was dangeling. Quite obvious, in
	  retrospect...

2005-06-11 17:23 +0000 [r424377]  tilladam

	* kmail/kmmessage.cpp: Backport of: SVN commit 423824 by tilladam:
	  Make sure that the cc is properly taken into account during
	  identity detection on replies.

2005-06-11 17:31 +0000 [r424379-424378]  tilladam

	* kresources/kolab/kcal/resourcekolab.cpp: Backport task
	  duplication fixes from trunk.

	* kmail/kmfoldercachedimap.cpp: Backport of: SVN commit 424289 by
	  tilladam: Don't use KIO::del to delete the local cache when
	  removing a dimap folder. The underlying KMFolderMailDir does
	  unlinks, to avoid a storm of progress dialogs, so let's rely on
	  that.

2005-06-11 20:02 +0000 [r424405]  tilladam

	* kmail/kmfoldercachedimap.cpp, kmail/kmfoldercachedimap.h:
	  Reimplement not only the setStatus taking a list of ids, but also
	  the one taking a single id, such that local status changes are
	  not lost on sync.

2005-06-12 09:08 +0000 [r424532]  djarvie

	* kalarm/widgets/label.h: Backport doxygen tweaks

2005-06-12 23:17 +0000 [r424741-424739]  djarvie

	* kalarm/messagewin.h, kalarm/messagewin.cpp: Prevent message
	  windows initially overspilling the desktop work area. Fix sizing
	  of message windows and their contents.

	* kalarm/Changelog: Update for recent changes

	* kalarm/kalarm.h: Update version number

2005-06-15 18:33 +0000 [r425850]  osterfeld

	* akregator/src/akregator_part.cpp: backport: make "mark all as
	  read" icon consistent with kmail and knode CCBUG: 107370

2005-06-16 17:46 +0000 [r426206]  dfaure

	* libkdepim/ldapclient.cpp: Fixed bug in the LDAP parsing code,
	  leading to corrupted entries, e.g. with 'name' and 'email' being
	  set to the name (e.g. in the kaddressbook LDAP search dialog).
	  Yeah this is the 4th copy of the file to be updated...

2005-06-17 11:04 +0000 [r426446]  tilladam

	* kmail/kmfoldercachedimap.cpp: Backport of sync safety from
	  proko2.

2005-06-18 12:25 +0000 [r426744]  osterfeld

	* akregator/src/feeddetector.cpp: ignore resources with type
	  attribute patch by Eckhart Woerner

2005-06-18 15:01 +0000 [r426781]  vkrause

	* knode/kncomposer.cpp: Backport from trunk for KDE 3.4.2: Fix
	  regression of my fix for #104788: Don't change cursor position in
	  subject field. BUG: 107032

2005-06-19 21:27 +0000 [r427176]  djarvie

	* kalarm/kalarmapp.cpp: Bug 101877: I hope this fixes hidden main
	  windows being shown when the session is restored

2005-06-22 23:13 +0000 [r428067]  kloecker

	* kioslaves/imap4/imapparser.cc: Backport: Fix another problem with
	  case-conversion in tr_TR (ISO) locale.

2005-06-24 16:00 +0000 [r428565]  lanius

	* akregator/src/tabwidget.cpp: backport bug #102956 fix copy link
	  address to use QClipboard::Selection as well

2005-06-25 16:04 +0000 [r428859]  osterfeld

	* akregator/src/articlelist.h, akregator/src/articlelist.cpp:
	  backport: improve article list speed significantly

2005-06-27 01:10 +0000 [r429252]  lanius

	* akregator/src/articleviewer.cpp: fix scrollbar positions backport
	  #108187

2005-06-27 15:18 +0000 [r429401]  binner

	* kdepim.lsm: 3.4.2 preparations

2005-06-28 13:49 +0000 [r429659]  lanius

	* akregator/src/tabwidget.cpp: fix --reverse BUG: 104398

2005-06-28 13:57 +0000 [r429660]  lanius

	* akregator/src/librss/document.cpp,
	  akregator/src/librss/tools_p.cpp,
	  akregator/src/librss/article.cpp, akregator/src/librss/tools_p.h,
	  akregator/src/feed.cpp: fix html tags in the title, bug #97991

2005-06-28 16:47 +0000 [r429722]  lanius

	* akregator/src/akregator_run.cpp: only use external browser if
	  selected in akregator

2005-06-28 19:09 +0000 [r429751]  djarvie

	* libkcal/event.cpp: Fix dtEndStr() etc. when the event has no end
	  date

2005-06-28 22:56 +0000 [r429810]  lanius

	* akregator/src/librss/tools_p.cpp: forgot

2005-06-28 23:01 +0000 [r429811]  lanius

	* akregator/src/librss/tools_p.cpp, akregator/src/librss/tools_p.h:
	  support for mode element in atom feeds

2005-07-01 08:41 +0000 [r430431]  dfaure

	* kmail/objecttreeparser.cpp: Fixed
	  https://intevation.de/roundup/aegypten/issue366 : don't start a
	  drag after clicking on a mail, when rendering it pops up a
	  dialog, in case of pinentry for S/MIME (the openpgp case already
	  had that fix)

2005-07-03 09:12 +0000 [r431106]  mdouhan

	* libemailfunctions/email.cpp: Backport of 418741 Implements full
	  atext support according to rfc2822 BUG:108476

2005-07-04 11:35 +0000 [r431453]  dfaure

	* kaddressbook/kabcore.cpp: Backport r426173: fix signal/slot
	  connection error

2005-07-04 18:06 +0000 [r431560]  tilladam

	* kmail/folderstorage.cpp, kmail/kmfolder.cpp,
	  kmail/kmfoldertree.cpp, kmail/folderstorage.h, kmail/kmfolder.h:
	  Backport of: SVN commit 431511 by tilladam: Tell folders about a
	  change in contents type in the folderstorage and propagate that
	  as an icon change to the foldertree, which makes sure that it has
	  the current type, before rendering the icon. All this to make the
	  right icon appear on newly downloaded folders.

2005-07-04 20:16 +0000 [r431598]  tilladam

	* kmail/kmreaderwin.cpp: Backport of: SVN commit 430147 by zander:
	  Only start the timer to set message read if the message is
	  currently not yet marked as read. Avoids problems when the user
	  marks a message as 'new' within the timeout and kmail resetting
	  it moments later again :)

2005-07-05 06:53 +0000 [r431755]  tilladam

	* kpilot/kpilot/Makefile.am, kpilot/lib/Makefile.am: Re-add $Id
	  tags which were in the release tarball, because otherwise diffing
	  the release against 3.4 branch and then applying the resulting
	  patch to the release tarball fails.

2005-07-05 10:37 +0000 [r431823]  hasso

	* kmail/kmreaderwin.cpp: Backport of i18n("Open") usage audit. No
	  new strings (i18n("to open", "Open") is already used in
	  kmail/kmcomposewin.cpp).

2005-07-05 14:32 +0000 [r431905]  hasso

	* kaddressbook/ldapsearchdialog.cpp: Backport of i18n("Search")
	  usage audit. No new strings, "&Search" is already in the
	  kdelibs.po. CCMAIL: kde-et@linux.ee

2005-07-05 20:21 +0000 [r432007-432006]  djarvie

	* kalarm/kalarmapp.cpp, kalarm/widgets/shellprocess.cpp,
	  kalarm/widgets/shellprocess.h: Insert shell identifier (e.g.
	  #!/bin/bash) at start of temporary script files when executing
	  command alarms.

	* kalarm/alarmevent.h, kalarm/alarmevent.cpp: Remove unused method

2005-07-06 00:46 +0000 [r432057]  djarvie

	* kalarm/Changelog: Update with latest change

2005-07-06 20:28 +0000 [r432267-432266]  djarvie

	* kalarm/main.cpp: Change --volume command line option abbreviation
	  to -V to avoid conflict with --version

	* kalarm/Changelog: Update with latest change

2005-07-06 23:52 +0000 [r432327]  djarvie

	* kalarm/widgets/buttongroup.h, kalarm/widgets/radiobutton.h,
	  kalarm/widgets/datetime.h, kalarm/widgets/messagebox.h,
	  kalarm/widgets/colourlist.h, kalarm/widgets/timespinbox.h,
	  kalarm/widgets/timeperiod.h, kalarm/widgets/spinbox2.h,
	  kalarm/widgets/timeedit.h, kalarm/widgets/shellprocess.h,
	  kalarm/widgets/combobox.h, kalarm/widgets/pushbutton.h,
	  kalarm/widgets/label.h, kalarm/widgets/slider.h,
	  kalarm/widgets/spinbox.h, kalarm/widgets/dateedit.h,
	  kalarm/widgets/colourcombo.h, kalarm/widgets/checkbox.h: Fix use
	  of @short in APIDOX comments

2005-07-07 02:47 +0000 [r432349]  winterz

	* konsolekalendar/konsolekalendaradd.cpp: Restore the import
	  functionality. Why was it removed in the first place??

2005-07-07 10:22 +0000 [r432427]  dfaure

	* karm/karmstorage.h, libkcal/resourcecached.cpp,
	  libkcal/resourcecached.h, karm/karmstorage.cpp: Backport of
	  432424: Changed karm to use a KCal::ResourceCalendar created in
	  memory, instead of one registered with CalendarResources (which
	  would then appear in korganizer, and there would be a KArm
	  resource created each time one uses karm, so one ends up with 20
	  of them).

2005-07-07 13:05 +0000 [r432464]  dfaure

	* kmail/kmfoldercachedimap.cpp, kmail/cachedimapjob.cpp: Backport
	  of 432275: Mark all mail as read in resource folders, nobody is
	  reading xml mails by hand.
	  https://intevation.de/roundup/kolab/issue837

2005-07-07 22:26 +0000 [r432596]  lanius

	* akregator/src/akregator_run.cpp: fix open external applications
	  for mimetypes BACKPORT: 107257

2005-07-08 00:44 +0000 [r432626]  lanius

	* akregator/src/viewer.h, akregator/src/pageviewer.cpp,
	  akregator/src/viewer.cpp, akregator/src/pageviewer.h: fix opening
	  of links BACKPORT: 107256

2005-07-08 06:40 +0000 [r432674]  tilladam

	* kresources/kolab/kcal/resourcekolab.h,
	  kresources/kolab/kcal/resourcekolab.cpp: Backport of data-loss
	  regression from trunk. 432481

2005-07-08 07:00 +0000 [r432677]  tilladam

	* kmail/kmsender.cpp: And once more for the kipper: Don't allow
	  "keep replies in this folder" to move mails into read-only
	  folders.

2005-07-08 07:20 +0000 [r432681]  tilladam

	* kmail/kmfolderdia.cpp: Backport from trunk of: Disable the "keep
	  replies in this folder" option on read-only folders.

2005-07-08 07:42 +0000 [r432689-432687]  tilladam

	* korganizer/actionmanager.cpp: Backport from trunk of: r432686 |
	  tilladam | 2005-07-08 09:40:35 +0200 (Fri, 08 Jul 2005) | 2 lines
	  Don't allow cut and delete on read-only items.

	* korganizer/koagenda.cpp: Backport from trunk of: r432685 |
	  tilladam | 2005-07-08 09:40:17 +0200 (Fri, 08 Jul 2005) | 2 lines
	  Allow selection of (but not actions on) read only items.

2005-07-08 07:59 +0000 [r432696]  tilladam

	* kresources/kolab/kcal/resourcekolab.cpp: Backport from trunk of:
	  r432693 | tilladam | 2005-07-08 09:57:49 +0200 (Fri, 08 Jul 2005)
	  | 2 lines Don't allow deletion or updates of read-only
	  incidences.

2005-07-08 09:11 +0000 [r432711]  tilladam

	* kresources/kolab/kabc/resourcekolab.cpp,
	  kresources/kolab/kabc/resourcekolab.h: Backport from trunk of:
	  Don't update or delete addressees in read-only folders.

2005-07-09 12:57 +0000 [r433018]  vkrause

	* knode/resource.h: Increment version number.

2005-07-09 20:05 +0000 [r433146]  tilladam

	* kmail/kmsender.cpp: Backport of: Improve the sent mail folder
	  logic and the readOnly protection to first try the folder
	  specified in the mail, then if there was none, or it's not
	  usable, try the identity one, and if that also fails, the system
	  one.

2005-07-11 10:17 +0000 [r433655]  kainhofe

	* korganizer/kowhatsnextview.cpp: Backport of SVN commit 433308 by
	  howells: Fix a (potentially considerable) memory leak, ipath got
	  created with new each updateView() got called but was never
	  delete'd

2005-07-11 14:47 +0000 [r433721]  dfaure

	* mimelib/mimelib/msgcmp.h: Fix crash due to storing the char*
	  address of a QString::latin1() call in
	  KMMessage::createDWBodyPart CCMAIL: Carsten Burghardt
	  <burghardt@kde.org>

2005-07-11 19:37 +0000 [r433763]  rytilahti

	* akregator/src/addfeeddialog.cpp, akregator/src/addfeeddialog.h:
	  backport my last commit

2005-07-11 19:47 +0000 [r433766]  rytilahti

	* akregator/src/addfeeddialog.cpp: .. and backport

2005-07-12 16:48 +0000 [r434025]  tilladam

	* kmail/kmailicalifaceimpl.cpp: Backport from trunk of: SVN commit
	  434018 by tilladam: Improve the logic for mixed type
	  (xml/icalvcard) folders to: if the foldertype in the config
	  differs from what the message in KMail contains, convert on write
	  to the set format. This is necessary as the resource has no way
	  of knowing what the old format of the incidence was, since only
	  KMail has access to the actual mail in the folder. That means
	  that the resource has to assume the folder's format is the
	  incidences, when writing, while the kmail end can recitify things
	  in the mail when it sees that mail contents and storage format do
	  not align. In effect this will result in incidences of one type
	  in folders configured to use the other one being converted if
	  they are edited. As long as they are not edited, nothing changes,
	  reading is transparent. This also fixes the problem of ical
	  incidences in xml folders suddenly losing their payload on change
	  and being replaced with a crippled xml-style mail.

2005-07-13 12:57 +0000 [r434255-434252]  winterz

	* libkholidays/holidays/Makefile.am: added holidays_lt

	* libkholidays/holidays/holiday_lt (added): Lithuanian holiday file
	  from Donatas.

2005-07-13 15:33 +0000 [r434280]  tokoe

	* kaddressbook/kabtools.cpp: Backport BugFix: use correct iterator

2005-07-13 16:05 +0000 [r434291]  tokoe

	* kaddressbook/viewmanager.cpp: Backport of BugFix #108684

2005-07-13 20:29 +0000 [r434362]  osterfeld

	* akregator/src/viewer.h, akregator/src/pageviewer.cpp,
	  akregator/src/viewer.cpp, akregator/src/pageviewer.h: fix forms -
	  again

2005-07-15 12:51 +0000 [r434798]  kainhofe

	* libkholidays/holidays/Makefile.am: Backport from head of SVN
	  commit 434797 by kainhofe: Uruguyan Holiday file, submitted long
	  ago by Pablo R Hoffman <prh@montevideo.com.uy>

2005-07-15 12:58 +0000 [r434801]  kainhofe

	* libkholidays/holidays/holiday_uy (added): Backport from head of
	  SVN commit 434797 by kainhofe: Uruguyan Holiday file, submitted
	  long ago by Pablo R Hoffman <prh@montevideo.com.uy>

2005-07-15 20:58 +0000 [r434953]  tilladam

	* kmail/configuredialog.cpp: Backport: Emit changed when the
	  storage format setting only is changed.

2005-07-16 21:35 +0000 [r435417]  tilladam

	* kmail/kmailicalifaceimpl.cpp: Backport of: Remove storage format
	  and pending changes information of folder that are deleted, or
	  removed as part of a sync.

2005-07-17 16:32 +0000 [r435609]  djarvie

	* kalarm/Changelog, kalarm/alarmcalendar.cpp, kalarm/kalarmapp.cpp:
	  Use 'KAlarm' untranslated in calendar file product ID, to cater
	  for locale changes

2005-07-18 11:52 +0000 [r435845]  tilladam

	* mimelib/msgcmp.cpp: Backport from trunk of: Make sure mId is
	  properly initialized, now that it is a DwString. I was getting
	  crashes when downloading new parts which don't have an id yet and
	  that id was being compared to others.

2005-07-18 17:25 +0000 [r435933]  mkelder

	* korn/accountmanager.cpp: It now doesn't run the command if KOrn
	  is restarted after resetting the counter to zero. (Bug #108041)

2005-07-18 22:56 +0000 [r436061]  dfaure

	* korganizer/filteredit_base.ui, korganizer/filtereditdialog.h,
	  korganizer/filtereditdialog.cpp: Backport new filter dialog from
	  trunk. This adds a few strings but it fixes many bugs since the
	  previous dialog was completely broken and unusuable. Sorry for
	  the new strings in the branch. CCMAIL: kde-i18n-doc@kde.org

2005-07-19 09:38 +0000 [r436202]  osterfeld

	* akregator/src/aboutdata.h, akregator/ChangeLog: bump version and
	  add bug fixes to changelog

2005-07-19 18:50 +0000 [r436428]  mkelder

	* korn/kconf_update/korn-3-4-config_change.cpp: Fix of bug 107064.
	  'host=' is replaced with 'server=', because the 'server'-entry is
	  read in the program.

2005-07-19 21:39 +0000 [r436516]  fzenith

	* libkholidays/holidays/holiday_it: Updating after discussion in
	  the kde-i18n-it mailing list. CCMAIL: kde-i18n-it@kde.org (Se
	  qualcuno avesse idee, lo dica subito che facciamo ancora in tempo
	  a cambiarlo!)

2005-07-20 06:24 +0000 [r436633]  osterfeld

	* akregator/src/akregator_view.cpp: fix quick search, reset state
	  on node correctly and update filters in view objects

2005-07-20 10:36 +0000 [r436837]  binner

	* kontact/src/main.cpp, kmail/kmversion.h,
	  kaddressbook/kabcore.cpp, korganizer/version.h: Increased version
	  number for KDE 3.4.2

