2008-03-05 18:31 +0000 [r782678]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/kdehandlers.cpp,
	  branches/KDE/3.5/kdebindings/korundum/ChangeLog: * Added a
	  marshaller for QValueList<WId>& as requested by volty on #qtruby
	  CCMAIL: kde-bindings@kde.org

2008-06-29 22:50 +0000 [r826176]  anagl

	* branches/KDE/3.5/kdebindings/xparts/mozilla/kmozilla.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/docs/examples/buttonmaker/buttonmaker.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/plugins/qprocess_plugin.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/plugins/customobject_plugin.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/docs/examples/envelopemaker/envelopemaker.desktop,
	  branches/KDE/3.5/kdebindings/kdejava/koala/test/kbase/kbase.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/kscript/swaptabs.desktop,
	  branches/KDE/3.5/kdebindings/xparts/xpart_notepad/xp_notepad.desktop,
	  branches/KDE/3.5/kdebindings/dcoppython/test/dcopserver/kdedcoptest.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/plugins/kfileitem_plugin.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/kjscmd.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/binding_type.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/plugins/imagefx_plugin.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/plugins/customqobject_plugin.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/docs/embedding/simple-embed/embedjs.desktop,
	  branches/KDE/3.5/kdebindings/kjsembed/kscript/javascript.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-08-19 19:47 +0000 [r849596]  coolo

	* branches/KDE/3.5/kdebindings/kdebindings.lsm: update for 3.5.10

