------------------------------------------------------------------------
r1108319 | rkcosta | 2010-03-29 05:00:07 +1300 (Mon, 29 Mar 2010) | 7 lines

Backport r1108236 by pino.

use the xz and lzma formats only if compiled in libarchive

CCMAIL: pino@kde.org
CCMAIL: lueck@hube-lueck.de

------------------------------------------------------------------------
r1109289 | kossebau | 2010-03-31 12:15:32 +1300 (Wed, 31 Mar 2010) | 1 line

changed: update minor version for next release
------------------------------------------------------------------------
r1109290 | kossebau | 2010-03-31 12:16:02 +1300 (Wed, 31 Mar 2010) | 1 line

changed: update minor version for next release
------------------------------------------------------------------------
r1109567 | kossebau | 2010-04-01 02:30:08 +1300 (Thu, 01 Apr 2010) | 3 lines

fixed: crashed after a reload while editing a value, because the current group was not reset to 0
BUG: 232775

------------------------------------------------------------------------
r1109769 | scripty | 2010-04-01 15:09:56 +1300 (Thu, 01 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110427 | scripty | 2010-04-03 14:51:13 +1300 (Sat, 03 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110497 | dakon | 2010-04-03 21:41:40 +1300 (Sat, 03 Apr 2010) | 4 lines

Fix mem leak

Backport of r1108542 by mlaurent

------------------------------------------------------------------------
r1110782 | scripty | 2010-04-04 13:46:34 +1200 (Sun, 04 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110994 | dakon | 2010-04-05 03:45:13 +1200 (Mon, 05 Apr 2010) | 6 lines

fix crash when saving editor file was cancelled

BUG:233259

backport of r1110991

------------------------------------------------------------------------
r1111925 | rkcosta | 2010-04-07 14:43:31 +1200 (Wed, 07 Apr 2010) | 7 lines

Backport recent commits to FindLibArchive.cmake in trunk.

 * Use a single style in the whole file.
 * Document the HAVE_LIBARCHIVE_*_SUPPORT variables.
 * Use mark_as_advanced for our variables.
 * Check for lzma and xz support besides gzip.

------------------------------------------------------------------------
r1111934 | rkcosta | 2010-04-07 14:47:37 +1200 (Wed, 07 Apr 2010) | 4 lines

Backport r1111927.

Const'ify a little and make the code easier to read.

------------------------------------------------------------------------
r1111940 | rkcosta | 2010-04-07 15:08:06 +1200 (Wed, 07 Apr 2010) | 4 lines

Backport some commits from trunk related to libarchive support for lzma and xz files.

Revisions: 1108537, 1108540, 1111929, 111930, 1111931, 1111932, 1111936.

------------------------------------------------------------------------
r1112384 | rkcosta | 2010-04-08 12:34:34 +1200 (Thu, 08 Apr 2010) | 8 lines

Backport r1112383.

Apply patch by Eike Lange which shows a wait cursor while calculating a factorial,
as that might take some time.

CCBUG: 139397 
CCMAIL: eike.lange@uni-due.de

------------------------------------------------------------------------
r1112397 | scripty | 2010-04-08 13:40:02 +1200 (Thu, 08 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1113203 | scripty | 2010-04-10 13:50:36 +1200 (Sat, 10 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1114077 | rkcosta | 2010-04-13 05:52:53 +1200 (Tue, 13 Apr 2010) | 8 lines

Backport r1114075.

Apply patch from Eike Krumbacher: make Ctrl+Q (actually, the shortcut configured to the Quit action) actually quit KTimer.

Thanks, Eike!

CCBUG: 224859

------------------------------------------------------------------------
r1115345 | scripty | 2010-04-16 13:51:14 +1200 (Fri, 16 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1115539 | rkcosta | 2010-04-17 07:26:53 +1200 (Sat, 17 Apr 2010) | 6 lines

Backport r1115538.

Put folders before files when sorting.

CCBUG: 234373

------------------------------------------------------------------------
r1116547 | mzanetti | 2010-04-20 06:57:18 +1200 (Tue, 20 Apr 2010) | 4 lines

don't take arguments out of the model when reading them

BUG: 234682

------------------------------------------------------------------------
r1116665 | scripty | 2010-04-20 14:08:02 +1200 (Tue, 20 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117333 | scripty | 2010-04-22 09:38:11 +1200 (Thu, 22 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117752 | rkcosta | 2010-04-23 09:59:11 +1200 (Fri, 23 Apr 2010) | 4 lines

Backport r1117751.

Follow Krazy's advice and use <> when including config.h.

------------------------------------------------------------------------
r1118221 | scripty | 2010-04-24 14:03:38 +1200 (Sat, 24 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1119908 | rkcosta | 2010-04-28 11:01:23 +1200 (Wed, 28 Apr 2010) | 13 lines

Backport r1119906.

When opening an HTML file with KHTMLPart, disable Java, JavaScript,
plugins and external references.

This is a saner and safer default, since the archive might come from an
unknown and untrusted sender. If access to the disabled stuff is needed,
the user should simply extract the file and view it in a browser.

See also bug 235468.

CCBUG: 235546

------------------------------------------------------------------------
r1120396 | scripty | 2010-04-29 13:59:49 +1200 (Thu, 29 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
