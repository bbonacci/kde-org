2006-01-21 00:33 +0000 [r500710]  harris

	* branches/KDE/3.5/kdeedu/doc/kstars/scriptbuilder.docbook: Fix bug
	  #120259; thanks for the report Will commit to trunk as well. BUG:
	  120259

2006-01-21 01:22 +0000 [r500721]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcdaylengthdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcplanets.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcvlsr.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcapcoord.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcplanetsdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcazel.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcjd.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/kstarsdata.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcjddlg.ui:
	  Fixing bug 119960; partial fix of 119963 11960: All time fields
	  in the astrocalculator now show the current UT, not local time
	  119963: modified the layout of the JD module, moved Convert/Clear
	  to the bottom Increased minimum width of date fields TODO: move
	  Now button (still considering it) update all 3 fields when Now is
	  pressed Fix manual input in Date line edit Thanks for the report.
	  Will port to trunk as well. BUG: 119960 CCBUG: 119963

2006-01-21 16:02 +0000 [r500929-500928]  harris

	* branches/KDE/3.5/kdeedu/libkdeedu/extdate/extdatetimeedit.cpp:
	  Partial fix of bug #119963 It is now possible to manually enter a
	  year in a date edit box CCBUG: 119963

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcjd.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcjddlg.ui:
	  Final fix for bug #119963 Moved the Now button The Now button now
	  fills in all three fields (JD, MJD and date) BUG: 119963

2006-01-21 23:48 +0000 [r501065]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/element.h: 12. Strange
	  number. I wonder where it came from... BUG: 120567 (no need to
	  forward port to trunk, as there there is another way of storing
	  element datas)

2006-01-22 01:09 +0000 [r501086]  harris

	* branches/KDE/3.5/kdeedu/doc/kstars/scriptbuilder.docbook: Fixing
	  the incomplete fix. CCBUG: 120259

2006-01-23 03:20 +0000 [r501479]  harris

	* branches/KDE/3.5/kdeedu/libkdeedu/extdate/extdatepicker.cpp,
	  branches/KDE/3.5/kdeedu/libkdeedu/extdate/extdatepicker.h: Make
	  the line edit in ExtDatePicker read-only; it is only meant to
	  display the date selected with the calendar widget, it is not for
	  user entry. BUG: 113465

2006-01-23 03:40 +0000 [r501480]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/skymapdraw.cpp: Fixing bug
	  #114605. All deep-sky objects were shown with incorrect position
	  angle (flipped horizontally). Thanks for the report. BUG: 114605

2006-01-23 03:51 +0000 [r501481]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/data/Cities.dat: Fixing
	  bugs in Cities.dat (1 misspelling and two encoding problems).
	  Thanks for the reports. BUG: 114607 BUG: 114608 BUG: 114609

2006-01-23 04:01 +0000 [r501482]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/data/Cities.dat: Fixing
	  another typo in Cities.dat Thanks for the report. BUG: 115108

2006-01-26 23:19 +0000 [r502688]  annma

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcjddlg.ui:
	  build for qt 3.3.5 by fixing <includehint>

2006-01-28 00:38 +0000 [r503058]  deller

	* branches/KDE/3.5/kdeedu/kstars/kstars/kstars.kcfg: it's a
	  directory (Path)

2006-01-28 19:04 +0000 [r503310]  iastrubni

	* branches/KDE/3.5/kdeedu/kbruch/src/taskview.cpp,
	  branches/KDE/3.5/kdeedu/kbruch/src/exercisecompare.cpp,
	  branches/KDE/3.5/kdeedu/kbruch/src/exerciseconvert.cpp: BUG:
	  116831 This fixes a few layout problems in RTL desktops. The code
	  is fully documented whith the changes I made, but be aware - they
	  are ugly. But, hell... this works :) Todo for Qt4: If someone
	  wants up port the fixes to Qt4, please don't. Qt4 has better ways
	  of doying this, (see the direction property of each widget).

2006-02-01 23:32 +0000 [r504722]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/data/Cities.dat: Somehow
	  the spelling fix only got applied to trunk...fixing in 3.5 now.
	  CCBUG: 114607

2006-02-02 14:58 +0000 [r504921]  annma

	* branches/KDE/3.5/kdeedu/khangman/khangman/khangman.cpp,
	  branches/KDE/3.5/kdeedu/khangman/khangman/khangmanui.rc: fix part
	  of 121179 untranslated string CCBUG=121179

2006-02-03 19:20 +0000 [r505377]  jriddell

	* branches/KDE/3.5/kdeedu/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdeedu
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdeedu

2006-02-08 13:20 +0000 [r507060]  annma

	* branches/KDE/3.5/kdeedu/khangman/khangman/khangman.desktop: bug
	  121577 however the Free Desktop standards don't REQUIRE this
	  field to be there and says:"should not be redundant with Name or
	  GenericName" difficult not to be redundant... maybe it's GNOME
	  that needs to be fixed so it uses Name if no Comment BUG=121577

2006-02-08 16:45 +0000 [r507147]  mueller

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/common-dialogs/pasteoptions.cpp:
	  fix

2006-02-13 17:42 +0000 [r509087-509086]  annma

	* branches/KDE/3.5/kdeedu/doc/klettres/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/klettres/klettres6.png: CCBUG=121616

	* branches/KDE/3.5/kdeedu/klettres/klettres/klettresview.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/klnewstuff.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/version.h,
	  branches/KDE/3.5/kdeedu/klettres/klettres/klettres.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/klettresview.h,
	  branches/KDE/3.5/kdeedu/klettres/klettres/klnewstuff.h,
	  branches/KDE/3.5/kdeedu/klettres/klettres/soundfactory.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/timer.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/klettres.h,
	  branches/KDE/3.5/kdeedu/klettres/klettres/klettres.desktop,
	  branches/KDE/3.5/kdeedu/klettres/klettres/main.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/soundfactory.h,
	  branches/KDE/3.5/kdeedu/klettres/klettres/timer.h: fix 121616 and
	  upate copyrights to 2006 BUG=121616

2006-02-13 17:47 +0000 [r509091]  annma

	* branches/KDE/3.5/kdeedu/kalzium/src/kalzium.desktop: BUG=121232

2006-02-13 17:51 +0000 [r509095]  annma

	* branches/KDE/3.5/kdeedu/kanagram/src/kanagram.desktop: BUG=121575

2006-02-13 17:51 +0000 [r509096]  pino

	* branches/KDE/3.5/kdeedu/kig/filters/exporter.cc: Before
	  attempting opening a file for writing, check that the current
	  file name represents a supported image type.

2006-02-13 17:53 +0000 [r509097]  annma

	* branches/KDE/3.5/kdeedu/keduca/resources/keduca.desktop:
	  BUG=121576

2006-02-13 17:53 +0000 [r509100-509098]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/kalzium.desktop: Place it
	  better.

	* branches/KDE/3.5/kdeedu/kalzium/src/element.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/settings_units.ui: Spell the
	  unit correctly. CCBUG: 120217

2006-02-13 17:57 +0000 [r509101]  annma

	* branches/KDE/3.5/kdeedu/doc/khangman/index.docbook: backport bug
	  121179 CCBUG=121179

2006-02-13 18:01 +0000 [r509104]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/settings_units.ui: Fix the
	  unit name here too. CCBUG: 120217

2006-02-13 18:06 +0000 [r509107]  annma

	* branches/KDE/3.5/kdeedu/doc/khangman/index.docbook: status bar ->
	  statusbar to be consistent with Settings menu entry

2006-02-13 18:24 +0000 [r509114]  pino

	* branches/KDE/3.5/kdeedu/kig/kig/kig.desktop: Add a Comment.
	  CCBUG: 121578

2006-02-13 18:25 +0000 [r509115]  annma

	* branches/KDE/3.5/kdeedu/doc/klettres/index.docbook: backport
	  121385 BUG=121385

2006-02-14 13:43 +0000 [r509359]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/detailinfodlg.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/orbitswidget.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/detailinfodlg.h,
	  branches/KDE/3.5/kdeedu/kalzium/src/spectrumwidget.h: Ok, here we
	  hare. I'm backporting almost all the improved Detail Info Dialog
	  from kalzium trunk (ie kde4) to the 3.5 branch, as last resource
	  to have a Detail Info Dialog really working without any bug
	  and/or crash. So, please please, please, reporters and readers of
	  bug 109035, update your kalzium from kdeedu 3.5.x and say whether
	  it works well without the reported crash. In case, we'll close
	  this bug. CCBUG: 109035

2006-02-14 14:50 +0000 [r509378]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/detailinfodlg.cpp: fix a
	  crash introduced by me in the last commit (sorry!)

2006-02-15 18:46 +0000 [r509869]  annma

	* branches/KDE/3.5/kdeedu/doc/kvoctrain/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kturtle/programming-reference.docbook,
	  branches/KDE/3.5/kdeedu/doc/kstars/indi.docbook: remove empty
	  <para> tags

2006-02-17 02:26 +0000 [r510363]  hedlund

	* branches/KDE/3.5/kdeedu/kwordquiz/src/qaview.cpp: Don't crash
	  when switching back to editor. Distros affected by this bug:
	  PCLinuxOS and Ubuntu. Distros not affected include Slackware and
	  Gentoo. BUG:122110

2006-02-17 03:19 +0000 [r510376]  hedlund

	* branches/KDE/3.5/kdeedu/kwordquiz/src/qaview.cpp: Display
	  incorrect answers correctly (sic!). BUG:122111

2006-02-18 15:20 +0000 [r511000]  iastrubni

	* branches/KDE/3.5/kdeedu/kbruch/src/exercisefactorize.cpp,
	  branches/KDE/3.5/kdeedu/kbruch/src/exercisefactorize.h: BUG:
	  116831 CCMAIL: kde-il@yahoogroups.com This patch fixes
	  completelly bug number 116831, and now kbruch is completelly
	  hebrew friendly. Now all exercises are displayed correctly on RTL
	  and LTR desktops. Last patch was done by Amit Ramon - Amit Ramon
	  (amit.ramon _at_ kdemail.net)

2006-02-19 14:24 +0000 [r511296]  pino

	* branches/KDE/3.5/kdeedu/kig/misc/kigpainter.cpp: Draw the vector
	  arrow with a normal style. CCBUG: 122285

2006-02-20 11:55 +0000 [r511578]  pino

	* branches/KDE/3.5/kdeedu/kig/objects/circle_imp.cc,
	  branches/KDE/3.5/kdeedu/kig/TODO: Improve the selection of
	  circles.

2006-02-21 15:22 +0000 [r512014]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/astrocalc.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalceclipticcoordsdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcplanetsdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcvlsrdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcapcoorddlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcangdistdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcprecdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcgalcoorddlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcazeldlg.ui:
	  Fixing bug #119959 (typos and string improvements in calculator)
	  BUG: 119959

2006-02-22 21:30 +0000 [r512558]  pvicente

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcgeoddlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcsidtimedlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcdaylengthdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalceclipticcoordsdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcplanetsdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcvlsrdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcapcoorddlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcequinoxdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcangdistdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcgalcoorddlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcazeldlg.ui:
	  Bug correction. It is not in bugs.kde.org, but it has been
	  mentioned in kstars development list. Widegt tab order was
	  incorrect in some calculator modules. CCMAIL:
	  kstars-devel@kde.org

2006-02-23 18:59 +0000 [r512843]  helio

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcsidtimedlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcdaylengthdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcplanetsdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcvlsrdlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcapcoorddlg.ui,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcazeldlg.ui: -
	  Fix compilation

2006-02-23 21:19 +0000 [r512879]  pino

	* branches/KDE/3.5/kdeedu/kig/modes/popup.cc,
	  branches/KDE/3.5/kdeedu/kig/modes/popup.h: Do not show the Style
	  and the Size submenus in the object popup if the selection
	  contains only labels. CCBUGS: 122381, 122382

2006-02-23 21:41 +0000 [r512896]  annma

	* branches/KDE/3.5/kdeedu/doc/klettres/index.docbook: improve
	  thanks Burkhard CCMAIL=121385

2006-02-23 21:54 +0000 [r512902]  aacid

	* branches/KDE/3.5/kdeedu/blinken/src/blinken.cpp,
	  branches/KDE/3.5/kdeedu/blinken/src/blinken.h: The "easy" fix for
	  steve.ttf not having "strange" characters BUGS: 118399

2006-02-23 22:25 +0000 [r512909]  pino

	* branches/KDE/3.5/kdeedu/kig/modes/popup.cc: Rename the "Set Size"
	  popup menu item to the more appropriate "Set Pen Width". CCBUG:
	  122445

2006-02-23 22:26 +0000 [r512912]  annma

	* branches/KDE/3.5/kdeedu/doc/khangman/index.docbook: improve
	  thanks to Burkhard CCBUG=121179

2006-02-24 12:18 +0000 [r513067]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/data/data.xml: Fix typo in
	  the origin of the name of Silver (Ag) CCMAIL:
	  353582-close@bugs.debian.org

2006-02-24 21:30 +0000 [r513308]  lueck

	* branches/KDE/3.5/kdeedu/doc/kwordquiz/kwq-dlg-configure-quiz.png,
	  branches/KDE/3.5/kdeedu/doc/kwordquiz/kwq-dlg-print-type.png,
	  branches/KDE/3.5/kdeedu/doc/kwordquiz/kwq-dlg-configure-appearance.png
	  (added), branches/KDE/3.5/kdeedu/doc/kmplot/griddlg.png
	  (removed),
	  branches/KDE/3.5/kdeedu/doc/kmplot/settings-scaling.png (added),
	  branches/KDE/3.5/kdeedu/doc/kmplot/scaledlg.png (removed),
	  branches/KDE/3.5/kdeedu/doc/klatin/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/using.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/axesopt.png (removed),
	  branches/KDE/3.5/kdeedu/doc/kmplot/axesdlg.png (removed),
	  branches/KDE/3.5/kdeedu/doc/kmplot/settings-fonts.png (added),
	  branches/KDE/3.5/kdeedu/doc/kmplot/namesdlg.png (removed),
	  branches/KDE/3.5/kdeedu/doc/kmplot/reference.docbook,
	  branches/KDE/3.5/kdeedu/doc/kwordquiz/kwq-dlg-configure-characters.png,
	  branches/KDE/3.5/kdeedu/doc/kwordquiz/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kvoctrain/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/settings-colors.png (added),
	  branches/KDE/3.5/kdeedu/doc/kmplot/functionsdlg.png (removed),
	  branches/KDE/3.5/kdeedu/doc/kmplot/lo32-app-kmplot.png (removed),
	  branches/KDE/3.5/kdeedu/doc/kwordquiz/kwq-dlg-configure-editor.png,
	  branches/KDE/3.5/kdeedu/doc/kmplot/toolbar.png (removed),
	  branches/KDE/3.5/kdeedu/doc/kmplot/stepdlg.png (removed),
	  branches/KDE/3.5/kdeedu/doc/kmplot/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/configuration.docbook,
	  branches/KDE/3.5/kdeedu/doc/klatin/klatin-configuration.png
	  (added): updated kdeedu docbooks for KDE 3.5.2
	  CCMAIL:kde-doc-english@kde.org

2006-02-25 01:16 +0000 [r513329]  annma

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/modcalcequinoxdlg.ui:
	  build with qt 3.3.5 (<includehint>)

2006-03-03 10:37 +0000 [r515281]  mueller

	* branches/KDE/3.5/kdeedu/kig/scripting/python_scripter.cc: the
	  usual "daily unbreak compilation"

2006-03-05 17:13 +0000 [r516004]  reitelbach

	* branches/KDE/3.5/kdeedu/ktouch/training/german2.ktouch.xml:
	  Remove trailing spaces in german training session. They are not
	  visible to the user and thus very confusing.

2006-03-05 21:42 +0000 [r516075]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/kalzium.cpp: Load also the
	  libkdeedu catalog. CCMAIL: 114882

2006-03-09 21:28 +0000 [r517084]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/indi/lilxml.c,
	  branches/KDE/3.5/kdeedu/kstars/kstars/indi/lilxml.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/indi/indiserver.c:
	  Backport: Renaming xmlMalloc to indi_xmlMalloc to avoid conflict
	  with libxml2. CCMAIL: kstars-devel@kde.org CCMAIL:
	  neoclust.kde@gmail.com

2006-03-14 15:54 +0000 [r518588]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/data/Cities.dat: Removing
	  Daylight Savings Time from locations in Hawaii and Puerto Rico.
	  Thanks for the patch; will port to trunk as well. CCMAIL: Patrice
	  Levesque <kstars.wayne@ptaff.ca> CCMAIL: kstars-devel@kde.org

2006-03-16 21:48 +0000 [r519337]  cniehaus

	* branches/KDE/3.5/kdeedu/kalzium/src/main.cpp: increase version
	  number for 3.5.2

2006-03-17 21:34 +0000 [r519782]  coolo

	* branches/KDE/3.5/kdeedu/kdeedu.lsm: tagging 3.5.2

