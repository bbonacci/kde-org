2006-07-30 20:34 +0000 [r567994]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxState.cc: make
	  nGfxBlendModeNames define return the correct size of the
	  gfxBlendModeNames array so it does not access invalid memory when
	  the blend mode is not found. Discovered by Krzysztof Kowalczyk

2006-08-01 18:43 +0000 [r568617]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.rc: update version number
	  so that cartman's change to not have a duplicate Tools menu when
	  embedded into konqueror are really used

2006-08-08 15:24 +0000 [r571112]  kebekus

	* branches/KDE/3.5/kdegraphics/kdvi/special.cpp: fixes bug #128358

2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-11 13:31 +0000 [r571992]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/gp_outputdev.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Catalog.cc: leaks--

2006-08-11 14:12 +0000 [r572008]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SplashOutputDev.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFontEngine.cc:
	  more leak fixes

2006-08-14 14:34 +0000 [r573005]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.h: Fix myself not
	  beign able to search for "cajón". Thanks BCoppens for the tip

2006-08-14 21:31 +0000 [r573104]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/gp_outputdev.cpp:
	  misspelling people's name is a bad idea, thanks Achim for
	  noticing

2006-08-17 11:53 +0000 [r573875]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  Fix crash introduced by leak fix better now ismail? CCMAIL:
	  ismail@pardus.org.tr

2006-08-18 17:42 +0000 [r574320]  helio

	* branches/KDE/3.5/kdegraphics/ksvg/plugin/ksvgplugin.desktop: -
	  Some servers report mime as image/svg

2006-08-21 15:05 +0000 [r575433]  jriddell

	* branches/KDE/3.5/kdegraphics/COPYING-DOCS (added): Add FDL
	  licence for documentation

2006-08-22 05:16 +0000 [r575797]  helio

	* branches/KDE/3.5/kdegraphics/kgamma/kcmkgamma/kgamma.cpp,
	  branches/KDE/3.5/kdegraphics/kgamma/kcmkgamma/kgamma.h: - Setting
	  module to use system defaults

2006-08-22 05:23 +0000 [r575799]  helio

	* branches/KDE/3.5/kdegraphics/kamera/kcontrol/kamera.h,
	  branches/KDE/3.5/kdegraphics/kamera/kcontrol/kamera.cpp: -
	  Setting module to use system defaults

2006-08-26 13:46 +0000 [r577381]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/tools/kptoolpen.cpp:
	  +TODO

2006-08-27 15:01 +0000 [r577796]  whuss

	* branches/KDE/3.5/kdegraphics/kviewshell/searchWidget.cpp: Always
	  select the text in the searchbar when it is shown. Also enable
	  searching when the searchbar is hidden, but a search phrase has
	  been entered previously. CCBUG: 131566

2006-08-28 18:36 +0000 [r578236]  aseigo

	* branches/KDE/3.5/kdegraphics/ksnapshot/ksnapshot.h: make it more
	  obvious that this can be dragged by changing the cursor on hover
	  to a hand and drawing a drag grippy on the pixmap. weeeeee... now
	  maybe people will figure it out that you can drag the damn thing
	  ;) CCMAIL:kde-usability@kde.org

2006-08-29 16:28 +0000 [r578562]  whuss

	* branches/KDE/3.5/kdegraphics/kviewshell/kviewpart.cpp: Fix crash
	  on session saving. BUG: 122961

2006-09-02 11:54 +0000 [r580022]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp: Fix
	  middle-click zoom not working after dragging to the top of the
	  screen BUG: 111255

2006-09-04 20:25 +0000 [r580938]  azehende

	* branches/KDE/3.5/kdegraphics/kpovmodeler/pmheightfieldroam.cpp:
	  Initialize variables in constructor BUG: 106340

2006-09-04 20:35 +0000 [r580945]  azehende

	* branches/KDE/3.5/kdegraphics/kpovmodeler/version.h,
	  branches/KDE/3.5/kdegraphics/kpovmodeler/pmfactory.cpp: increased
	  version as critical bug was fixed updated copyright year

2006-09-06 20:09 +0000 [r581596]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  Fix for crash comment #6 on bug 113635

2006-09-09 17:14 +0000 [r582529]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.cpp: Fix
	  page number not beign correct when using dcop and changing pages
	  through the presentation widget BUGS: 133549

2006-09-09 19:15 +0000 [r582558]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GlobalParams.cc: add
	  path of gs fonts on Arch Linux. Thank $deity that we switched to
	  fontconfig in poppler Tal, you should ask Arch to rebuild their
	  kpdf package with this patch added. BUGS: 133819

2006-09-09 19:22 +0000 [r582561]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.h,
	  branches/KDE/3.5/kdegraphics/kpdf/part.h,
	  branches/KDE/3.5/kdegraphics/kpdf/shell/shell.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/shell/shell.h,
	  branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Fix restoring the
	  correct viewport on session restore when the document is non
	  local. Seems it also fixes the small-window-on-local-files
	  behaviour BUGS: 133507

2006-09-12 09:03 +0000 [r583403]  whuss

	* branches/KDE/3.5/kdegraphics/kdvi/dviRenderer.cpp: Backport of
	  commit 583400: Don't crash when trying to open a DVI file that
	  has been only partially written. CCBUG: 133910

2006-09-19 12:50 +0000 [r586388]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: up ver to
	  1.4.5_relight for KDE 3.5.5

2006-09-19 17:30 +0000 [r586460]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: increase version

2006-09-20 13:41 +0000 [r586747]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/minibar.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/pageviewutils.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.h: Make KPDF a bit
	  more friendly for users of RtL languages: - correctly place the
	  message pane (top-left corner of page view for LtR, top-right for
	  RtL) - make the mini-progressbar fully usable for both the
	  writing directions - use the right icons for the previous/next
	  page button (those near the page counter) CCMAIL:
	  kde-il@yahoogroups.com

2006-09-23 12:13 +0000 [r587617]  kling

	* branches/KDE/3.5/kdegraphics/kgamma/kcmkgamma/kgamma.cpp: Fixed
	  memory leak + X11 connection leak (KGamma's XVidExtWrap)

2006-09-26 12:39 +0000 [r588593]  kling

	* branches/KDE/3.5/kdegraphics/ksvg/plugin/backends/libart/LibartCanvasItems.cpp:
	  Libart will hang in art_vpath_dash() if passed an array with only
	  zeroes, detect this and avoid the call. Ran into this when
	  Konqueror tried to generate a thumbnail of "konqui.svgz" which we
	  ship with KDE. BUG: 107831

2006-09-26 16:40 +0000 [r588689]  kling

	* branches/KDE/3.5/kdegraphics/ksvg/core/KSVGCanvas.cpp: Don't blit
	  canvas if height or width is 0. BUG: 134693

2006-09-28 14:12 +0000 [r589711]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/main.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/version.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.h: - don't
	  crash during shutdown (session management) when filewidget was
	  not created yet - don't show browser if the previous session
	  didn't show it BUG: 133897

2006-10-02 11:08 +0000 [r591330]  coolo

	* branches/KDE/3.5/kdegraphics/kdegraphics.lsm: updates for 3.5.5

