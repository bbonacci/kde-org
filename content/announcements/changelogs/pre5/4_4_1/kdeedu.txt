------------------------------------------------------------------------
r1086040 | khudyakov | 2010-02-06 11:50:59 +0000 (Sat, 06 Feb 2010) | 5 lines

Backport bugfix to 4.4 branch

CCBUG: 218235


------------------------------------------------------------------------
r1086823 | arieder | 2010-02-07 23:36:43 +0000 (Sun, 07 Feb 2010) | 2 lines

backport of r1086822: fix crash when trying to load a file when the current worksheet is invalid

------------------------------------------------------------------------
r1086832 | arieder | 2010-02-07 23:58:41 +0000 (Sun, 07 Feb 2010) | 3 lines

backport of arieder r1086825: introduce id() for backends, as we cannot rely on name() because it might be translated
this fixes problems with hardcoded backend names (e.g. nullbackend in the loading code)

------------------------------------------------------------------------
r1086975 | arieder | 2010-02-08 11:33:53 +0000 (Mon, 08 Feb 2010) | 2 lines

backport of SVN commit 1086885 by jkekkonen: fix build

------------------------------------------------------------------------
r1087472 | scripty | 2010-02-09 04:15:01 +0000 (Tue, 09 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1087853 | lueck | 2010-02-09 17:20:24 +0000 (Tue, 09 Feb 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1089385 | scripty | 2010-02-13 04:25:25 +0000 (Sat, 13 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1090668 | khudyakov | 2010-02-15 18:37:32 +0000 (Mon, 15 Feb 2010) | 11 lines

Backporting bug fix into 4.4 branch.

Trunk require more well thought approach. Order of initialization
of KStars and SkyMap is too interweaved. It must be simplified

Thanks to Sruthi Devi for finding and fixing bug.

CCBUG: 226159
CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r1090814 | scripty | 2010-02-16 04:34:02 +0000 (Tue, 16 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1092358 | aacid | 2010-02-18 19:32:55 +0000 (Thu, 18 Feb 2010) | 4 lines

backport r1092356 | aacid | 2010-02-18 19:30:43 +0000 (Thu, 18 Feb 2010) | 2 lines

typo--

------------------------------------------------------------------------
r1093442 | laidig | 2010-02-20 22:26:34 +0000 (Sat, 20 Feb 2010) | 1 line

don't crash when the test type gets messed up in the config
------------------------------------------------------------------------
r1093446 | laidig | 2010-02-20 22:41:57 +0000 (Sat, 20 Feb 2010) | 6 lines

delay the reset() of the VocabularyView when setting a new document

in some cases, the reset causes the model to be accessed, which still
has a pointer to the old document and therefore crashes

BUG:227815
------------------------------------------------------------------------
r1093767 | ilic | 2010-02-21 12:36:31 +0000 (Sun, 21 Feb 2010) | 1 line

i18n fixes: recovered i18n wraps lost in refactoring.
------------------------------------------------------------------------
r1095223 | aacid | 2010-02-23 20:44:39 +0000 (Tue, 23 Feb 2010) | 4 lines

backport r1094585 | lauranger | 2010-02-22 23:06:51 +0000 (Mon, 22 Feb 2010) | 1 line

Change default number of questions to the max. BUG:228108

------------------------------------------------------------------------
r1095363 | scripty | 2010-02-24 04:27:53 +0000 (Wed, 24 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1096378 | jmhoffmann | 2010-02-26 15:27:35 +0000 (Fri, 26 Feb 2010) | 3 lines

Bump Marble version to 0.9.2 for KDE 4.4.2, unfortunately we forgot to
increase the version number for KDE 4.4.1.

------------------------------------------------------------------------
