------------------------------------------------------------------------
r1085107 | scripty | 2010-02-04 11:22:30 +0000 (Thu, 04 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1086381 | scripty | 2010-02-07 05:02:04 +0000 (Sun, 07 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1087390 | aseigo | 2010-02-08 22:44:09 +0000 (Mon, 08 Feb 2010) | 3 lines

* only check for kopete running once per query session
* use QDBusReply<bool> QDBusConnectionInterface::isServiceRegistered() instead of creating a QDBusInterface object

------------------------------------------------------------------------
r1087882 | asouza | 2010-02-09 18:00:51 +0000 (Tue, 09 Feb 2010) | 7 lines

Remove Luna's background (backport to 4.4)

Make luna's applet background to be fully transparent,
giving it a better look&feel.

BUG:203413

------------------------------------------------------------------------
r1088514 | scripty | 2010-02-11 04:40:27 +0000 (Thu, 11 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1088715 | annma | 2010-02-11 12:40:47 +0000 (Thu, 11 Feb 2010) | 2 lines

typo

------------------------------------------------------------------------
r1089315 | ivan | 2010-02-12 21:36:58 +0000 (Fri, 12 Feb 2010) | 3 lines

Backported crash fix


------------------------------------------------------------------------
r1089321 | ivan | 2010-02-12 21:51:04 +0000 (Fri, 12 Feb 2010) | 3 lines

Backported group reloading bugfix


------------------------------------------------------------------------
r1089810 | astromme | 2010-02-14 04:32:20 +0000 (Sun, 14 Feb 2010) | 2 lines

Backport if/else fix to 4.4

------------------------------------------------------------------------
r1089814 | astromme | 2010-02-14 04:41:14 +0000 (Sun, 14 Feb 2010) | 2 lines

Backport 1089813 to 4.4

------------------------------------------------------------------------
r1090866 | annma | 2010-02-16 09:48:34 +0000 (Tue, 16 Feb 2010) | 3 lines

Allow the Apply button to be enabled and highlight selected wallpaper correctly
BUG=227140

------------------------------------------------------------------------
r1090870 | annma | 2010-02-16 10:18:11 +0000 (Tue, 16 Feb 2010) | 3 lines

this is more elegant. it did not work just because the slot was not declared as a slot! will forward port to trunk.
CCBUG=227140

------------------------------------------------------------------------
r1093286 | sebas | 2010-02-20 14:41:37 +0000 (Sat, 20 Feb 2010) | 9 lines

Add wiki: as  keyword to the mediawiki runner

This way we can enable this runner (and its decendants) by default, without hammering
all kinds of webservers on every keystroke (which is also a security problem).

Trading in a bit of convenience here.

CCMAIL:rich@kde.org, plasma-devel@kde.org

------------------------------------------------------------------------
r1093287 | sebas | 2010-02-20 14:43:05 +0000 (Sat, 20 Feb 2010) | 1 line

enable all these little runners by default, now we don't trigger on every keystroke
------------------------------------------------------------------------
r1093292 | sebas | 2010-02-20 14:53:24 +0000 (Sat, 20 Feb 2010) | 1 line

no : to use the wikis
------------------------------------------------------------------------
r1095364 | scripty | 2010-02-24 04:28:06 +0000 (Wed, 24 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
