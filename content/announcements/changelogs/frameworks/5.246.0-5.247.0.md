---
title: KDE Frameworks 5.247.0 complete changelog
version: 5.247.0
hidden: true
frameworks: true
type: fulllog
---
{{< details title="attica" href="https://commits.kde.org/attica" >}}
+ Port away from deprecated QDateTime API. [Commit.](http://commits.kde.org/attica/b657043b74d7e58171b0bef676f9a77d0fab1249) 
{{< /details >}}

{{< details title="Baloo" href="https://commits.kde.org/baloo" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/baloo/fa190c1b4478b0ed0322da763541fa6b9994d0e7) 
{{< /details >}}

{{< details title="bluez-qt" href="https://commits.kde.org/bluez-qt" >}}
+ Fix QML unit tests. [Commit.](http://commits.kde.org/bluez-qt/629ad4434f35af643dcb91c9308c3ff056d7e6f1) 
+ Fix '"__BYTE_ORDER" is not defined, evaluates to 0' warning. [Commit.](http://commits.kde.org/bluez-qt/a2ecf8c09914a1ca8343d1131907fa40481e5435) 
+ Port away from deprecated implicit lambda captures. [Commit.](http://commits.kde.org/bluez-qt/57c5fc18f0e0229e5adf3d5b882dc6ca517e9273) 
{{< /details >}}

{{< details title="breeze-icons" href="https://commits.kde.org/breeze-icons" >}}
+ Fix path for file publishing. [Commit.](http://commits.kde.org/breeze-icons/c1857ea155ffab40125e0a31fc80e3e9a94905f5) 
+ Setup job to build and publish webfonts to cdn.kde.org/breeze-icons/. [Commit.](http://commits.kde.org/breeze-icons/0d1cb806a9e2daff750b81147fb828de61903ca4) 
+ Remove Telegram from the readme. [Commit.](http://commits.kde.org/breeze-icons/2126c3c086f5923f63aa6394c6e63622f7bce49c) 
+ Update Telegram's app & status icons to better match upstream iconography. [Commit.](http://commits.kde.org/breeze-icons/3d5e8ad8f18ca6560fce3157e2c2e4dce82eb08f) 
+ Revert "Breeze icons sysguardicon". [Commit.](http://commits.kde.org/breeze-icons/3ca4f4d0dbe0ef281e8fe5218e1cc1e4148a6044) 
+ Revert "Revert "fix for qtsvg"". [Commit.](http://commits.kde.org/breeze-icons/14003d93ff1b189bf6c51d377d6c0e11fc6b5bde) 
+ Revert "fix for qtsvg". [Commit.](http://commits.kde.org/breeze-icons/24c47362cc78b358a63513eca2d0702a8627552d) 
+ Breeze icons sysguardicon. [Commit.](http://commits.kde.org/breeze-icons/2ea9ff7aedd242b6efd403f6d58e9df043fb4766) 
+ Replace ViewText with Text and ButtonFocus with Highlight. [Commit.](http://commits.kde.org/breeze-icons/0ec7c1479c42edeca77b5d5f15abfb38f2efe76b) Fixes bug [#477289](https://bugs.kde.org/477289)
{{< /details >}}

{{< details title="extra-cmake-modules" href="https://commits.kde.org/extra-cmake-modules" >}}
+ Remove warning when using KDEFrameworkCompilerSettings multiple times. [Commit.](http://commits.kde.org/extra-cmake-modules/2d071663301ab843d7d552fc6b1264226f81fd3d) 
+ Bump version for megarelease beta 2. [Commit.](http://commits.kde.org/extra-cmake-modules/f3d06feb637306e73c83ef6b01e2354d8e6f4d7b) 
+ QCH: Use light theme for generated doxygen docs. [Commit.](http://commits.kde.org/extra-cmake-modules/7a90baa9578740e8357d180282b93f72c2443a2c) 
+ Disable CMAKE_CXX_EXTENSIONS and enable CMAKE_CXX_STANDARD_REQUIRED. [Commit.](http://commits.kde.org/extra-cmake-modules/11dd72b0cea55e606c42bbf55a3be08323d0e063) 
+ Interlingua is now supported by Android. [Commit.](http://commits.kde.org/extra-cmake-modules/132966ae66786de73b2894a704473614d01092cf) 
+ ECMQtDeclareLoggingCategory: support kdebugsettings files w/ . in basename. [Commit.](http://commits.kde.org/extra-cmake-modules/19a7574141e257588209244d8f5d344bf53e3bad) 
+ Frameworks 6 beta 1. [Commit.](http://commits.kde.org/extra-cmake-modules/d9543f0935e0cc990d8b7a52ce580f4f086d0375) 
{{< /details >}}

{{< details title="frameworkintegration" href="https://commits.kde.org/frameworkintegration" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/frameworkintegration/6b784beef077af4fece6f2eb3445643043639eb0) 
{{< /details >}}

{{< details title="kapidox" href="https://commits.kde.org/kapidox" >}}
+ Bump version number for megarelease beta 2. [Commit.](http://commits.kde.org/kapidox/cf9c9d94b24ea06521849d0737c60d3a3af68fb8) 
+ Update frozen PyYAML version. [Commit.](http://commits.kde.org/kapidox/8bbdc8bcaa09d8853223f7b6e89506494c727e39) 
+ Fix typo in customdoxygen.css. [Commit.](http://commits.kde.org/kapidox/088b93cc39e196fa7041e5e380bfd82ef587bbbe) 
{{< /details >}}

{{< details title="karchive" href="https://commits.kde.org/karchive" >}}
+ KBzip2Filter: improve error message in uncompress. [Commit.](http://commits.kde.org/karchive/832218c4630ddfe4d78494abf17e7a376fa526ed) 
{{< /details >}}

{{< details title="kauth" href="https://commits.kde.org/kauth" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kauth/aa472f6c3ad4e6b88d0c49fe9e0de045b8c086c5) 
+ Remove unused static. [Commit.](http://commits.kde.org/kauth/76e73646d39fb6db199ece8bf2ec9f4a9fcab82c) 
{{< /details >}}

{{< details title="kbookmarks" href="https://commits.kde.org/kbookmarks" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kbookmarks/a85b4844ac4136eb0183ad807e098c0f0c779534) 
{{< /details >}}

{{< details title="kcalendarcore" href="https://commits.kde.org/kcalendarcore" >}}
+ Port away from deprecated QHash API. [Commit.](http://commits.kde.org/kcalendarcore/54b8495623bdf47bb30f5665b98bbeb94815fbd1) 
{{< /details >}}

{{< details title="kcmutils" href="https://commits.kde.org/kcmutils" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kcmutils/c26a5e8668100d14ca49470ec403a44399d22d20) 
+ Improve style of KPluginWidget. [Commit.](http://commits.kde.org/kcmutils/8dd8dc2d2e1ce09c856e72869e65e0bcdd1e7929) 
+ SharedQmlEngine: Use built-in operator<< for printing QQmlError. [Commit.](http://commits.kde.org/kcmutils/f1164dd5aebc5b53fafe1374c6c56ee333ed165b) 
+ SharedQmlEngine: Improve errors reporting. [Commit.](http://commits.kde.org/kcmutils/70d356c28b4ed5c8e11168c557148a2e4a1ad305) 
+ SharedQmlEngine: Get rid of unnecessary incubator. [Commit.](http://commits.kde.org/kcmutils/f83271ab07860665da5af6a71b0ddde28ee67920) 
+ SettingHighlighterPrivate: Declare QQmlParserStatus interface. [Commit.](http://commits.kde.org/kcmutils/6c6c5b83b74a9656b1c4151904ffbcbc816d0869) 
+ Get rid of custom incubator, make initialProperties map truly initial. [Commit.](http://commits.kde.org/kcmutils/42bbf95251a517fba4e638bf67eb581bce562264) 
+ ScrollViewKCM: Clip Flickable content whenever there is a header and/or a footer. [Commit.](http://commits.kde.org/kcmutils/98c18c7e6deafa872f72b005312749421a31b470) See bug [#478170](https://bugs.kde.org/478170)
+ AbstractKCM: remove footer line for unframed KCMs w/o extra footer padding. [Commit.](http://commits.kde.org/kcmutils/2edbfc892bed7c5a0f9fc2b6f7fe78d61dee5474) Fixes bug [#477967](https://bugs.kde.org/477967)
{{< /details >}}

{{< details title="kcolorscheme" href="https://commits.kde.org/kcolorscheme" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kcolorscheme/e40a6e0090bad6353fd9e705e87d61dc407bf1fb) 
+ Also adapt to the system color scheme on Android. [Commit.](http://commits.kde.org/kcolorscheme/dd4cfaab71defa4593ba9b93303d3bae83c508a3) 
{{< /details >}}

{{< details title="kcompletion" href="https://commits.kde.org/kcompletion" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kcompletion/bbd3b2bcce02369637db88a25f826b1da63c5248) 
{{< /details >}}

{{< details title="kconfig" href="https://commits.kde.org/kconfig" >}}
+ Use range-for with KEntryMap. [Commit.](http://commits.kde.org/kconfig/f17ee3b0bac4601e83acc53055b9d435eda68fbc) 
+ Fix unnecessary capture warning. [Commit.](http://commits.kde.org/kconfig/6cc59e716772bd81684de8e53c54fa1591f68b50) 
+ Fix method can be made const warning. [Commit.](http://commits.kde.org/kconfig/40576c7829189a50ca8b61a234c52c36dd475448) 
+ Fix unnecessary include clangd warnings. [Commit.](http://commits.kde.org/kconfig/c644734452fd2b49dba1b010f6c89fad84889124) 
+ Port KEntryMap to std::map. [Commit.](http://commits.kde.org/kconfig/dedfa7b586b341e5b56bb795a78c0c9851e7d18a) 
+ Add missing Qt6Qml link interface dependency. [Commit.](http://commits.kde.org/kconfig/825b7bd8e733ad9376e6b0b15ac635699738af73) 
+ Remove unused dependency on Qt::Xml. [Commit.](http://commits.kde.org/kconfig/40ce9dc93fceadfa2282ebb2eefa4dca0ae607a5) 
{{< /details >}}

{{< details title="kconfigwidgets" href="https://commits.kde.org/kconfigwidgets" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kconfigwidgets/d231605dcff658b25650f7671c1a0bf13168a243) 
+ Make commandbar a normal widget instead of a window. [Commit.](http://commits.kde.org/kconfigwidgets/19c9a61a3e761aea8df07dcd5378680bc0de4062) Fixes bug [#468433](https://bugs.kde.org/468433)
+ Kcommandbar: match by full row including category name. [Commit.](http://commits.kde.org/kconfigwidgets/44958741adf1b74077b8b7f5e7cfc07f7f6f4baa) 
{{< /details >}}

{{< details title="kcontacts" href="https://commits.kde.org/kcontacts" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kcontacts/488216c175cb0dd0038033c0c3d8dfa412030d18) 
{{< /details >}}

{{< details title="kcoreaddons" href="https://commits.kde.org/kcoreaddons" >}}
+ Fix KProcessInfoList unixProcessListPS on OpenBSD. [Commit.](http://commits.kde.org/kcoreaddons/c1ce821ab0210a5aa9b63d72ebd39d9a31a3c43b) 
+ KUrlMimeData: don't use xdg-portal when copy&paste in the same process. [Commit.](http://commits.kde.org/kcoreaddons/ab55eb6e9eb54d2982b026db40cec9c218a043be) Fixes bug [#470147](https://bugs.kde.org/470147)
+ KUrlMimeData: change DecodeOptions to QFlags. [Commit.](http://commits.kde.org/kcoreaddons/4bfd8b53471fbb2b6dcfe8d0665b75d5ee8a8f6f) 
+ Ktexttohtml: Silence clazy warning instead of wrong fix. [Commit.](http://commits.kde.org/kcoreaddons/17948c76abb069eb86d0b326095a5c967af70760) 
+ Kcoreaddons_target_static_plugins: Silence false positive non-pod-global-static warnings. [Commit.](http://commits.kde.org/kcoreaddons/395f00910db44c1400b55fb41166cbd3f8118908) 
+ Fix clazy warnings about static regex. [Commit.](http://commits.kde.org/kcoreaddons/f7d225cb741d7a71de7b893202a98a07d6e1346c) 
+ Add missing include header. [Commit.](http://commits.kde.org/kcoreaddons/e86b10068be264af5705b93c5ef64dccbea51021) 
+ Fix installing of version header. [Commit.](http://commits.kde.org/kcoreaddons/8377bf72c2981b8187cc2bb774e2165491432452) 
+ Format CMake files with neocmakelsp. [Commit.](http://commits.kde.org/kcoreaddons/2aad2ffa903494fc75b739474a11bae1464d6908) 
+ Use non deprecated variant of QDateTime::fromMSecsSinceEpoch. [Commit.](http://commits.kde.org/kcoreaddons/d60f8030dad4de86e2a1da95d6c7452b886008bd) 
{{< /details >}}

{{< details title="kcrash" href="https://commits.kde.org/kcrash" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kcrash/054beffa74e54c8cc88499340d0b8fdcb9e614be) 
+ Metadata: add documentation about the argv counting system. [Commit.](http://commits.kde.org/kcrash/e971f72fa60227757dfda05c9c6cdb8697321cbf) 
+ Metadata: add documentation about the argv counting system. [Commit.](http://commits.kde.org/kcrash/97d8fb6653860a1575b2b064214beaef442af03b) 
+ Metadata: make writes blocking. [Commit.](http://commits.kde.org/kcrash/e8484cd90dea0a5b53e92077b2a91f3ebbeea8e3) 
+ Detect active exceptions and put them in the metadata. [Commit.](http://commits.kde.org/kcrash/1cfbe46675cb5e5cab18ac70b3c09edd14e22445) See bug [#287458](https://bugs.kde.org/287458)
+ Metadatainiwriter: move to metadata file naming v2. [Commit.](http://commits.kde.org/kcrash/5989ed727ac2e557d186ca0e49af84c3af46c546) 
+ Drop redundant initializer. [Commit.](http://commits.kde.org/kcrash/894d1806faa76421d76ab4639dab2ba0aa95e90b) 
+ Don't use Q_GLOBAL_STATIC. [Commit.](http://commits.kde.org/kcrash/fe3ebee73ead0b7e9836e2b3c0514c8aebba7c47) 
+ Remove the automatic kcrash init. [Commit.](http://commits.kde.org/kcrash/65ef768964e0b884a560a74cbbf054a833af38fe) Fixes bug [#477842](https://bugs.kde.org/477842)
+ Metadata: detect glrenderer. [Commit.](http://commits.kde.org/kcrash/f626716f5a80f6dfa8a8ea36d7819b8ae8fc4a98) 
+ Reduce default verbosity. [Commit.](http://commits.kde.org/kcrash/43f5efc6a1c8ac3a95beed322ff978b0ebaa5dc9) 
+ Only close sockets when starting drkonqi. [Commit.](http://commits.kde.org/kcrash/b16a4fe764f24c1dfbccea037d9761b32a9eb92b) 
+ Auto size. [Commit.](http://commits.kde.org/kcrash/377ee50ed524cbe8b663d37108983513a881aab5) 
+ Use chrono literals. [Commit.](http://commits.kde.org/kcrash/bb6bc3fc4dce771af78bfbb981878276f0a9e4f2) 
+ Disable copy & move operations on our Args helper. [Commit.](http://commits.kde.org/kcrash/26b644ad095177e1d169f63212cd2ae9743644e8) 
+ Use not-deprecated c compat headers. [Commit.](http://commits.kde.org/kcrash/710c10d2c60e737fb13d5c1af7154fec48612a9b) 
+ Remove klauncher code copies. [Commit.](http://commits.kde.org/kcrash/b55a39fd5a9571090b7e27e05f1c62115542f5a1) 
+ Default to coredumping instead of just in time debugging. [Commit.](http://commits.kde.org/kcrash/3bf6f67a8bd14c9c2f7618e4f0517472c17fafbf) 
{{< /details >}}

{{< details title="kdav" href="https://commits.kde.org/kdav" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kdav/314ce1f85c0c830544b5628446afd27bc7f5b7ae) 
+ Port away from deprecated QDomDocument API. [Commit.](http://commits.kde.org/kdav/41b1fc74af36ae76e269655fcc5e59dbb5b5890d) 
+ Only retry requests when there is a chance of succeeding. [Commit.](http://commits.kde.org/kdav/bf4e843c1ae1cc79ab72daacf993e58502d16b3f) 
{{< /details >}}

{{< details title="kdeclarative" href="https://commits.kde.org/kdeclarative" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kdeclarative/db4f5843332817c1f8344285986bc63ece7ade81) 
+ Fix badge effect. [Commit.](http://commits.kde.org/kdeclarative/282928cd17deb976769d9be81734df97a01f81c0) Fixes bug [#478516](https://bugs.kde.org/478516)
{{< /details >}}

{{< details title="kded" href="https://commits.kde.org/kded" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kded/5badfe5be9bc493b9910ae34aa8ce897e4ce55f9) 
{{< /details >}}

{{< details title="kdesu" href="https://commits.kde.org/kdesu" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kdesu/4b18e1599379c9141ac5e81b495d50564d28a01c) 
{{< /details >}}

{{< details title="kdoctools" href="https://commits.kde.org/kdoctools" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kdoctools/60649e1d11ac9ea8b835c1669ae752931bb8a249) 
+ Src/customization/xsl/eo.xml : fix code for small letter u with breve (ŭ). [Commit.](http://commits.kde.org/kdoctools/a41e0e9b61055f0408462856944595f74bc2f187) 
{{< /details >}}

{{< details title="kfilemetadata" href="https://commits.kde.org/kfilemetadata" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kfilemetadata/2fb8561b76a0a47933f69872ac004330d9c21b2d) 
+ Fix compilation with latest TagLib git master. [Commit.](http://commits.kde.org/kfilemetadata/c9a90eedc975f469c35369b9ed9462bb4cb48f79) 
{{< /details >}}

{{< details title="kglobalaccel" href="https://commits.kde.org/kglobalaccel" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kglobalaccel/1c808401851e6ec47f6000fe86ea074232f86d38) 
{{< /details >}}

{{< details title="kguiaddons" href="https://commits.kde.org/kguiaddons" >}}
+ Implement color scheme watching using Qt 6.5 API. [Commit.](http://commits.kde.org/kguiaddons/a3736e86fe13402bced3aed6b0744b902b9492c5) 
{{< /details >}}

{{< details title="kholidays" href="https://commits.kde.org/kholidays" >}}
+ Introduce holidays observed in Kenya. [Commit.](http://commits.kde.org/kholidays/9c0803f3ec469c634012af3a2ca7a5c95a09f275) 
+ Register enum gadget types correctly with QML. [Commit.](http://commits.kde.org/kholidays/8066d56a82bfa2d902e32920c9ab9307975bae07) 
{{< /details >}}

{{< details title="ki18n" href="https://commits.kde.org/ki18n" >}}
+ Add/respect KF_SKIP_PO_PROCESSING cmake option. [Commit.](http://commits.kde.org/ki18n/e23bbaf5ef17856833d06d0b35070b302e318a20) 
+ Suppress warning about unused private field. [Commit.](http://commits.kde.org/ki18n/7ac5b68f921aaa13eba4bd3b1431c5c99eda28fe) 
+ Fix 'ignoring return value of QMutexLocker' warning. [Commit.](http://commits.kde.org/ki18n/aca5ce5b1acd3cb4cf2107cfbd22a7a7cf70af7b) 
{{< /details >}}

{{< details title="kiconthemes" href="https://commits.kde.org/kiconthemes" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kiconthemes/fbe125cce9e07b8a43492fb3c2039f0ff981e7f7) 
{{< /details >}}

{{< details title="kimageformats" href="https://commits.kde.org/kimageformats" >}}
+ Code is qt6 only now. Remove unused check. [Commit.](http://commits.kde.org/kimageformats/8d1ef536be03202abedcf245d39b18ea8bfa207c) 
+ Avif: new quality settings. [Commit.](http://commits.kde.org/kimageformats/da8ed31aec6a303f64aefc3b799c2227b918dd56) 
+ HEIF plug-in extended to support HEJ2 format. [Commit.](http://commits.kde.org/kimageformats/ce8383e5fd95064c65b4ed91ee9b9251e9b188d4) 
{{< /details >}}

{{< details title="kio" href="https://commits.kde.org/kio" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kio/619ac46b6cb72ad1df0e3c696c465a4127215876) 
+ Kpropertiesdialog: remove unused variable. [Commit.](http://commits.kde.org/kio/de4b56b6b6956ecdd6869dac71ee48d2f5030e8c) 
+ Fix launching service file action. [Commit.](http://commits.kde.org/kio/25add040dd83a36585cccd88f984167b14e58e4e) 
+ Fix build break due to KUrlMimeData::DecodeOptions change. [Commit.](http://commits.kde.org/kio/44e1515e539e7ffda77bbe5527f5712ece047189) 
+ Kpropertiesdialog: don't copy in loop. [Commit.](http://commits.kde.org/kio/e59335b9736c80740907d076ecef995d22d83914) 
+ Kpropertiesdialog: don't trip over malformed Exec. [Commit.](http://commits.kde.org/kio/78d4364677fbe658c6e05d19bb158f895403ccc9) Fixes bug [#465290](https://bugs.kde.org/465290)
+ Trash: assert with context. [Commit.](http://commits.kde.org/kio/561d633e0af740f60dc0247ff11acc327dc63021) 
+ Fix i18n regression. [Commit.](http://commits.kde.org/kio/6e7f19b78386b9b641463ade1878e56f459f338b) 
+ Fix loading desktopfile-based service actions. [Commit.](http://commits.kde.org/kio/2f4677d5fa1bebe096c83216a20c4141c2605fc4) Fixes bug [#477315](https://bugs.kde.org/477315)
+ Fix service name for kpasswdserver. [Commit.](http://commits.kde.org/kio/6d0088cbad8589a84afde757ad66b356d329ad0d) 
+ Fix kpasswdserver D-Bus service name. [Commit.](http://commits.kde.org/kio/3d42fef13479432890836df8b7ca3e45558331ee) 
+ Fix crash if window is closed when undoing. [Commit.](http://commits.kde.org/kio/d807cbe0a7fb02ff6bda4f7be066c1323423743f) Fixes bug [#469586](https://bugs.kde.org/469586)
+ [kfilefilter] Restore behavior for entering plain filter string. [Commit.](http://commits.kde.org/kio/9cfbbf868ecfbdcde9a3a3ae72f1bcb96a1ef7d2) 
+ Redesign rename/overwrite dialog. [Commit.](http://commits.kde.org/kio/aff3eef6c027a7578d1a4c71c127f8f7dc530497) 
+ Reintroduce isExecutableFile. [Commit.](http://commits.kde.org/kio/cbafb685bce8a7a09543d5fa3df286955cd6d5af) 
+ WidgetsAskUserActionHandler: Use QPointer to check the validity of parent widgets. [Commit.](http://commits.kde.org/kio/bdef648edd54e146c30e881d8eb95990a59c5bbc) Fixes bug [#448532](https://bugs.kde.org/448532)
{{< /details >}}

{{< details title="kirigami" href="https://commits.kde.org/kirigami" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kirigami/31967e102884658cd3d93e633482d8e097de9d81) 
+ Don't show the footer toolbar if there's a context drawer. [Commit.](http://commits.kde.org/kirigami/a86d009944dc33bb70814507ebeea6c4fbd3f4c7) 
+ Remove one last reference to Qt5Compat. [Commit.](http://commits.kde.org/kirigami/04765a6fa31d442d4aefcb335eabeb62f57f1e84) 
+ Port ScrollablePage progress indicator away from Qt5Compat. [Commit.](http://commits.kde.org/kirigami/d598cb552d68419cc19fc11392775743ffd6b63c) 
+ Add the option of horizontally scrolling pages. [Commit.](http://commits.kde.org/kirigami/31f6e874070c43b7dddd7da9be87acd91b877f9c) Fixes bug [#478060](https://bugs.kde.org/478060)
+ GlobalDrawerActionItem: improve accessible properties. [Commit.](http://commits.kde.org/kirigami/c9b453236308b58ea239540b2081014bfaea4f29) 
+ Update bundled icon list to include all icons used in Kirigami. [Commit.](http://commits.kde.org/kirigami/6f0ca59687f062f188699c7af1b8f91bfc5f8606) 
+ Implement EdgeShadow without Qt5Compat.GraphicalEffects. [Commit.](http://commits.kde.org/kirigami/14f61171764080d5beb25d783ffe4cad370b1fec) 
+ Don't let ListSectionHeader text overflow. [Commit.](http://commits.kde.org/kirigami/37acc54b270fd2942a6e939e08f64df418e44a8b) Fixes bug [#477843](https://bugs.kde.org/477843)
+ Fix URLButton. [Commit.](http://commits.kde.org/kirigami/8f2d05e81502c5551340be92ad0db35831d7d894) 
+ Port Dialog away from Qt5Compat.GraphicalEffects. [Commit.](http://commits.kde.org/kirigami/68bb9fbf573dfdaddb74dcbf93dfd36eea7e2069) 
+ Complete list of entries in qmldir for templates/private. [Commit.](http://commits.kde.org/kirigami/cfb800bb6667494efddada7f44e80b0ed9865d9e) 
+ [units] Make sure property name is fully qualified. [Commit.](http://commits.kde.org/kirigami/9c1754f16bafe100ab660484c1903ba696bd54a7) 
+ Add missing QtQuick dependency declaration to qml plugins. [Commit.](http://commits.kde.org/kirigami/17ca11a0350b5be79aeb62e764ee831257df0f23) 
+ Add missing entry in private qmldir file. [Commit.](http://commits.kde.org/kirigami/97ac46cd16c5982557e2d10d3c7ac1645ad237af) 
+ Port Material style InlineMessage away from Qt5Compat.GraphicalEffects. [Commit.](http://commits.kde.org/kirigami/1a0899cdc513b88cefca2867f171c8f19887c511) 
+ Test Kirigami.ImageColors can extract colors from items. [Commit.](http://commits.kde.org/kirigami/96ecfc14dbbf4d3619602b8c6ef511cfef1c2059) 
+ Remove unused CornerShadow element. [Commit.](http://commits.kde.org/kirigami/516d25ddfe0f569d9db160d60232188da515a777) 
+ Add an optional dependency on the QQC2 Breeze style on Android. [Commit.](http://commits.kde.org/kirigami/d48d6d7b6b5a77a6955efeb55d4cb4b160557ecc) 
{{< /details >}}

{{< details title="kjobwidgets" href="https://commits.kde.org/kjobwidgets" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kjobwidgets/599a458efbde88a5890e970abe8837c5284367e0) 
+ KUiServerV2JobTracker: prevent potenial use-after-free. [Commit.](http://commits.kde.org/kjobwidgets/75410fa3df5fbb182790a14af22ce5705cc1b86d) See bug [#471531](https://bugs.kde.org/471531)
{{< /details >}}

{{< details title="knewstuff" href="https://commits.kde.org/knewstuff" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/knewstuff/de6ec2b2a41b5dcf46391bdc21f40a5af7d722cc) 
+ Consistently disable implicit string casts for tests as well. [Commit.](http://commits.kde.org/knewstuff/dfbd23ffc629ea796a74eafbaf8ee90a37affec4) 
+ Use Q_ENUM instead of deprecated Q_ENUMS. [Commit.](http://commits.kde.org/knewstuff/9a1c25a7a1ed3d50c3961ecea70589539585ef03) 
+ Make methods virtual instead of QMetaObject workarounds. [Commit.](http://commits.kde.org/knewstuff/110890c03456cc28054812c5872f06abfb638d30) 
+ Transaction.cpp: Add missing emit keyword. [Commit.](http://commits.kde.org/knewstuff/9f7d46105af7694856950d06d75750e0d5541b4b) 
+ Fully qualify types in signals/slots. [Commit.](http://commits.kde.org/knewstuff/6bd989665a91e754e1107d2db2490268e2780c3c) 
+ Avoid detaching of containers in for loop. [Commit.](http://commits.kde.org/knewstuff/6efef0476e7141626453222cbdc51c398b4dd216) 
+ Installation_p.h: Fix getters being marked as slot. [Commit.](http://commits.kde.org/knewstuff/b2f9e498e8470e580928f182aff8b76a73fcac19) 
+ Fix/silence small clazy/clang warnings. [Commit.](http://commits.kde.org/knewstuff/37ec6fa9c53c1c9ddd73fbb45664eff2d4334bd4) 
{{< /details >}}

{{< details title="knotifications" href="https://commits.kde.org/knotifications" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/knotifications/3889ee6781b85827f88b542299ee0c68b8a4a77b) 
{{< /details >}}

{{< details title="knotifyconfig" href="https://commits.kde.org/knotifyconfig" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/knotifyconfig/593f581b8be235010afd1b7b7a28239440c484d9) 
{{< /details >}}

{{< details title="kpackage" href="https://commits.kde.org/kpackage" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kpackage/e08e20c97da004795a85334709f3cebed5941490) 
+ ConvertCompatMetaDataDesktopFile: Add default arg. [Commit.](http://commits.kde.org/kpackage/b15cd73b44e93d58c20d73bf9b70721a21363327) 
+ PackageJobThread: Read metadata from KPackage rather than metadata.json file. [Commit.](http://commits.kde.org/kpackage/a51505385047b201a011b9b70e595348c0e92e01) See bug [#472925](https://bugs.kde.org/472925)
+ Packagestructure_compat_p.h: Wrap json keys in QLatin1String. [Commit.](http://commits.kde.org/kpackage/88d2db26e14adbdd501fe9c06a577902e04b7d7c) 
+ Improve docs and fix use of basename for folder. [Commit.](http://commits.kde.org/kpackage/295ea670bd0a1c32d3483562dff6c128fe23f598) 
+ Add compatibility header for minimal desktop to json conversion. [Commit.](http://commits.kde.org/kpackage/10b9e6ded32781c6b60350f2223fcc549e90e54b) 
+ PackageLoader: Create better names overload for reading package metadata. [Commit.](http://commits.kde.org/kpackage/6cad72c1d8338582f8cc0d179450cd729ee780f7) 
+ PackageLoader: Create separate method for listing packages instead of metadata. [Commit.](http://commits.kde.org/kpackage/4b68f1571b9e528c72cfa2e76cf405beaa789c14) 
+ Explicitly specify that we only want to handle metadata.json metadata files internally. [Commit.](http://commits.kde.org/kpackage/44eaa3168af3b4e378d3c4add3f872c31b521e6d) 
+ Do not override metadata set in packagestructure. [Commit.](http://commits.kde.org/kpackage/e1771ce1a8e61f00acc281b4419e4f57edda55a3) 
+ Fix tests. [Commit.](http://commits.kde.org/kpackage/7902ad460cd46d0b87d99300d7bd3cb92ecca4e0) 
+ Fix validity check always returning false when reading filePath in packagestructure code. [Commit.](http://commits.kde.org/kpackage/edbc00077179ca7b1f4c26688f0a797b12257ff0) 
+ Ensure Package is valid before succeeding installation. [Commit.](http://commits.kde.org/kpackage/c90094383a537301be727ca167817796974627f0) 
+ Don't require metadata.json for installing package. [Commit.](http://commits.kde.org/kpackage/114d6fd1ec65145efe74779a4f70e031e1108d36) Fixes bug [#472925](https://bugs.kde.org/472925)
{{< /details >}}

{{< details title="kparts" href="https://commits.kde.org/kparts" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kparts/8b136d269304cccf97db2a27473067a907b5a49a) 
{{< /details >}}

{{< details title="kpeople" href="https://commits.kde.org/kpeople" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kpeople/0d813f2b632bb5ed4a07fc79cb2e631ae3c3942c) 
{{< /details >}}

{{< details title="kpty" href="https://commits.kde.org/kpty" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kpty/f03d5cd7ab86d0b7bb0526448358ece1ea147f77) 
{{< /details >}}

{{< details title="kquickcharts" href="https://commits.kde.org/kquickcharts" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kquickcharts/c1f741a61000626a2ef6b4f5b710f304a5c19477) 
{{< /details >}}

{{< details title="krunner" href="https://commits.kde.org/krunner" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/krunner/2906017a5368cf2e5e75be27307fff408d8a4818) 
+ Templates/runner6python: Use default value for object path. [Commit.](http://commits.kde.org/krunner/bd1fb3c47c72fe95a9bb7bfa74765c0ab9f56b44) 
+ DBusRunner: Use /runner as default for X-Plasma-DBusRunner-Path property. [Commit.](http://commits.kde.org/krunner/6a17ee900abd7e1dcdc1af2a483b58445ba8d552) 
+ RunnerManager: Silence false positive clang-tidy warning. [Commit.](http://commits.kde.org/krunner/113a385f28cab15344888637a866c17f4a78844a) 
{{< /details >}}

{{< details title="kservice" href="https://commits.kde.org/kservice" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kservice/4f9eeb8c0a5f19ff19873a3a83b46a7ec8d17cbe) 
{{< /details >}}

{{< details title="kstatusnotifieritem" href="https://commits.kde.org/kstatusnotifieritem" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kstatusnotifieritem/809871a2849e8fc8524e8d02cb5678c00c3aaed6) 
+ Fix dangling pointer access when the QWindow was deleted. [Commit.](http://commits.kde.org/kstatusnotifieritem/e9c027e8c6f8dac8eb5518fc5659a1116e67b08e) 
{{< /details >}}

{{< details title="ksvg" href="https://commits.kde.org/ksvg" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/ksvg/cd5a544cb3ec318f781663f0ad367833529476c4) 
{{< /details >}}

{{< details title="ktexteditor" href="https://commits.kde.org/ktexteditor" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/ktexteditor/c151a910424c3943fe702a4246eb82d0e85c0800) 
+ Remove shortcut that clashes with font size change. [Commit.](http://commits.kde.org/ktexteditor/8f3d5f7983ef24139a2df25f9689fa585182e0a8) Fixes bug [#476696](https://bugs.kde.org/476696)
+ Katemessagewidget: Use KMessageBox::Position. [Commit.](http://commits.kde.org/ktexteditor/faa980229a003f9b054e39b3c945f8b6dba6dc84) 
+ Remember scroll position in session. [Commit.](http://commits.kde.org/ktexteditor/c8ed17734729b11e17447b3030a0a8214a98f512) Fixes bug [#468109](https://bugs.kde.org/468109)
+ Allow co-installability of the KAuth files with KF5. [Commit.](http://commits.kde.org/ktexteditor/55bd0a256c56584e926c78c81e6e71930786f952) 
+ Better x11 check in the tests. [Commit.](http://commits.kde.org/ktexteditor/81a70568a4d23119012483016682382c040de431) 
+ Avoid use of KF6::WindowSystem. [Commit.](http://commits.kde.org/ktexteditor/94d7eb6a1a7d7cffd5dc129a21aa97985665c1c3) 
+ Try to avoid pure virtual function call. [Commit.](http://commits.kde.org/ktexteditor/563afd8fc992a4be8223a6a49985aa290c8812bc) 
+ View/kateview.cpp (KTextEditor::ViewPrivate::setupActions) : Fix small typo in setWhatsThis text. [Commit.](http://commits.kde.org/ktexteditor/ebbe3f0626a299436adce1bc003058c708ce4cbd) 
+ Same code for wayland. [Commit.](http://commits.kde.org/ktexteditor/d9a0ed1b1bd618c41b321761afe95116cc7b80a1) 
+ Screenshot: Fix row number alignment. [Commit.](http://commits.kde.org/ktexteditor/9c6ddf811bdd1b5e471d954f6038b181bc48f7e2) Fixes bug [#477497](https://bugs.kde.org/477497)
+ Hide completion widget on creation. [Commit.](http://commits.kde.org/ktexteditor/2d139f838ff22574655f9754b4b3aa5f733559c7) Fixes bug [#477746](https://bugs.kde.org/477746)
{{< /details >}}

{{< details title="ktextwidgets" href="https://commits.kde.org/ktextwidgets" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/ktextwidgets/0e7f9328567efcdceee7fed83cc491d4580ad5a4) 
+ Fix unused-result warning in lastIndexOf() call. [Commit.](http://commits.kde.org/ktextwidgets/376fdb7cd3f537b662cc024778715b7fd471fff7) 
{{< /details >}}

{{< details title="kunitconversion" href="https://commits.kde.org/kunitconversion" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kunitconversion/7ef5603831cdffe0c37619ea156a0df3b9303e04) 
+ Port away from deprecated QLocale API. [Commit.](http://commits.kde.org/kunitconversion/50e3b2fa78baf58a27a32099f5d80d62cc413c24) 
{{< /details >}}

{{< details title="kuserfeedback" href="https://commits.kde.org/kuserfeedback" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kuserfeedback/2b552045bcb723243278336695c51d04d5d8766a) 
+ Add missing QDebug include. [Commit.](http://commits.kde.org/kuserfeedback/9a23019d49513acba5128c5173e622ae2f53ced6) 
{{< /details >}}

{{< details title="kwallet" href="https://commits.kde.org/kwallet" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kwallet/25d6ab66946037cf069a1d185c1a1efd9c6a5711) 
+ State that kwalletd6 is starting, not kwalletd5. [Commit.](http://commits.kde.org/kwallet/95ac935ec161fa3d00b40199e428aa76998135c2) 
+ Update maintainer information. [Commit.](http://commits.kde.org/kwallet/ab298d49014647822594be3093c62b3fd8def2fa) 
{{< /details >}}

{{< details title="kwidgetsaddons" href="https://commits.kde.org/kwidgetsaddons" >}}
+ KPageView: Add runtime introspection for page header & footer. [Commit.](http://commits.kde.org/kwidgetsaddons/54681cc5feed0ff8d04a55f5207b5d2888168b43) 
+ Remove mention of obsolete api in KFontChooser apidoc. [Commit.](http://commits.kde.org/kwidgetsaddons/27b2c38fa344fc8355c5bd060789374247f2a097) 
{{< /details >}}

{{< details title="kwindowsystem" href="https://commits.kde.org/kwindowsystem" >}}
+ Use toULongLong for converting to WId. [Commit.](http://commits.kde.org/kwindowsystem/082799ad826e0ea93fade40f5d5d3ae9dac6763c) 
+ Add support for XDG Foreign. [Commit.](http://commits.kde.org/kwindowsystem/e3cc69f38b5d4ff3cc7a53b7ee9d374a6793d467) 
+ Dont't  crash if a null QWindow is passed to requestToken. [Commit.](http://commits.kde.org/kwindowsystem/07b1f54a1a97bc0058efbbc498269ef51f3bfcdc) 
+ Use surfaceForWindow function to obtain a surface. [Commit.](http://commits.kde.org/kwindowsystem/125e0711fb33c412a67856af091bed8b3d5b13d5) 
+ Use public API to obtain last serial and seat. [Commit.](http://commits.kde.org/kwindowsystem/ea5bdeeaf1b4b1ecbe38d72547bf15e79d8b83c5) 
+ Fix kwindowinfox11test. [Commit.](http://commits.kde.org/kwindowsystem/b56107605472f92a44c644c978a4528504fdb3da) 
{{< /details >}}

{{< details title="kxmlgui" href="https://commits.kde.org/kxmlgui" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/kxmlgui/528114b3242192dfad313263ed3e9e7122d8ed53) 
+ Fix kcommandbar. [Commit.](http://commits.kde.org/kxmlgui/51b6bb7cbee86d27b8922a38de879180cb8d5827) 
+ Adapt to api change. [Commit.](http://commits.kde.org/kxmlgui/5e70a3e68e2e3dc97d19707ee798b03935a5c498) 
{{< /details >}}

{{< details title="purpose" href="https://commits.kde.org/purpose" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/purpose/1f9981891808f57c9e4c5a12cbdcfa366af194a6) 
+ Rename icons which clash with purpose in kf5 and drop unused youtube ones. [Commit.](http://commits.kde.org/purpose/64fc7cab84de39a92e740a38729351e5ac9546cf) 
+ Adapt to KAccounts API change. [Commit.](http://commits.kde.org/purpose/a6be8945461923224b34e89634524fb4b92aa05f) 
+ Fix opening KAccounts KCM. [Commit.](http://commits.kde.org/purpose/4ac140f607d835aa0b669d6fafb114bd61679e68) 
+ Reenable and fix KAccounts integration. [Commit.](http://commits.kde.org/purpose/d912311c25e8e77b14a84fe27a6f038f47371894) 
{{< /details >}}

{{< details title="qqc2-desktop-style" href="https://commits.kde.org/qqc2-desktop-style" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/qqc2-desktop-style/b179b3ba9d07676fed75277d391504493ba93ed5) 
+ Menu: Don't overlap menu items with scrollbar. [Commit.](http://commits.kde.org/qqc2-desktop-style/8b35ea1f06dbd972adab736a050d0fb0591abc37) Fixes bug [#438996](https://bugs.kde.org/438996)
+ Use medium spacing for DialogButtonBox. [Commit.](http://commits.kde.org/qqc2-desktop-style/8012ec0c72cbcb7af326e666509b528e2b3a017d) 
+ ItemDelegate: improve accessible properties. [Commit.](http://commits.kde.org/qqc2-desktop-style/a605a3f7d38800680dab4d9d8e185e0b8a02a7ef) 
+ HeaderView: Paint end item in free space at the end. [Commit.](http://commits.kde.org/qqc2-desktop-style/a28fdb8b76dd84b156acb4664c5bf11364096290) Fixes bug [#478124](https://bugs.kde.org/478124)
+ SwitchIndicator: make active background follow current position. [Commit.](http://commits.kde.org/qqc2-desktop-style/0a2de2e8017a5a468f5627b6a10fe22a07f725e1) 
+ Don't set a background color for normal unhovered list items. [Commit.](http://commits.kde.org/qqc2-desktop-style/b0167e224fa1eaa6c1c94322102b8fd61073f39a) Fixes bug [#477792](https://bugs.kde.org/477792)
+ Allow right-clicks on text fields/areas with a touchpad. [Commit.](http://commits.kde.org/qqc2-desktop-style/974ad3494238366d05bfe88955cb4bb46bf50ff2) 
+ Slider: Fix slider scrolling direction. [Commit.](http://commits.kde.org/qqc2-desktop-style/5d6d74ef68889da7400cb2a16a73527f1927424d) Fixes bug [#459624](https://bugs.kde.org/459624)
+ TreeViewDelegate: Use correct palette for painting item branches. [Commit.](http://commits.kde.org/qqc2-desktop-style/84a46ed22194ef3cf60098c1acb489f136de5b94) 
+ TreeViewDelegate: Fix colorSet for the default background. [Commit.](http://commits.kde.org/qqc2-desktop-style/67b03d6e755c2a4b19cac46915dc14901e8c9ebe) 
+ TreeViewDelegate: Forward selected state to styles. [Commit.](http://commits.kde.org/qqc2-desktop-style/2d095a86aa4796c0549efc49f227484c6c7febc8) 
+ Remove workarounds for QTBUG-95873. [Commit.](http://commits.kde.org/qqc2-desktop-style/a4ce16f7ec3a95d38d3a7799b6522a695edca488) 
+ Add background for modal dialog. [Commit.](http://commits.kde.org/qqc2-desktop-style/bac29a7d4fc60c05d13a69f664496f39b9a148f2) 
+ Kirigami-plasmadesktop-integration: test animation speed modifier. [Commit.](http://commits.kde.org/qqc2-desktop-style/243043f6a24fb8c31a88d640bb494f93bae6873a) See bug [#476450](https://bugs.kde.org/476450)
+ ScrollView: Port internal method to new optional chaining syntax. [Commit.](http://commits.kde.org/qqc2-desktop-style/62bc2245865035e11e66fc0d4f04ce0bbd1d96a1) 
+ ScrollView: Stop clipping by default. [Commit.](http://commits.kde.org/qqc2-desktop-style/8bb5b1a54697d523efeccaf6461b9d3507bf354e) 
{{< /details >}}

{{< details title="solid" href="https://commits.kde.org/solid" >}}
+ Backend/upower: Support HeadphoneBattery, HeadsetBattery, TouchpadBattery in BatteryType enum. [Commit.](http://commits.kde.org/solid/fa848ee68b80684d51088a5e1c9fb0468d8652ca) 
+ Predicate: match iterable elements when possible. [Commit.](http://commits.kde.org/solid/25e2b228653cceef77be9171916c3f0c4b93a54f) Fixes bug [#478285](https://bugs.kde.org/478285)
+ Imobile: work around some bugs. [Commit.](http://commits.kde.org/solid/8831787b9cba6b77b50657be49e91a4378dd5ab4) 
{{< /details >}}

{{< details title="sonnet" href="https://commits.kde.org/sonnet" >}}
+ Use the cmake variables rather than if(TARGET). [Commit.](http://commits.kde.org/sonnet/9e7d3ad2f594ae56fda86ff2587f7a5b1ee03e13) 
{{< /details >}}

{{< details title="syndication" href="https://commits.kde.org/syndication" >}}
+ Update version for new release. [Commit.](http://commits.kde.org/syndication/edc222de5796e4fe693e4b1d4cac6c0a28d7ee34) 
{{< /details >}}

{{< details title="syntax-highlighting" href="https://commits.kde.org/syntax-highlighting" >}}
+ Add *.md.html. [Commit.](http://commits.kde.org/syntax-highlighting/0198ada8fcb79fd059358a23747d2d65ed5bfa09) Fixes bug [#450724](https://bugs.kde.org/450724)
+ Sync test results with new state. [Commit.](http://commits.kde.org/syntax-highlighting/0c3e8af455d5ce62a082d0a3ca3d6abdbc97b09d) 
+ Systemd unit: fix update to systemd v255. [Commit.](http://commits.kde.org/syntax-highlighting/bd2a0a4ff9fa9b24d2a2252447b303553f0a77cf) 
+ Systemd unit: update to systemd v255. [Commit.](http://commits.kde.org/syntax-highlighting/451ade5c93f4bd6a7b7cc5f98a93b1644da677df) 
+ Less clang-tidy warnings about used nullptrs. [Commit.](http://commits.kde.org/syntax-highlighting/13528895a94cb9b1ae8be9d38e30c2865e532b54) 
+ Init all members. [Commit.](http://commits.kde.org/syntax-highlighting/803e93639ea094e2a75677d70af0219d600de060) 
+ Update known options for kdesrc-buildrc. [Commit.](http://commits.kde.org/syntax-highlighting/45e878cda2beb09dead5de6dd5f2b453d93c99cd) 
+ Cmake.xml: add CMake 3.28 features. [Commit.](http://commits.kde.org/syntax-highlighting/cf40e4b2411d83f4ce715f379d9d3728f489b469) 
+ Add support for viper language. [Commit.](http://commits.kde.org/syntax-highlighting/3ca9f32b404fbbe0d403bd2c2745748ec8bee8f5) 
{{< /details >}}

