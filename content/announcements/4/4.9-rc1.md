---
aliases:
- ../announce-4.9-rc1
date: '2012-06-27'
description: KDE Provides Candidate Release 1 for 4.9 Workspaces, Applications and
  Platform.
title: KDE Announces 4.9 RC1
---

<p align="justify">

Today KDE released the first release candidate for its renewed Workspaces, Applications, and Development
Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing
bugs and further polishing new and old functionality. Highlights of 4.9 will include:

<ul>
    <li>
    Qt Quick in Plasma Workspaces -- Qt Quick is continuing to make its way into the Plasma Workspaces, the Qt Quick Plasma Components, which have been introduced with 4.8, mature further. Additional integration of various KDE APIs was added through new modules. More parts of Plasma Desktop have been ported to QML. While preserving their functionality, the Qt Quick replacements of common plasmoids are visually more attractive, easier to enhance and extend and behave better on touchscreens.
    </li>
    <li>The Dolphin file manager has improved its display and sorting and searching based on metadata. Files can now be renamed inline, giving a smoother user experience.
    <li>
    Deeper integration of Activities for files, windows and other resources: Users can now more easily associate files and windows with an Activity, and enjoy a more properly organized workspace. Folderview can now show files related to an Activity, making it easier to organize files into meaningful contexts.
    </li>
    <li>
    Many performance improvements and bugfixes improve the overall user experience, making the KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.9_Feature_Plan">4.9 Feature Plan</a>. As with any large number of changes, we need to give 4.9 a good testing in order to maintain and improve the quality and our user's user experience when they get the update.

<h2>Testing in Progress</h2>
<p>
As with the two beta releases the testing continues for the release candidate RC1.
 
The KDE Testing Team is continuously growing and collaborates directly with the developers
of several KDE projects. 91 more bugs were fixed since the beta2 release and about 40 regressions
were identified and mostly fixed as well. Under the hood there is more work going on in collaboration
with the KDE sysadmins to facilitate automatic testing for all KDE projects, continuous integration is already used by several core components of KDE.

To join the KDE Testing Team please come to the #kde-quality channel on irc.freenode.net or join the <a href="https://mail.kde.org/mailman/listinfo/kde-testing">mailing list kde-testing@kde.org</a>.

<h3>KDE Software Compilation 4.9 RC1</h3>
<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.8.95/">download.kde.org</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing 4.9 RC1 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.9 RC1 (internally 4.8.95)
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.8.95#binary">4.8.95 Info
Page</a>.
</p>

<h4>
  Compiling 4.9 RC1
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.9 RC1 may be <a
href="http://download.kde.org/unstable/4.8.95/src/">freely downloaded</a>.
Instructions on compiling and installing 4.8.95
  are available from the <a href="/info/4/4.8.95">4.8.95 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or
become a KDE e.V. supporting member through our new
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>


