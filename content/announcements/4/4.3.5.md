---
aliases:
- ../announce-4.3.5
date: '2010-01-26'
description: KDE Community Ships Fifth Maintainance Update for KDE SC 4.3 Series.
title: KDE Software Compilation 4.3.5 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Software Compilation 4.3.5 Out Now: Codename "Clue"
</h3>

<p align="justify">
  <strong>
KDE Community Ships Fifth Translation and Service Release of the 4.3
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</strong>
</p>

<p align="justify">
Today, KDE has released a new version of the KDE Software Compilation (KDE SC). <a href="/announcements/4/4.3.5">This month's edition of KDE SC</a> is a bugfix and translation update to KDE SC 4.3. KDE SC 4.3.5 is a recommended upgrade for everyone running KDE SC 4.3.4 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. Users around the world will appreciate that KDE SC 4.3.5 is more completely translated. KDE 4 is already translated into more than 50 languages, with more to come.
</p>
<p>
KDE SC 4.3.5 has a number of improvements:
<ul>
    <li>A number of bugs in KIO have been squashed, making updating of shown directories work again and solving some other stability issues</li>
    <li>A crash when creating an avatar from the webcam in Kopete has been fixed</li>
    <li>kded taking 100% CPU time in certain situations has been cured</li>
</ul>
The <a href="/announcements/changelogs/changelog4_3_4to4_3_5">changelog</a> lists more, if not all improvements since KDE SC 4.3.4.
</p>

<div class="text-center">
	<a href="/announcements/4/4.3.0/images/kde430-desktop.png">
	<img src="/announcements/4/4.3.0/images/kde430-desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The KDE 4.3 Desktop</em>
</div>
<br/>

<p align="justify">
Note that the changelog is usually incomplete, for a complete list of
changes that went into KDE SC 4.3.5, you can browse the Subversion log.
KDE SC 4.3.5 also ships a more complete set of translations for many of the 50+ supported languages.
<p />
To find out more about the KDE 4.3 desktop and applications, please refer to the
<a href="/announcements/4.3/">KDE 4.3.0</a>,
<a href="/announcements/4.2/">KDE 4.2.0</a>,
<a href="/announcements/4.1/">KDE 4.1.0</a> and
<a href="/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE SC 4.3.5 is a recommended update for everyone running KDE 4.3.4 or earlier versions.
<p />

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.3.5/">download.kde.org</a> and can
also be obtained on <a href="/download">CD-ROM</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing KDE SC 4.3.5 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.3.5
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.3.5/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.3.5">KDE SC 4.3.5 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.3.5
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE SC 4.3.5 may be <a
href="http://download.kde.org/stable/4.3.5/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.3.5
  are available from the <a href="/info/4/4.3.5#binary">KDE SC 4.3.5 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>


