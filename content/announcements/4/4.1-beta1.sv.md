---
aliases:
- ../announce-4.1-beta1
date: '2008-05-27'
title: KDE 4.1 Beta1 Release Announcement
---

<h3 align="center">
  Första betaversionen av KDE 4.1 lanserad
</h3>

<p align="justify">
  <strong>
KDE-projektet lanserar första betaversionen av KDE 4.1.</strong>
</p>

<p align="justify">
<a href="/">KDE Projektet</a> tillkännager stolt den första
betaversionen av KDE 4.1. Beta 1 är till för testare, projektmedlemmar och andra
entusiaster för att identifiera fel och regressioner, så att KDE 4.1 äntligen
kan ersätta KDE 3 för slutanvändare. KDE 4.1 beta 1 finns tillgänglig som binära
paket för ett stort antal plattformar och även som källkodspaket. Den slutliga versionen
av KDE 4.1 är planerad att släppas under juli 2008.
</p>

<h4>
  <a id="changes">KDE 4.1 Beta 1 höjdpunkter</a>
</h4>

<p align="justify">
<ul>
    <li>Skrivbordet har fått en kraftigt förbättrad funktionalitet och konfigurerbarhet.
    </li>
    <li>KDE's personliga kommunikationssvit har blivit portad till KDE 4.
    </li>
    <li>Många nya och nyportade applikationer.
    </li>
</ul>
</p>

<h4>
  Plasma växer upp
</h4>
<p align="justify">
Plasma, det innovativa nya systemet som skapar menyer, paneler och skrivbordet
som utgör skrivbordet har mognat snabbt. Det stöder nu multipla och flexibla paneler
vilket ger användaren möjlighet att att själva komponera sin miljö på ett så
flexibelt sätt som möjligt. Applikationsmenyn Kickoff, har blivit uppdaterad med
ett nytt rent utseende och många optimeringar. Kör program dialogen har fått en rejäl
omskrivning för att ge användare möjlighet att snabbt starta program, öppna
dokument och öppna sajter. Den kompositerande fönsterhanteraren har fått flera 
prestandaförbättringar för att ge bättre ergonomi och effekter inklusive den nya
Cover Switch alt-tab effekten och wobbly windows.
</p>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-krunner.png">
	<img src="/announcements/4/4.1-beta1/plasma-krunner-small.png" class="img-fluid" alt="Den nya KRunner Alt-F2 dialogen">
	</a> <br/>
	<em>Den nya KRunner Alt-F2 dialogen</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-panelcontroller.png">
	<img src="/announcements/4/4.1-beta1/plasma-panelcontroller-small.png" class="img-fluid" alt="Panelhanteringen återvände">
	</a> <br/>
	<em>Panelhanteringen återvände</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-coverswitch.png">
	<img src="/announcements/4/4.1-beta1/kwin-coverswitch-small.png" class="img-fluid" alt="KWin:s Cover Switch effekt">
	</a> <br/>
	<em>KWin:s Cover Switch effekt</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly1.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly1-small.png" class="img-fluid" alt="Wobblande fönster">
	</a> <br/>
	<em>Wobblande fönster</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly2.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly2-small.png" class="img-fluid" alt="Fler wobblande fönster">
	</a> <br/>
	<em>Fler wobblande fönster</em>
</div>
<br/>

<h4>
  Kontact återvänder
</h4>
<p align="justify">
Kontact, KDE kommunikationssvit och dess associera verktyg har blivit portade till
KDE 4 och släpps därmed för första gången med KDE 4.1. Många funktioner från Enterprise
grenen av KDE 3 har inkluderats vilket gör Kontact än mer användbar i affärssammanhang. 
Förbättringar inkluderar nya komponenter som KTimeTracker och KJots för hantering av 
anteckningar, ett nytt utseende, bättre stöd för multipla kalendrar och tidszoner och en 
mer robust eposthantering.</p>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-calendar.png">
	<img src="/announcements/4/4.1-beta1/kontact-calendar-small.png" class="img-fluid" alt="Multipla kalendrar">
	</a> <br/>
	<em>Multipla kalendrar</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-kjots.png">
	<img src="/announcements/4/4.1-beta1/kontact-kjots-small.png" class="img-fluid" alt="KJots">
	</a> <br/>
	<em>KJots</em>
</div>
<br/>

<h4>
KDE 4 applikationerna växer
</h4>
<p align="justify">
Över hela KDE gemenskapen har flera applikationer nu portats över till KDE 4
eller har förbättrats sedan KDE 4 lanserades. Dragon Player den snabba videospelaren
debuterar. KDE:s CD-spelare återvänder. En ny utskriftsapplet ger överlägset stöd för
att styra sin utskrifter. Konqueror får stöd för sessioner, ett Undoläge och 
förbättrad scrollning. Ett nytt läge för bildvisning inklusive ett fullskärmsläge
för Gwenview. Dolphin får stöd för flikar och ett antal andra funktioner från KDE 3.
Många applikationer har fått stöd för att hämta nya teman, ikoner m.m via Get New Stuff
som har fått ett nytt förbättrat gränssnitt. Stöd för Zeroconf nätverkshantering har
lagts till till flera spel och andra applikationer för att förenkla hanteringen för 
att sätta upp nätverksspel eller fjärråtkomst.
</p>
<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dolphin-treeview.png">
		<img src="/announcements/4/4.1-beta1/dolphin-treeview-small.png" class="img-fluid" alt="Dolphin:s trädvy">
		</a> <br/>
		<em>Dolphin:s trädvy</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dragonplayer.png">
		<img src="/announcements/4/4.1-beta1/dragonplayer-small.png" class="img-fluid" alt="Dragon mediaspelare">
		</a> <br/>
		<em>Dragon mediaspelare</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kdiamond.png">
		<img src="/announcements/4/4.1-beta1/games-kdiamond-small.png" class="img-fluid" alt="Pusselspelet KDiamond">
		</a> <br/>
		<em>Pusselspelet KDiamond</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kubrick.png">
		<img src="/announcements/4/4.1-beta1/games-kubrick-small.png" class="img-fluid" alt="80-talet på ditt skrivbord">
		</a> <br/>
		<em>80-talet på ditt skrivbord</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/konqueror.png">
		<img src="/announcements/4/4.1-beta1/konqueror-small.png" class="img-fluid" alt="Konqueror">
		</a> <br/>
		<em>Konqueror</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/marble-openstreetmap.png">
		<img src="/announcements/4/4.1-beta1/marble-openstreetmap-small.png" class="img-fluid" alt="Marble med OpenStreetMaps">
		</a> <br/>
		<em>Marble med OpenStreetMaps</em>
	</div>
	<br/>

<h4>
Förbättringar av ramverken
</h4>
<p align="justify">
Utvecklarna har lagt ner mycket tid på att utöka och förbättra 
KDE:s kärnbibliotek. KHTML får en prestandraförbättring genom 
att inkludera

Developers have been busy enriching the core KDE libraries and
infrastructure too. KHTML gets a speed boost from anticiperande resursladdning,
och WebKit, dess avkomma, läggs till Plasma för att ge stöd för OSX
Dashboard widgets i KDE. Widgets on Canvas stödet i Qt 4.4 gör Plasma
mer stabilt och lättviktigt. KDE:s karakteristiska en-klicks baserade gränssnitt
får en ny markeringsmekanism för ökad hastighet och tillgänglighet. Phonon det plattformsoberoende
mediaramverket får stöd för undertexter och GStreamer, DirectShow 9 and QuickTime
implementationer. Nätverkshantering får stöd för flera versioner av NetworkManager.  
Stöd för flera freedesktop.org standarder såsom popupnotifiering och bokmärken har påbörjats
så att andra applikationer passar in i KDE 4.1 skrivbordsmiljö.

</p>

<h4>
  KDE 4.1 slutlig lansering
</h4>
<p align="justify">
KDE 4.1 är planerad att släppas den 29 juli 2008, sex månader efter KDE 4.0.
</p>

<h4>Installera KDE 4.1 beta 1</h4>
<p align="justify">
  <em>Paket</em>.
  Några Linux/UNIX distributörer tillhandahåller binära paket av KDE 4.1 beta 1 och i 
vissa fall har andra personer i omgivningaen skapat paket.
  <ul>
<li><a href="http://fedoraproject.org/wiki/Releases/Rawhide"></a>Fedora</li>
<li><em>Debian</em> has KDE 4.1beta1 in <em>experimental</em>.</li>
<li><em>Kubuntu</em> packages are in preparation.</li>
<li><a href="http://wiki.mandriva.com/en/2008.1_Notes#Testing_KDE_4">Mandriva</a></li>
<li><a href="http://en.opensuse.org/KDE4#KDE_4_UNSTABLE_Repository_--_Bleeding_Edge">openSUSE</a></li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">Windows</a></li>
<li><a href="http://mac.kde.org/">Mac OS X</a></li>
</ul>
</p>

<h4>
  Kompilera KDE 4.1 beta 1 (4.0.80) själv
</h4>
<p align="justify">
  <a id="source_code"></a><em>Källkod</em>.
  All källkod till KDE 4.1 beta 1 kan laddas ner  <a
href="/info/4/4.0.80">härifrån</a>.
Instruktioner hur man kompilerar och installerar KDE 4.0.80 finns på
<a href="/info/4/4.0.80">KDE 4.0.80 Info Page</a>, eller på 
<a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Stöd KDE
</h4>
<p align="justify">
 KDE är ett <a href="http://www.gnu.org/philosophy/free-sw.html">Fri Mjukvaru</a>
projekt som existerar och växer endast tack vare hjälp från volontärer som donerar
sin tid och sitt arbete. KDE behöver alltid mer hjälp från frivilliga vare sig det
gäller kodning, buggrättning eller rapportering, hjälp med dokumentation, översättning,
marknadsföring, ekonomisk hjälp etc. All hjälp uppskattas och accepteras gladeligen. 
Se <a href="/community/donations/">Stöd KDE</a> för mer information. </p>

<p align="justify">
Vi ser fram emot att få höra av dig snart!
</p>

<h2>Om KDE 4</h2>
<p>
KDE 4.0 är den innovativa fria skrivbordsmiljön med massor av appplikationer för alla uppgifter
i dagligt användande och för specifika uppgifter. Plasma är det nya skrivbordsskalet för KDE 4 
som tillhandahåller ett intiutivt gränssnitt för att interagera med skrivbordet och applikationerna.
Webbläsaren Konqueror integrerar Internet med skrivbordet. Filhanteraren Dolphin, dokumentvisaren 
Okular och kontrollpanelen System Settings kompletterar den grundläggande skrivbordsmiljön.
<br />
KDE är byggt på KDEs biblioteken som ger enkel åtkomst till nätverket genom KIO och avancerade
grafiska funktioner genom Qt4. Phonon och Solid, som också ingår i KDEs bibliotek tillhandahåller
stöd för multimedia och bättre hårdvaruintegration för alla KDEs applikationer.
</p>



