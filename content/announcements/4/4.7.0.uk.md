---
aliases:
- ../4.7
custom_about: true
custom_contact: true
date: '2011-07-27'
description: KDE Ships 4.7.0 Workspaces, Applications and Platform.
title: Випущено KDE 4.7
---

</p>
<p>
KDE із задоволенням повідомляє про свій найсвіжіший набір випусків, зокрема оновлену версію робочого простору Плазми KDE, програм KDE та платформи розробки KDE. Ці випуски, які позначено номером версії 4.7, надають у розпорядження користувачів багато нових можливостей та покращень стабільності та швидкодії.</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/general-desktop.png">
	<img src="/announcements/4/4.7.0/thumbs/general-desktop.png" class="img-fluid" alt="Plasma and Applications 4.7">
	</a> <br/>
	<em>Plasma and Applications 4.7</em>
</div>
<br/>

<h3>
<a href="./plasma">
Робочі простори Плазми тепер є портативнішими завдяки KWin
</a>
</h3>

<p>

<a href="./plasma">
<img src="/announcements/4/4.7.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.7" />
</a>

Робочі простори Плазми значно виграли від інтенсивної роботи над розробкою композитного засобу керування вікнами KDE, KWin, та від використання нових технологій Qt, зокрема Qt Quick. Щоб дізнатися більше, ознайомтеся з <a href="./plasma">оголошенням про випуск Плазми для стаціонарних та портативних комп’ютерів 4.7</a>.

<br />
</p>

<h3>
<a href="./applications">
У оновлених програмах KDE ви зможете скористатися багатьма чудовими можливостями
</a>
</h3>

<p>
<a href="./applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.7"/>
</a>
Значну частину програм KDE було оновлено. Зокрема до основного циклу випусків долучився комплекс програм для групової роботи Kontact, всі його основні компоненти тепер портовано на Akonadi. Випущено нову основну версію digiKam, багатого на можливості інструмента для роботи з фотографіями у KDE. Щоб дізнатися більше, ознайомтеся з  <a href="./applications">оголошеннями про випуск програм KDE 4.7</a>.
<br />
</p>

<h3>
<a href="./platform">
Покращені мультимедійні і семантичні можливості та можливості з обміну повідомленнями у платформі KDE
</a>
</h3>

<p>
<a href="./platform">
<img src="/announcements/4/4.7.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.7"/>
</a>

Широкий діапазон програмного забезпечення KDE та стороннього програмного забезпечення тепер може користуватися перевагами нової версії Phonon та значними удосконаленнями у компонентах семантичної стільниці, зі збагаченими програмними інтерфейсами та покращеною стабільністю. Нова оболонка KDE Telepathy надає можливості з інтеграції обміну повідомленнями безпосередньо до робочих просторів та програм. Покращено швидкодію та стабільність майже всіх компонентів, що усуває проблеми у користуванні та навантаженні на систему програм, які використовують платформу KDE 4.7. Щоб дізнатися більше, ознайомтеся з <a href="./platform">оголошенням щодо випуску платформи KDE 4.7</a>.
<br />

</p>

<h3>
Нові можливості обміну повідомленнями вбудовано безпосередньо у стільницю
</h3>
<p>
Команда KDE-Telepathy з радістю повідомляє про випуск попередньої першої в історії тестової версії нової платформи обміну повідомленнями для KDE. Хоча платформа перебуває на ранній стадії свого розвитку, ви вже можете користуватися всіма типами облікових записів, зокрема GTalk та Facebook. Ви можете змінювати вигляд вікна повідомлень за допомогою тем з Adium. Ви також можете розмістити віджет присутності Плазми безпосередньо на панелі для керування вашим станом у мережі. Оскільки цей проект ще недостатньо стабільний для використання у основній версії KDE, його запаковано та випущено окремо, поруч з іншими основними частинами KDE.
</p>

<h3>
Стабільність та можливості
</h3>
<p>
Окрім багатьох нових можливостей, описаних у оголошеннях щодо випуску, учасниками розробки KDE оброблено понад 12000 звітів щодо вад (зокрема понад 2000 різних вад у випущеному сьогодні програмному забезпеченні) з часу попереднього основного випуску програмного забезпечення KDE. У результаті наше програмне забезпечення є стабільнішим, ніж будь-яка попередня версія.
</p>

<h3>
Розкажіть іншим і будьте свідком результатів: мітка «KDE»
</h3>
<p>
Команда KDE буде вдячна всім за поширення інформації у соціальних мережах. Надсилайте ваші повідомлення на сайти новин, використовуйте канали розповсюдження повідомлень, зокрема delicious, digg, reddit, twitter, identi.ca. Вивантажуйте знімки вікон на служби зберігання зображень Facebook, Flickr, ipernity та Picasa та створюйте дописи з ними у відповідних групах. Створюйте відеодемонстрації і вивантажуйте їх на YouTube, Blip.tv, Vimeo та інші служби. Будь ласка, додавайте до вивантажених матеріалів мітку "KDE", щоб ці матеріали було простіше знайти, отже спростити команді KDE створенні звітів щодо поширення для 4.7 випусків програмного забезпечення KDE.
Стежити за розвитком подій у соціальних мережах можна на каналі подачі KDE. На цьому сайті будуть збиратися всі записи з identi.ca, twitter, youtube, flickr, picasaweb, блогів та багатьох інших сайтів соціальних мереж у режимі реального часу. Подачу новин можна знайти на <a href="buzz.kde.org">buzz.kde.org</a>.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/news/technology/kde_software_compilation_4_7_0_released"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/f9d9t/kde_software_compilation_470_released/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde46"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde46"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde47"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde47"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde47"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">мікрокнопки</a></span>
</div>
</p>

<h3>
Про ці оголошення щодо випуску
</h3><p>
Авторами цих оголошень щодо випусків є Algot Runeman, Dennis Nienhüser, Dominik Haumann, Jos Poortvliet, Markus Slopianka, Martin Klapetek, Nick Pantazis, Sebastian K&uuml;gler, Stuart Jarvis, Vishesh Handa, Vivek Prakash, Carl Symons та інші учасники маркетингової команди KDE та спільноти. Тут висвітлено лише основні зміни у програмного забезпеченні KDE протягом останніх шести місяців.
</p>

<h4>Підтримка KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.7.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify">За допомогою нової <a
href="http://jointhegame.kde.org/">програми підтримки членством</a> ви за 25&euro; на квартал можете підтримати міжнародну спільноту KDE у продовженні розробки вільного програмного забезпечення найкращої якості.</p>

<p>&nbsp;</p>
