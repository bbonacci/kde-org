---
aliases:
- ../../plasma-5.11.1
changelog: 5.11.0-5.11.1
date: 2017-10-17
layout: plasma
youtube: nMFDrBIA0PM
figure:
  src: /announcements/plasma/5/5.11.0/plasma-5.11.png
  class: text-center mt-4
asBugfix: true
---

- Fixed being unable to switch users from the Switch User screen. <a href="https://commits.kde.org/plasma-workspace/ac40f7dec47df9c48fa55d90be67ea4cbebcb09d">Commit.</a>
- Fixed issue that caused pinned applications in task manager to erroneously shift around. <a href="https://commits.kde.org/plasma-workspace/88dbb40ddedee4740b904e9a6f57beda80013550">Commit.</a> Fixes bug <a href="https://bugs.kde.org/385594">#385594</a>. Phabricator Code review <a href="https://phabricator.kde.org/D8258">D8258</a>
- Fixed application progress in task manager no longer working. <a href="https://commits.kde.org/plasma-desktop/a1a85f95bb487ac74ef2aa903ca09ae4ed2a125c">Commit.</a> Fixes bug <a href="https://bugs.kde.org/385730">#385730</a>. Phabricator Code review <a href="https://phabricator.kde.org/D8327">D8327</a>
