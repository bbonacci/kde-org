---
date: 2021-10-19
changelog: 5.23.0-5.23.1
layout: plasma
asBugfix: true
draft: false
---

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.mp4" poster="https://kde.org/announcements/plasma/5/5.23.0/Plasma25AE_b.png" >}}

+ Discover: Flatpak, do not crash when a source is disabled. [Commit.](http://commits.kde.org/discover/5f7350e84caf1499bb920f7eed32c3ce7973ba01)
+ KScreenLocker: Fix sleep and hibernate actions. [Commit.](http://commits.kde.org/kscreenlocker/7458926a43370cb1f7ba83c6fa87884f04d1ba6a)
+ Plasma Desktop: Fix "clear emoji history" action. [Commit.](http://commits.kde.org/plasma-desktop/839f6bc0cf42620ef5f22224b1761ee303203413) Fixes bug [#443974](https://bugs.kde.org/443974)
