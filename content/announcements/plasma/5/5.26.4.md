---
date: 2022-11-29
changelog: 5.26.3-5.26.4
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Bluedevil: Install translated documentation from po/ too. [Commit.](http://commits.kde.org/bluedevil/62d1bba67a90635817564ca4ce74da2b68981895) 
+ Discover: Prefer openining the installed version of an app. [Commit.](http://commits.kde.org/discover/4549356075552941b64ba0d1066f325a55801688) Fixes bug [#461664](https://bugs.kde.org/461664)
+ Discover Packagekit: check free space when update size changes. [Commit.](http://commits.kde.org/discover/cb421186f365ac279ff16c4aa3c7dc491bf51a64) 
