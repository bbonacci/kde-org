---
aliases:
- ../../plasma-5.7.95
changelog: 5.7.5-5.7.95
date: 2016-09-15
layout: plasma
title: 'KDE at 20: Plasma 5.8 LTS Beta. Here for the Long Term.'
figure:
  src: /announcements/plasma/5/5.8.0/plasma-5.8.png
---

{{% i18n_date %}}

Today KDE releases a beta of its first Long Term Support edition of its flagship desktop software, Plasma. This marks the point where the developers and designers are happy to recommend Plasma for the widest possible audience be they enterprise or non-techy home users. If you tried a KDE desktop previously and have moved away, now is the time to re-assess, Plasma is simple by default, powerful when needed.

## Plasma's Comprehensive Features

Take a look at what Plasma offers, a comprehensive selection of features unparalleled in any desktop software.

### Desktop Widgets

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-widgets.png" alt="Desktop Widgets " class="text-center" width="600px" caption="Desktop Widgets">}}

Cover your desktop in useful widgets to keep you up to date with weather, amused with comics or helping with calculations.

### Get Hot New Stuff

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-hotnewstuff.png" alt="Get Hot New Stuff " class="text-center" width="600px" caption="Get Hot New Stuff">}}

Download wallpapers, window style, widgets, desktop effects and dozens of other resources straight to your desktop. We work with the new <a href="http://store.kde.org">KDE Store</a> to bring you a wide selection of addons for you to install.

### Desktop Search

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-search-launch.png" alt="Desktop Search " class="text-center" width="600px" caption="Desktop Search">}}

Plasma will let you easily search your desktop for applications, folders, music, video, files... everything you have.

### Unified Look

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-toolkits.png" alt="Unified Look " class="text-center" width="600px" caption="Unified Look">}}

Plasma's default Breeze theme has a unified look across all the common programmer toolkits - Qt 4 &amp; 5, GTK 2 &amp; 3, even LibreOffice.

### Phone Integration

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-kdeconnect.png" alt="Phone Integration " class="text-center" width="600px" caption="Phone Integration">}}

Using KDE Connect you'll be notified on your desktop of text message, can easily transfer files, have your music silenced during calls and even use your phone as a remote control.

### Infinitely Customisable

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-custom.jpg" alt="Infinitely Customisable " class="text-center" width="600px" caption="Infinitely Customisable">}}

Plasma is simple by default but you can customise it however you like with new widgets, panels, screens and styles.

## New in Plasma 5.8

### Unified Boot to Shutdown Artwork

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-boot.png" alt="Unified Boot to Shutdown Artwork " class="text-center" width="600px" caption="Unified Boot to Shutdown Artwork">}}

This release brings an all-new login screen design giving you a complete Breeze startup to shutdown experience. The layout has been tidied up and is more suitable for workstations that are part of a domain or company network. While it is much more streamlined, it also allows for greater customizability: for instance, all Plasma wallpaper plugins, such as slideshows and animated wallpapers, can now be used on the lock screen.

### Right-to-Left Language Support

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-reverse.png" alt="Right-to-Left Language Support " class="text-center" width="600px" caption="Right-to-Left Language Support">}}

Support for Semitic right-to-left written languages, such as Hebrew and Arabic, has been greatly improved. Contents of panels, the desktop, and configuration dialogs are mirrored in this configuration. Plasma’s sidebars, such as widget explorer, window switcher, activity manager, show up on the right side of the screen.

### Improved Applets

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-media-controls.png" alt="Context Menu Media Controls " class="text-center" width="600px" caption="Context Menu Media Controls">}}

The virtual desktop switcher (“Pager”) and window list applets have been rewritten, using the new task manager back-end we introduced in Plasma 5.7. This allows them to use the same dataset as the task manager and improves their performance while reducing memory consumption. The virtual desktop switcher also acquired an option to show only the current screen in multi-screen setups and now shares most of its code with the activity switcher applet.

Task manager gained further productivity features in this release. Media controls that were previously available in task manager tooltips only are now accessible in the context menus as well. In addition to bringing windows to the front during a drag and drop operation, dropping files onto task manager entries themselves will now open them in the associated application. Lastly, the popup for grouped windows can now be navigated using the keyboard and text rendering of its labels has been improved.

### Simplified Global Shortcuts

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-global-shortcuts.png" alt="Global Shortcuts Setup " class="text-center" width="600px" caption="Global Shortcuts Setup">}}

Global shortcuts configuration has been simplified to focus on the most common task, that is launching applications. Building upon the jump list functionality added in previous releases, global shortcuts can now be configured to jump to specific tasks within an application.

Thanks to our Wayland effort, we can finally offer so-called “modifier-only shortcuts”, enabling you to open the application menu by just pressing the Meta key. Due to popular demand, this feature also got backported to the X11 session.

### Other improvements

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-discover.png" alt="Plasma Discover's new UI " class="text-center" width="600px" caption="Plasma Discover's new UI">}}

This release sees many bugfixes in multi-screen support and, together with Qt 5.6.1, should significantly improve your experience with docking stations and projectors.

KWin, Plasma’s window manager, now allows compositing through llvmpipe, easing the deployment on exotic hardware and embedded devices. Now that there is a standardized and widely-used interface for applications to request turning off compositing, the “Unredirect Fullscreen” option has been removed. It often lead to stability issues and because of that was already disabled for many drivers.

Now that <a href="https://dot.kde.org/2016/08/10/kdes-kirigami-ui-framework-gets-its-first-public-release">Kirigami</a>, our set of versatile cross-platform UI components, has been released, we’re pleased to bring you a revamped version of Plasma Discover based on Kirigami.

We have new default fonts, the Noto font from Google covers all scripts available in the Unicode standard while our new monospace font Hack is perfect for coders and terminal users.

### We’re in Wayland!

{{<figure src="/announcements/plasma/5/5.8.0/plasma-5.8-wayland.png" alt="Plasma on Wayland Now with GTK+ support " class="text-center" width="600px" caption="Plasma on Wayland Now with GTK+ support">}}

Plasma on Wayland has come a long way in the past months. While our long term support promise does not apply to the fast-evolving Wayland stack, we think it is ready to be tested by a broader audience. There will still be minor glitches and missing features, but we are now at a point where we can ask you to give it a try and report bugs. Notable improvements in this release include:

- Support for xdg-shell, i.e. GTK+ applications are now supported
- Much improved touch screen support
- Support for touchpad gestures – the infrastructure is there, there aren't any gestures by default yet
- The “Sliding Popups” effect is now supported
- Clipboard contents are synced between X11 and Wayland applications
