---
aliases:
- ../announce-applications-15.12.1
changelog: true
date: 2016-01-12
description: KDE toob välja KDE rakendused 15.12.1
layout: application
title: KDE toob välja KDE rakendused 15.12.1
version: 15.12.1
---
January 12, 2016. Today KDE released the first stability update for <a href='../15.12.0'>KDE Applications 15.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 30 teadaoleva veaparanduse hulka kuuluvad Kdelibsi, Kdepimi, Kdenlive'i, Marble'i, Konsooli, Spectacle'i, Akonadi, Arki, Umbrello ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.16.
