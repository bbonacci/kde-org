---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE Ships Plasma Çalışma Alanları, Uygulamaları ve Platform 4.11.
title: KDE Yazılım Derlemesi 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`KDE Plasma Çalışma Alanları 4.11` >}} <br />

14 Ağustos 2013. KDE Topluluğu, plazma Çalışma Alanları, uygulamalar ve geliştirme platformunun en son önemli güncellemelerini duyurmaktan gurur duyuyor ve yeni özellikler ve düzeltmeler sunuyor ve platformu daha fazla evrim için hazırlıyor. Plasma çalışma alanları 4.11, ekip, Frameworks 5'e teknik geçiş üzerinde durduğu için uzun vadeli destek alacaktır. Bu daha sonra çalışma alanlarının, uygulamaların ve platformun aynı sürüm numarası altında son birleşik sürümünü sunar.

This release is dedicated to the memory of <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>, a great Free and Open Source Software champion from India. Atul led the Linux Bangalore and FOSS.IN conferences since 2001 and both were landmark events in the Indian FOSS scene. KDE India was born at the first FOSS.in in December 2005. Many Indian KDE contributors started out at these events. It was only because of Atul's encouragement that the KDE Project Day at FOSS.IN was always a huge success. Atul left us on June 3rd after fighting a battle with cancer. May his soul rest in peace. We are grateful for his contributions to a better world.

Bu yayınların tümü 54 dile çevrilmiştir; KDE tarafından sonraki aylık küçük hata düzeltme sürümlerinde daha fazla dilin eklenmesini bekliyoruz. Dokümantasyon Ekibi, bu sürüm için 91 uygulama el kitabını güncelledi.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Çalışma Alanları 4.11, Kullanıcı Deneyimini İyileştirmeyi Sürdürüyor</a>

Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE uygulamaları 4.11, Kişisel Bilgi Yönetimi ve geliştirmede büyük bir adım atıyor</a>

Bu sürüm, KDE PIM yığınında çok daha iyi performans ve birçok yeni özellik sağlayan büyük iyileştirmeleri işaret ediyor. Kate, yeni eklentilerle Python ve Javascript geliştiricilerinin üretkenliğini artırıyor, Dolphin daha hızlı hale geldi ve eğitim uygulamaları çeşitli yeni özellikler getiriyor.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Daha İyi Performans Sunar</a>

KDE Platform 4.11'in bu sürümü, kararlılığa odaklanmaya devam ediyor. Gelecekteki KDE Frameworks 5.0 sürümümüz için yeni özellikler uygulanmaktadır, ancak kararlı sürüm için Nepomuk çerçevemiz için optimizasyonları sıkıştırmayı başardık.

<br />
Yükseltme yaparken, lütfen <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>sürüm notlarına</a> bir göz atın.<br />

## Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.11 releases of KDE software.

## Sürüm Partileri

As usual, KDE community members organize release parties all around the world. Several have already been scheduled and more will come later. Find <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>a list of parties here</a>. Anyone is welcome to join! There will be a combination of interesting company and inspiring conversations as well as food and drinks. It's a great chance to learn more about what is going on in KDE, get involved, or just meet other users and contributors.

We encourage people to organize their own parties. They're fun to host and open for everyone! Check out <a href='http://community.kde.org/Promo/Events/Release_Parties'>tips on how to organize a party</a>.

## Bu sürüm duyuruları hakkında

These release announcements were prepared by Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin and other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.

#### KDE'yi Destekle

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.
