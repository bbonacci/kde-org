---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE, KDE Uygulamaları 17.08.3'ü Gönderdi
layout: application
title: KDE, KDE Uygulamaları 17.08.3'ü Gönderdi
version: 17.08.3
---
November 9, 2017. Today KDE released the third stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen yaklaşık bir düzine hata düzeltmesi, diğerleri arasında Kontak, Ark, Gwenview, KGpg, KWave, Okular, Spectacle'daki iyileştirmeleri içerir.

This release also includes the last version of KDE Development Platform 4.14.38.

İyileştirmeler şunları içerir:

- Parola korumalı SMB paylaşımları ile bir Samba 4.7 regresyonunda çalışın
- Okular artık belirli rotasyon işlerinden sonra çökmüyor
- Ark, ZIP arşivlerini çıkarırken dosya değiştirme tarihlerini korur
