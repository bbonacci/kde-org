---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE Uygulamalar 15.12.'yi Gönderdi
layout: application
release: applications-15.12.0
title: KDE, KDE Uygulamalar 15.12.0'ı Gönderdi
version: 15.12.0
---
16 Aralık 2015. Bugün KDE, KDE Uygulamaları 15.12'yi yayınladı.

KDE, KDE Uygulamaları için Aralık 2015 güncellemesi olan KDE Uygulamaları 15.12'nin piyasaya sürüldüğünü duyurmaktan heyecan duyuyor. Bu sürüm, mevcut uygulamalara bir yeni uygulama, özellik eklemeleri ve hata düzeltmeleri getiriyor. Ekip, masaüstünüze ve bu uygulamalara her zaman en iyi kaliteyi getirmeye çalışmaktadır, bu nedenle geri bildiriminizi göndermeniz için size güveniyoruz.

In this release, we've updated a whole host of applications to use the new <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, including <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> and more of the KDE Games, apart from the KDE Image Plugin Interface and its support libraries. This brings the total number of applications that use KDE Frameworks 5 to 126.

### A Spectacular New Addition

14 yıl KDE'nin bir parçası olduktan sonra, KSnapshot emekli oldu ve yeni bir ekran görüntüsü uygulaması olan Spectacle ile değiştirildi.

With new features and a completely new UI, Spectacle makes taking screenshots as easy and unobtrusive as it can ever be. In addition to what you could do with KSnapshot, with Spectacle you can now take composite screenshots of pop-up menus along with their parent windows, or take screenshots of the entire screen (or the currently active window) without even starting Spectacle, simply by using the keyboard shortcuts Shift+PrintScreen and Meta+PrintScreen respectively.

We've also aggressively optimised application start-up time, to absolutely minimise the time lag between when you start the application and when the image is captured.

### Polish Everywhere

Uygulamaların çoğu, kararlılık ve hata düzeltmelerine ek olarak, bu döngüde önemli ölçüde cilalandı.

Doğrusal olmayan video düzenleyici <a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, Kullanıcı Arayüzünde büyük düzeltmeler gördü. Artık zaman çizelgenizdeki öğeleri kopyalayıp yapıştırabilir ve belirli bir parçada şeffaflığı kolayca değiştirebilirsiniz. Simge renkleri otomatik olarak ana kullanıcı arayüzü temasına göre ayarlanarak daha kolay görülmelerini sağlar.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark artık ZIP yorumlarını gösterebilir`>}}

Arşiv yöneticisi <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, artık ZIP ve RAR arşivlerine gömülü yorumları görüntüleyebilir. ZİP arşivlerindeki dosya adlarındaki Unicode karakterleri için desteği geliştirdik ve artık bozuk arşivleri algılayabilir ve onlardan mümkün olduğunca fazla veri kurtarabilirsiniz. Arşivlenmiş dosyaları varsayılan uygulamalarında da açabilirsiniz ve XML dosyalarının önizlemelerinin yanı sıra masaüstüne sürükleyip bırakmayı da düzelttik.

### All Play and No Work

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`KSudoku yeni Karşılama Ekranı (Mac OS X'te)`>}}

KDE Games geliştiricileri son birkaç aydır oyunlarımızı daha akıcı ve zengin bir deneyim için optimize etmek için sıkı bir şekilde çalışıyorlar ve bu alanda kullanıcılarımızın eğlenmesi için pek çok yeni şeyimiz var.

In <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, we've added two new sets of levels, one allowing digging while falling and one not. We've added solutions for several existing sets of levels. For an added challenge, we now disallow digging while falling in some older sets of levels.

In <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, you can now print Mathdoku and Killer Sudoku puzzles. The new multi-column layout in KSudoku's Welcome Screen makes it easier to see more of the many puzzle types available, and the columns adjust themselves automatically as the window is resized. We've done the same to Palapeli, making it easier to see more of your jigsaw puzzle collection at once.

We've also included stability fixes for games like KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> and <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, and the overall experience is now greatly improved. KTuberling, Klickety and KNavalBattle have also been updated to use the new KDE Frameworks 5.

### Important Fixes

Don't you simply hate it when your favorite application crashes at the most inconvinient time? We do too, and to remedy that we've worked very hard at fixing lots of bugs for you, but probably we've left some sneak, so don't forget to <a href='https://bugs.kde.org'>report them</a>!

In our file manager <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, we've included various fixes for stability and some fixes to make scrolling smoother. In <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, we've fixed a bothersome issue with white text on white backgrounds. In <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, we've attempted to fix a crash that was occurring on shutdown, in addition to cleaning up the UI and adding in a new cache cleaner.

The <a href='https://userbase.kde.org/Kontact'>Kontact Suite</a> has seen tons of added features, big fixes and performance optimisations. In fact, there's been so much development this cycle that we've bumped the version number to 5.1. The team is hard at work, and is looking forward to all your feedback.

### Moving Forward

As part of the effort to modernise our offerings, we've dropped some applications from KDE Applications and are no longer releasing them as of KDE Applications 15.12

We've dropped 4 applications from the release - Amor, KTux, KSnapshot and SuperKaramba. As noted above, KSnapshot has been replaced by Spectacle, and Plasma essentially replaces SuperKaramba as a widget engine. We've dropped standalone screensavers because screen locking is handled very differently in modern desktops.

We've also dropped 3 artwork packages (kde-base-artwork, kde-wallpapers and kdeartwork); their content had not changed in long time.

### Full Changelog
