---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE, KDE Uygulamaları 18.04.1'i Gönderdi
layout: application
title: KDE, KDE Uygulamaları 18.04.1'i Gönderdi
version: 18.04.1
---
May 10, 2018. Today KDE released the first stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen yaklaşık 20 hata düzeltmesi, diğerleri arasında Kontak, Cantor, Dolphin, Gwenview, JuK, Okular, Umbrello için iyileştirmeler içerir.

İyileştirmeler şunları içerir:

- Dolphin'in yerler panelindeki yinelenen girişler artık çökmelere neden olmuyor
- Gwenview'da SVG dosyalarını yeniden yüklemeyle ilgili eski bir hata düzeltildi
- Umbrello's C++ import now understands the 'explicit' keyword
