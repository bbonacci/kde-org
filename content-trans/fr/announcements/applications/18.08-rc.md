---
aliases:
- ../announce-applications-18.08-rc
date: 2018-08-03
description: KDE publie la version candidate 18.08 des applications de KDE.
layout: application
release: applications-18.07.90
title: KDE publie la version candidate des applications de KDE en version 18.08
---
03 Août 2018. Aujourd'hui, KDE publie la version candidate des nouvelles versions des applications KDE. Les dépendances et les fonctionnalités sont figées. L'attention de l'équipe KDE se porte à présent sur la correction des bogues et les dernières finitions.

Veuillez vérifier les <a href='https://community.kde.org/Applications/18.08_Release_Notes'>notes de mises à jour de la communauté</a> pour plus d'informations sur les nouveaux fichiers compressés et les problèmes connus. Une annonce plus complète sera disponible avec la mise à jour finale.

Les versions des applications de KDE 18.08 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version candidate et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.
