---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE publie les applications de KDE en version 18.08.0
layout: application
title: KDE publie les applications de KDE en version 18.08.0
version: 18.08.0
---
16 Août 2018. Les applications de KDE 18.08.0 ont été mises à jour.

Nous travaillons de façon continue sur l'amélioration des logiciels dont la série d'applications de KDE. Nous espérons que vous trouverez toutes les nouvelles améliorations et les corrections de bogues utiles ! 

### Quoi de neuf dans les applications de KDE 18.08

#### Système

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, le puissant gestionnaire de fichiers de KDE, a reçu des améliorations variées concernant sa qualité de fonctionnement.

- La boîte de dialogue « Configuration » a été remaniée pour mieux s'adapter à nos exigences de conception et et être plus intuitive.
- De nombreuses fuites de mémoire pouvant ralentir votre ordinateur ont été corrigées.
- Les éléments de menus « Créer Nouveau » ne sont plus disponibles lors de l'affichage de la corbeille.
- L'application maintenant s'adapte mieux aux écrans de haute résolution.
- Le menu contextuel intègre maintenant plus d'options utiles, vous permettant de trier et de modifier directement le mode d'affichage.
- Le tri par date de modification est maintenant 12 fois plus rapide. De plus, vous pouvez maintenant de nouveau lancer Dolphin lorsque vous êtes connecté en utilisant le compte utilisateur superutilisateur. La prise en charge de la modification des fichiers appartenant à la racine lors de l'exécution de Dolphin en tant qu'utilisateur normal est toujours en cours.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Les multiples améliorations de <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, l'application d'émulation de terminale de KDE sont disponibles :

- Le composant graphique « Rechercher » apparaîtra maintenant sur le haut de la fenêtre dans perturber votre processus de travail. 
- La prise en charge de plus de séquences d'échappement (DECSCUSR & Mode de défilement alternatif XTerm) a été ajouté.
- Vous pouvez maintenant associer n'importe quel caractère comme clé pour un raccourci.

#### Graphiques

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

La version 18.08 est une version majeure pour <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, l'afficheur et l'organisateur d'images de KDE. Dans les derniers mois, les contributeurs ont travaillé sur de nombreuses améliorations. Les principales en sont :

- La barre d'état de Gwenview propose maintenant un compteur d'images et affiche le nombre total d'images.
- Il est maintenant possible de trier par notation et en ordre décroissant. Le tri par date sépare maintenant les dossiers et les archives et a été corrigé pour certaines situations.
- La prise en charge du glisser-déposer a été améliorée pour permettre de faire glisser les fichiers et les dossiers vers le mode d'affichage et ainsi de pouvoir les afficher, ainsi que de faire glisser les éléments affichés vers des applications externes.
- Le collage d'images copiées à partir de Gwenview fonctionne désormais également pour les applications qui n'acceptent que des données d'image brutes, mais pas de chemin d'accès au fichier. La copie d'images modifiées est également prise en charge.
- La boîte de dialogue pour le redimensionnement de l'image a été remaniée pour une plus grande ergonomie et pour l'ajout d'une option pour le redimensionnement des images selon un pourcentage.
- Le curseur de taille et le réticule pour l'outil de réduction de l'effet « Yeux rouges » ont été corrigés.
- La sélection d'un arrière-plan transparent possède maintenant une option « Aucun » et peut être configurée aussi pour les objets « svg ».

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

Le zoom sur une image est devenu plus pratique :

- Zoom activé par le défilement ou par un clic, de même pour faire un panorama, même quand les outils de rognage ou de réduction « Yeux rouges » sont actifs. 
- Un clic central permet de nouveau le basculement entre le zoom d'adaptation à la page et le zoom à 100 %.
- Ajout de raccourcis de clavier « Clic central » + « Maj » et « Maj » + « F » pour basculer le zoom pour la fonction « Remplir ».
- Le « CTRL » + clic réalise maintenant un zoom plus rapide et plus fiable.
- Gwenview permet maintenant de zoomer vers la position courante du curseur pour les opérations de zooms Avant / Arrière et de zoom à 100 %, lors de l'utilisation des raccourcis de souris et de clavier.

Le mode de comparaison d'images a reçu plusieurs améliorations :

- Correction de l'alignement et de la taille de la mise en surbrillance pour la sélection
- Correction de la surcharge « SVG » lors de la mise en surbrillance de la sélection
- Pour les petites images « svg », la mise en surbrillance de la sélection correspond à la taille de l'image.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

Un nombre de plus petites améliorations a été apporté pour rendre vos processus de travail encore plus agréables :

- Améliorations des transitions en fondu entre les images de tailles et de transparences variées.
- Correction de la visibilité des icônes dans certains boutons flottants quand un thème clair de couleurs est utilisé.
- Lors de l'enregistrement d'une image sous un nouveau nom, l'afficheur ne se positionne pas ensuite sur une image sans rapport. 
- Lorsque le bouton de partage est cliqué et que les kipi-plugins ne sont pas installés, Gwenview invite l'utilisateur à les installer. Après l'installation, ils sont immédiatement affichés.
- La barre latérale empêche maintenant de réaliser un masquage accidentellement lors du redimensionnement et se souvient de sa largeur.

#### Bureau

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, le puissant client de messagerie de KDE, présente quelques améliorations dans le moteur d'extraction des données de voyage. Il prend désormais en charge les codes-barres des billets de train UIC 918.3 et SNCF et la recherche de l'emplacement des gares enregistrées dans la base de données Wikidata. La prise en charge des itinéraires multi-voyageurs a été ajoutée, et KMail est désormais intégré à l'application KDE Itinerary.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, le moteur de gestion des informations personnelles, est désormais plus rapide grâce aux charges utiles de notification et offre un support XOAUTH pour SMTP, permettant une authentification native avec Gmail.

#### Éducation

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, une interface de KDE pour les logiciels de mathématiques, enregistre maintenant l'état des tableaux de bords (\"Variables\", \"Aide\", etc.) séparément pour chaque session. Les sessions de Julia sont devenues plus rapides à créer.

L'ergonomie de <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, la calculatrice graphique, a été améliorée de façon significative pour les périphériques tactiles.

#### Utilitaires

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Les contributeurs à <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>,l'outil polyvalent de copie d'écran, se sont attachés à l'amélioration du mode « Région rectangulaire » :

- En mode « Région rectangulaire », il y a maintenant une loupe pour vous aider à dessiner un rectangle de sélection parfaitement adapté aux pixels.
- Vous pouvez maintenant déplacer et re-dimensionner le rectangle de sélection par utilisation du clavier.
- L'interface utilisateur suit maintenant le thème de couleurs de l'utilisateur et la présentation du texte d'aide a été améliorée.

Pour faciliter le partage de vos copies d'écran avec d'autres personnes, les liens des images partagées sont désormais automatiquement copiés dans le presse-papiers. Les copies d'écran peuvent désormais être enregistrées automatiquement dans des sous-dossiers spécifiés par l'utilisateur.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, l'enregistrement vidéo de la caméra, a été mis à jour pour éviter des plantage avec les nouvelles versions de GStreamer.

### Éradication des bogues

Plus de 120 corrections de bogues ont été apportées dans les applications dont la suite Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle, Umbrello et bien d'autres !

### Journal complet des changements
