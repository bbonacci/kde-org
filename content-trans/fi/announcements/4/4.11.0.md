---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.
title: KDE-ohjelmistokokonaisuus 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`KDE Plasma -työtilat 4.11` >}} <br />

August 14, 2013. The KDE Community is proud to announce the latest major updates to the Plasma Workspaces, Applications and Development Platform delivering new features and fixes while readying the platform for further evolution. The Plasma Workspaces 4.11 will receive long term support as the team focuses on the technical transition to Frameworks 5. This then presents the last combined release of the Workspaces, Applications and Platform under the same version number.<br />

Tämä julkaisu on omistettu <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul ’toolz’ Chitnisin</a> muistolle. Hän oli merkittävä vapaan ja avoimen lähdekoodin puolustaja Intiasta. Atul johti Linux Bangalore ja FOSS.IN-konferensseja vuodesta 2001. Ne olivat molemmat virstanpylvästapahtumia intialaisissa VALO (FOSS) -piireissä. KDE India syntyi ensimmäisessa FOSS.in-tapahtumassa joulukuussa 2005. Monet KDE:n intialaiset tekijät alkoivat silloin osallistua KDE:n toimintaan. FOSS.INin KDE Project Day (suomennettuna KDE-projektin päivä) onnistui Atulin tuen ansiosta. Atul nukkui pois 3. kesäkuuta taisteltuaan syöpää vastaan. Olemme kiitollisia hänen avustaan paremman maailman puolesta. Levätköön hänen sielunsa rauhassa.

Nämä julkaisut on käännetty 54 kielelle. Kielten määrän odotetaan lisääntyvän seuraavissa kuukausittaisissa korjausjulkaisuissa. Dokumentaatiotiimi päivitti 91 sovelluksen käyttöohjeet tähän julkaisuun.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Valmistautuen pitkäaikaiseen ylläpitöön Plasma-työtilat parantaa perustoimintoja edelleen: julkaisussa on mukana jouhevampi tehtäväpalkki, älykkäämpi akkusovelma ja parannettu äänimikseri. KScreenin käyttöönotto tekee useiden näyttöjen hoitamisesta fiksumpaa, ja mittavat suorituskykyparannukset yhdessä pienten käytettävyysparannusten kanssa tekevät käyttökokemuksesta paremman.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

Tätä julkaisua leimaa runsaat parannukset KDE:n PIM-pinossa (henkilökohtaisten tietojen hallinnassa). Niiden ansiosta sen suorituskyky on nyt paljon parempi ja siinä on paljon uusia ominaisuuksia. Kate parantaa Python- ja Javascript-kehittäjien tuottavuutta uusilla liitännäisillä. Dolphin nopeutui, ja opetusohjelmissa on monia uusia ominaisuuksia.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

Tämä KDE-ohjelmistoalustan 4.11-julkaisu keskittyy edelleen vakauteen. Uusia ominaisuuksia toteutetaan parhaillaan myöhemmin tulossa olevaan KDE Frameworks 5.0 -julkaisuun, mutta onnistuimme mahduttamaan vakaaseen julkaisuumme optimointeja Nepomuk-sovelluskehykseemme.

<br />
When upgrading, please observe the <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>release notes</a>.<br />

## Levitä sanaa ja seuraa, mitä on meneillään: käytä tunnistetta ”KDE”

KDE rohkaisee ihmisiä levittämään sanaa sosiaalisessa mediassa. Kirjoita juttuja uutissivustoille tai käytä kanavia kuten Delicious, Digg, reddit, Twitter ja identi.ca. Lähetä kuvankaappauksia esimerkiksi Facebookiin, Flickriin, ipernityyn ja Picasaan, sekä lähetä ne sopiville ryhmille. Luo esittelyvideoita ja lähetä ne Youtubeen, Blip.tv:hen sekä Vimeoon. Lisääthän kirjoituksiisi ja lähettämiisi aineistoihin tunnisteen (tag) ”KDE”. Sen lisääminen helpottaa löytämistä, ja antaa KDE:n promoryhmälle keinon analysoida KDE-ohjelmien 4.11-julkaisujen näkyvyyttä.

## Julkaisujuhlat

KDE-yhteisön jäsenet järjestävät tapansa mukaan julkaisujuhlia ympäri maailmaa. Useiden juhlien ajankohta on jo päätetty ja lisää on tulossa myöhemmin. <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>Juhlaluettelon löydät tästä</a>. Kaikki ovat tervetulleita! Luvassa on mielenkiintoista seuraa ja innostavia keskusteluja sekä ruokaa ja juomaa. Juhlat ovat oiva tilaisuus tutustua paremmin siihen, mitä KDE:ssa on meneillään, osallistua toimintaan tai ihan vain tutustua muihin käyttäjiin ja tekijöihin.

Rohkaisemme ihmisiä järjestämään omia juhlia. Niiden järjestäminen on hauskaa ja sallittua kaikille. Katso <a href='http://community.kde.org/Promo/Events/Release_Parties'>vinkit juhlan järjestämiseen</a>.

## Tietoa näistä julkaisutiedotteista

Nämä julkaisutiedotteet valmistelivat Jos Poortvliet, Sebastien Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin ja muut KDE:n promoryhmän jäsenet sekä laajempi KDE-yhteisö. Ne kattavat kohokohdat muutoksista, jotka KDE-ohjelmiin on tehty kuuden viime kuukauden aikana.

#### Tue KDE:tä

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.
