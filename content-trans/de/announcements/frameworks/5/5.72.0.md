---
aliases:
- ../../kde-frameworks-5.72.0
date: 2020-07-04
layout: framework
libCount: 70
---
### KDAV : New Module

DAV protocol implementation with KJobs.

Calendars and todos are supported, using either GroupDAV or CalDAV, and contacts are supported using GroupDAV or CardDAV.

### Baloo

+ [Indexers] Ignore name-based mimetype for initial indexing decisions (bug 422085)

### BluezQt

+ Expose the service advertisement data of a device

### Breeze Icons

+ Fixed three 16px icons for application-x-... to be more pixel-perfect (bug 423727)
+ Add support for &gt;200% scale for icons which should stay monochrome
+ Cut out center of window-close-symbolic
+ cervisia app and action icon update
+ Add README section about the webfont
+ Use grunt-webfonts instead of grunt-webfont and disable generation for symbolic linked icons
+ add kup icon from kup repo

### Extra CMake-Module

+ Remove support for png2ico
+ Deal with Qt's CMake code modifying CMAKE_SHARED_LIBRARY_SUFFIX
+ Add FindTaglib find module

### KDE Doxygen Tools

+ Support "repo_id" in metainfo.yaml to override guessed repo name

### KCalendarCore

+ Check for write error in save() if the disk is full (bug 370708)
+ Correct icon names for recurring to-dos
+ Fix serialization of recurring to-do's start date (bug 345565)

### KCMUtils

+ Fix changed signal for plugin selector

### KCodecs

+ Add some n otherwise text can be very big in messagebox

### KConfig

+ Also pass locationType to KConfigSkeleton when construction from group
+ Make "Switch Application Language..." text more consistent

### KConfigWidgets

+ Make "Switch Application Language..." text more consistent

### KCoreAddons

+ KRandom::random -&gt; QRandomGenerator::global()
+ Deprecate KRandom::random

### KCrash

+ Add missing declaration of environ, otherwise available only on GNU

### KDocTools

+ Use consistent style for FDL notice (bug 423211)

### KFileMetaData

+ Adapt kfilemetadata to "audio/x-speex+ogg" as recently changed in shared-mime-info

### KI18n

+ Also add quotes around rich text &lt;filename&gt; tagged text

### KIconThemes

+ Drop assignment in resetPalette
+ Return QPalette() if we have no custom palette
+ Respect QIcon::fallbackSearchpaths() (bug 405522)

### KIO

+ [kcookiejar] Fix reading "Accept For Session" cookie setting (bug 386325)
+ KDirModel: fix hasChildren() regression for trees with hidden files, symlinks, pipes and character devices (bug 419434)
+ OpenUrlJob: fix support for shell scripts with a space in the filename (bug 423645)
+ Add webshortcut for DeepL and ArchWiki
+ [KFilePlacesModel] Show AFC (Apple File Conduit) devices
+ Also encode space characters for webshortcut URLs (bug 423255)
+ [KDirOperator] Actually enable dirHighlighting by default
+ [KDirOperator] Highlight the previous dir when going back/up (bug 422748)
+ [Trash] Handle ENOENT error when renaming files (bug 419069)
+ File ioslave: set nano sec timestamp when copying (bug 411537)
+ Adjust URLs for Qwant searchproviders (bug 423156)
+ File ioslave: Add support for reflink copying (bug 326880)
+ Fix setting of default shortcut in webshortcuts KCM (bug 423154)
+ Ensure readability of webshortcuts KCM by setting minimum width (bug 423153)
+ FileSystemFreeSpaceJob: emit error if the kioslave didn't provide the metadata
+ [Trash] Remove trashinfo files referencing files/dirs that don't exist (bug 422189)
+ kio_http: Guess the mime-type ourselves if server returns application/octet-stream
+ Deprecate totalFiles and totalDirs signals, not emitted
+ Fix reloading of new web shortcuts (bug 391243)
+ kio_http: Fix status of rename with overwriting enabled
+ Do not interpret name as hidden file or file path (bug 407944)
+ Do not display deleted/hidden webshortcuts (bug 412774)
+ Fix crash when deleting entry (bug 412774)
+ Do not allow to assign existing shortcuts
+ [kdiroperator] Use better tooltips for back and forward actions (bug 422753)
+ [BatchRenameJob] Use KJob::Items when reporting the progress info (bug 422098)

### Kirigami

+ [aboutpage] Use OverlaySheet for license text
+ fix collapsed mode icons
+ Use light separators for DefaultListItemBackground
+ Add weight property to Separator
+ [overlaysheet] Avoid fractional height for contentLayout (bug 422965)
+ Fix pageStack.layers docs
+ Reintroduce layer support to Page.isCurrentPage
+ support icon color
+ use the internal component MnemonicLabel
+ Reduce global toolbar left padding when title is not used
+ Only set implicit{Width,Height} for Separator
+ Update KF5Kirigami2Macros.cmake to use https with git repo in order to avoid error when trying to access
+ Fix: avatar loading
+ Make PageRouterAttached#router WRITE-able
+ Fix OverlaySheet closing when clicking inside layout (bug 421848)
+ Add missing id to GlobalDrawerActionItem in GlobalDrawer
+ Fix OverlaySheet being too tall
+ Fix PlaceholderMessage code example
+ render border at the bottom of groups
+ use an LSH component
+ Add background to headers
+ Better collapsing handling
+ Better presentation for list header items
+ Add bold property to BasicListItem
+ Fix Kirigami.Units.devicePixelRatio=1.3 when it should be 1.0 at 96dpi
+ Hide OverlayDrawer handle when not interactive
+ Adjust ActionToolbarLayoutDetails calculations to make better use of screen real estate
+ ContextDrawerActionItem: Prefer text property over tooltip

### KJobWidgets

+ Integrate the KJob::Unit::Items (bug 422098)

### KJS

+ Fix crash when using KJSContext::interpreter

### KNewStuff

+ Move explanatory text from sheet header to list header
+ Fix paths for install script and uncompression
+ Hide the ShadowRectangle for non-loaded previews (bug 422048)
+ Don't allow content to overflow in the grid delegates (bug 422476)

### KNotification

+ Don't use notifybysnore.h on MSYS2

### KParts

+ Deprecate PartSelectEvent and co

### KQuickCharts

+ Elide value Label of LegendDelegate when there isn't enough width
+ Fix for error "C1059: non constant expression ..."
+ Account for line width when bounds checking
+ Don't use fwidth when rendering line chart lines
+ Rewrite removeValueSource so it doesn't use destroyed QObjects
+ Use insertValueSource in Chart::appendSource
+ Properly initialise ModelHistorySource::{m_row,m_maximumHistory}

### KRunner

+ Fix RunnerContextTest to not assume presence of .bashrc
+ Use embedded JSON metadata for binary plugins &amp; custom for D-Bus plugins
+ Emit queryFinished when all jobs for current query have finished (bug 422579)

### KTextEditor

+ Make "goto line" work backwards (bug 411165)
+ fix crash on view deletion if ranges are still alive (bug 422546)

### KWallet Framework

+ Introduce three new methods that return all "entries" in a folder

### KWidgetsAddons

+ Fix KTimeComboBox for locales with unusual characters in formats (bug 420468)
+ KPageView: remove invisible pixmap on right side of header
+ KTitleWidget: move from QPixmap property to QIcon property
+ Deprecate some KMultiTabBarButton/KMultiTabBarTab API using QPixmap
+ KMultiTabBarTab: make styleoption state logic follow QToolButton even more
+ KMultiTabBar: do not display checked buttons in QStyle::State_Sunken
+ [KCharSelect] Initially give focus to the search lineedit

### KWindowSystem

+ [xcb] Send correctly scaled icon geometry (bug 407458)

### KXMLGUI

+ Use kcm_users instead of user_manager
+ Use new KTitleWidget::icon/iconSize API
+ Move "Switch Application Language" to Settings menu (bug 177856)

### Plasma Framework

+ Show clearer warning if the requested KCM could not be found
+ [spinbox] Don't use QQC2 items when we should use PlasmaComponents (bug 423445)
+ Plasma components: restore lost color control of TabButton label
+ Introduce PlaceholderMessage
+ [calendar] Reduce month label's size
+ [ExpandableListItem] Use same logic for action and arrow button visibility
+ [Dialog Shadows] Port to KWindowSystem shadows API (bug 416640)
+ Symlink widgets/plasmoidheading.svgz in breeze light/dark
+ Fix Kirigami.Units.devicePixelRatio=1.3 when it should be 1.0 at 96dpi
+ Add property to access the ExpandableListItem loader's item

### Prison

+ Deprecate AbstractBarcode::minimumSize() also for the compiler

### Purpose

+ Explicitly use Kirigami units instead of implicitly using Plasma units
+ Port job dialogs to Kirigami.PlaceholderMessage

### QQC2StyleBridge

+ Set selectByMouse to true for SpinBox
+ Fix menu icons becoming blurry with some font sizes (bug 423236)
+ Prevent delegates and scrollbar overlapping in combobox popups
+ Fix Slider vertical implicitWidth and a binding loop
+ Make ToolTips more consistent with Breeze widget style tooltips
+ Set editable to true by default for SpinBox
+ Fix Connections warnings in Menu.qml

### Solid

+ Enable UDisks2 backend on FreeBSD unconditionally

### Sonnet

+ Fix "Using QCharRef with an index pointing outside the valid range of a QString"
+ Restore default auto-detect behavior
+ Fix default language (bug 398245)

### Syndication

+ Fix a couple BSD-2-Clause license headers

### Syntax Highlighting

+ CMake: Updates for CMake 3.18
+ Add Snort/Suricata language highlighting
+ Add YARA language
+ remove deprecated binary json in favor of cbor
+ JavaScript/TypeScript: highlight tags in templates

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
