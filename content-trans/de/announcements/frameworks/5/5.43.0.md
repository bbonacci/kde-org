---
aliases:
- ../../kde-frameworks-5.43.0
date: 2018-02-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### New modules

KHolidays: Holiday calculation library

This library provides a C++ API that determines holiday and other special events for a geographical region.

Purpose: Offers available actions for a specific purpose

This framework offers the possibility to create integrate services and actions on any application without having to implement them specifically. Purpose will offer them mechanisms to list the different alternatives to execute given the requested action type and will facilitate components so that all the plugins can receive all the information they need.

### Baloo

- balooctl status: Produce parseable output
- Fix KIO Slave tagged folder deep copies. This breaks listing tagged folders in the tag tree, but is better than broken copies
- Skip queueing newly unindexable files and remove them from the index immediately
- Delete newly unindexable moved files from the index

### Breeze Icons

- Add missing Krusader icons for folder sync (bug 379638)
- Update list-remove icon with - instead cancel icon (bug 382650)
- add icons for pulsaudio plasmoid (bug 385294)
- use everywhere the same opacity 0.5
- New virtualbox icon (bug 384357)
- make weather-fog day/night neutral (bug 388865)
- actually install the new animations context
- QML file mime look now the same in all sizes (bug 376757)
- Update animation icons (bug 368833)
- add emblem-shared colored icon
- Fix broken index.theme files, "Context=Status" was missing in status/64
- Remove 'executable' permission from .svg files
- Action icon download is linked to edit-download (bug 382935)
- Update Dropbox systemtray icon theme (bug 383477)
- Missing emblem-default-symbolic (bug 382234)
- Type in mimetype filename (bug 386144)
- Use a more specific octave logo (bug 385048)
- add vaults icons (bug 386587)
- scalle px status icons (bug 386895)

### Extra CMake-Module

- FindQtWaylandScanner.cmake: Use qmake-query for HINT
- Make sure to search for Qt5-based qmlplugindump
- ECMToolchainAndroidTest doesn't exist anymore (bug 389519)
- Don't set the LD_LIBRARY_PATH in prefix.sh
- Add FindSeccomp to find-modules
- Fall back to language name for translations lookup if locale name fails
- Android: Add more includes

### KAuth

- Fix linking regression introduced in 5.42.

### KCMUtils

- Adds tooltips to the two buttons on each entry

### KCompletion

- Fix incorrect emission of textEdited() by KLineEdit (bug 373004)

### KConfig

- Use Ctrl+Shift+, as the standard shortcut for "Configure &lt;Program&gt;"

### KCoreAddons

- Match also spdx keys LGPL-2.1 &amp; LGPL-2.1+
- Use the much faster urls() method from QMimeData (bug 342056)
- Optimize inotify KDirWatch backend: map inotify wd to Entry
- Optimize: use QMetaObject::invokeMethod with functor

### KDeclarative

- [ConfigModule] Re-use QML context and engine if any (bug 388766)
- [ConfigPropertyMap] Add missing include
- [ConfigPropertyMap] Don't emit valueChanged on initial creation

### KDED

- Don't export kded5 as a CMake target

### KDELibs 4 Support

- Refactor Solid::NetworkingPrivate to have a shared and platform specific implementation
- Fix mingw compile error "src/kdeui/kapplication_win.cpp:212:22: error: 'kill' was not declared in this scope"
- Fix kded dbus name in solid-networking howto

### KDesignerPlugin

- Make kdoctools dependency optional

### KDESU

- Make KDESU_USE_SUDO_DEFAULT mode build again
- Make kdesu work when PWD is /usr/bin

### KGlobalAccel

- Use cmake function 'kdbusaddons_generate_dbus_service_file' from kdbusaddons to generate dbus service file (bug 382460)

### KDE GUI Addons

- Fix linking of created QCH file into QtGui docs

### KI18n

- Fix finding libintl when "cross"-compiling native Yocto packages

### KInit

- Fix cross-compiling with MinGW (MXE)

### KIO

- Repair copying file to VFAT without warnings
- kio_file: skip error handling for initial perms during file copy
- kio_ftp: don't emit error signal before we tried all list commands (bug 387634)
- Performance: use the destination KFileItem object to figure out of it's writable instead of creating a KFileItemListProperties
- Performance: Use the KFileItemListProperties copy constructor instead of the conversion from KFileItemList to KFileItemListProperties. This saves re-evaluating all items
- Improve error handling in file ioslave
- Remove PrivilegeExecution job flag
- KRun: allow executing "add network folder" without confirmation prompt
- Allow to filter places based on alternative application name
- [Uri Filter Search Provider] Avoid double delete (bug 388983)
- Fix overlap of the first item in KFilePlacesView
- Temporarily disable KAuth support in KIO
- previewtest: Allow specifying the enabled plugins
- [KFileItem] Use "emblem-shared" for shared files
- [DropJob] Enable drag and drop in a read-only folder
- [FileUndoManager] Enable undoing changes in read-only folders
- Add support for privilege execution in KIO jobs (temporarily disabled in this release)
- Add support for sharing file descriptor between file KIO slave and its KAuth helper
- Fix KFilePreviewGenerator::LayoutBlocker (bug 352776)
- KonqPopupMenu/Plugin can now use the X-KDE-RequiredNumberOfUrls key to require a certain number of files to be selected before being shown
- [KPropertiesDialog] Enable word wrap for checksum description
- Use cmake function 'kdbusaddons_generate_dbus_service_file' from kdbusaddons to generate dbus service file (bug 388063)

### Kirigami

- support for ColorGroups
- no click feedback if the item doesn't want mouse events
- work around for apps that use listitems incorrectly
- space for the scrollbar (bug 389602)
- Provide a tooltip for the main action
- cmake: Use the official CMake variable for building as a static plugin
- Update human-readable tier designation in API dox
- [ScrollView] Scroll one page with Shift+wheel
- [PageRow] Navigate between levels with mouse back/forward buttons
- Ensure DesktopIcon paints with the correct aspect ratio (bug 388737)

### KItemModels

- KRearrangeColumnsProxyModel: fix crash when there's no source model
- KRearrangeColumnsProxyModel: reimplement sibling() so it works as expected

### KJobWidgets

- Code de-duplication in byteSize(double size) (bug 384561)

### KJS

- Make kdoctools dependency optional

### KJSEmbed

- Unexport kjscmd
- Make kdoctools dependency optional

### KNotification

- The "Run Command" notification action has been fixed (bug 389284)

### KTextEditor

- Fix: View jumps when Scroll past end of document is enabled (bug 306745)
- Use at least the requested width for the argument hint tree
- ExpandingWidgetModel: find the right-most column based on location

### KWidgetsAddons

- KDateComboBox: fix dateChanged() not emitted after typing a date (bug 364200)
- KMultiTabBar: Fix regression in conversion to new style connect()

### Plasma Framework

- Define property in Units.qml for the Plasma styles
- windowthumbnail: Fix the GLXFBConfig selection code
- [Default Tooltip] Fix sizing (bug 389371)
- [Plasma Dialog] Call window effects only if visible
- Fix one source of log spam referenced in Bug 388389 (Empty filename passed to function)
- [Calendar] Adjust the calendar toolbar anchors
- [ConfigModel] Set QML context on ConfigModule (bug 388766)
- [Icon Item] Treat sources starting with a slash as local file
- fix RTL appearance for ComboBox (bug 387558)

### QQC2StyleBridge

- Add BusyIndicator to the styled controls list
- remove flicker when hovering scrollbar

### Solid

- [UDisks] Only ignore non-user backing file if it is known (bug 389358)
- Storage devices mounted outside of /media, /run/media, and $HOME are now ignored, as well as Loop Devices whose (bug 319998)
- [UDisks Device] Show loop device with their backing file name and icon

### Sonnet

- Find Aspell dictionaries on Windows

### Syntax Highlighting

- Fix C# var regex
- Support for underscores in numeric literals (Python 3.6) (bug 385422)
- Highlight Khronos Collada and glTF files
- Fix ini highlighting of values containing ; or # characters
- AppArmor: new keywords, improvements &amp; fixes

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
