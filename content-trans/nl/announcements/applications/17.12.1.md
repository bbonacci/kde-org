---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE stelt KDE Applicaties 17.12.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.12.1 beschikbaar
version: 17.12.1
---
11 januari 2018. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../17.12.0'>KDE Applicaties 17.12</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Ongeveer 20 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Dolphin, Filelight, KGet, Okteta, Umbrello, naast andere.

Verbeteringen bevatten:

- Verzenden van e-mails in Kontact is gerepareerd voor bepaalde SMTP-servers
- Tijdlijn en tags zoeken in Gwenview is verbeterd
- JAVA importeren is gerepareerd in het hulpmiddel Umbrello UML diagram
