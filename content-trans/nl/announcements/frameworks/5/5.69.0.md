---
aliases:
- ../../kde-frameworks-5.69.0
date: 2020-04-05
layout: framework
libCount: 70
---
### Baloo

- [SearchStore] gecategoriseerde logging gebruiken
- [QueryParser] gebroken detectie van eind quote repareren
- [EngineQuery] toString(Term) overladen voor QTest leveren
- [EngineQuery] Ongebruikt positielid verwijderen, testen uitbreiden
- [SearchStore] Lange regels en nesten van functies vermijden
- [baloosearch] Stop eerder als gespecificeerde map niet geldig is
- [MTimeDB] Code voor behandeling van tijdinterval consolideren
- [AdvancedQueryParser] Testen of frasen tussen quotes juist worden doorgegeven
- [Term] toString(Term) overload voor QTest leveren
- [ResultIterator] onnodige SearchStore forwarddeclaratie verwijderen
- [QueryTest] testgeval van frase gegevensgedreven maken en uitbreiden
- [Inotify] Start de MoveFrom verlooptimer hoogstens één keer per inotify-batch
- [UnindexedFileIndexer] bestand alleen voor inhoud indexering markeren indien nodig
- [Inotify] Roep QFile::decode slechts aan op een enkele plaats
- [QML] Waak op de juiste manier op registratie ongedaan maken
- [FileIndexScheduler] Voortgang van inhoud indexeren vaker bijwerken
- [FileIndexerConfig] configuratie QString,bool paar vervangen door toegekende klasse
- [QML] Stel de resterende tijd in de monitor meer betrouwbaar in
- [TimeEstimator] Batchgrootte corrigeren, verwijzing naar configuratie verwijderen
- [FileIndexScheduler] status van wijziging LowPowerIdle uitzenden
- [Debug] Leesbaarheid van positie-informatie debugformaat verbeteren
- [Debug] Uitvoer van *::toTestMap() corrigeren, stilte geen fout
- [WriteTransactionTest] alleen verwijdering positie testen
- [WriteTransaction] testcase van positie uitbreiden
- [WriteTransaction] Aangroeien van m_pendingOperations tweemaal bij vervangen vermijden
- [FileIndexScheduler] afhandeling van opschonen firstRun
- [StorageDevices] Volgorde vanf melding verbinden en initialisatie repareren
- [Config] Verwijder/keur af disableInitialUpdate

### Breeze pictogrammen

- Gebroken symbolische koppelingen repareren
- Hoekvouw naar rechtsboven verplaatsen in 24 pictogrammen
- find-location een vergrootglas op een kaart laten tonen, om anders te zijn dan mark-location (bug 407061)
- 16px LibreOffice-pictogrammen toevoegen
- Configureren repareren wanneer xmllint niet aanwezig is
- stijlsheet koppelen in 8 pictogrammen repareren
- Enige stijlsheetkleuren in 2 pictogrambestanden repareren
- Symbolische koppelingen naar onjuiste pictogramgrootte repareren
- Invoer-kiespad en voicemailaanroep toevoegen
- buho-pictogram toevoegen
- Calindori pictogram in de nieuwe pm-stijl toevoegen
- nota-pictogram toevoegen
- [breeze-icons] schaduw in enige gebruikerspictogrammen (applets/128) repareren
- Inkomende/gemiste/uitgaande aanroepen toevoegen
- sed van busybox behandelen zoals sed van GNU
- Transmissie-tray-pictogram toevoegen
- Pixeluitlijning en marges van keepassxc systeemvakpictogrammen verbeteren
- Terugdraaien van "[breeze-icons] pictogrammen in systeemvak van telegram-bureaublad toevoegen"
- Kleine pictogrammen voor KeePassXC toevoegen
- [breeze-icons] TeamViewer-systeemvakpictogrammen toevoegen
- Reset-van-bewerken toevoegen
- Stijl van document-terugdraaien wijzigen om meer te lijken op bewerken ongedaan maken
- Pictogrammen voor categorieën van emoji
- Flameshot-systeemvakpictogrammen toevoegen

### KAuth

- Type naamruimte vereiste repareren

### KBookmarks

- KBookmarksMenu ontkoppelen van KActionCollection

### KCalendarCore

- Terugvallen naar laden vCalendar bij falen van laden van iCalendar repareren

### KCMUtils

- naar passiveNotificationRequested luisteren
- workaround om nooit toepassingsitem zichzelf van grootte te laten wijzigen

### KConfig

- [KConfigGui] Lettertypegewicht controleren wanneer eigenschap styleName wordt gewist
- KconfigXT: Een waardeattribuut toevoegen aan Enum veldkeuzes

### KCoreAddons

- kdirwatch: een recent geïntroduceerde crash repareren (bug 419428)
- KPluginMetaData: ongeldige mimetype in supportsMimeType behandelen

### KCrash

- Definitie van setErrorMessage verplaatsen uit de linux ifdef
- Leveren van een foutmelding uit de toepassing toestaan (bug 375913)

### KDBusAddons

- Juiste bestand controleren voor detectie van sandbox

### KDeclarative

- Api voor passieve meldingen introduceren
- [KCM Controls GridDelegate] <code>ShadowedRectangle</code> gebruiken
- [kcmcontrols] Zichtbaarheid van koptekst/voettekst respecteren

### KDocTools

- Vet cursief gebruiken op 100% for sect4 titels en vet 100% for sect5 titels (bug 419256)
- De lijst met entities van Italië bijwerken
- Dezelfde stijl gebruiken voor informele tabel als voor tabel (bug 418696)

### KIdleTime

- Oneindige recursie repareren in xscreensaver-plug-in

### KImageFormats

- De HDR-plug-in overzetten van sscanf() naar QRegularExpression. Repareert FreeBSD

### KIO

- Nieuwe klasse KIO::CommandLauncherJob in KIOGui om KRun::runCommand te vervangen
- Nieuwe klasse KIO::ApplicationLauncherJob in KIOGui om KRun::run te vervangen
- Bestand-ioslave: betere instelling gebruiken voor sendfile syscall (bug 402276)
- FileWidgets: Teruggeefgebeurtenissen van KDirOperator negeren (bug 412737)
- [DirectorySizeJob] Tellingen van submappen repareren bij oplossen van symbolische koppelingen naar mappen
- Aankoppelingen van KIOFuse markeren als mogelijk langzaam
- kio_file: KIO::StatResolveSymlink honoreren voor UDS_DEVICE_ID en UDS_INODE
- [KNewFileMenu] extensie toevoegen aan voorgestelde bestandsnaam (bug 61669)
- [KOpenWithDialog] Generieke naam uit .desktop bestanden toevoegen als een tekstballon (bug 109016)
- KDirModel: tonen van een root-node implementeren voor de gevraagde URL
- Registreer gesplitste toepassingen als een onafhankelijke cgroups
- Voorlooptekst "Stat" toevoegen aan items in StatDetails Enum
- Windows: ondersteuning toevoegen voor datum bestandscreatie
- KAbstractFileItemActionPlugin: ontbrekende accenten toevoegen in voorbeeldcode
- Dubbel ophalen en tijdelijke hexcodering vermijden voor NTFS attributen
- KMountPoint: swap overslaan
- Een pictogram toekennen aan actiesubmenu's
- [DesktopExecParser] Open {ssh,telnet,rlogin}:// url's met ktelnetservice (bug 418258)
- Exitcode van kioexec repareren wanneer uitvoerbaar programma niet bestaat (en --tempfiles is ingesteld)
- [KPasswdServer] foreach vervangen door op range/index gebaseerde for
- KProcessRunner van Run: opstartmelding beëindigen ook bij een fout
- [http_cache_cleaner] gebruik van foreach vervangen door QDir::removeRecursively()
- [StatJob] een QFlag gebruiken om de details teruggekregen van StatJob te specificeren

### Kirigami

- Hotfix voor D28468 om gebroken referenties naar variabele te repareren
- de incubator kwijt maken
- muiswiel volledig uitschakelen in outside flickable
- Ondersteuning voor eigenschap initialiseren aan PagePool toevoegen
- Opnieuw maken van OverlaySheet
- ShadowedImage en ShadowedTexture items toevoegen
- [controls/formlayout] niet proberen om implicitWidth te resetten
- Nuttige tips voor invoermethode in wachtwoordveld standaard toevoegen
- [FormLayout] Compressietimerinterval op 0 instellen
- [UrlButton] Uitschakelen wanneer er geen URL is
- grootte van header vereenvoudigen (bug 419124)
- Eexportheader verwijderen uit statische installatie
- Pagina met Over Qt 5.15 repareren
- Gebroken paden repareren in kirigami.qrc.in
- "veryLongDuration" animatieduur toevoegen
- meerdere rijenmeldingen repareren
- niet afhankelijk zijn van actief venster voor de timer
- Meerdere gestapelde passieve meldingen ondersteunen
- Inschakelen van rand voor ShadowedRectangle bij aanmaken item repareren
- op bestaan van venster controleren
- Ontbrekend typen aan qrc toevoegen
- Ongedefinieerde controle in globale lade-menumodus (bug 417956)
- Op een eenvoudige rechthoek terugvallen bij gebruik van software rendering
- Kleur vooraf vermenigvuldigen en alfa-mengen
- [FormLayout] FormData.enabled ook doorzetten naar label
- Een ShadowedRectangle item toevoegen
- eigenschap alwaysVisibleActions
- geen exemplaren aanmaken wanneer de toepassing wordt beëindigd
- Geen paletwijzigingen uitsturen als het palet niet is gewijzigd

### KItemModels

- [KSortFilterProxyModel QML] invalidateFilter publiek maken

### KNewStuff

- Indeling in DownloadItemsSheet repareren (bug 419535)
- [QtQuick dialoog] naar UrlBUtton overzetten en verbergen wanneer er geen URL is
- Omschakelen naar gebruik van ShadowedRectangle van Kirigami
- Bijwerkscenarios repareren zonder expliciet geselecteerde downloadlink (bug 417510)

### KNotification

- Nieuwe klasse KNotificationJobUiDelegate

### KNotifyConfig

- libcanberra gebruiken als primair middel van voorvertoning van het geluid (bug 418975)

### KParts

- Nieuwe klasse PartLoader als vervanging van KMimeTypeTrader voor onderdelen

### KService

- KAutostart: statische methode toevoegen om startconditie te controleren
- KServiceAction: ouderservice opslaan
- De tekenreekslijst X-Flatpak-RenamedFrom op de juiste manier lezen list uit desktop-bestanden

### KTextEditor

- Laat het compileren tegen qt 5.15
- invouwcrash voor invouwen van enkele regel invouwen repareren (bug 417890)
- [VIM Modus] g&lt;up&gt; g&lt;down&gt; commando's toevoegen (bug 418486)
- MarkInterfaceV2 toevoegen aan s/QPixmap/QIcon/g voor symbolen of markeringen
- inlineNotes tekenen na tekenen van marker voor woordafbreking

### KWayland

- [xdgoutput] alleen initiële naam en beschrijving verzenden indien ingesteld
- XdgOutputV1 versie 2 toevoegen
- Toepassingsmenu naar hulpbronnen rondzenden bij registeren ervan
- Een implementatie leveren voor het tabletinterface
- [server] geen aannames doen over de volgorde van damage_buffer en verzoeken voor bijvoegen
- Een toegewijde fd doorgeven aan elk toetsenbord voor de xkb-keymap (bug 381674)
- [server] SurfaceInterface::boundingRect() introduceren

### KWidgetsAddons

- Nieuwe klasse KFontChooserDialog (gebaseerd op KFontDialog uit KDELibs4Support)
- [KCharSelect] geen enkelvoudige tekens vereenvoudigen in zoeken (bug 418461)
- Als we items lezen moeten we ze eerst wissen. Anders zullen we een gedupliceerde lijst zien
- kcharselect-data bijwerken tot Unicode 13.0

### KWindowSystem

- EWMH non-compliance voor NET::{OnScreenDisplay,CriticalNotification} repareren
- KWindowSystem: KStartupInfoData::launchedBy afkeuren, ongebruikt
- Toepassingsmenu via KWindowInfo blootstellen

### Plasma Framework

- Pagina-element toegevoegd
- [pc3/busyindicator] Verbergen als deze niet actief is
- Venster-pin bijwerken, meer groottes toevoegen, redundante bewerken-verwijderen verwijderen
- Een nieuw TopArea-element aanmaken met widgets/toparea-svg
- Plasmoid-heading-svg toegevoegd
- Geaccentueerde eigenschap laten werken voor ronde knop

### Prison

- Ook de ware minimum grootte doorgeven aan QML
- Een nieuw set functie voor barcodegroottes toevoegen
- Behandelen van minimale grootte vereenvoudigen
- Logica van barcode-imageschaling verplaatsen naar AbstractBarcode
- API toevoegen aan controle of een barcode een- of tweedimensionaal is

### QQC2StyleBridge

- [Dialog] <code>ShadowedRectangle</code> gebruiken
- Grootte bepalen van CheckBox en RadioButton repareren (bug 418447)
- <code>ShadowedRectangle</code> gebruiken

### Solid

- [Fstab] uniekheid voor alle typen bestandssystemen verzekeren
- Samba: Ga na om verschil te maken tussen aankoppelingen die dezelfde bron delen (bug 418906)
- hardware tool: syntaxis definiëren via syntaxis argument

### Sonnet

- Sonnet automatisch detecteren die falen op Indiase talen
- ConfigView een niet-beheerde ConfigWidget aanmaken

### Accentuering van syntaxis

- LaTeX: wiskundige haakjes repareren in optionele labels (bug 418979)
- Inno-setupsyntaxis toevoegen, ingebedde Pascal-scripting wordt ingevoegd
- Lua: # toevoegen als extra scheidingsteken om automatisch aanvullen met <code>#iets</code> te activeren
- C: ' als scheidingsteken van cijfer verwijderen
- enig commentaar over de zaken met offset overslaan toevoegen
- dynamische regex matching optimaliseren (bug 418778)
- regex regels verkeerd gemarkeerd als dynamisch repareren
- indexer uitbreiden om dynamic=true regexes te detecteren die geen plaatshouders naar adapt hebben
- Accentuering bij overgaan van QL toevoegen
- Agda: sleutelwoorden bijgewerkt naar 2.6.0 en zwevende punten repareren

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
