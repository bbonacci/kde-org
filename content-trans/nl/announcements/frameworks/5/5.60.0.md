---
aliases:
- ../../kde-frameworks-5.60.0
date: 2019-07-13
layout: framework
libCount: 70
---
### Algemeen

- Qt &gt;= 5.11 is nu vereist, nu Qt 5.13 is vrijgegeven.

### Baloo

- [QueryTest] Test of onafhankelijke uitdrukkingen echt onafhankelijk zijn
- [TermGenerator] Voeg een lege positie in tussen onafhankelijke termen
- [QueryTest] testen herstructureren om gemakkelijkere extensie toe te staan
- [TermGenerator] zinnen met een enkele term uit de PositionDB laten
- [TermGenerator] Afkorten van term doen vóór conversie naar UTF-8
- [PostingIterator] Methode positions() verplaatsen naar VectorPositionInfoIterator
- [TermGenerator] UTF-8 ByteArray gebruiken voor termList
- [WriteTransactionTest] verwarring van QString en QByteArray opklaren
- [experimenteel/BalooDB] waarschuwing triviale 0 / nullptr repareren
- [PositionDbTest] triviale memleak in test repareren
- [PendingFileQueueTest] aanmaken + verwijderen verifiëren dat ze geen extra gebeurtenissen uitzenden
- [PendingFileQueueTest] verwijderen + aanmaken verifiëren dat het echt werkt
- [PendingFileQueue] verwijderen + aanmaken / aanmaken + verwijderen race vermijden
- [PendingFileQueueTest] synthetische timergebeurtenisssen gebruiken om test te versnellen
- [XAttrIndexer] DocumentTime bijwerken wanneer XAttrs bijgewerkt wordt
- [PendingFileQueueTest] tijdslimieten verkorten, volgtijd verifiëren
- [PendingFileQueue] betere berekening van resterende tijd
- [ModifiedFileIndexer] juiste mimetype gebruiken voor mappen, uitstellen totdat het nodig is
- [NewFileIndexer] symbolische koppelingen weglaten uit de index
- [ModifiedFileIndexer] schaduwen van XAttr-wijzigingen vermijden door wijzigingen in inhoud
- [NewFileIndexer] juiste mimetype voor mapppen gebruiken, controleer excludeFolders
- [UnindexedFileIndexer] wijzigingen in commentaar, tags en waardering oppikken
- [UnindexedFileIndexer] controle op tijd van bestand voor nieuwe bestanden overslaan
- [DocumentUrlDB] manipulatie van de gehele boomstructuur vermijden bij triviale hernoeming
- [DocumentUrlDB] ongeldige URL's vroeg vangen
- [DocumentUrlDB] ongebruikte method 'rename' verwijderen
- [balooctl] besturingscommando's van indexer stroomlijnen
- [Transaction] sjabloon vervangen voor functie met std::function
- [FirstRunIndexer] juiste mimetype voor mapppen gebruiken
- Invariant indexniveau uit de loop verwijderen
- [BasicIndexingJob] opzoeken van baloo-documenttype voor mapppen overslaan
- [FileIndexScheduler] ga na indexeerder draait niet in onderbroken status
- [PowerStateMonitor] conservatief zijn bij bepalen van status van power
- [FileIndexScheduler] de indexeerder stoppen wanneer quit() wordt aangeroepen via DBus
- Losmaken van container vermijden op een paar plaatsen
- Niet proberen naar QLatin1String achter te voegen
- Valgrind-detectie uitschakelen bij compileren met MSVC
- [FilteredDirIterator] alle achtervoegsels combineren in één groot RegExp
- [FilteredDirIterator] RegExp-overhead vermijden voor exacte overeenkomsten
- [UnindexedFileIterator] bepaling van mimetype vertragen totdat het nodig is
- [UnindexedFileIndexer] niet proberen om niet bestaand bestand toe te voegen aan index
- Valgrind detecteren, verwijderen van database vermijden bij gebruik van valgrind
- [UnindexedFileIndexer] optimaliseren van loop (losmaken vermijden, onveranderlijken)
- Uitvoeren UnindexedFileIndexer en IndexCleaner vertragen
- [FileIndexScheduler] nieuwe status voor inactief op batterij
- [FileIndexScheduler] huiswerktaken uitstellen tijdens op batterij
- [FileIndexScheduler] uitzender van wijzigingen in status meerdere keren vermijden
- [balooctl] status uitvoer helderder maken en uitbreiden

### BluezQt

- MediaTransport API toevoegen
- LE Advertising en GATT API's toevoegen

### Breeze pictogrammen

- id="current-color-scheme" toevoegen aan collapse-all pictogrammen (bug 409546)
- Pictogrammen voor disk-quota toevoegen (bug 389311)
- Symbolische koppeling install maken naar edit-download
- Joystick instellingen pictogram naar spelcontroller wijzigen (bug 406679)
- edit-select-text toevoegen, 16px draw-text maken zoals 22px
- KBruch pictogram bijwerken
- Help-donate-[valuta] pictogrammen toevoegen
- Breeze Dark hetzelfde Kolourpaint pictogram laten gebruiken als Breeze
- 22px meldingenpictogrammen toevoegen

### KActivitiesStats

- Een crash in KactivityTestApp repareren wanneer Result tekenreeks heeft met niet-ASCII

### KArchive

- Niet crashen als het binnenste bestand groter wil zijn dan maximale grootte van QByteArray

### KCoreAddons

- KPluginMetaData: Q_DECLARE_METATYPE gebruiken

### KDeclarative

- [GridDelegate] gaten repareren in hoeken van accentuering thumbnailArea
- blockSignals weghalen
- [KCM GridDelegate] waarschuwing over stilte
- [KCM GridDelegate] houd rekening met implicitCellHeight voor binnenste  hoogte van delegatie
- GridDelegate-pictogram repareren
- Fragiele vergelijking met i18n("None") repareren en gedrag beschrijven in documenten (bug 407999)

### KDE WebKit

- Waardeer KDEWebKit af van Tier 3 naar Porting Aids

### KDocTools

- pt-BR user.entities bijwerken

### KFileMetaData

- Repareer uitpakken van enige eigenschappen om overeen te komen met wat geschreven was (bug 408532)
- Debuggingcategorie gebruiken in taglib extractor/writer
- Formaat van basiswaarde belichting van de foto (bug 343273)
- Eigenschapnaam repareren
- Prefix photo verwijderen uit elke exif-eigenschapsnaam (bug 343273)
- Eigenschappen van ImageMake en ImageModel hernoemen (bug 343273)
- [UserMetaData] methode toevoegen aan zoektekst over welke attributen zijn ingesteld
- Brandpuntsafstand formatteren als millimeter
- Belichting van foto formatteren als rationeel getal indien van toepassing (bug 343273)
- usermetadatawritertest inschakelen voor alle UNIXes, niet alleen Linux
- De waarden van lensopening formatteren als F-getallen (bug 343273)

### KDE GUI-addons

- KModifierKeyInfo: we delen de interne implementatie
- Dubbel opzoeken verwijderen
- Besissing naar runtime verplatsen om x11 te gebruiken of niet

### KHolidays

- UK Early May bank holiday voor 2020 bijwerken (bug 409189)
- ISO-code voor Hessen / Duitsland repareren

### KImageFormats

- QImage::byteCount -&gt; QImage::sizeInByes

### KIO

- Test van KFileItemTest::testIconNameForUrl repareren om met andere naam van pictogram rekening te houden
- Fout in i18n number-of-arguments repareren in waarschuwingsbericht knewfilemenu
- [ftp] verkeerde toegangstijd in Ftp::ftpCopyGet() repareren (bug 374420)
- [CopyJob] hoeveelheid gerapporteerd aan verwerkte batches
- [CopyJob] resultaten rapporteren na beëindigen van kopiëren (bug 407656)
- Redundante logica in KIO::iconNameForUrl() naar KFileItem::iconName() verplaatsen (bug 356045)
- KFileCustomDialog installeren
- [Places panel] Root niet standaard tonen
- Waardeer dialoogvak "Kon rechten niet wijzigen" af naar een qWarning
- O_PATH is alleen beschikbaar op linux. Om te voorkomen dat de compiler een fout geeft
- Terugkoppeling inline tonen bij aanmaken van nieuwe bestanden of mappen
- Ondersteuning van Auth: privileges laten vallen als doel geen eigenaar root heeft
- [copyjob] Tijd van wijziging alleen instellen als de kio-slave het leverde (bug 374420)
- Bewerken van rechten annuleren voor alleen-lezen doel met de huidige gebruiker als eigenaar
- KProtocolInfo::defaultMimetype toevoegen
- Instellingen van weergave altijd opslaan bij omschakelen van de ene weergavemodus naar een ander
- Exclusieve groep herstellen voor sorteren van menu-items
- Dolphin-weergavemodussen in de bestandsdialoog (bug 86838)
- kio_ftp: foutbehandeling verbeteren wanneer kopiëren naar FTP mislukt
- kioexec: de afschrikkende debugmeldingen wijzigen voor vertraagde verwijdering

### Kirigami

- [ActionTextField] Laat actie oplichten bij indrukken
- tekstmodus en positionering ondersteunen
- effect bij erboven zweven voor broodkruimel op bureaublad
- een minimum hoogte van 2 rastereenheden afdwingen
- SwipeListItem implicitHeight instellen op het maximum van inhoud en acties
- Tekstballon verbergen wanneer PrivateActionToolButton wordt ingedrukt
- Per ongeluk ingeslopen backtraces van cmake-code voor Plasma-stijl verwijderen
- ColumnView::itemAt
- breeze-internal afdwingen als er geen thema is gespecificeerd
- navigatie op linker vastgepinde pagina corrigeren
- de ruimte bedekt door vastgepinde pagina's bijhouden
- een scheidingsteken tonen bij modus in linker zijbalk
- in modus enkele kolom, pin heeft geen effect
- eerste prototype van half werkend vastpinnen

### KJobWidgets

- [KUiServerJobTracker] wijziging in eigendom behandelen

### KNewStuff

- [kmoretools] pictogrammen toevoegen om acties te downloaden en te installeren

### KNotification

- Niet naar phonon zoeken op Android

### KParts

- Profiel van ondersteuningsinterface voor TerminalInterface toevoegen

### KRunner

- Uitzenden niet oneindig vertragen van matchesChanged

### KService

- X-Flatpak-RenamedFrom toevoegen als herkende sleutel

### KTextEditor

- ga naar centreren van lijn repareren (bug 408418)
- Bladwijzerpictogram tonen op rand pictogram met lage dpi repareren
- Actie "Pictogramrand tonen" repareren om rand opnieuw om te schakelen
- Lege pagina's in afdrukvoorbeeld en tweemaal afgedrukte regels repareren (bug 348598)
- gebruikte header niet langer verwijderen
- snelheid van automatisch omlaag scrollen repareren (bug 408874)
- Standaard variabelen toevoegen voor interface van variabelen
- Maak dat automatische spellingcontrole werkt na opnieuw laden van een document (bug 408291)
- limiet van de standaard regellengte ophogen naar 10000
- WIP: accentuering uitschakelen na 512 tekens op een regel
- KateModeMenuList: naar QListView verplaatsen

### KWayland

- Een beschrijving opnemen
- Proof of concept van een wayland-protocol om de keystate-dataengine te laten werken

### KWidgetsAddons

- KPasswordLineEdit erft nu op de juiste manier zijn focusPolicy van QLineEdit (bug 398275)
- Knop "Details" vervangen door KCollapsibleGroupBox

### Plasma Framework

- [Svg] Fout bij overzetten van QRegExp::exactMatch repareren
- ContainmentAction: laden van KPlugin repareren
- [TabBar] uitwendige marges verwijderen
- Methoden van een lijst maken van Applet, DataEngine en containment met inPlasma::PluginLoader filtert niet langer de plug-ins met X-KDE-ParentAppprovided wanneer een lege tekenreeks wordt doorgegeven
- Laat knijpen in agenda weer werken
- Pictogrammen voor disk-quota toevoegen (bug 403506)
- Zorg dat Plasma::Svg::elementRect een beetje slanker is
- Zet de versie van desktopthemes-pakketten naar KF5_VERSION
- Niet melden over wijziging naar dezelfde status waarin het was 
- De uitlijning van het label van de hulpmiddelknop repareren
- [PlasmaComponents3] Tekst op knop ook verticaal centreren

### Omschrijving

- Initiële grootte van de configuratiedialoog wijzigen
- Pictogrammen en tekst voor jobdialoogknoppen verbeteren
- Vertaling van actiondisplay repareren
- Geen foutmelding tonen als delen geannuleerd is door de gebruiker
- Waarschuwing repareren bij lezen van metagegevens van plug-in
- Nieuw ontwerp van pagina's voor configuratie
- ECMPackageConfigHelpers -&gt; CMakePackageConfigHelpers
- phabricator: doorheen vallen in omschakelen repareren

### QQC2StyleBridge

- Sneltoets in menu-item tonen indien gespecificeerd (bug 405541)
- MenuSeparator toevoegen
- ToolButton blijvend in een ingedrukte status na indrukken
- [ToolButton] aangepaste pictogramgrootte doorgeven aan StyleItem
- zichtbaarheidsbeleid honoreren (bug 407014)

### Solid

- [Fstab] toepasselijk pictogram selecteren voor persoonlijke hoofdmap of root-map
- [Fstab] aangekoppelde "overlay"-bestandssystemen tonen
- [UDev Backend] er wordt afgevraagd naar smalle apparaten

### Accentuering van syntaxis

- Fortran: ook opnieuw licentie naar MIT
- Syntaxis accentuering van het Fortran vastformaat verbeteren
- Fortran: vrije &amp; vast formaten implementeren
- CMake COMMANDO accentuering van geneste paren repareren
- Meer sleutelwoorden toevoegen en ook ondersteuning-rr in gdb-accentueerder
- Commentaarregels vroeg in GDB-accenteerder detecteren
- AppArmor: syntaxis bijwerken
- Julia: syntaxis bijwerken en sleutelwoorden voor constanten toevoegen (bug 403901)
- CMake: de standaard CMake omgevingsvariabelen accentueren
- Syntaxisdefinitie voor ninja-build toevoegen
- CMake: ondersteuning voor 3.15 functies
- Jam: verschillende verbeteringen en reparaties
- Lua: update voor Lua54 en einde van functie als sleutelwoord anders dan besturing
- C++: update for C++20
- debchangelog: Eoan Ermine toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
