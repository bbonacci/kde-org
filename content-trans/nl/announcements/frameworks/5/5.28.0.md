---
aliases:
- ../../kde-frameworks-5.28.0
date: 2016-11-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nieuw framework: syntaxisaccentuering

Engine voor accentuering van de syntaxis voor Kate syntaxisdefinities

Dit is een alleenstaande implementatie van de engine voor syntaxisaccentuering in Kate. Het is bedoeld als een bouwblok voor tekstbewerkers evenals voor eenvoudige geaccentueerde tekstweergave (bijv. als HTML), die zowel integreert met een aangepaste bewerker evenals een gereed zijnde QSyntaxHighlighter sub-class.

### Breeze pictogrammen

- actiepictogrammen van kstars bijwerken (bug 364981)
- Breeze Dark wordt in systeeminstellingen fout Breeze genoemd in .themes-bestand (bug 370213)

### Extra CMake-modules

- Zorg dat KDECMakeSettings werkt met KDE_INSTALL_DIRS_NO_DEPRECATED
- Vereis geen python-bindings-afhankelijkheden voor ECM
- De PythonModuleGeneration module toevoegen

### KActivitiesStats

- Status van koppeling negeren bij sortering UsedResources en LinkedResources model

### KDE Doxygen hulpmiddelen

- [CSS] wijzigingen terugdraaien gedaan door doxygen 1.8.12
- doxygenlayout-bestand toevoegen
- Manier van definiëren van groepsnamen bijwerken

### KAuth

- Ga na dat we meer dan één verzoek kunnen doen
- Ga na dat we de voortgang bijhouden door de uitvoer van het programma te lezen

### KConfig

- Ga na dat we compilatie niet breken met gebroken eenheden uit het verleden
- Vang de fout on bij een bestandsveld die niet goed behandeld wordt

### KCoreAddons

- Foute url weergeven
- Avatars van gebruikers laden uit AccountsServicePath als het bestaat (bug 370362)

### KDeclarative

- [QtQuickRendererSettings] standaard is leeg in plaats van "false" repareren

### Ondersteuning van KDELibs 4

- Laat de Franse vlag in werkelijkheid alles van de pixmap gebruiken

### KDocTools

- 'checkXML5 genereert html bestanden in werkmap voor geldige docbooks repareren (bug 369415)

### KIconThemes

- Schaalfactor met niet gehele getallen ondersteunen in kiconengine (bug 366451)

### KIdleTime

- Schakel teveel uitvoer op de console met 'waiting for' berichten uit

### KImageFormats

- imageformats/kra.h - gaat boven capabilities() en create() voor KraPlugin

### KIO

- HTTP datumformat verzonden door kio_http repareren om altijd de C locale te gebruiken (bug 372005)
- KACL: geheugenlekken repareren ontdekt door ASAN
- Geheugenlekken repareren in KIO::Scheduler, ontdekt door ASAN
- Dubbele wisknop verwijderd (bug 369377)
- Bewerken van autostart items wanneer /usr/local/share/applications niet bestaat repareren (bug 371194)
- [KOpenWithDialog] TreeView kop verbergen
- Buffergrootte van namen van symbolische koppelingen schoonmaken (bug 369275)
- Beëindig DropJobs wanneer trigger niet is uitgestuurd (bug 363936)
- ClipboardUpdater: nog een crash op Wayland repareren (bug 359883)
- ClipboardUpdater: crash op Wayland repareren (bug 370520)
- Schaalfactoren met niet gehele getallen ondersteunen in KFileDelegate (bug 366451)
- kntlm: onderscheid maken tussen NULL en leeg domein
- Overschrijfdialoog niet tonen als bestandsnaam leeg is
- kioexec: vriendelijke bestandsnamen gebruiken
- Eigendom van focus repareren als URL is gewijzigd voor het tonen van het widget
- Belangrijke prestatieverbetering bij voorbeelden uitzetten in de bestandsdialoog (bug 346403)

### KItemModels

- Python-bindings toevoegen

### KJS

- FunctionObjectImp exporteren, gebruikt door debugger van khtml

### KNewStuff

- Sorteerrollen en -filters scheiden
- Het mogelijk maken geïnstalleerde items op te vragen

### KNotification

- Do geen dereferentie van een object dat niet is gerefereerd wanneer melding geen actie heeft
- KNotification zal niet langer crashen bij gebruik in een QGuiApplication en geen meldingenservice actief is (bug 370667)
- Crashes repareren in NotifyByAudio

### KPackage-framework

- Ga na dat we zowel zoeken naar json als desktop-metagegevens
- Bewaak het verwijderen van Q_GLOBAL_STATIC bij afsluiten van app
- Hangende pointer in KPackageJob gerepareerd (bug 369935)
- Verwijder ontdekking geassocieerd aan een toets bij verwijdering van een definitie
- Genereer het pictogram in het appstream-bestand

### KPty

- ulog-helper gebruiken op FreeBSD in plaats van utempter
- zoek beter naar utempter met ook basis cmake prefix
- werk om mislukkingen van find_program ( utempter ...) heen
- ECM-pad gebruiken om utempter binair programma te vinden, betrouwbaarder dan eenvoudige cmake prefix

### KRunner

- i18n: behandel tekenreeksen in kdevtemplate bestanden

### KTextEditor

- Breeze Dark: maak achtergrond huidige-regelkleur donkerder voor betere leesbarheid (bug 371042)
- Gesorteerde Dockerbestandsinstructies
- Breeze (Dark): Maak commentaar een beetje lichter voor betere leesbaarheid (bug 371042)
- CStyle en C++/boost indenteerders repareren bij inschakelen van automatische haakjes (bug 370715)
- Modeline 'automatische blokhaakjes' toevoegen
- Tekst invoegen na einde van bestand repareren (zeldzaam geval)
- Ongeldige xml accentuering bestanden repareren
- Maxima: hard gecodeerde kleuren verwijderen, itemData label repareren
- Syntaxis definities voor OBJ, PLY en STL toevoegen
- Syntaxisaccentuering voor Praat toevoegen

### KUnitConversion

- Nieuwe thermische en elektrische eenheden en gemaksfunctie voor eenheden

### KWallet Framework

- Als Gpgmepp niet wordt gevonden, probeer KF5Gpgmepp te gebruiken
- Gpgmepp uit GpgME-1.7.0 gebruiken

### KWayland

- Verbeterde relocatie van CMake export
- [tools] generatie van wayland_pointer_p.h repareren
- [tools] Genereer eventQueue methoden alleen voor global klassen
- [server] crash repareren bij bijwerken toetsenbordoppervlak met focus
- [server] mogelijke crash repareren bij aanmaken van DataDevice
- [server] ga na dat een DataSource bestaat op DataDevice in setSelection
- [tools/generator] verbeter vernietiging van hulpbron aan de kant van de server
- Voeg verzoek toe om focus te hebben in een PlasmaShellSurface van het Role Panel
- Voeg ondersteuning toe voor auto-hiding panel aan PlasmaShellSurface interface
- Doorgeven van algemeen QIcon via PlasmaWindow interface ondersteunen
- [server] Implementeer de algemene venstereigenschap in QtSurfaceExtension
- [client] Voeg methoden toe om ShellSurface uit een QWindow te krijgen
- [server] Stuur pointer-gebeurtenissen naar alle wl_pointer hulpbronnen van een client
- [server] Roep wl_data_source_send_send niet aan als DataSource niet is gebonden
- [server] deleteLater gebruiken wanneer een ClientConnection vernietigd wordt (bug 370232)
- Ondersteuning voor het relatieve pointer-protocol implementeren
- [server] vorige selectie uit SeatInterface::setSelection annuleren
- [server] sleutelgebeurtenissen verzenden naar alle wl_keyboard hulpbronnen van een client

### KWidgetsAddons

- kcharselect-generate-datafile.py verplaatsen naar src subdir
- Importeer script kcharselect-generate-datafile.py met geschiedenis
- Verouderde sectie verwijderen
- Unicode auteursrechtvermelding en toestemming toevoegen
- Waarschuwing repareren: ontbrekende overschrijving
- Symbool SMP blokken toevoegen
- "Zie ook" verwijderingen repareren
- Ontbrekende Unicode blokken toevoegen; verbeter volgorde (bug 298010)
- tekencategorieën toevoegen aan het gegevensbestand
- Unicode categorieën bijwerken in het generatiescript van het gegevensbestand
- pas het generatiescript van het gegevensbestanden aan zodat de unicode 5.2.0 gegevensbestanden ontleed kunnen worden
- reparatie van overzetten doorzetten voor generatie van vertalingen
- laat het script om het gegevensbestand voor kcharselect ook een vertalingen dummy genereren
- Voeg het script om het gegevensbestand voor KCharSelect te genereren toe
- nieuwe KCharSelect toepassing (gebruikt nu kcharselect widget uit kdelibs)

### KWindowSystem

- Verbeterde relocatie van CMake export
- Ondersteuning voor desktopFileName aan NETWinInfo toevoegen

### KXMLGUI

- Nieuwe stijl verbinden toestaan in KActionCollection::add<a href="">Actie</a>

### ModemManagerQt

- Include dir in pri-bestand repareren

### NetworkManagerQt

- Include dir in pri-bestand repareren
- Repareer moc fout vanwege gebruik van Q_ENUMS being in een naamruimte, met Qt branch 5.8

### Plasma Framework

- ga na dat OSD geen Dialog flag heeft (bug 370433)
- contexteigenschappen instellen alvorens de qml te herladen (bug 371763)
- Ontleed het metagegevensbestand niet opnieuw als het al is geladen
- Crash in qmlplugindump repareren wanneer geen QApplication beschikbaar is
- Menu "Alternativen" niet standaard tonen
- Nieuwe bool om geactiveerd signaal te gebruiken als omschakelaar van uitgeklapt (bug 367685)
- Reparaties voor bouwen van plasma-framework met Qt 5.5
- [PluginLoader] operator&lt;&lt; gebruiken voor finalArgs in plaats van initializer list
- kwayland gebruiken voor schaduw en dialoogpositionering
- Overblijvende ontbrekende pictogrammen en netwerkverbeteringen
- availableScreenRect/Region omhoog verplaatsen naar AppletInterface
- Containmentacties niet laden voor ingebedde containments (systeemvakken)
- Zichtbaarheid van alternatieve menu-items bijwerken op aanvraag

### Solid

- Onstabiele volgorde van resultaten van opvragen nog eens repareren
- Een CMake optie toevoegen aan optie tussen HAL en UDisks managers op FreeBSD
- Maak dat UDisks2 backend compileert op FreeBSD (en, mogelijk, andere UNIXen)
- Windows: geen foutdialogen weergeven (bug 371012)

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
