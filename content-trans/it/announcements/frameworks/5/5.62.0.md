---
aliases:
- ../../kde-frameworks-5.62.0
date: 2019-09-14
layout: framework
libCount: 70
---
### Attica

- Fix the attica pkgconfig file

### Baloo

- Fixes a crash in Peruse triggered by baloo

### Icone Brezza

- Add new activities and virtual desktops icons
- Make small recent documents icons look like documents and improve clock emblems
- Create new "Recent folders" icon (bug 411635)
- Add "preferences-desktop-navigation" icon (bug 402910)
- Add 22px dialog-scripts, change script actions/places icons to match it
- Improve "user-trash" icon
- Use empty/filled style for monochrome empty/full trash
- Make notification icons use outline style
- Make user-trash icons look like trashcans (bug 399613)
- Add breeze icons for ROOT cern files
- Remove applets/22/computer (bug 410854)
- Add view-barcode-qr icons
- Krita has split from Calligra and now uses Krita name instead of calligrakrita (bug 411163)
- Add battery-ups icons (bug 411051)
- Make "monitor" a link to "computer" icon
- Add FictionBook 2 icons
- Add icon for kuiviewer, needs updating -&gt; bug 407527
- Symlink "port" to "anchor", which displays more appropriate iconography
- Change radio to device icon, add more sizes
- Make <code>pin</code> icon point to an icon that looks like a pin, not something unrelated
- Fix missing digit and pixel-perfect alignment of depth action icons (bug 406502)
- Make 16px folder-activities look more like larger sizes
- add latte-dock icon from latte dock repo for kde.org/applications
- icon for kdesrc-build used by kde.org/applications to be redrawn
- Rename media-show-active-track-amarok to media-track-show-active

### Moduli CMake aggiuntivi

- ECMAddQtDesignerPlugin: pass code sample indirectly via variable name arg
- Keep 'lib' as default LIBDIR on Arch Linux based systems
- Enable autorcc by default
- Define install location for JAR/AAR files for Android
- Add ECMAddQtDesignerPlugin

### KActivitiesStats

- Add Term::Type::files() and Term::Type::directories() to filter only directories or excluding them
- Add @since 5.62 for newly added setters
- Add proper logging using ECMQtDeclareLoggingCategory
- Add setter to Type, Activity, Agent and UrlFilter query fields
- Use special values constants in terms.cpp
- Allow date range filtering of resource events using Date Term

### KActivities

- [kactivities] Use new activities icon

### KArchive

- Fix creating archives on Android content: URLs

### KCompletion

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KConfig

- Fix memory leak in KConfigWatcher
- Disable KCONFIG_USE_DBUS on Android

### KConfigWidgets

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)
- [KColorSchemeManager] Optimize preview generation

### KCoreAddons

- KProcessInfo::name() now returns only the name of the executable. For the full command line use KProcessInfo::command()

### KCrash

- Avoid enabling kcrash if it's only included via a plugin (bug 401637)
- Disable kcrash when running under rr

### KDBusAddons

- Fix race on kcrash auto-restarts

### KDeclarative

- Warn if KPackage is invalid
- [GridDelegate] Don't select unselected item when clicking on any of its action buttons (bug 404536)
- [ColorButton] Forward accepted signal from ColorDialog
- use zero-based coordinate system on the plot

### KDesignerPlugin

- Deprecate kgendesignerplugin, drop bundle plugim for all KF5 widgets

### KDE WebKit

- Use ECMAddQtDesignerPlugin instead of private copy

### KDocTools

- KF5DocToolsMacros.cmake: Use non-deprecated KDEInstallDirs variables (bug 410998)

### KFileMetaData

- Implement writing of images

### KHolidays

- Display filename where we return an error

### KI18n

- Localize long number strings (bug 409077)
- Support passing target to ki18n_wrap_ui macro

### KIconThemes

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KIO

- Undoing trashing files on the desktop has been fixed (bug 391606)
- kio_trash: split up copyOrMove, for a better error than "should never happen"
- FileUndoManager: clearer assert when forgetting to record
- Fix exit and crash in kio_file when put() fails in readData
- [CopyJob] Fix crash when copying an already existing dir and pressing "Skip" (bug 408350)
- [KUrlNavigator] Add MIME types supported by krarc to isCompressedPath (bug 386448)
- Added dialog to set execute permission for executable file when trying to run it
- [KPropertiesDialog] Always check mount point being null (bug 411517)
- [KRun] Check mime type for isExecutableFile first
- Add an icon for the trash root and a proper label (bug 392882)
- Add support for handling QNAM SSL errors to KSslErrorUiData
- Making FileJob behave consistently
- [KFilePlacesView] Use asynchronous KIO::FileSystemFreeSpaceJob
- rename internal 'kioslave' helper executable to 'kioslave5' (bug 386859)
- [KDirOperator] Middle-elide labels that are too long to fit (bug 404955)
- [KDirOperator] Add follow new directories options
- KDirOperator: Only enable "Create New" menu if the selected item is a directory
- KIO FTP: Fix file copy hanging when copying to existing file (bug 409954)
- KIO: port to non-deprecated KWindowSystem::setMainWindow
- Make file bookmark names consistent
- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)
- [KDirOperator] Use more human-readable sort order descriptions
- [Permissions editor] Port icons to use QIcon::fromTheme() (bug 407662)

### Kirigami

- Replace the custom overflow button with PrivateActionToolButton in ActionToolBar
- If a submenu action has an icon set, make sure to also display it
- [Separator] Match Breeze borders' color
- Add Kirigami ListSectionHeader component
- Fix context menu button for pages not showing up
- Fix PrivateActionToolButton with menu not clearing checked state properly
- allow to set custom icon for the left drawer handle
- Rework the visibleActions logic in SwipeListItem
- Allow usage of QQC2 actions on Kirigami components and now make K.Action based on QQC2.Action
- Kirigami.Icon: Fix loading bigger images when source is a URL (bug 400312)
- Add icon used by Kirigami.AboutPage

### KItemModels

- Add Q_PROPERTIES interface to KDescendantsProxyModel
- Port away from deprecated methods in Qt

### KItemViews

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KNotification

- Avoid duplicate notifications from showing up on Windows and remove whitespaces
- Have 1024x1024 app icon as fallback icon in Snore
- Add <code>-pid</code> parameter to Snore backend calls
- Add snoretoast backend for KNotifications on Windows

### KPeople

- Make it possible to delete contacts from backends
- Make it possible to modify contacts

### KPlotting

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KRunner

- Make sure we're checking whether tearing down is due after finishing a job
- Add a done signal to FindMatchesJob instead of using QObjectDecorator wrongly

### KTextEditor

- Allow to customize attributes for KSyntaxHighligting themes
- Fix for all themes: allow turn off attributes in XML highlighting files
- simplify isAcceptableInput + allow all stuff for input methods
- simplify typeChars, no need for return code without filtering
- Mimic QInputControl::isAcceptableInput() when filtering typed characters (bug 389796)
- try to sanitize line endings on paste (bug 410951)
- Fix: allow turn off attributes in XML highlighting files
- improve word completion to use highlighting to detect word boundaries (bug 360340)
- More porting from QRegExp to QRegularExpression
- properly check if diff command can be started for swap file diffing (bug 389639)
- KTextEditor: Fix left border flicker when switching between documents
- Migrate some more QRegExps to QRegularExpression
- Allow 0 in line ranges in vim mode
- Use CMake find_dependency instead of find_package in CMake config file template

### KTextWidgets

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KUnitConversion

- Add decibel power units (dBW and multiples)

### KWallet Framework

- KWallet: fix starting kwalletmanager, the desktop file name has a '5' in it

### KWayland

- [server] Wrap proxyRemoveSurface in smart pointer
- [server] Use cached current mode more and assert validness
- [server] Cache current mode
- Implement zwp_linux_dmabuf_v1

### KWidgetsAddons

- [KMessageWidget] Pass widget to standardIcon()
- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KWindowSystem

- KWindowSystem: add cmake option KWINDOWSYSTEM_NO_WIDGETS
- Deprecate slideWindow(QWidget *widget)
- Add KWindowSystem::setMainWindow(QWindow *) overload
- KWindowSystem: add setNewStartupId(QWindow *...) overload

### KXMLGUI

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### NetworkManagerQt

- Rename WirelessDevice::lastRequestScanTime to WirelessDevice::lastRequestScan
- Add property lastScanTime and lastRequestTime to WirelessDevice

### Plasma Framework

- Make notification icons use outline style
- make the sizing of the toolbuttons more coherent
- Allow applets/containments/wallpaper to defer UIReadyConstraint
- Make notification icons look like bells (bug 384015)
- Fix incorrect initial tabs position for vertical tab bars (bug 395390)

### Scopo

- Fixed Telegram Desktop plugin on Fedora

### QQC2StyleBridge

- Prevent dragging QQC2 ComboBox contents outside menu

### Solid

- Make battery serial property constant
- Expose technology property in battery interface

### Sonnet

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### Evidenziazione della sintassi

- C &amp; ISO C++: add digraphs (folding &amp; preprocessor) (bug 411508)
- Markdown, TypeScript &amp; Logcat: some fixes
- Format class: add functions to know if XML files set style attributes
- combine test.m stuff into existing highlight.m
- Support for native Matlab strings
- Gettext: Add "Translated String" style and spellChecking attribute (bug 392612)
- Set the OpenSCAD indentor to C-style instead of none
- Possibility to change Definition data after loading
- Highlighting indexer: check kateversion
- Markdown: multiple improvements and fixes (bug 390309)
- JSP: support of &lt;script&gt; and &lt;style&gt; ; use IncludeRule ##Java (bug 345003)
- LESS: import CSS keywords, new highlighting and some improvements
- JavaScript: remove unnecessary "Conditional Expression" context
- New syntax: SASS. Some fixes for CSS and SCSS (bug 149313)
- Use CMake find_dependency in CMake config file instead of find_package
- SCSS: fix interpolation (#{...}) and add the Interpolation color
- fix additionalDeliminator attribute (bug 399348)
- C++: contracts are not in C++20
- Gettext: fix "previous untranslated string" and other improvements/fixes
- Jam: Fix local with variable without initialisation and highlight SubRule
- implicit fallthrough if there is fallthroughContext
- Add common GLSL file extensions (.vs, .gs, .fs)
- Latex: several fixes (math mode, nested verbatim, ...) (bug 410477)
- Lua: fix color of end with several levels of condition and function nesting
- Highlighting indexer: all warnings are fatal

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
