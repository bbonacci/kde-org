---
aliases:
- ../../kde-frameworks-5.32.0
date: 2017-03-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Implement nested tags

### Icone Brezza

- Added icons for Plasma Vault
- Renamed icons for encrypted and decrypted folders
- Add torrents icon 22px
- Add nm-tray icons (bug 374672)
- color-management: removed undefined links (bug 374843)
- system-run is now an action until &lt;= 32px and 48px an app icon (bug 375970)

### Moduli CMake aggiuntivi

- Detect inotify
- Revert "Automatically mark classes with pure virtual functions as /Abstract/."

### KActivitiesStats

- Allow to plan ahead and set the order for an item not yet in the list

### KArchive

- Fix Potential leak of memory pointed to by 'limitedDev'

### KCMUtils

- Fixed potential crash in QML KCMs when application palette changes

### KConfig

- KConfig: stop exporting and installing KConfigBackend

### KConfigWidgets

- KColorScheme: default to application scheme if set by KColorSchemeManager (bug 373764)
- KConfigDialogManager: get change signal from metaObject or special property
- Fix KCModule::setAuthAction error checking

### KCoreAddons

- Exclude (6) from emoticons recognition
- KDirWatch: fix memory leak on destruction

### Supporto KDELibs 4

- Fix bug in kfiledialog.cpp that causes crashing when native widgets are used

### KDocTools

- meinproc5: link to the files, not to the library (bug 377406)
- Remove the KF5::XsltKde static library
- Export a proper shared library for KDocTools
- Port to categorized logging and clean includes
- Add function to extract a single file
- Fail the build early if xmllint is not available (bug 376246)

### KFileMetaData

- New maintainer for kfilemetadata
- [ExtractorCollection] Use mimetype inheritance to return plugins
- add a new property DiscNumber for audio files from multi-disc albums

### KIO

- Cookies KCM: disable "delete" button when there is no current item
- kio_help: Use the new shared library exported by KDocTools
- kpac: Sanitize URLs before passing them to FindProxyForURL (security fix)
- Import remote ioslave from plasma-workspace
- kio_trash: implement renaming of toplevel files and dirs
- PreviewJob: Remove maximum size for local files by default
- DropJob: allow to add application actions on an open menu
- ThumbCreator: deprecate DrawFrame, as discussed in https://git.reviewboard.kde.org/r/129921/

### KNotification

- Add support for flatpak portals
- Send desktopfilename as part of notifyByPopup hints
- [KStatusNotifierItem] Restore minimized window as normal

### KPackage Framework

- Finish support for opening compressed packages

### KTextEditor

- Remember file type set by user over sessions
- Reset filetype when opening url
- Added getter for word-count configuration value
- Consistent conversion from/to cursor to/from coordinates
- Update file type on save only if path changes
- Support for EditorConfig configuration files (for details: http://editorconfig.org/)
- Add FindEditorConfig to ktexteditor
- Fix: emmetToggleComment action doesn't work (bug 375159)
- Use sentence style capitalization with label texts of edit fields
- Reverse meaning of :split, :vsplit to match vi and Kate actions
- Use C++11 log2() instead of log() / log(2)
- KateSaveConfigTab: put spacer behind last group on Advanced tab, not inside
- KateSaveConfigTab: Remove wrong margin around content of Advanced tab
- Borders config subpage: fix scrollbar visibility combobox being off-placed

### KWidgetsAddons

- KToolTipWidget: hide tooltip in enterEvent if hideDelay is zero
- Fix KEditListWidget losing the focus on click of buttons
- Add decomposition of Hangul Syllables into Hangul Jamo
- KMessageWidget: fix behaviour on overlapping calls of animatedShow/animatedHide

### KXMLGUI

- Don't use KConfig keys with backslashes

### NetworkManagerQt

- Sync introspections and generated files with NM 1.6.0
- Manager: Fix emitting deviceAdded twice when NM restarts

### Plasma Framework

- set default hints when repr doesn't export Layout.* (bug 377153)
- possible to set expanded=false for a containment
- [Menu] Improve available space correction for openRelative
- move setImagePath logic into updateFrameData() (bug 376754)
- IconItem: Add roundToIconSize property
- [SliderStyle] Allow providing a "hint-handle-size" element
- Connect all connections to action in QMenuItem::setAction
- [ConfigView] Honor KIOSK Control Module restrictions
- Fix disabling the spinner animation when Busy indicator has no opacity
- [FrameSvgItemMargins] Don't update on repaintNeeded
- Applet icons for the Plasma Vault
- Migrate AppearAnimation and DisappearAnimation to Animators
- Align bottom edge to top edge of visualParent in the TopPosedLeftAlignedPopup case
- [ConfigModel] Emit dataChanged when a ConfigCategory changes
- [ScrollViewStyle] Evaluate frameVisible property
- [Button Styles] Use Layout.fillHeight instead of parent.height in a Layout (bug 375911)
- [ContainmentInterface] Also align containment context menu to panel

### Prison

- Fix min qt version

### Solid

- Floppy disks now show up as "Floppy Disk" instead of "0 B Removable Media"

### Evidenziazione della sintassi

- Add more keyword. Disable spellchecking for keywords
- Add more keyword
- Add *.RHTML file extension to Ruby on Rails highlighting (bug 375266)
- Update SCSS and CSS syntax highlight (bug 376005)
- less highlighting: Fix single line comments starting new regions
- LaTeX highlighting: fix alignat environment (bug 373286)

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
