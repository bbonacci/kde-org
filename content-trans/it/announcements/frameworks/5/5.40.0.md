---
aliases:
- ../../kde-frameworks-5.40.0
date: 2017-11-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Consider DjVu files to be documents (bug 369195)
- Fix spelling so WPS Office presentations are recognized correctly

### Icone Brezza

- add folder-stash for the stash Dolphin toolbar icon

### KArchive

- Fix potential mem leak. Fix logic

### KCMUtils

- no margins for qml modules from qwidget side
- Initialize variables (found by coverity)

### KConfigWidgets

- Fix icon of KStandardAction::MoveToTrash

### KCoreAddons

- fix URL detection with double urls like "http://www.foo.bar&lt;http://foo.bar/&gt;"
- Use https for KDE urls

### Supporto KDELibs 4

- full docu for disableSessionManagement() replacement
- Make kssl compile against OpenSSL 1.1.0 (bug 370223)

### KFileMetaData

- Fix display name of Generator property

### KGlobalAccel

- KGlobalAccel: fix support numpad keys (again)

### KInit

- Correct installation of start_kdeinit when DESTDIR and libcap are used together

### KIO

- Fix display of remote:/ in the qfiledialog
- Implement support for categories on KfilesPlacesView
- HTTP: fix error string for the 207 Multi-Status case
- KNewFileMenu: clean up dead code, spotted by Coverity
- IKWS: Fix possible infinite loop, spotted by Coverity
- KIO::PreviewJob::defaultPlugins() function

### Kirigami

- syntax working on older Qt 5.7 (bug 385785)
- stack the overlaysheet differently (bug 386470)
- Show the delegate highlighted property as well when there's no focus
- preferred size hints for the separator
- correct Settings.isMobile usage
- Allow applications to be somewhat convergent on a desktop-y system
- Make sure the content of the SwipeListItem doesn't overlap the handle (bug 385974)
- Overlaysheet's scrollview is always ointeractive
- Add categories in gallery desktop file (bug 385430)
- Update the kirigami.pri file
- use the non installed plugin to do the tests
- Deprecate Kirigami.Label
- Port gallery example use of Labels to be consistently QQC2
- Port Kirigami.Controls uses of Kirigami.Label
- make the scrollarea interactive on touch events
- Move the git find_package call to where it's used
- default to transparent listview items

### KNewStuff

- Remove PreferCache from network requests
- Don't detach shared pointers to private data when setting previews
- KMoreTools: Update and fix desktopfiles (bug 369646)

### KNotification

- Remove check for SNI hosts when chosing whether to use legacy mode (bug 385867)
- Only check for legacy system tray icons if we're going to make one (bug 385371)

### KPackage Framework

- use the non installed service files

### KService

- Initialize values
- Initialize some pointer

### KTextEditor

- API dox: fix wrong names of methods and args, add missing \\since
- Avoid (certain) crashes while executing QML scripts (bug 385413)
- Avoid a QML crash triggered by C style indentation scripts
- Increase size of trailing mark
- fix some indenters from indenting on random characters
- Fix deprecation warning

### KTextWidgets

- Initialize value

### KWayland

- [client] Drop the checks for platformName being "wayland"
- Don't duplicate connect to wl_display_flush
- Wayland foreign protocol

### KWidgetsAddons

- fix createKMessageBox focus widget inconsistency
- more compact password dialog (bug 381231)
- Set KPageListView width properly

### KWindowSystem

- KKeyServer: fix handling of Meta+Shift+Print, Alt+Shift+arrowkey etc
- Support flatpak platform
- Use KWindowSystem's own platform detection API instead of duplicated code

### KXMLGUI

- Use https for KDE urls

### NetworkManagerQt

- 8021xSetting: domain-suffix-match is defined in NM 1.2.0 and newer
- Support "domain-suffix-match" in Security8021xSetting

### Plasma Framework

- manually draw the circle arc
- [PlasmaComponents Menu] Add ungrabMouseHack
- [FrameSvg] Optimize updateSizes
- Don't position a Dialog if it's of type OSD

### QQC2StyleBridge

- Improve compilation as a static plugin
- make the radiobutton a radiobutton
- use qstyle to paint the Dial
- use a ColumnLayout for menus
- fix Dialog
- remove invalid group property
- Fix formatting of the md file so it matches the other modules
- behavior of combobox closer to qqc1
- workaround for QQuickWidgets

### Sonnet

- Add assignByDictionnary method
- Signal if we are able to assign dictionary

### Evidenziazione della sintassi

- Makefile: fix regexpr matching in "CXXFLAGS+"

### ThreadWeaver

- CMake cleanup: Don't hardcode -std=c++0x

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
