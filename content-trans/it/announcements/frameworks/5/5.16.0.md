---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Monitor lib: Use Kformat::spelloutDuration to localize time string
- Use KDE_INSTALL_DBUSINTERFACEDIR to install dbus interfaces
- UnindexedFileIndexer: Handle files that have been moved when baloo_file was not running
- Remove Transaction::renameFilePath and add DocumentOperation for it.
- Make constructors with a single parameter explicit
- UnindexedFileIndexer: only index required parts of file
- Transaction: add method to return timeInfo struct
- Added exclude mimetypes to balooctl's config
- Databases: Use QByteArray::fromRawData when passing data to a codec
- Balooctl: Move 'status' command to its own class
- Balooctl: Show help menu if the command is not recognized
- Balooshow: Allow us to lookup files by their inode + devId
- Balooctl monitor: stop if baloo dies
- MonitorCommand: Use both the started and finished signals
- Balooctl monitor: Move to a proper command class
- Add dbus notification for when we start/finish indexing a file
- FileIndexScheduler: Forcibly kill threads on exit
- WriteTransaction commit: Avoid fetching the positionList unless required
- WriteTransaction: Extra asserts in replaceDocument

### BluezQt

- isBluetoothOperational now also depends on unblocked rfkill
- Fix determining global state of rfkill switch
- QML API: Mark properties without notify signal as constants

### Moduli CMake aggiuntivi

- Warn instead of error if ecm_install_icons finds no icons. (bug 354610)
- make it possible to build KDE Frameworks 5 with a plain qt 5.5.x installed from the normal qt.io installer on mac os
- Do not unset cache variables in KDEInstallDirs. (bug 342717)

### Integrazione della struttura

- Set default value for WheelScrollLines
- Fix WheelScrollLines settings with Qt &gt;= 5.5 (bug 291144)
- Switch to Noto font for Plasma 5.5

### KActivities

- Fixing the build against Qt 5.3
- Moved the boost.optional include to the place that uses it
- Replacing the boost.optional usage in continuations with a slimmer optional_view structure
- Added support for a custom ordering of linked results
- Allow QML to invoke activities KCM
- Adding the support for activity deletion to activities KCM
- New activity configuration UI
- New configuration UI that supports adding description and wallpaper
- Settings UI is now properly modularized

### KArchive

- Fix KArchive for behavior change in Qt 5.6
- Fix memleaks, lower memory usage

### KAuth

- Handle proxying qInfo messages
- Wait for async call starting helper to finish before checking the reply (bug 345234)
- Fix variable name, otherwise there's no way the include can work

### KConfig

- Fix usage of ecm_create_qm_loader.
- Fix include variable
- Use KDE*INSTALL_FULL* variant, so there is no ambiguity
- Allow KConfig to use resources as fallback config files

### KConfigWidgets

- Make KConfigWidgets self contained, bundle the one global file in a resource
- Make doctools optional

### KCoreAddons

- KAboutData: apidoc "is is" -&gt; "is" addCredit(): ocsUserName -&gt; ocsUsername
- KJob::kill(Quiet) should also exit the event loop
- Add support for desktop file name to KAboutData
- Use correct escaping character
- Reduce some allocations
- Make KAboutData::translators/setTranslators simple
- Fix setTranslator example code
- desktopparser: skip the Encoding= key
- desktopfileparser: Address review comments
- Allow setting service types in kcoreaddons_desktop_to_json()
- desktopparser: Fix parsing of double and bool values
- Add KPluginMetaData::fromDesktopFile()
- desktopparser: Allow passing relative paths to service type files
- desktopparser: Use more categorized logging
- QCommandLineParser uses -v for --version so just use --verbose
- Remove lots of duplicated code for desktop{tojson,fileparser}.cpp
- Parse ServiceType files when reading .desktop files
- Make SharedMimeInfo an optional requirement
- Remove call to QString::squeeze()
- desktopparser: avoid unnecessary utf8 decoding
- desktopparser: Don't add another entry if entry ends in a separator
- KPluginMetaData: Warn when a list entry is not a JSON list
- Add mimeTypes() to KPluginMetaData

### KCrash

- Improve search for drkonqui and keep it silent per default if not found

### KDeclarative

- ConfigPropertyMap can now be queried for immutable config options using the isImmutable(key) method
- Unbox QJSValue in config property map
- EventGenerator: Add support for sending wheel events
- fix lost QuickViewSharedEngine initialSize on initializing.
- fix critical regression for QuickViewSharedEngine by commit 3792923639b1c480fd622f7d4d31f6f888c925b9
- make pre-specified view size precede initial object size in QuickViewSharedEngine

### KDED

- Make doctools optional

### Supporto KDELibs 4

- Don't try to store a QDateTime in mmap'ed memory
- Sync and adopt uriencode.cmake from kdoctools.

### KDesignerPlugin

- Add KCollapsibleGroupBox

### KDocTools

- update pt_BR entities

### KGlobalAccel

- Do not XOR Shift for KP_Enter (bug 128982)
- Grab all keys for a symbol (bug 351198)
- Do not fetch keysyms twice for every keypress

### KHTML

- Fix printing from KHTMLPart by correctly setting printSetting parent

### KIconThemes

- kiconthemes now support themes embedded in qt resources inside the :/icons prefix like Qt does itself for QIcon::fromTheme
- Add missing required dependencies

### KImageFormats

- Recognize image/vnd.adobe.photoshop instead of image/x-psd
- Partially revert d7f457a to prevent crash on application exit

### KInit

- Make doctools optional

### KIO

- Save proxy url with correct scheme
- Ship the "new file templates" in the kiofilewidgets library using a .qrc (bug 353642)
- Properly handle middle click in navigatormenu
- Make kio_http_cache_cleaner deployable in application installer/bundles
- KOpenWithDialog: Fix creating desktop file with empty mimetype
- Read protocol info from plugin metadata
- Allow local kioslave deployment
- Add a .protocol to JSON converted
- Fix double-emit of result and missing warning when listing hits an inaccessible folder (bug 333436)
- Preserve relative link targets when copying symlinks. (bug 352927)
- Using suitable icons for default folders in the user home (bug 352498)
- Add an interface which allow plugin to show custom overlay icons
- Make KNotifications dep in KIO (kpac) optional
- Make doctools + wallet optional
- Avoid kio crashes if no dbus server is running
- Add KUriFilterSearchProviderActions, to show a list of actions for searching some text using web shortcuts
- Move the entries for the "Create New" menu from kde-baseapps/lib/konq to kio (bug 349654)
- Move konqpopupmenuplugin.desktop from kde-baseapps to kio (bug 350769)

### KJS

- Use "_timezone" global variable for MSVC instead of "timezone". Fixes build with MSVC 2015.

### KNewStuff

- Fix 'KDE Partition Manager' desktop file and homepage URL

### KNotification

- Now that kparts no longer needs knotifications, only things that really want notifications require on this framework
- Add description + purpose for speech + phonon
- Make phonon dependency optional, purely internal change, like it is done for speech.

### KParts

- Use deleteLater in Part::slotWidgetDestroyed().
- Remove KNotifications dep from KParts
- Use function to query ui_standards.rc location instead of hardcoding it, allows resource fallback to work

### KRunner

- RunnerManager: Simplify plugin loading code

### KService

- KBuildSycoca: always save, even if no change in .desktop file was noticed. (bug 353203)
- Make doctools optional
- kbuildsycoca: parse all the mimeapps.list files mentioned in the new spec.
- Use largest timestamp in subdirectory as resource directory timestamp.
- Keep MIME types separate when converting KPluginInfo to KPluginMetaData

### KTextEditor

- highlighting: gnuplot: add .plt extension
- fix validation hint, thanks to "Thomas Jarosch" &lt;thomas.jarosch@intra2net.com&gt;, add hint about the compile time validation, too
- Don't crash when command is not available.
- Fix bug #307107
- Haskell highlighting variables starting with _
- simplify git2 init, given we require recent enough version (bug 353947)
- bundle default configs in resource
- syntax highlighting (d-g): use default styles instead of hard-coded colors
- better scripts search, first user local stuff, then the stuff in our resources, then all other stuff, that way the user can overwrite our shipped scripts with local ones
- package all js stuff in resources, too, only 3 config files missing and ktexteditor could be just used as a library without any bundled files
- next try: put all bundled xml syntax files into a resource
- add input mode switch shortcut (bug 347769)
- bundle xml files in resource
- syntax highlighting (a-c): migrate to new default styles, remove hard-coded colors
- syntax highlighting: remove hard-coded colors and use default styles instead
- syntax highlighting: use new default styles (removes hard-coded colors)
- Better "Import" default style
- Introduce "Save As with Encoding" to save a file with different encoding, using the nice grouped encoding menu we have and replacing all save dialogs with the correct ones of the operating system without loosing this important feature.
- bundle ui file into lib, using my extension to xmlgui
- Printing again honors the selected font &amp; color schema (bug 344976)
- Use breeze colors for saved and modified lines
- Improved icon border default colors of scheme "Normal"
- autobrace: only insert brace when next letter is empty or not alphanumeric
- autobrace: if removing start parenthesis with backspace, remove end as well
- autobrace: only establish connection once
- Autobrace: eat closing parentheses under some conditions
- Fix shortcutoverride not being forwarded to the mainwindow
- Bug 342659 - Default "bracket highlighting" color is hard to see (Normal schema fixed) (bug 342659)
- Add proper default colors for "Current Line Number" color
- bracket matching &amp; auto-brackets: share code
- bracket matching: guard against negative maxLines
- bracket matching: just because the new range matches the old doesn't mean no update is required
- Add the width of half a space to allow painting the cursor at EOL
- fix some HiDPI issues in the icon border
- fix bug #310712: remove trailing spaces also on line with cursor (bug 310712)
- only display "mark set" message when vi input mode is active
- remove &amp; from button text (bug 345937)
- fix update of current line number color (bug 340363)
- implement brackets insert on writing a bracket over a selection (bug 350317)
- auto brackets (bug 350317)
- fix alert HL (bug 344442)
- no column scrolling with dyn word wrap on
- remember if highlighting was set by user over sessions to not loose it on save after restore (bug 332605)
- fix folding for tex (bug 328348)
- fixed bug #327842: End of C-style comment is misdetected (bug 327842)
- save/restore dyn word wrap on session save/restore (bug 284250)

### KTextWidgets

- Add a new submenu to KTextEdit to switch between spell-checking languages
- Fix loading Sonnet default settings

### KWallet Framework

- Use KDE_INSTALL_DBUSINTERFACEDIR to install dbus interfaces
- Fixed KWallet configuration file warnings on login (bug 351805)
- Prefix the kwallet-pam output properly

### KWidgetsAddons

- Add collapsible container widget, KCollapsibleGroupBox
- KNewPasswordWidget: missing color initialization
- Introduce KNewPasswordWidget

### KXMLGUI

- kmainwindow: Pre-fill translator information when available. (bug 345320)
- Allow to bind the contextmenu key (lower right) to shortcuts (bug 165542)
- Add function to query standards xml file location
- Allow kxmlgui framework to be used without any installed files
- Add missing required dependencies

### Plasma Framework

- Fix TabBar items being cramped together on initial creation, which can be observed in eg. Kickoff after Plasma start
- Fix dropping files onto the desktop/panel not offering a selection of actions to take
- Take QApplication::wheelScrollLines into account from ScrollView
- Use BypassWindowManagerHint only on platform X11
- delete old panel background
- more readable spinner at small sizes
- colored view-history
- calendar: Make the entire header area clickable
- calendar: Don't use current day number in goToMonth
- calendar: Fix updating decade overview
- Theme breeze icons when loaded trough IconItem
- Fix Button minimumWidth property (bug 353584)
- Introduce appletCreated signal
- Plasma Breeze Icon: Touchpad add svg id elements
- Plasma Breeze Icon: change Touchpad to 22x22px size
- Breeze Icon: add widget icon to notes
- A script to replace hardcoded colors with stylesheets
- Apply SkipTaskbar on ExposeEvent
- Don't set SkipTaskbar on every event

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
