---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE rilascia KDE Applications 16.12.3
layout: application
title: KDE rilascia KDE Applications 16.12.3
version: 16.12.3
---
9 marzo 2017. Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../16.12.0'>KDE Applications 16.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 20 errori corretti includono, tra gli altri, miglioramenti a kdepim, Ark, Filelight, Gwenview, Kate, Kdenlive e Okular.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.30.
