---
aliases:
- ../announce-applications-18.12-beta
date: 2018-11-16
description: KDE rilascia Applications 18.12 Beta.
layout: application
release: applications-18.11.80
title: KDE rilascia la beta di KDE Applications 18.12
---
16 novembre 2018. Oggi KDE ha rilasciato la beta della nuova versione delle applicazioni KDE (Applications). Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione dei bug e sull'ulteriore rifinitura del sistema.

Controlla le <a href='https://community.kde.org/Applications/18.12_Release_Notes'>note di rilascio della comunità</a> per informazioni sugli archivi e sui problemi noti. Un annuncio più completo verrà reso disponibile in concomitanza con la versione finale

I rilasci di KDE Applications 18.12 hanno bisogno di una verifica accurata per mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare gli errori il più presto possibile affinché possano essere eliminati prima della versione finale. Valutate la possibilità di partecipare alla squadra installando la versione &quot;beta&quot; <a href='https://bugs.kde.org/'>e segnalando qualsiasi problema</a>.
