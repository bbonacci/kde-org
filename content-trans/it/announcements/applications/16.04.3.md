---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE rilascia KDE Applications 16.04.3
layout: application
title: KDE rilascia KDE Applications 16.04.3
version: 16.04.3
---
12 luglio 2016. Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../16.04.0'>KDE Applications 16.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 20 errori corretti includono, tra gli altri, miglioramenti a Ark, Cantor, Kate, kdepim e Umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.22.
