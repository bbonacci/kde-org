---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: O KDE disponibiliza o KDE Applications 16.12.1
layout: application
title: O KDE disponibiliza o KDE Applications 16.12.1
version: 16.12.1
---
12 de Janeiro de 2017. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../16.12.0'>Aplicações do KDE 16.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Este lançamento inclui o reparo de um bug de PERDA DE DADOS no componente iCal que falha em criar o std.ics caso este não exista.

Mais de 40 correções de erros registradas incluem melhorias no kdepim, ark, gwenview, kajongg, okular, kate, kdenlive, entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.28.
