---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: O KDE disponibiliza o KDE Applications 16.04.0
layout: application
title: O KDE disponibiliza o KDE Applications 16.04.0
version: 16.04.0
---
20 de Abril de 2016. O KDE lançou hoje o KDE Applications 16.04, com uma lista impressionante variedade de atualizações relacionadas a uma melhor facilidade de acesso. A introdução de funcionalidades realmente úteis, bem como a eliminação de alguns problemas menores, faz com que o KDE Applications fique um passo mais próximo de lhe oferecer a configuração perfeita para o seu dispositivo.

O <a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, o <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, o <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> e o <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> foram agora migrados para as Plataformas do KDE 5 e estamos à espera da sua reacção e opiniões sobre as funcionalidades mais recentes que foram introduzidas com esta versão. Também o encorajamos em grande medida a dar o seu suporte ao <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a>, já que teve a sua introdução nas nossas Aplicações do KDE, para obter alguma informação sobre o que gostaria de ver.

### Novidade mais recente do KDE

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

Foi adicionada uma nova aplicação ao pacote de Educação do KDE. O <a href='https://minuet.kde.org'>Minuet</a> é uma Aplicação de Educação Musical que lhe oferece suporte completo de MIDI, com o controlo do tempo, timbre e volume, o que o torna adequado tanto para os músicos em aprendizagem como para os mais experientes.

O Minuet inclui 44 exercícios para treinamento de ouvido sobre escalas, acordes, intervalos e ritmos, ativa a visualização do conteúdo musical nas teclas do piano e permite a integração transparente dos seus próprios exercícios.

### Mais ajuda para você

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

O KHelpCenter, que antigamente era distribuído com o Plasma, agora integra o KDE Applications.

Uma forte campanha de triagem e limpeza de erros conduzida pela equipe do KHelpCenter resultou em 49 erros resolvidos, muitos dos quais estavam relacionados com a melhoria e no retorno da funcionalidade de pesquisa, que não estava funcionando.

O mecanismo de pesquisa interno dos documentos, que tinha como base o software ht::/dig, foi substituído por um sistema de indexação e pesquisa novo baseado no Xapian, o que leva ao retorno de funcionalidades, como a pesquisa dentro das páginas de manual (man e info) e da documentação fornecida pelo KDE, assim como a adição da funcionalidade de adicionar favoritos para as páginas da documentação.

Com a remoção do suporte para o KDELibs4 e com mais alguma limpeza do código e alguns erros menores, a manutenção do código foi também reorganizada profundamente para lhe apresentar o KHelpCenter de uma forma nova e agradável.

### Controle agressivo de problemas

A suíte Kontact teve um conjunto de 55 erros corrigidos; alguns dos quais estavam relacionados com problemas na definição de alarmes, bem como na importação de e-mails do Thunderbird, na redução do tamanho dos ícones do Skype e do Google Talk na área do Painel de Contatos, algumas soluções alternativas relacionadas com o KMail, como a importação de pastas, as importações de vCards, a abertura de anexos de e-mail em ODF, a introdução de URLs do Chromium e as diferenças no menu Ferramentas com o aplicativo iniciado como componente do Kontact, em comparação com o uso individual, a falta de um item de menu 'Enviar', entre outros. Foi adicionado o suporte para feeds RSS em Português do Brasil, em conjunto com a correção dos endereços dos feeds em Húngaro e Espanhol.

As novas funcionalidades incluem uma remodelação do editor de contatos KAddressbook, um novo tema padrão para cabeçalhos do KMail, melhorias no Exportador de Configurações e uma correção no suporte de Favicons no Akregator. A interface do compositor do KMail foi limpa, em conjunto com a introdução de um novo tema de cabeçalhos padrão do KMail com o tema Grantlee usado na página 'Sobre o KMail', tal como o Kontact e o Akregator. O Akregator agora usa o QtWebKit - um dos mecanismos mais importantes para renderização de páginas Web e execução de código em JavaScript - dado que o mecanismo de renderização Web e o processo de implementação do suporte para o QtWebEngine no Akregator e em outros aplicativos da Suíte Kontact já tinham sido iniciados. Embora diversas funcionalidades já estivessem implementadas como plugins no 'kdepim-addons', as bibliotecas PIM foram divididas em diversos pacotes.

### Manipulação de arquivos compactados

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

O <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, o gestor de pacotes, teve dois erros relevantes corrigidos, pelo que agora o Ark avisa o utilizador se uma extracção for mal-sucedida por causa de falta de espaço na pasta de destino, assim como também não preenche a RAM durante a extracção de ficheiros enormes por arrastamento.

O Ark também inclui agora uma caixa de diálogo de propriedades que mostra informações como o tipo de arquivo, o tamanho compactado e não-compactado, os códigos de validação MD5/SHA-1/SHA-256.

O Ark agora também consegue abrir e extrair arquivos RAR sem usar os utilitários não-livres, podendo também abrir, extrair e criar arquivos TAR compactados com os formatos lzop/lzip/lrzip.

O menu e a barra de ferramentas da interface de usuário do Ark foi reorganizada, de forma a melhorar a usabilidade e remover algumas ambiguidades. Agora a barra de status fica oculta por padrão, o que permite aproveitar melhor o espaço da janela.

### Mais precisão nas edições de vídeos

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

O <a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, o editor de vídeo não-linear, tem diversas novas funcionalidades implementadas. O gerador de títulos tem agora uma funcionalidade de grelha, gradientes, a adição de sombras no texto e o ajuste do espaço entre letras/linhas.

A apresentação integrada dos níveis de áudio permite o fácil monitoramento do áudio no seu projeto, em conjunto com novas camadas do monitor de vídeo que apresentam a taxa de imagens da reprodução, as áreas seguras e as formas de onda de áudio, bem como uma barra de ferramentas com marcadores de posição e um monitor de zoom.

Foi também adicionada uma nova funcionalidade da biblioteca que lhe permite copiar/colar as sequências entre projetos e uma exibição dividida na linha do tempo para comparar e visualizar os efeitos aplicados ao clipe. Um lado da divisão exibe com o efeito e o outro sem ele.

A caixa de diálogo de desenho foi reescrita com uma opção adicional para obter uma codificação mais rápida, produção de arquivos grandes e o efeito de velocidade foi feito também para funcionar com o som.

As curvas nos quadros-chave foram introduzidas para alguns efeitos e agora os seus efeitos preferidos podem ser rapidamente acessados através do widget de efeitos favoritos. Ao usar a ferramenta de corte, agora a linha vertical na linha do tempo irá mostrar a imagem exata onde será efetuado o corte, sendo que os geradores de clipes recém-adicionados permitem-lhe criar clipes de barras de cores e contadores. Além disso, a contagem de utilização do clipe foi reintroduzida nos arquivos do projeto, sendo que as miniaturas de áudio também foram reescritas para ficarem muito mais rápidas.

As correções de erros mais importantes incluem o falha inesperada na utilização de títulos (que obriga a usar a última versão do MLT), a correção de problemas que ocorriam quando a taxa de imagens por segundo do projeto são diferentes de 25, as falhas na exclusão de faixas, o modo de sobrescrição com problemas na linha do tempo e os problemas/perdas dos efeitos quando usavam um idioma que tem a vírgula como separador. Além desses problemas, a equipe trabalhou muito para conseguir novas melhorias na estabilidade, no funcionamento e em pequenas questões de usabilidade.

### E mais!

O visualizador de documentos <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> traz novas funcionalidades sob a forma de anotações incorporadas personalizadas com contornos, a possibilidade de abrir directamente ficheiros incorporados, em vez de apenas os gravar, e apresentar um índice de marcadores quando as ligações-filhas estiverem recolhidas.

O <a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> introduziu um suporte melhorado para o GnuPG 2.1, com uma correcção de erros de testes automáticos e as actualizações incómodas de chaves provocadas pela nova disposição de directórios do GnuPG (GNU Privacy Guard). A geração de chaves ECC foi adicionada, com a apresentação agora dos detalhes da Curva nos certificados ECC. O Kleopatra agora é lançado como um pacote em separado e foi incluído o suporte para os ficheiros .pfx e .crt. Para resolver as diferenças no comportamento das chaves privadas importadas e nas geradas, o Kleopatra permite-lhe agora muda a confiança no dono para total, no caso das chaves privadas importadas. 
