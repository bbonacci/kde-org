---
aliases:
- ../announce-applications-15.12-rc
date: 2015-12-03
description: O KDE Lança as Aplicações do KDE 15.12 Pré-Lançamento.
layout: application
release: applications-15.11.90
title: O KDE disponibiliza a versão Release Candidate do KDE Applications 15.12
---
3 de dezembro de 2015. Hoje o KDE disponibilizou o Release Candidate da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Com diversos aplicativos sendo preparados para o KDE Frameworks 5, as versões do KDE Applications 15.12 tem qualidade e experiência do usuário. Precisamos dos usuários atuais para manter a alta qualidade do KDE, porque os desenvolvedores simplesmente não conseguem testar todas as configurações possíveis. Contamos com você para nos ajudar a encontrar erros antecipadamente, para que possam ser corrigidos antes da versão final. Por favor, considere juntar-se à equipe, instalando esta versão e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
