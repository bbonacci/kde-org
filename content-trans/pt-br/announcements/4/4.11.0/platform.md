---
date: 2013-08-14
hidden: true
title: A Plataforma KDE 4.11 proporciona melhor desempenho
---
A Plataforma do KDE 4 tem estado com sem adição de novas funcionalidades desde a versão 4.9. Esta versão, por consequência, só inclui um conjunto de correções de erros e melhorias de desempenho.

O motor semântico do Nepomuk recebeu grandes optimizações de performance (p.ex., ler os dados é agora 6 ou mais vezes mais rápido). A indexação ocorre em duas fases: a primeira fase recolhe as informações gerais (como o tipo de ficheiro e o nome) de forma imediata: as informações adicionais como as marcas de MP3, a informação do autor e outras diversas são extraídas numa segunda fase mais lenta. A apresentação dos meta-dados é agora muito mais rápida. Para além disso, o sistema de salvaguarda e reposição do Nepomuk foi melhorado. O sistema recebeu também novos módulos de indexação para documentos, como o ODF ou o DOCX.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`As funcionalidades semânticas em ação no Dolphin` width="600px">}}

O formato de armazenamento otimizado e a nova indexação de e-mails do Nepomuk irão obrigar a reindexação de parte do disco rígido. Por consequência, a reindexação irá consumir uma quantidade anormal de desempenho de computação durante um determinado período – que depende da quantidade de conteúdo que precisa ser reindexada. Uma conversão automática do banco de dados Nepomuk será executada ao iniciar a sessão pela primeira vez.

Há também algumas pequenas correções que <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>podem ser consultadas no registro histórico do Git</a>.

#### Instalação da Plataforma de Desenvolvimento do KDE

O KDE, incluindo todas as suas bibliotecas e aplicações, está disponível gratuitamente segundo licenças de código aberto. As aplicações do KDE correm sobre várias configurações de 'hardware' e arquitecturas de CPU, como a ARM e a x86, bem como em vários sistemas operativos e gestores de janelas ou ambientes de trabalho. Para além do Linux e de outros sistemas operativos baseados em UNIX, poderá descobrir versões para o Microsoft Windows da maioria das aplicações do KDE nas <a href='http://windows.kde.org'>aplicações do KDE em Windows</a>, assim como versões para o Mac OS X da Apple nas <a href='http://mac.kde.org/'>aplicações do KDE no Mac</a>. As versões experimentais das aplicações do KDE para várias plataformas móveis, como o MeeGo, o MS Windows Mobile e o Symbian poderão ser encontradas na Web, mas não são suportadas de momento. O <a href='http://plasma-active.org'>Plasma Active</a> é uma experiência de utilizador para um espectro mais amplo de dispositivos, como tabletes ou outros dispositivos móveis.

As aplicações do KDE podem ser obtidas nos formatos de código-fonte e em vários formatos binários a partir de <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> e também podem ser obtidos via <a href='/download'>CD-ROM</a> ou com qualquer um dos <a href='/distributions'>principais sistemas GNU/Linux e UNIX</a> dos dias de hoje.

##### Pacotes

Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do %1 para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. <br />

##### Locais dos pacotes

Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki da Comunidade</a>.

Poderá <a href='/info/4/4.11.0'>transferir à vontade</a> o código-fonte completo de 4.11.0. As instruções de compilação e instalação da aplicação do KDE 4.11.0 está disponível na <a href='/info/4/4.11.0#binary'>Página de Informações do 4.11.0</a>.

#### Requisitos do sistema

Para tirar o máximo partido destas versões, recomendamos a utilização de uma versão recente do Qt, como a 4.8.4. Isto é necessário para garantir uma experiência estável e rápida, assim como algumas melhorias feitas no KDE poderão ter sido feitas de facto na plataforma Qt subjacente.<br />Para tirar um partido completo das capacidades das aplicações do KDE, recomendamos também que use os últimos controladores gráficos para o seu sistema, dado que isso poderá melhorar substancialmente a experiência do utilizador, tanto nas funcionalidades opcionais como numa performance e estabilidade globais.

## Também anunciado hoje:

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="A Área de Trabalho Plasma do KDE 4.11" width="64" height="64" /> A Área de Trabalho Plasma 4.11 Continua a Afinar a Experiência do Utilizador</a>

Preparando-se para uma manutenção a longo prazo, o Espaço de Trabalho Plasma oferece melhorias para as funcionalidades básicas com uma barra de tarefas mais suave, um widget de bateria mais inteligente e um mixer som melhorado. A inclusão do KScreen traz um tratamento inteligente para vários monitores do ambiente de trabalho, assim como algumas melhorias de desempenho em grande escala, combinadas com pequenos ajustes de usabilidade, para proporcionar uma experiência geral mais agradável.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="As Aplicações do KDE 4.11"/> As Aplicações do KDE 4.11 Dão um Enorme Passo em Frente na Gestão de Informações Pessoais e Tiveram Melhorias em Todo o Lado</a>

Esta versão marca grandes melhorias na plataforma do KDE PIM, ganhando melhor desempenho e muitas novas funcionalidades. O Kate melhora a produtividade de programadores em Python e JavaScript com novos plugins, o Dolphin ficou mais rápido e os aplicativos educacionais ganharam diversas novas funcionalidades.
