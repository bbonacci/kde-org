---
aliases:
- ../../kde-frameworks-5.1
- ./5.1
customIntro: true
date: '2014-08-08'
description: O KDE Lança a Segunda Versão das Plataformas 5.
layout: framework
qtversion: 5.2
title: Segunda versão do KDE Frameworks 5
---
7 de Agosto de 2014. O KDE lança hoje a segunda versão das Plataformas do KDE 5. Alinhado com a política planeada de lançamento das Plataformas do KDE, esta versão vem um mês depois da versão inicial e contém tanto correcções de erros com funcionalidades novas.

{{% i18n "annc-frameworks-intro" "60" "/announcements/frameworks/5/5.0" %}}

## {{< i18n "annc-frameworks-new" >}}

This release, versioned 5.1, comes with a number of bugfixes and new features including:

- KTextEditor: Major refactorings and improvements of the vi-mode
- KAuth: Now based on PolkitQt5-1
- New migration agent for KWallet
- Windows compilation fixes
- Translation fixes
- New install dir for KXmlGui files and for AppStream metainfo
- <a href='http://www.proli.net/2014/08/04/taking-advantage-of-opengl-from-plasma/'>Plasma Taking advantage of OpenGL</a>
