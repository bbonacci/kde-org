---
aliases:
- ../../kde-frameworks-5.36.0
date: 2017-07-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Todas as plataformas: Opção para compilar &amp; instalar o ficheiro QCH com a documentação pública da API

### Baloo

- Uso do FindInotify.cmake para decidir se o 'inotify' está disponível

### Ícones do Brisa

- Não depender desnecessariamente do 'bash' e não validar os ícones por omissão

### Módulos Extra do CMake

- FindQHelpGenerator: evitar a selecção da versão para Qt4
- ECMAddQch: falhar directamente caso as ferramentas necessárias não estejam presentes, para evitar surpresas
- Eliminação do 'perl' como dependência do ecm_add_qch, desnecessário/não usado
- Pesquisa a pasta de instalação inteira por dependências do QML
- Novo: ECMAddQch, para gerar ficheiros de marcas do 'qch' &amp; 'doxygen'
- Correcção do KDEInstallDirsTest.relative_or_absolute_usr, evitando o uso de locais do Qt

### KAuth

- Verificação do estado de erro após cada uso do PolKitAuthority

### KBookmarks

- Emissão de erros quando falta o 'keditbookmarks' (erro 303830)

### KConfig

- Correcção para o CMake 3.9

### KCoreAddons

- Uso do FindInotify.cmake para decidir se o 'inotify' está disponível

### KDeclarative

- KKeySequenceItem: possibilidade de registar o Ctrl+Num+1 como um atalho
- Início do arrasto nos eventos tácteis de pressão e manutenção da mesma (erro 368698)
- Não se basear no QQuickWindow a entregar o QEvent::Ungrab como mouseUngrabEvent (já que não é mais feito no Qt 5.8+) (erro 380354)

### Suporte para a KDELibs 4

- Pesquisa pelo KEmoticons, que é uma dependência de acordo com o config.cmake.in do CMake (erro 381839)

### KFileMetaData

- Adição de uma extracção com o 'qtmultimedia'

### KI18n

- Validação de que é gerado o destino do 'tsfiles'

### KIconThemes

- Mais detalhes sobre a instalação de temas de ícones no Mac &amp; MSWin
- Mudança do tamanho por omissão do painel para 48

### KIO

- [KNewFileMenu] Esconder o menu "Atalho para Dispositivo" se este ficar em branco (erro 381479)
- Uso do KIO::rename em vez do KIO::moveAs no 'setData' (erro 380898)
- Correcção da posição do menu no Wayland
- KUrlRequester: definição do sinal NOTIFY como 'textChanged()' na propriedade de texto
- [KOpenWithDialog] protecção do HTML no nome do ficheiro
- KCoreDirLister::cachedItemForUrl: não criar a 'cache' se não existir antes
- Uso de "data" como nome do ficheiro ao copiar URL's 'data' (erro 379093)

### KNewStuff

- Correcção da detecção incorrecta do erro na falta de ficheiros do 'knsrc'
- Exposição e uso da variável do tamanho de página do Engine
- Possibilidade de uso do QXmlStreamReader para ler um ficheiro de registo do KNS

### Plataforma KPackage

- Adição do ficheiro 'kpackage-genericqml.desktop'

### KTextEditor

- Correcção de picos no uso do CPU após mostrar a barra de comandos do VI (erro 376504)
- Correcção do arrastamento intermitente da barra de deslocamento quando está o mini-mapa activo
- Saltar para a posição carregada na barra de deslocamento quando o mini-mapa está activo (erro 368589)

### KWidgetsAddons

- Actualização do kcharselect-data para o Unicode 10.0

### KXMLGUI

- KKeySequenceWidget: possibilidade de registo do Ctrl+Num+1 como um atalho (erro 183458)
- Reversão do "Ao criar hierarquias de menus, associar como pais dos menus os seus contentores"
- Reversão do "uso directo do pai-transitório"

### NetworkManagerQt

- WiredSetting: as propriedade de 'activação-por-LAN' foram migradas para a versão anterior NM 1.0.6
- WiredSetting: a propriedade 'metered' foi migrada para a versão anterior NM 1.0.6
- Adição de novas propriedades a muitas classes de configuração
- Device: adição das estatísticas do dispositivo
- Adição do dispositivo IpTunnel
- WiredDevice: adição de informações sobre a versão necessária do NM para a propriedade 's390SubChannels'
- TeamDevice: adição da nova propriedade 'config' (desde o NM 1.4.0)
- Dispositivo com fios: adição da propriedade 's390SubChannels'
- Actualização das introspecções (NM 1.8.0)

### Plataforma do Plasma

- Validação de que o tamanho é final após o 'showEvent'
- Correcção das margens e do esquema de cores do ícone da bandeja do VLC
- Possibilidade de os Containments terem foco dentro da sua área (erro 381124)
- geração da chave antiga antes de actualizar o 'enabledborders' (erro 378508)
- mostrar o botão de apresentação da senha mesmo com texto vazio (erro 378277)
- Emissão do 'usedPrefixChanged' quando o prefixo está vazio

### Solid

- cmake: criação de infra-estrutura do 'udisks2' no FreeBSD só quando estiver activo

### Realce de Sintaxe

- Realce dos ficheiros *.julius como código JavaScript
- Haskell: Adição de todos os 'pragmas' da linguagem como palavras-chave
- CMake: OR/AND não realçado após a expressão entre () (erro 360656)
- Makefile: Remoção de itens inválidos de palavras-chave no makefile.xml
- indexação: Melhoria na apresentação dos erros
- Actualização da versão do ficheiro de sintaxe de HTML
- Adição dos modificadores do Angular nos atributos de HTML
- Actualização dos dados de referência dos testes a seguir às actualizações da modificação anterior
- Erro 376979 - parêntesis rectos nos comentários do 'doxygen' quebram o realce de sintaxe

### ThreadWeaver

- Alternativa ao erro do compilador do MSVC2017

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
