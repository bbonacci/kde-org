---
aliases:
- ../announce-applications-17.04-beta
date: 2017-03-24
description: O KDE Lança as Aplicações do KDE 17.04 Beta.
layout: application
release: applications-17.03.80
title: O KDE Lança a Primeira Versão 17.04 Beta das Aplicações
---
24 de Março de 2017. Hoje o KDE lançou a primeira das versões beta das novas Aplicações do KDE. Com as dependências e as funcionalidades estabilizadas, o foco da equipa do KDE é agora a correcção de erros e mais algumas rectificações.

Veja mais informações nas <a href='https://community.kde.org/Applications/17.04_Release_Notes'>notas de lançamento da comunidade</a> sobre novos pacotes, pacotes que sejam agora baseados no KF5 e problemas conhecidos. Será disponibilizado um anúncio mais completo para a versão final

As versões Aplicações do KDE 17.04 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do utilizador. Os utilizadores actuais são críticos para manter a alta qualidade do KDE, dado que os programadores não podem simplesmente testar todas as configurações possíveis. Contamos consigo para nos ajudar a encontrar erros antecipadamente, para que possam ser rectificados antes da versão final. Por favor, pense em juntar-se à equipa, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
