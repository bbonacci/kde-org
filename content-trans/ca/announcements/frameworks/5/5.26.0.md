---
aliases:
- ../../kde-frameworks-5.26.0
date: 2016-09-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Afegeix Qt5Network com a dependència pública

### BluezQt

- Esmena la inclusió de directori en el fitxer «pri»

### Icones Brisa

- Afegeix definicions de prefix d'espai de noms que mancaven
- Comprova que les icones SVG estiguin ben formades
- Esmena totes les icones «edit-clear-location-ltr» (error 366519)
- Afegeix la implementació per les icones d'efecte del KWin
- Reanomena «caps-on» a «input-caps-on»
- Afegeix icones de majúsculs per l'entrada de text
- Afegeix diverses icones específiques del Gnome del Sadi58
- Afegeix icones d'aplicació des de «gnastyle»
- Les icones del Dolphin, Konsole i Umbrello s'han optimitzat per 16px, 22px, 32px
- S'ha actualitzat la icona del VLC per 22px, 32px i 48px
- S'ha afegit una icona d'aplicació per al Subtitle Composer
- Esmena la icona nova del Kleopatra
- Afegeix una icona d'aplicació per al Kleopatra
- S'han afegit icones per al Wine i el Wine-qt
- Soluciona l'error d'icona de les presentacions gràcies al Sadi58 (error 358495)
- Afegeix la icona «system-log-out» de 32px
- Afegeix les icones «system-» de 32px, elimina les icones «system-» acolorides
- Afegeix la implementació d'icones del Pidgin i el Banshee
- Elimina la icona de l'aplicació VLC per problemes de llicència, afegeix una icona nova del VLC (error 366490)
- Afegeix la implementació d'icones del Gthumb
- Usa «HighlightedText» per les icones de carpeta
- Les icones de carpeta de llocs ara usen full d'estils (color «highlight»)

### Mòduls extres del CMake

- ecm_process_po_files_as_qm: Omet les traduccions no enllestides
- El nivell per omissió per enregistrar categories hauria de ser «Info» abans que «Avís»
- Documenta la variable ARGS en els destins de «create-apk-*»
- Crea una prova que valida la informació «appstream» dels projectes

### Eines de Doxygen del KDE

- Afegeix una condició si les plataformes de grup no estan definides
- Plantilla: Ordena alfabèticament les plataformes

### KCodecs

- Adapta de les «kdelibs» el fitxer usat per generar «kentities.c»

### KConfig

- Afegeix l'entrada Donatius a KStandardShortcut

### KConfigWidgets

- Afegeix l'acció estàndard de Donatius

### KDeclarative

- [kpackagelauncherqml] Assumeix que el nom del fitxer «desktop» és el mateix que «pluginId»
- Carrega els paràmetres de representació del QtQuick des d'un fitxer de configuració i els defineix per omissió
- icondialog.cpp - Esmena adequada de la compilació que no mostra «shadow m_dialog»
- Esmena una fallada quan el QApplication no estigui disponible
- Exposa el domini de les traduccions

### Compatibilitat amb les KDELibs 4

- Esmena un error de compilació del Windows a «kstyle.h»

### KDocTools

- Afegeix camins per «config», «cache» + «data» a «general.entities»
- Fa una actualització a la versió anglesa
- Afegeix les entitats de les tecles Espai i Meta a src/customization/en/user.entities

### KFileMetaData

- Només requereix Xattr si el sistema operatiu és el Linux
- Restaura la construcció per al Windows

### KIdleTime

- [xsync] XFlush a «simulateUserActivity»

### KIO

- KPropertiesDialog: Elimina la nota d'avís de la documentació, ja que l'error s'ha solucionat
- [programa de prova] Resoldre els camins relatius usant QUrl::fromUserInput
- KUrlRequester: Esmena el quadre d'error en seleccionar un fitxer i tornar a obrir el diàleg de fitxer
- Proporcionar una alternativa si els esclaus no llisten l'entrada «.» (error 366795)
- Esmena la creació d'enllaços simbòlics mitjançant el protocol «desktop»
- KNewFileMenu: En crear enllaços simbòlics usa KIO::linkAs en lloc de KIO::link
- KFileWidget: Esmena el «/» doble en el camí
- KUrlRequester: Usa la sintaxi «connect()» estàtica, era incoherent
- KUrlRequester: Passa «window()» com a pare per al «QFileDialog»
- Evita la crida «connect(null, .....)» del KUrlComboRequester

### KNewStuff

- Descomprimeix arxius en subcarpetes
- Ja no permet instal·lar en carpetes de dades genèriques per un forat de seguretat potencial

### KNotification

- Obté la propietat ProtocolVersion de StatusNotifierWatcher de manera asíncrona

### Paquets dels Frameworks

- Silencia els avisos d'obsolescència de «contentHash»

### Kross

- Reverteix «Elimina les dependències no utilitzades del KF5»

### KTextEditor

- Elimina els conflictes d'acceleradors (error 363738)
- Esmena el ressaltat d'adreces de correu en el «doxygen» (error 363186)
- Detecta més fitxers Json, com els nostres propis projectes ;)
- Millora la detecció del tipus MIME (error 357902)
- Error 363280 - Ressaltat: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif (error 363280)
- Error 363280 - Ressaltat: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif
- Error 351496 - El plegat del Python no funciona durant el tecleig inicial (error 351496)
- Error 365171 - Ressaltat de la sintaxi del Python: No funciona correctament amb les seqüències d'escapada (error 365171)
- Error 344276 - La «nowdo» del PHP no es plega correctament (error 344276)
- Error 359613 - Diverses propietats del CSS3 no s'admeten en el ressaltat de sintaxi (error 359613)
- Error 367821 - Sintaxi del wineHQ: La secció d'un fitxer «reg» no es ressalta correctament (error 367821)
- Millora la gestió del fitxer d'intercanvi si s'ha especificat el directori «swap»
- Soluciona la fallada en recarregar documents amb ajust automàtic de línia per al límit de longitud de la línia (error 366493)
- Soluciona fallades constants relacionades amb la barra d'ordres del «vi» (error 367786)
- Solució: Els números de línia en documents impresos ara comencen per 1 (error 366579)
- Còpia de seguretat de fitxers remots: Tracta els fitxers muntats com a fitxers remots
- Neteja la lògica de la creació de la barra de cerca
- Afegeix el ressaltat per a Magma
- Permet només un nivell de recursivitat
- Soluciona el fitxer d'intercanvi trencat al Windows
- Pedaç: Afegeix la implementació del «bitbake» en el motor de ressaltat de sintaxi
- Autobrace: Cerca l'atribut de verificació ortogràfica a on s'ha introduït el caràcter (error 367539)
- Ressalta QMAKE_CFLAGS
- No surt del context principal
- Afegeix diversos noms d'executables que són d'ús comú

### KUnitConversion

- Afegeix la unitat de massa britànica «stone»

### Framework del KWallet

- Mou el «docbook» del kwallet-query al subdirectori correcte
- Correcció del mot «an» -&gt; «one»

### KWayland

- Fa opcional «linux/input.h» en temps de compilació

### KWidgetsAddons

- Soluciona el fons dels caràcters no BMP
- Afegeix la cerca en UTF-8 escapat en octal al C
- Fa que KMessageBoxDontAskAgainMemoryStorage desi per defecte a QSettings

### KXMLGUI

- Adaptat a l'acció estàndard per a Donatius
- Elimina l'adaptació a la «authorizeKAction» obsoleta

### Frameworks del Plasma

- Soluciona la icona de 22px de dispositiu que no funcionava en el fitxer antic
- WindowThumbnail: Fa les crides GL en el fil correcte (error 368066)
- Fa que «plasma_install_package» funcioni amb KDE_INSTALL_DIRS_NO_DEPRECATED
- Afegeix un marge i farciment a la icona start.svgz
- Soluciona el full d'estil a la icona d'ordinador
- Afegeix icones d'ordinador i de portàtil per al Kicker (error 367816)
- Esmena de l'avís «No es pot assignar indefinit a un doble» a DayDelegate
- Esmena el full d'estil dels fitxers «svgz» amb problemes
- Reanomena les icones 22px a 22-22-x i les icones 32px a x per al Kicker
- [PlasmaComponents TextField] No es molesta en carregar icones per als botons no utilitzats
- Comprovació extra a Containment::corona en el cas especial de la safata del sistema
- En marcar un contenidor com a suprimit, marcar també totes les subminiaplicacions com suprimides - soluciona la no supressió de les configuracions del contenidor safata del sistema
- Soluciona la icona del Notificador de dispositius
- Afegeix «system-search» a «system» en les mides 32 i 22px
- Afegeix icones monocromes per al Kicker
- Defineix l'esquema de colors de la icona «system-search»
- Mou «system-search» dins system.svgz
- Esmena els «X-KDE-ParentApp» erronis o que manquin a les definicions del fitxer «desktop»
- Esmena la «dox» de l'API del Plasma::PluginLoader: confusió de miniaplicacions/motors de dades/serveis/...
- Afegeix la icona «system-search» per al tema de l'SDDM
- Afegeix una icona de 32px del Nepomuk
- Actualitza la icona del ratolí tàctil per la safata del sistema
- Elimina codi que mai es pot executar
- [ContainmentView] Mostra els plafons quan la IU estigui preparada
- No tornar a declarar la propietat «implicitHeight»
- Usa QQuickViewSharedEngine::setTranslationDomain (error 361513)
- Afegeix la implementació de les icones de 22px i 32px del Brisa
- Elimina les icones de sistema acolorides i afegeix les monocromes de 32px
- Afegeix un botó opcional de revelació de contrasenya al TextField
- Els consells d'eina estàndards ara s'emmirallen en els idiomes d'esquerra a dreta
- Ha millorat notablement el rendiment en canviar els mesos del calendari

### Sonnet

- No passa a minúscules els noms dels idiomes en l'anàlisi dels trigrames
- Soluciona una fallada immediata en iniciar per un apuntador de connector nul
- Gestiona els diccionaris sense els noms correctes
- Substitueix la llista manual de mapatges entre idiomes i alfabets, usa els noms adequats dels idiomes
- Afegeix una eina per generar trigrames
- Soluciona una mica la detecció d'idioma
- Usa l'idioma seleccionat com a suggeriment per a la detecció
- Usa els verificadors d'ortografia en memòria cau en la detecció d'idioma, millora el rendiment
- Millora de la detecció d'idioma
- Filtra la llista de suggeriments dels diccionaris disponibles, elimina els duplicats
- Recorda l'addició de la darrera coincidència de trigrama
- Comprova si qualsevol dels trigrames realment coincideixen
- Gestiona diversos idiomes amb la mateixa puntuació en el cercador de coincidències de trigrames
- No comprova dues vegades la mida mínima
- Neteja la llista d'idiomes a partir dels idiomes disponibles
- Usa la mateixa longitud mínima a tota la detecció d'idiomes
- Comprovació de seguretat que el model carregat té la quantitat correcta de trigrames per a cada idioma

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
