---
aliases:
- ../../kde-frameworks-5.75.0
date: 2020-10-10
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

+ [AdvancedQueryParser] Relaxa l'anàlisi de final de cadena amb parèntesis
+ [AdvancedQueryParser] Relaxa l'anàlisi de final de cadena amb comparador
+ [AdvancedQueryParser] Corregeix un accés fora de límit si el darrer caràcter és un comparador
+ [Term] Substitueix el constructor Term::Private pels valors predeterminats
+ [BasicIndexingJob] Drecera de recuperació del XAttr per als fitxers sense atributs
+ [extractor] Corregeix el tipus de document al resultat de l'extracció
+ Canvia la llicència del fitxer a la LGPL-2.0 o posterior

### BluezQt

+ Afegeix la propietat «rfkill» al gestor
+ Afegeix la propietat «status» a «rfkill»
+ Registra Rfkill per al QML
+ Exporta Rfkill
+ Implementa el subministrament de valors de dades de serveis per als anuncis LE

### Icones Brisa

+ Afegeix la icona genèrica nova «behavior»
+ Fa dependre la validació de la icona només de la generació de la icona si s'ha habilitat
+ Substitueix l'script Bash de les icones de 24px per un script Python
+ Usa una iconografia d'estil indicador per «view-calendar-holiday»
+ Afegeix el logo del Plasma Nano
+ Afegeix «application-x-kmymoney»
+ Afegeix la icona del KMyMoney

### Mòduls extres del CMake

+ Corregeix «fetch-translations» per als URL de l'«invent»
+ Inclou FeatureSummary i cerca de mòduls
+ Introdueix una comprovació de plausibilitat per a les llicències de sortida
+ Afegeix CheckAtomic.cmake
+ Corregeix la configuració amb «pthread» a l'Android de 32 bits
+ Afegeix el paràmetre RENAME a «ecm_generate_dbus_service_file»
+ Corregeix «find_library» a l'Android amb NDK &lt; 22
+ Ordena explícitament les llistes de versió de l'Android
+ Emmagatzema en variables {min,target,compile}Sdk de l'Android

### Eines de Doxygen del KDE

+ Millores de llicència
+ Corregeix api.kde.org al mòbil
+ Fa que api.kde.org sigui una PWA

### KArchive

+ Canvia la llicència del fitxer a la LGPL-2.0 o posterior

### KAuth

+ Usa una variable nova d'instal·lació (error 415938)
+ Marca en David Edmundson com a mantenidor del KAuth

### KCalendarCore

+ Canvia la llicència del fitxer a la LGPL-2.0 o posterior

### KCMUtils

+ Elimina la gestió d'esdeveniments de dins del pedaç de pestanya (error 423080)

### KCompletion

+ Canvia la llicència de fitxers a la LGPL-2.0 o posterior

### KConfig

+ CMake: també defineix SKIP_AUTOUIC en els fitxers generats
+ Usa l'ordre invers a KDesktopFile::locateLocal per a iterar pels camins de configuració genèrics

### KConfigWidgets

+ Corregeix «isDefault» que provoca que el KCModule no actualitzi apropiadament el seu estat predeterminat
+ [kcolorscheme] Afegeix «isColorSetSupported» per a comprovar si un esquema de color té un conjunt de colors indicat
+ [kcolorscheme] Llegeix apropiadament els colors personalitzats Inactive per als fons

### KContacts

+ Elimina el fitxer obsolet de llicència de LGPL-2.0 només
+ Canvia la llicència de fitxers a la LGPL-2.0 o posterior

### KCoreAddons

+ KJob: emet «result()» i «finished()» una vegada com a màxim
+ Afegeix el «getter» protegit KJob::isFinished()
+ Fa obsolet KRandomSequence a favor de QRandomGenerator
+ Inicialitza la variable a la classe capçalera + «const'ify» de variable/apuntador
+ Proves més sòlides basades en missatge contra l'entorn (error 387006)
+ Simplifica la prova de vigilància de «qrc» (error 387006)
+ Refcount i supressió de les instàncies KDirWatchPrivate (error 423928)
+ Fa obsolet KBackup::backupFile() per la pèrdua de funcionalitat
+ Fa obsolet KBackup::rcsBackupFile(...) perquè no hi ha usuaris coneguts

### KDBusAddons

+ Canvia la llicència de fitxers a la LGPL-2.0 o posterior

### KDeclarative

+ Afegeix QML per a i18n als KF 5.17
+ Canvia la llicència de fitxers a la LGPL-2.0 o posterior
+ Bloqueja les dreceres en enregistrar les seqüències de tecles (error 425979)
+ Afegeix SettingHighlighter com una versió manual del ressaltat fet per SettingStateBinding

### Compatibilitat amb les KDELibs 4

+ KStandardDirs: sempre resol els enllaços simbòlics per als fitxers de configuració

### KHolidays

+ Descomenta els camps de descripció dels fitxers dels festius de «mt_*»
+ Afegeix els festius nacionals de Malta tant en anglès (en-gb) com maltès (mt)

### KI18n

+ Canvia la llicència del fitxer a la LGPL-2.0 o posterior

### KIO

+ KUrlNavigator: sempre usa «desktop:/», no «desktop:»
+ Permet la sintaxi «bang» del DuckDuckGo a les dreceres web (error 374637)
+ KNewFileMenu: KIO::mostLocalUrl només és útil amb els protocols «:local»
+ Fa obsolet KIO::pixmapForUrl
+ kio_trash: elimina una comprovació innecessària de permís estricte (error 76380)
+ OpenUrlJob: gestiona coherentment tots els scripts de text (error 425177)
+ KProcessRunner: més metadades del «systemd»
+ KDirOperator: no crida «setCurrentItem» en un URL buit (error 425163)
+ KNewFileMenu: corregeix la creació d'un directori nou amb el nom que comença per «:» (error 425396)
+ StatJob: fa més clar que «mostLocalUr» només funciona amb els protocols «:local»
+ Documenta com afegir rols «random» nous a «kfileplacesmodel.h»
+ Elimina el pedaç antic de «kio_fonts» a KCoreDirLister, el nom de màquina s'ha extret incorrectament
+ KUrlCompletion: ajusta els protocols «:local» que usen el nom de màquina a l'URL
+ Divideix el codi del mètode «addServiceActionsTo» en mètodes més petits
+ [kio] Error: permetre cometes dobles al diàleg d'obrir/desar (error 185433)
+ StatJob: cancel·la el treball si l'URL no és vàlid (error 426367)
+ Connecta explícitament els sòcols en lloc d'usar connexions automàtiques
+ Fa que funcioni realment l'API de «filesharingpage»

### Kirigami

+ AbstractApplicationHeader: «anchors.fill» en lloc d'ancoratge depenent de la posició
+ Millora l'aspecte i la coherència de GlobalDrawerActionItem
+ Elimina el sagnat del formulari per a disposicions estretes
+ Reverteix «Permet personalitzar els colors de la capçalera»
+ Permet personalitzar els colors de la capçalera
+ Afegeix un «@since» que mancava a les propietats de l'àrea pintada
+ Presenta les propietats «paintedWidth»/«paintedHeight» de l'estil Image del QtQuick
+ Afegeix una propietat «placeholder» d'imatge a la icona (a l'estil de «fallback»)
+ Elimina el comportament «implicitWidth/implicitHeight» personalitzat d'Icon
+ Evita un apuntador potencialment null (que resulta sorprenentment normal)
+ S'ha protegit «setStatus»
+ Presenta la propietat «status»
+ Implementa els proveïdors d'imatge de tipus ImageResponse i Texture a Kirigami::Icon
+ Avisa que la gent no usi ScrollView a ScrollablePage
+ Reverteix «Mostra sempre el separador»
+ Fa que «mobilemode» admeti delegats de títol personalitzats
+ Oculta la línia separadora dels fils d'Ariadna si la disposició dels botons és visible però d'amplada 0 (error 426738)
+ [icon] Considera no vàlida una icona quan l'origen és un URL buit
+ Canvia Units.fontMetrics per usar realment FontMetrics
+ Afegeix la propietat Kirigami.FormData.labelAlignment
+ Mostra sempre el separador
+ Usa els colors de Header per a l'estil de l'escriptori AbstractApplicationHeader
+ Usa el context del component en crear delegats per a ToolBarLayout
+ Interromp i suprimeix els incubadors en suprimir ToolBarLayoutDelegate
+ Elimina les accions i els delegats de ToolBarLayout quan es destrueixen (error 425670)
+ Substitueix el «cast» d'apuntador de l'estil C a «sizegroup»
+ Les constants binàries són una ampliació del C++14
+ sizegroup: Corregeix una enumeració que no gestiona els avisos
+ sizegroup: Corregeix els «connects» de 3arg
+ sizegroup: Afegeix CONSTANT al senyal
+ Corregeix determinats casos d'usar bucles d'intervals en contenidors Qt «non-const»
+ Restringeix l'alçada del botó a la barra d'eines global
+ Mostra un separador entre els fils d'Ariadna i les icones a l'esquerra
+ Usa KDE_INSTALL_TARGETS_DEFAULT_ARGS
+ La mida d'ApplicationHeaders usant la SizeGroup
+ Presenta SizeGroup
+ Correcció: fa que l'indicador d'actualització aparegui sobre les capçaleres de la llista
+ Posa les OverlaySheets sobre dels calaixos

### KItemModels

+ Ignora «sourceDataChanged» als índexs no vàlids
+ Implementa els nodes «collapsing» de KDescendantProxyModel

### KNewStuff

+ Segueix manualment el cicle de vida dels detalls interns de l'executor del «kpackage»
+ Actualitza les versions per a les actualitzacions «falses» del «kpackage» (error 427201)
+ No usa el paràmetre predeterminat quan no sigui necessari
+ Corregeix una fallada en instal·lar «kpackages» (error 426732)
+ Detecta quan canvia la memòria cau i reacciona en conseqüència
+ Corregeix l'actualització de l'entrada si el número de versió està buit (error 417510)
+ «Const'ify» un apuntador + inicialitza la variable a la capçalera
+ Canvia la llicència del fitxer a la LGPL-2.0 o posterior
+ Accepta el suggeriment d'adoptar el manteniment

### KNotification

+ Rebaixa el connector de l'Android fins que estigui disponible el Gradle nou
+ Usa les versions de l'SDK de l'Android des de l'ECM
+ Fa obsolet el constructor del KNotification que agafa un paràmetre del giny

### Framework del KPackage

+ Corregeix les notificacions del DBus quan s'instal·la/actualitza
+ Canvia la llicència del fitxer a la LGPL-2.0 o posterior

### KParts

+ Instal·la els fitxers de definició de tipus de servei «krop» i «krwp» per al tipus de coincidència de nom de fitxer

### KQuickCharts

+ Evita el bucle de vinculació dins el Legend
+ Elimina la comprovació del GLES3 al SDFShader (error 426458)

### KRunner

+ Afegeix la propietat «matchRegex» per a evitar un engendrament innecessari de fils
+ Permet definir accions al QueryMatch
+ Permet especificar accions individuals per a les coincidències d'executor de D-Bus
+ Canvia la llicència de fitxers a la LGPL-2.0 o posterior
+ Afegeix la propietat del comptador de lletres mínim
+ Considera la variable d'entorn XDG_DATA_HOME per als directoris d'instal·lació de les plantilles
+ Millora els missatges d'error dels executors de D-Bus
+ Comença a emetre avisos d'adaptació de les metadades en temps d'execució

### KService

+ Retornar «disableAutoRebuild» des del límit (error 423931)

### KTextEditor

+ [kateprinter] Elimina l'adaptació dels mètodes obsolets de QPrinter
+ No crea una memòria intermèdia temporal per a detectar el tipus MIME del fitxer local desat
+ Evita penjar-se per la càrrega del diccionari i dels trigrames en el primer tecleig
+ [Vimode] Mostra sempre les memòries intermèdies «a-z» en minúscules
+ [KateFadeEffect] Emet «hideAnimationFinished()» quan un esvaïment de sortida s'interromp amb un esvaïment d'entrada
+ Assegura una vora perfecta dels píxels inclús per a una renderització escalada
+ Assegura el sobrepintat del separador de la vora sobre tota la resta, com el ressaltat del plegat
+ Mou el separador entre la vora de la icona i els nombres de línia a entre la barra i el text
+ [Vimode] Corregeix el comportament dels registres numerats
+ [Vimode] Posa el text suprimit al registre adequat
+ Restaura el comportament de cercar la selecció quan no hi ha disponible cap selecció
+ No més fitxers LGPL-2.1-only o LGPL-3.0-only
+ Canvia la llicència de fitxers a la LGPL-2.0 o posterior
+ Usa un mètode de definició no necessari per a diversos colors de tema
+ La 5.75 serà incompatible més endavant, valor predeterminat a «Auto Color Theme Selection» per als temes
+ Canvia esquema =&gt; tema al codi per a evitar confusió
+ Escurça la capçalera proposada de llicència a l'estat actual
+ Assegura que sempre s'acaba amb un tema vàlid
+ Millora el diàleg Copia...
+ Corregeix més noms: nou =&gt; copia
+ Afegeix alguns KMessageWidget que afecten temes de només lectura per a copiar-los, reanomena nou =&gt; copia
+ Inhabilita l'edició de temes de només lectura
+ Treballs de desament de sobreescriptura d'estil específic de ressaltat, només es desen les diferències
+ Simplifica la creació d'atributs, els colors transparents ara es gestionen apropiadament a Format
+ Intenta limitar l'exportació a canvis dels atributs, això funciona a mitges, però encara s'exporten els noms incorrectes de les definicions incloses
+ Comença a calcular els valors predeterminats «reals» basats en el tema i els formats actuals sense sobreescriptura d'estil per al ressaltat
+ Corregeix l'acció de reinici
+ Emmagatzematge de sobreescriptures específiques de sintaxi, actualment s'emmagatzema tot el que s'ha carregat a la vista en arbre
+ Comença el treball a les sobreescriptures específiques de sintaxi, de moment només mostra els estils amb un ressaltat que és el seu
+ Permet que es desin els canvis de l'estil predeterminat
+ Permet que es desin els canvis de color
+ Implementa l'exportació de tema: còpia única de fitxer
+ No s'importa/exporta un ressaltat específic, no té sentit amb el format nou de «.theme»
+ Implementa la importació de fitxers «.theme»
+ Treball de tema nou i supressió. El nou copiarà el tema actual com a punt de partida
+ Usa els colors de tema per tot arreu
+ Eliminació de més codi antic d'esquema a favor de KSyntaxHighlighting::Theme
+ Comença a usar els colors establerts al tema, sense lògica pròpia referent a això
+ Inicialitza «m_pasteSelection» i augmenta la versió del fitxer de la IU
+ Afegeix una drecera per a enganxar la selecció del ratolí
+ Evita «setTheme», només cal passar el tema a les funcions de l'ajudant
+ Corregeix el consell d'eina, això el reiniciarà al predeterminat del tema
+ Exporta la configuració dels estils predeterminats a un tema JSON
+ Comença a treballar en l'exportació JSON del tema, activada per usar l'extensió «.theme» al diàleg d'exportació
+ Reanomena «Usa el tema de color KDE» a «Usa els colors predeterminats», que és l'efecte real
+ No distribueix el tema «KDE» buit de manera predeterminada
+ Permet la selecció automàtica del tema correcte per al tema de color Qt/KDE actual
+ Converteix els noms antics dels temes als nous, usa el fitxer nou de configuració, transfereix una vegada les dades
+ Mou la configuració del tipus de lletra a aparença, reanomena esquema =&gt; tema de color
+ Elimina el nom de tema predeterminat que hi ha al codi font, usa els mètodes d'accés de KSyntaxHighlighting
+ Carrega els colors alternatius del tema
+ No empaqueta els colors incrustats en absolut
+ Usa la funció correcta per a cercar el tema
+ Ara s'usen els colors de l'editor des del Tema
+ Usa l'enumeració KSyntaxHighlighting::Theme::EditorColorRole
+ Gestiona la transició Normal =&gt; Predeterminat
+ Primer pas: carrega la llista de temes des de KSyntaxHighlighting, ara ja estan com a esquemes coneguts al KTextEditor

### KUnitConversion

+ Usa majúscules per a «Fuel Efficiency»

### KWayland

+ No posa l'iterador QList::end() a la memòria cau si s'usa «erase()»

### KWidgetsAddons

+ kviewstateserializer.cpp - Protegeix contra una fallada a «restoreScrollBarState()»

### KWindowSystem

+ Canvia la llicència per a ser compatible amb la LGPL-2.1

### KXMLGUI

+ [kmainwindow] No suprimeix entrades d'un «kconfiggroup» no vàlid (error 427236)
+ No superposa les finestres principals en obrir instàncies addicionals (error 426725)
+ [kmainwindow] No crea finestres natives per les finestres que no són del nivell superior (error 424024)
+ KAboutApplicationDialog: evita la pestanya «Biblioteques» buida si s'ha definit HideKdeVersion
+ Mostra el codi de l'idioma addicionalment al seu nom (traduït) en canviar el diàleg d'idioma de l'aplicació
+ Fer obsolet KShortcutsEditor::undoChanges() a favor del nou «undo()»
+ Gestiona el tancament doble a la finestra principal (error 416728)

### Frameworks del Plasma

+ Corregeix «plasmoidheading.svgz» que s'instal·lava al lloc incorrecte (error 426537)
+ Proporciona el valor «lastModified» en el ThemeTest
+ Detecta que s'està cercant un element buit i surt aviat
+ Fa que els Tooltips del PlasmaComponents3 usin l'estil típic dels consells d'eina (error 424506)
+ Usa els colors de Header a les capçaleres de PlasmoidHeading
+ Canvia l'animació del moviment de ressaltat de la TabBar de PC2 alleujant el tipus a OutCubic
+ Afegeix la implementació per al conjunt de colors de Tooltip
+ Corregeix les icones Button/ToolButton de PC3 que no sempre tenen el conjunt de colors correcte (error 426556)
+ Assegura que FrameSvg usa el segell de temps «lastModified» en usar la memòria cau (error 426674)
+ Assegura que sempre hi ha un segell de temps «lastModified» vàlid quan es crida «setImagePath»
+ Fa obsolet un segell de temps «lastModified» de 0 a Theme::findInCache (error 426674)
+ Adapta la importació del QQC2 a l'esquema nou de versionat
+ [windowthumbnail] Verifica que existeixi el GLContext més important, no qualsevol
+ Afegeix la importació de PlasmaCore que manca a ButtonRow.qml
+ Corregeix més errors de referència a PlasmaExtras.ListItem
+ Corregeix un error per «implicitBackgroundWidth» a PlasmaExtras.ListItem
+ Crida el mode d'edició «Mode d'edició»
+ Corregeix un TypeError a QueryDialog.qml
+ Corregeix ReferenceError a PlasmaCore en Button.qml

### QQC2StyleBridge

+ També usa el color de text de ressalt quan «checkDelegate» està ressaltat (error 427022)
+ Respecta el clic a la barra de desplaçament per a saltar a la posició indicada (bug 412685)
+ No hereta els colors a l'estil de la barra d'eines de l'escriptori de manera predeterminada
+ Canvia la llicència del fitxer a la LGPL-2.0 o posterior
+ Usa els colors de capçalera només per a les barres d'eines de capçalera
+ Mou la declaració del conjunt de colors a un lloc que es pugui substituir
+ Usa els colors de Header per a la ToolBar d'estil Desktop
+ Afegeix la propietat «isItem» que manca, necessària per als arbres

### Sonnet

+ Desactualitza la sortida de trigrames

### Ressaltat de la sintaxi

+ AppArmor: corregeix la «regexp» de detecció de camins
+ AppArmor: actualitza el ressaltat per a l'AppArmor 3.0
+ Memòria cau del color per a les conversions «rgb» a «ansi256colors» (accelera la càrrega del Markdown)
+ SELinux: usa paraules clau d'inclusió
+ SubRip Subtitles i Logcat: millores petites
+ Generador per a «doxygenlua.xml»
+ Corregeix les fórmules «latex» del «doxygen» (error 426466)
+ Usa Q_ASSERT com a l'entorn de treball romanent + correcció d'assercions
+ Reanomena «--format-trace» a «--syntax-trace»
+ Aplica un estil a les regions
+ Traça els contexts i les regions
+ Usa el color de fons de l'editor de manera predeterminada
+ Ressaltador ANSI
+ Afegeix el dret de còpia i actualitza el color del separador al tema Radical
+ Actualitza el color del separador als temes Solarized
+ Millora el color del separador i la vora de la icona per als temes Ayu, Nord i Vim fosc
+ Fa menys intrusiu el color del separador
+ Importa l'esquema del Kate al convertidor de temes, per en Juraj Oravec
+ Una secció més predominant quant a la llicència, enllaça a la nostra còpia del MIT.txt
+ Primera plantilla per al generador «base16», https://github.com/chriskempson/base16
+ Afegeix la informació apropiada de llicència a tots els temes
+ Afegeix el tema de color Radical
+ Afegeix el tema de color Nord
+ Millora la presentació dels temes per a mostrar més estils
+ Afegeix els temes clar i fosc del «gruvbox», amb llicència MIT
+ Afegeix el tema de color «ayu» (amb variants clara, fosca i miratge)
+ Afegeix una alternativa POSIX per a l'assignació única de variables
+ Eines per a generar un gràfic a partir d'un fitxer de sintaxi
+ Corregeix la conversió automàtica del color QRgb == 0 a «black» en lloc de «transparent»
+ Afegeix el registre de canvis de Debian i fitxers d'exemple de control
+ Afegeix el tema de color «Dracula»

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
