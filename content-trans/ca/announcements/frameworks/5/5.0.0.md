---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE distribueix el primer llançament dels Frameworks 5.
layout: framework
qtversion: 5.2
title: Primer llançament dels Frameworks 5 de KDE
---
7 de juliol de 2014. La comunitat KDE anuncia amb satisfacció els Frameworks 5.0 de KDE. Els Frameworks 5 són la propera generació de les biblioteques del KDE, modularitzades i optimitzades per a una integració senzilla en les aplicacions Qt. Els Frameworks ofereixen una àmplia varietat de les funcionalitats necessàries més comunes amb unes biblioteques madures, revisades per iguals i ben provades, amb unes clàusules amistoses de llicència. Hi ha uns 50 Frameworks diferents com a part d'aquesta publicació que proporcionen solucions que inclouen integració de maquinari, implementació de formats de fitxer, ginys addicionals, funcions de dibuixat, verificació ortogràfica i més. Molts Frameworks són multiplataforma i tenen dependències mínimes o no en tenen d'extres, facilitant la seva construcció i afegir-les a qualsevol aplicació Qt.

Els Frameworks de KDE representen un esforç per refer les potents biblioteques de la Plataforma 4 del KDE en un conjunt de mòduls independents multiplataforma que estaran disponibles aviat a tots els desenvolupadors en les Qt per simplificar, accelerar i reduir el cost de desenvolupament en les Qt. Els Frameworks individuals són multiplataforma i estan ben documentats i provats i el seu ús serà familiar als desenvolupadors en les Qt, seguint l'estil i els estàndards definits pel projecte Qt. Els Frameworks s'han desenvolupat d'acord amb el model provat de govern del KDE amb una planificació de publicacions predictible, un procés de col·laboració clar i independent de venedors, governació oberta i llicència flexible (LGPL).

Els Frameworks tenen una estructura clara de dependències, dividides en categories i nivells. Les categories es refereixen a dependències en temps d'execució:

- Els elements <strong>funcionals</strong> no tenen dependències en temps d'execució.
- La <strong>integració</strong> designa codi que pot requerir dependències en temps d'execució per integració en funció del què ofereixi el SO o la plataforma.
- Les <strong>solucions</strong> tenen dependències en temps d'execució obligatòries.

Els <strong>nivells</strong> («Tiers») es refereixen a dependències d'altres Frameworks en temps de compilació. Els Frameworks de nivell 1 no tenen dependències amb els Frameworks i només necessiten les Qt i altres biblioteques importants. Els Frameworks de nivell 2 poden dependre només del nivell 1. Els Frameworks de nivell 3 poden dependre d'altres Frameworks de nivell 3 i també de nivell 2 i nivell 1.

La transició des de la Plataforma als Frameworks s'ha portat a terme durant 3 anys, guiat pels principals col·laboradors tècnics del KDE. Apreneu més quant als Frameworks 5 <a href='http://dot.kde.org/2013/09/25/frameworks-5'>en aquest article de l'any passat</a>.

## Cal destacar

Actualment hi ha més de 50 Frameworks disponibles. Consulteu el conjunt complet <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>a la documentació en línia de l'API</a>. A sota hi ha una impressió d'algunes funcionalitats que els Frameworks ofereixen als desenvolupadors d'aplicacions en Qt.

El <strong>KArchive</strong> ofereix la implementació per a molts còdecs populars de compressió en una biblioteca d'arxiu i extracció de fitxers autocontinguda, plena de funcionalitats i d'ús senzill. N'hi ha prou en passar-hi fitxers; no hi ha cap necessitat de reinventar cap funció d'arxiu en una aplicació basada en Qt!

El <strong>ThreadWeaver</strong> ofereix una API d'alt nivell per gestionar fils usant interfícies basades en treballs i cues. Permet una planificació senzilla de fils d'execució especificant dependències entre fils i executant-los en satisfer-se aquestes dependències, simplificant enormement l'ús de fils múltiples.

El <strong>KConfig</strong> és un Framework per tractar amb l'emmagatzematge i recuperació dels paràmetres de configuració. Ofereix una API orientada a grups. Funciona amb fitxers INI i directoris en cascada que compatibles amb XDG. Genera codi basat en fitxers XML.

El <strong>Solid</strong> ofereix detecció del maquinari i pot informar a una aplicació quant als dispositius d'emmagatzematge i volums, CPU, estat de les bateries, gestió d'energia, estat de la xarxa i interfícies, i Bluetooth. Per a les particions encriptades, energia i xarxes es requereix l'execució de dimonis.

El <strong>KI18n</strong> afegeix la funcionalitat del Gettext a les aplicacions, facilitant la integració del flux de treball de les traduccions a les aplicacions Qt en la infraestructura de traducció general de molts projectes.
