---
aliases:
- ../announce-applications-17.12-rc
custom_spread_install: true
date: 2017-12-01
description: Es distribueixen les aplicacions 17.12 versió candidata del KDE.
layout: application
release: applications-17.11.90
title: KDE distribueix la versió candidata 17.12 de les aplicacions del KDE
---
1 de desembre de 2017. Avui KDE distribueix la versió candidata de les noves versions de les aplicacions del KDE. S'han congelat les dependències i les funcionalitats, i ara l'equip del KDE se centra a corregir els errors i acabar de polir-la.

Verifiqueu les <a href='https://community.kde.org/Applications/17.12_Release_Notes'>notes de llançament de la comunitat</a> per a informació quant als arxius tar nous, els quals estan basats en els KF5 i quant als problemes coneguts. Hi haurà un anunci més complet disponible per al llançament final.

La distribució 17.12 de les aplicacions del KDE necessita una prova exhaustiva per tal de mantenir i millorar la qualitat i l'experiència d'usuari. Els usuaris reals són imprescindibles per mantenir l'alta qualitat del KDE, perquè els desenvolupadors no poden provar totes les configuracions possibles. Comptem amb vós per ajudar a trobar errors amb anticipació, a fi que es puguin solucionar abans de la publicació final. Considereu unir-vos a l'equip instal·lant la versió candidata i <a href='https://bugs.kde.org/'>informant de qualsevol error</a>.

#### Instal·lació dels paquets executables de la versió candidata 17.12 de les aplicacions del KDE

<em>Paquets</em>. Alguns venedors de Linux/UNIX OS han proporcionat gentilment els paquets executables de la 17.12 versió candidata (internament 17.11.90) de les aplicacions del KDE per a algunes versions de la seva distribució, i en altres casos ho han fet voluntaris de la comunitat. En les setmanes vinents poden arribar a estar disponibles paquets binaris addicionals, i també actualitzacions dels paquets actualment disponibles.

<em>Ubicacions dels paquets</em>. Per a una llista actual dels paquets executables disponibles dels quals s'ha informat l'equip de llançament del KDE, si us plau, visiteu el <a href='https://community.kde.org/KDE_Applications/Binary_Packages'>wiki de la comunitat</a>.

#### Compilació de les aplicacions 17.12 versió candidata del KDE

El codi font complet per a la 17.12 versió candidata de les aplicacions del KDE es pot <a href='https://download.kde.org/unstable/applications/17.11.90/src/'>descarregar de franc</a>. Les instruccions per a compilar i instal·lar el programari estan disponibles a la <a href='/info/applications/applications-17.11.90'>pàgina d'informació de la versió candidata de les aplicacions del KDE</a>.
