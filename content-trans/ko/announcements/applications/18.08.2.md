---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE에서 KDE 프로그램 18.08.2 출시
layout: application
title: KDE에서 KDE 프로그램 18.08.2 출시
version: 18.08.2
---
October 11, 2018. Today KDE released the second stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Dolphin, Gwenview, KCalc, Umbrello 등에 10개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- Dolphin에서 파일을 드래그할 때 실수로 즉시 이름 바꾸기가 실행되지 않음
- KCalc에서 소수점을 입력할 때 '마침표'와 '쉼표'를 다시 모두 사용할 수 있음
- KDE 카드 게임의 파리 카드 덱의 모양이 개선됨
