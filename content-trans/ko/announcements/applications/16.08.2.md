---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE에서 KDE 프로그램 16.08.2 출시
layout: application
title: KDE에서 KDE 프로그램 16.08.2 출시
version: 16.08.2
---
October 13, 2016. Today KDE released the second stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

kdepim, Ark, Dolphin, KGpg, Kolourpaint, Okular 등에 30개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.25.
