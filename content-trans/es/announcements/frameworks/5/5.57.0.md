---
aliases:
- ../../kde-frameworks-5.57.0
date: 2019-04-13
layout: framework
libCount: 70
---
### Attica

- Aceptar cualquier estado de HTTP entre 100 y 199 como benigna

### Baloo

- [DocumentIdDB] Silence non-error debug message, warn on errors
- [baloosearch] Allow specifying a time when using e.g. mtime
- [indexcleaner] Avoid removing included folders below excluded ones
- [MTimeDB] Fix lookup for the LessEqual range
- [MTimeDB] Fix lookup when time range should return empty set
- Correct asserts/error handling in MTimeDB
- Protegerse de nodos superiores no válidos en «IdTreeDB».
- Remove document from MTimeDB/DocumentTimeDB even when timestamp is 0
- Ser más preciso en la detección de tipos MIME (error 403902).
- [timeline] Canonicalize Url
- [timeline] Fix missing/misplaced SlaveBase::finished() calls
- [balooshow] Several extensions basic file information output
- [timeline] Fix warning, add missing UDS entry for "."
- [balooctl] Reduce nesting level for addOption arguments, cleanup
- React to config updates inside indexer (bug 373430)
- Fix regression when opening DB in read-write mode (bug 405317)
- [balooctl] Cleanup trailing whitespace
- [engine] Unbreak code, revert renaming of Transaction::abort()
- Harmonize handling of underscore in query parser
- Baloo engine: treat every non-success code as a failure (bug 403720)

### BluezQt

- Move Media interface into Adapter
- Manager: Don't require Media1 interface for initialization (bug 405478)
- Dispositivos: Comprobar la ruta del objeto en la ranura eliminada de la interfaz (error 403289).

### Iconos Brisa

- Se han añadido los iconos «notifications» y «notifications-disabled» (error 406121).
- make start-here-kde also available start-here-kde-plasma
- Sublime Merge Icon
- Give applications-games and input-gaming more contrast with Breeze Dark
- Make 24px go-up actually 24px
- Add preferences-desktop-theme-applications and preferences-desktop-theme-windowdecorations icons
- Add symlinks from "preferences-desktop-theme" to "preferences-desktop-theme-applications"
- Remove preferences-desktop-theme in preparation to making it a symlink
- Add collapse/expand-all, window-shade/unshade (bug 404344)
- Improve consistency of window-* and add more
- Make go-bottom/first/last/top look more like media-skip*
- Change go-up/down-search symlinks target to go-up/down
- Improve pixel grid alignment of go-up/down/next/previous/jump
- Change media-skip* and media-seek* style
- Forzar el nuevo estilo de icono silenciado en todos los iconos de acciones.

### Módulos CMake adicionales

- Re-enable the setting of QT_PLUGIN_PATH
- ecm_add_wayland_client_protocol: Improve error messages
- ECMGeneratePkgConfigFile: make all vars dependent on ${prefix}
- Se ha añadido el módulo para encontrar UDev.
- ECMGeneratePkgConfigFile: add variables used by pkg_check_modules
- Restore FindFontconfig backward compatibility for plasma-desktop
- Se ha añadido el módulo para encontrar Fontconfig.

### Integración con Frameworks

- use more appropriate plasma-specific icon for plasma category
- use plasma icon as icon for plasma notification category

### Herramientas KDE Doxygen

- Update URLs to use https

### KArchive

- Fix crash in KArchive::findOrCreate with broken files
- Se ha corregido la lectura de memoria no inicializada en KZip.
- Add Q_OBJECT to KFilterDev

### KCMUtils

- [KCModuleLoader] Pass args to created KQuickAddons::ConfigModule
- Pass focus to child searchbar when KPluginSelector is focused (bug 399516)
- Se ha mejorado el mensaje de error de KCM.
- Add runtime guard that pages are KCMs in KCMultiDialog (bug 405440)

### KCompletion

- Don't set a null completer on a non-editable combobox

### KConfig

- Add Notify capability to revertToDefault
- Hacer que el «readme» apunte a la página wiki.
- kconfig_compiler: new kcfgc args HeaderExtension &amp; SourceExtension
- [kconf_update] move from custom logging tech to qCDebug
- Se ha eliminado la referencia de «const KConfigIniBackend::BufferFragment &amp;».
- KCONFIG_ADD_KCFG_FILES macro: ensure a change of File= in kcfg is picked up

### KCoreAddons

- Fix "* foo *" we don't want to bold this string
- Fix Bug 401996 - clicking contact web url =&gt; uncomplete url is selected (bug 401996)
- Print strerror when inotify fails (typical reason: "too many open files")

### KDBusAddons

- Se han convertido dos conectores del estilo antiguo al nuevo.

### KDeclarative

- [GridViewKCM] Se ha corregido el cálculo de anchura implícita.
- move the gridview in a separate file
- Avoid fractionals in GridDelegate sizes and alignments

### Soporte de KDELibs 4

- Se han eliminado los módulos para encontrar que proporcionaba ECM.

### KDocTools

- Se ha actualizado la traducción ucraniana.
- Actualizaciones del catalán.
- it entities: update URLs to use https
- Update URLs to use https
- Usar la traducción indonesia.
- Se ha actualizado el diseño para que se asemeje más a kde.org.
- Se han añadido los archivos necesarios para usar el lenguaje indonesio nativo en todos los documentos en indonesio.

### KFileMetaData

- Se ha implementado la posibilidad de escribir información de puntuación en el escritor de «taglib».
- Se han implementado más etiquetas en el escritor de «taglib».
- Rewrite taglib writer to use property interface
- Test ffmpeg extractor using mime type helper (bug 399650)
- Propose Stefan Bruns as KFileMetaData maintainer
- Declare PropertyInfo as QMetaType
- Salvaguardias contra archivos no válidos.
- [TagLibExtractor] Use the correct mimetype in case of inheritance
- Add a helper to determine actual supported parent mime type
- [taglibextractor] Test extraction of properties with multiple values
- Generate header for new MimeUtils
- Use Qt function for string list formatting
- Fix number localization for properties
- Verify mimetypes for all existing sample files, add some more
- Se ha añadido una función auxiliar para determinar el tipo MIME basándose en el contenido y en la extensión (error 403902).
- Permitir la extracción de datos de archivos «ogg» y «ts» (error 399650).
- [ffmpegextractor] Se ha añadido el caso de prueba para vídeo Matroska (error 403902).
- Rewrite the taglib extractor to use the generic PropertyMap interface (bug 403902)
- [ExtractorCollection] Load extractor plugins lazily
- Corregir la extracción de la propiedad de relación de aspecto.
- Increase precision of frame rate property

### KHolidays

- Sort the polish holidays categories

### KI18n

- Report human-readable error if Qt5Widgets is required but is not found

### KIconThemes

- Fix padding icon that doesn't exactly match the requested size (bug 396990)

### KImageFormats

- ora:kra: qstrcmp -&gt; memcmp
- Corregir «RGBHandler::canRead».
- xcf: Don't crash with files with unsupported layer modes

### KIO

- Replace currentDateTimeUtc().toTime_t() with currentSecsSinceEpoch()
- Replace QDateTime::to_Time_t/from_Time_t with to/fromSecsSinceEpoch
- Se han mejorado los iconos de los botones del diálogo ejecutable (error 406090).
- [KDirOperator] Show Detailed Tree View by default
- KFileItem: call stat() on demand, add SkipMimeTypeDetermination option
- KIOExec: fix error when the remote URL has no filename
- KFileWidget In saving single file mode an enter/return press on the KDirOperator triggers slotOk (bug 385189)
- [KDynamicJobTracker] Use generated DBus interface
- [KFileWidget] When saving, highlight filename after clicking existing file also when using double-click
- Don't create thumbnails for encrypted Vaults (bug 404750)
- Fix WebDAV directory renaming if KeepAlive is off
- Show list of tags in PlacesView (bug 182367)
- Delete/Trash confirmation dialogue: Fix misleading title
- Display the correct file/path in "too bit for fat32" error message (bug 405360)
- Phrase error message with GiB, not GB (bug 405445)
- openwithdialog: use recursive flag in proxy filter
- Remove URLs being fetched when listing job is completed (bug 383534)
- [CopyJob] Treat URL as dirty when renaming file as conflict resolution
- Pass local file path to KFileSystemType::fileSystemType()
- Fix upper/lower case rename on case insensitive fs
- Fix "Invalid URL: QUrl("some.txt")" warnings in Save dialog (bug 373119)
- Se ha corregido un fallo al mover archivos.
- Fix NTFS hidden check for symlinks to NTFS mountpoints (bug 402738)
- Make file overwrite a bit safer (bug 125102)

### Kirigami

- fix listItems implicitWidth
- shannon entropy to guess monochrome icon
- Impedir que el cajón de contexto desaparezca.
- remove actionmenuitembase
- No intentar obtener la versión de compilaciones estáticas.
- [Mnemonic Handling] Replace only first occurrence
- sync when any model property updates
- use icon.name in back/forward
- fix toolbars for layers
- Fix errors in kirigami example files
- Se han añadido los componentes «SearchField» y «PasswordField».
- Se han corregido los iconos de asa (error 404714).
- [InlineMessage] Do not draw shadows around the message
- immediately layout on order changed
- fix breadcrumb layout
- never show toolbar when the current item asks not to
- manage back/forward in the filter as well
- Permitir el uso de los botones del ratón para ir atrás y adelante.
- Add lazy instantiation for submenus
- fix toolbars for layers
- kirigami_package_breeze_icons: Search among size 16 icons as well
- Fix Qmake based build
- get the attached property of the proper item
- fix logic when to show the toolbar
- possible to disable toolbar for layer's pages
- always show global toolbar on global modes
- signal Page.contextualActionsAboutToShow
- a bit of space to the right of the title
- relayout when visibility changes
- ActionTextField: Properly place actions
- topPadding and BottomPadding
- text on images always need to be white (bug 394960)
- clip overlaysheet (bug 402280)
- avoid parenting OverlaySheet to ColumnView
- use a qpointer for the theme instance (bug 404505)
- hide breadcrumb on pages that don't want a toolbar (bug 404481)
- don't try to override the enabled property (bug 404114)
- Posibilidad de tener cabecera y pie personalizados en «ContextDrawer» (error 404978).

### KConfigWidgets

- [KUiServerJobTracker] Update destUrl before finishing the job

### KNewStuff

- Cambiar varias URL a «https».
- Update link to fsearch project
- Handle unsupported OCS commands, and don't over-vote (bug 391111)
- New location for KNSRC files
- [knewstuff] Remove qt5.13 deprecated method

### KNotification

- [KStatusNotifierItem] Send desktop-entry hint
- Permitir la personalización de consejos para las notificaciones.

### KNotifyConfig

- Allow selecting only supported audio files (bug 405470)

### Framework KPackage

- Fix finding the host tools targets file in the Android docker environment
- Permitir la compilación cruzada de «kpackagetool5».

### KService

- Add X-GNOME-UsesNotifications as recognized key
- Add bison minimum version of 2.4.1 due to %code

### KTextEditor

- Corrección: Aplicar correctamente los colores del texto del esquema escogido (error 398758).
- DocumentPrivate: Add option "Auto Reload Document" to View menu (bug 384384)
- DocumentPrivate: Support to set dictionary on block selection
- Fix Words &amp; Chars String on katestatusbar
- Fix Minimap with QtCurve style
- KateStatusBar: Show lock icon on modified label when in read-only mode
- DocumentPrivate: Skip auto quotes when these looks already balanced (bug 382960)
- Se ha añadido la interfaz «Variable» a «KTextEditor::Editor».
- relax code to only assert in debug build, work in release build
- ensure compatibility with old configs
- more use of generic config interface
- simplify QString KateDocumentConfig::eolString()
- transfer sonnet setting to KTextEditor setting
- ensure now gaps in config keys
- convert more things to generic config interface
- more use of the generic config interface
- generic config interface
- No producir un fallo cuando se usan archivos de resaltado de sintaxis mal formados.
- IconBorder: Accept drag&amp;drop events (bug 405280)
- ViewPrivate: Make deselection by arrow keys more handy (bug 296500)
- Fix for showing argument hint tree on non-primary screen
- Se ha portado algún método desaconsejado.
- Restore the search wrapped message to its former type and position (bug 398731)
- ViewPrivate: Make 'Apply Word Wrap' more comfortable (bug 381985)
- ModeBase::goToPos: Ensure jump target is valid (bug 377200)
- ViInputMode: Remove unsupported text attributes from the status bar
- KateStatusBar: Add dictionary button
- add example for line height issue

### KWidgetsAddons

- Make KFontRequester consistent
- Se ha actualizado «kcharselect-data» a Unicode 12.0.

### KWindowSystem

- Send blur/background contrast in device pixels (bug 404923)

### NetworkManagerQt

- WireGuard: make marshalling/demarshalling of secrets from map to work
- Añadir el soporte para WireGuard que faltaba en la clase de preferencias base.
- WireGuard: manejar la clave privada como secretos.
- WireGuard: la propiedad «peers» debe ser «NMVariantMapList».
- Añadir soporte para el tipo de conexión WireGuard.
- ActiveConnecton: añadir la señal «stateChangedReason» donde se pueda ver el motivo del cambio de estado.

### Framework de Plasma

- [AppletInterface] Comprobar la existencia de «corona» antes de acceder a ella.
- [Dialog] Don't forward hover event when there is nowhere to forward it to
- [Menú] Corregir la señal de disparo.
- Reducir la importancia de alguna información de depuración para que se puedan ver las advertencias reales.
- [ComboBox de PlasmaComponents3] Corregir «textColor».
- FrameSvgItem: catch margin changes of FrameSvg also outside own methods
- Se ha añadido «Theme::blurBehindEnabled()».
- FrameSvgItem: fix textureRect for tiled subitems to not shrink to 0
- Fix breeze dialog background with Qt 5.12.2 (bug 405548)
- Se ha eliminado un fallo en «plasmashell».
- [Icon Item] Also clear image icon when using Plasma Svg (bug 405298)
- Altura del campo de text basada solo en texto en claro (fallo 399155).
- Enlazar «alternateBackgroundColor».

### Purpose

- Añadir el complemento SMS a KDE Connect.

### QQC2StyleBridge

- the plasma desktop style supports icon coloring
- [SpinBox] Se ha mejorado el comportamiento de la rueda del ratón.
- Se ha añadido un poco de relleno en las barras de herramientas.
- fix RoundButton icons
- scrollbar based padding on all delegates
- look for a scrollview to take its scrollbar for margins

### Solid

- Permitir la compilación sin UDev en Linux.
- Only get clearTextPath when used

### Resaltado de sintaxis

- Se ha añadido la definición de sintaxis para el lenguaje Elm al resaltado de sintaxis.
- AppArmor &amp; SELinux: remove one indentation in XML files
- Doxygen: no usar color negro en las etiquetas.
- Allow line end context switches in empty lines (bug 405903)
- Fix endRegion folding in rules with beginRegion+endRegion (use length=0) (bug 405585)
- Se han añadido extensiones para el resaltado de sintaxis de groovy (fallo 403072).
- Añadir el archivo de resaltado de sintaxis para Smali.
- Añadir «.» como delimitador débil en el archivo de resaltado de sintaxis de Octave.
- Logcat: Se ha corregido el color de «dsError» con «underline="0"».
- Se ha corregido un fallo del resaltador de sintaxis para un archivo «hl» dañado.
- guard target link libraries for older CMake version (bug 404835)

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
