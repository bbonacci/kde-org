---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Módulos CMake adicionales

- Mejora de la notificación de errores de la macro query_qmake

### BluezQt

- Desconectar todos los dispositivos del adaptador antes de desconectar el adaptador (error 349363).
- Actualizar los enlaces en README.md

### KActivities

- Añadir la opción de no realizar el seguimiento del usuario cuando está realizando actividades concretas (parecido al modo «navegación privada» en un navegador web)

### KArchive

- Conservar los permisos de ejecución de los archivos en copyTo()
- Mejorar la claridad de ~KArchive mediante la eliminación de código muerto

### KAuth

- Permitir utilizar kauth-policy-gen desde diferentes orígenes

### KBookmarks

- No añadir marcadores cuando la URL y el texto están vacíos
- Codificar la URL de KBookmark para solucionar el problema de compatibilidad con las aplicaciones KDE4

### KCodecs

- Eliminar el sondeo x-euc-tw 

### KConfig

- Instalar kconfig_compiler en libexec
- Nueva opción de generación de códigoTranslationDomain= para utilizarla con TranslationSystem=kde; normalmente utilizada en bibliotecas.
- Permitir utilizar kconfig_compiler desde diferentes orígenes

### KCoreAddons

- KDirWatch: establecer la conexión con FAM solo si es necesario
- Permitir el filtrado de complementos y aplicaciones por parte de formfactor
- Permitir utilizar desktoptojson desde diferentes orígenes

### KDBusAddons

- Mejorar la claridad del valor de salida para instancias únicas

### KDeclarative

- Añadir un clon QQC de KColorButton
- Asignar un QmlObject para cada instancia de kdeclarative cuando sea posible
- Compilar Qt.quit() a partir de código QML
- Incorporar rama 'mart/singleQmlEngineExperiment'
- Implementar sizeHint basad en implicitWidth/height
- Subclase de QmlObject con motor estático

### Soporte de KDELibs 4

- Corregir la implementación de KMimeType::Ptr::isNull.
- Volver a activar la transmisión sin interrupciones de KDateTime para kDebug/qDebug, para más SC
- Cargar el catálogo de traducción correcto para kdebugdialog
- No omitir la documentación de métodos obsoletos, permitiendo que se puedan leer las notas de adaptación

### KDESU

- Corregir CMakeLists.txt para pasar KDESU_USE_SUDO_DEFAULT al compilador, de modo que se pueda usar en suprocess.cpp

### KDocTools

- Actualizar las plantillas docbook de K5

### KGlobalAccel

- Se instala API privada en tiempo de ejecución para permitir que KWin proporcione complementos a Wayland.
- Fallback para la resolución de nombres de componentFriendlyForAction

### KIconThemes

- No intentar dibujar el icono si el tamaño no es válido.

### KItemModels

- Nuevo modelo de proxy: KRearrangeColumnsProxyModel. Admite la reordenación y el ocultado de columnas a partir del modelo de origen.

### KNotification

- Se han corregido los tipos pixmap en org.kde.StatusNotifierItem.xml
- [ksni] Se ha añadido un método para recuperar la acción por su nombre (error 349513)

### KPeople

- Se han implementado utilidades para el filtrado de PersonsModel

### KPlotting

- KPlotWidget: añade setAutoDeletePlotObjects para corregir una fuga de memoria en replacePlotObject
- Corregir la ausencia de marcas cuando x0 &gt; 0.
- KPlotWidget: no hace falta «setMinimumSize» ni «resize».

### KTextEditor

- debianchangelog.xml: añadir Debian/Stretch, Debian/Buster, Ubuntu-Wily
- Corregir el comportamiento del par sustituto «backspace/delete» de UTF-16.
- Permitir que QScrollBar maneje eventos de la rueda del ratón (error 340936)
- Aplicar el parche de desarrollo de KWrite de «Alexander Clay» &lt;tuireann@EpicBasic.org&gt;

### KTextWidgets

- Corregir la activación y desactivación del botón «Aceptar»

### Framework KWallet

- Se ha importado y mejorado la herramienta de la línea de órdenes «kwallet-query».
- Permitir el uso de sobrescritura de entradas de mapas.

### KXMLGUI

- No mostrar la «versión de KDE Frameworks» en el diálogo de «Acerca de KDE»

### Framework de Plasma

- Hacer que el tema oscuro sea completamente oscuro, incluso para el grupo complementario
- Usar la caché para «naturalsize» de forma separada por «scalefactor»
- ContainmentView: ya no falla cuando hay metadatos no válidos
- AppletQuickItem: no acceder a KPluginInfo si no es válido
- Corregir las páginas de configuración de la miniaplicación ocasionalmente vacías (error 349250)
- Mejorar el uso de hidpi en el componente de la cuadrícula de Calendar
- Verificar que KService posee información válida sobre complementos antes de usarlo
- [calendario] Asegurar que la cuadrícula se vuelve a dibujar cuando cambia el tema
- [calendario] Siempre se empieza a contar la semana en lunes (error 349044)
- [calendario] Volver a dibujar la cuadrícula cuando cambia el ajuste de mostrar los números de las semanas
- Ahora se usa un tema opaco cuando solo está disponible el efecto de hacerse borroso (error 348154)
- Lista blanca para componentes/versiones para un motor aparte
- Presentar la nueva clase ContainmentView

### Sonnet

- Permitir el uso de resalte de comprobación ortográfica en QPaintTextEdit

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
