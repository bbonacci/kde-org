---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE publica a versión 17.04.3 das aplicacións de KDE
layout: application
title: KDE publica a versión 17.04.3 das aplicacións de KDE
version: 17.04.3
---
July 13, 2017. Today KDE released the third stability update for <a href='../17.04.0'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 25 correccións de erros inclúen melloras en, entre outros, KDE PIM, Dolphin, DragonPlayer, Kdenlive e Umbrello.

This release also includes Long Term Support version of KDE Development Platform 4.14.34.
