---
aliases:
- ../announce-applications-14.12-rc
custom_spread_install: true
date: '2014-11-27'
description: KDE Ships Applications 14.12 Release Candidate.
layout: application
title: KDE publica a candidata a versión final da versión 14.12 das aplicacións de
  KDE
---
27 de novembro de 2014. Hoxe KDE publicou a candidata a versión final da nova versión das súas aplicacións. Coa desautorización temporal de dependencias e funcionalidades novas, agora o equipo de KDE centrase en solucionar fallos e pulir funcionalidades.

Debido ao gran número de aplicativos que agora se basean na versión 5 das infraestruturas de KDE, hai que probar ben a versión 14.12 das aplicacións de KDE para manter e mellorar a calidade e a experiencia de usuario. Os usuarios reais son críticos para manter unha alta calidade en KDE, porque os desenvolvedores simplemente non poden probar todas as configuracións posíbeis. Contamos con vostede para axudarnos a atopar calquera fallo canto antes para poder solucionalo antes da versión final. Considere unirse ao equipo instalando a candidata a versión final <a href='https://bugs.kde.org/'>e informando de calquera fallo</a>.

#### Instalar os paquetes binarios da candidata a versión final da versión 14.12 das aplicacións de KDE

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 14.12 Release Candidate (internally 14.11.97) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Compilar a candidata a versión final da versión 14.12 das aplicacións de KDE

The complete source code for KDE Applications 14.12 Release Candidate may be <a href='http://download.kde.org/unstable/applications/14.11.97/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-14.11.97'>KDE Applications Release Candidate Info Page</a>.
