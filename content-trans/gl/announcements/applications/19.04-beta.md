---
aliases:
- ../announce-applications-19.04-beta
date: 2019-03-22
description: KDE Ships Applications 19.04 Beta.
layout: application
release: applications-19.03.80
title: KDE publica a beta da versión 19.04 das aplicacións de KDE
version_number: 19.03.80
version_text: 19.04 Beta
---
22 de marzo de 2019. Hoxe KDE publicou a beta da nova versión das súas aplicacións. Coa desautorización temporal de dependencias e funcionalidades novas, agora o equipo de KDE centrase en solucionar fallos e pulir funcionalidades.

Check the <a href='https://community.kde.org/Applications/19.04_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

Hai que probar ben a versión 19.04 das aplicacións de KDE para manter e mellorar a calidade e a experiencia de usuario. Os usuarios reais son críticos para manter unha alta calidade en KDE, porque os desenvolvedores simplemente non poden probar todas as configuracións posíbeis. Contamos con vostede para axudarnos a atopar calquera erro canto antes para poder solucionalo antes da versión final. Considere unirse ao equipo instalando a beta <a href='https://bugs.kde.org/'>e informando de calquera fallo</a>.
