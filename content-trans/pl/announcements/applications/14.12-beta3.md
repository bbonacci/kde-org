---
aliases:
- ../announce-applications-14.12-beta3
custom_spread_install: true
date: '2014-11-20'
description: KDE wydało Aplikacje KDE 14.12 Beta 2.
layout: application
title: KDE wydało trzecią wersję beta Aplikacji KDE 14.12
---
20 listopad 2014. Dzisiaj KDE wydało betę nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym polerowaniu.

Ze względu na obecność wielu programów opartych na Szkieletach KDE 5, wydanie Aplikacji 14.12 wymaga dokładnego przetestowania w celu utrzymania, a nawet poprawienia jakości wrażeń użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu 4.12 poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.

#### Instalowanie pakietów binarnych Aplikacji KDE 14.12 Beta3

<em>Pakiety</em>. Niektórzy wydawcy systemów operacyjnych Linux/UNIX  uprzejmie dostarczyli pakiety binarne Aplikacji KDE 4.12 Beta 3 (wewnętrznie 14.11.95) dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności. Dodatkowe pakiety binarne, tak samo jak uaktualnienia do pakietów już dostępnych, mogą stać się dostępne w przeciągu nadchodzących tygodni.

<em>Położenie Pakietów</em>. Po bieżącą listę dostępnych pakietów  binarnych, o których został poinformowany Projekt KDE, zajrzyj na  stronę <a href='http://community.kde.org/KDE_SC/Binary_Packages'> Społeczności Wiki</a>.

#### Kompilowanie Aplikacji KDE 14.12 Beta 3

Pełny kod źródłowy dla Aplikacji KDE 14.12 Beta 3 można <a href='http://download.kde.org/unstable/applications/14.11.95/src/'>pobrać bez opłaty</a>.Instrukcje na temat kompilowania i instalowania są dostępne na <a href='/info/applications/applications-14.11.95.php'>Stronie informacyjnej Aplikacji KDE Beta 3</a>.
