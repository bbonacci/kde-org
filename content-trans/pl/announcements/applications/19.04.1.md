---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE wydało Aplikacje 19.04.1.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE wydało Aplikacje KDE 19.04.1
version: 19.04.1
---
{{% i18n_date %}}

Dzisiaj KDE wydało pierwszą aktualizację stabilności <a href='../19.04.0'> dla  Aplikacji KDE 19.04</a> . To wydanie zawiera tylko poprawki błędów i  aktualizacje tłumaczeń, zapewniając bezpieczną i przyjemną aktualizację  dla wszystkich.

About twenty recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle, Umbrello, among others.

Wśród ulepszeń znajdują się:

- Tagging files on the desktop no longer truncates the tag name
- A crash in KMail's text sharing plugin has been fixed
- Several regressions in the video editor Kdenlive have been corrected
