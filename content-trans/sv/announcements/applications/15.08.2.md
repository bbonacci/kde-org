---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE levererar KDE-program 15.08.2
layout: application
title: KDE levererar KDE-program 15.08.2
version: 15.08.2
---
13:e oktober, 2015. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../15.08.0'>KDE-program 15.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 30 registrerade felrättningar omfattar förbättringar av ark, gwenview, kate, kbruch, kdelibs, kdepim, lokalize och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.13.
