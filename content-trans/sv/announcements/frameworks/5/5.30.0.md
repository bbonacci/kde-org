---
aliases:
- ../../kde-frameworks-5.30.0
date: 2017-01-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Breeze-ikoner

- Lägg till Haguichi-ikoner från Stephen Brandt (tack)
- Slutför stöd för calligra-ikon
- Lägg till cups-ikon, tack Colin (fel 373126)
- Uppdatera kalarm-ikon (fel 362631)
- Lägg till ikon för krfb-program (fel 373362)
- Lägg till stöd för r Mime-typ (fel 371811)
- Med mera

### Extra CMake-moduler

- appstreamtest: Hantera icke-installerade program
- Aktivera färglagda varningar i utmatning från ninja
- Rätta saknad :: i dokumentation av programmeringsgränssnittet för att utlösa kodformatering
- Ignorera värddatorfil libs/includes/cmakeconfig i Android-verktygskedja
- Dokumentera användning av gnustl_shared med Android-verktygskedja
- Använd aldrig -Wl,--no-undefined på Mac (Apple)

### Integrering med ramverk

- Förbättra KPackage KNSHandler
- Sök efter den mer precisa version av AppstreamQt som krävs
- Lägg till stöd för heta nyheter i KPackage

### KActivitiesStats

- Gör så att kompilering med -fno-operator-names fungerar
- Hämta inte fler länkade resurser när en av dem försvinner
- Tillägg av modellroll för att hämta listan med länkade aktiviteter för en resurs

### KDE Doxygen-verktyg

- Lägg till Android i listan över tillgängliga plattformar
- Inkludera ObjC++ källkodsfiler

### KConfig

- Skapa en instans med KSharedConfig::Ptr för singleton och argument
- kconfig_compiler: Använd nullptr i genererad kod

### KConfigWidgets

- Dölj alternativet "Visa menyrad" om alla menyrader är inbyggda
- KConfigDialogManager: Ta bort kdelibs3-klasser

### KCoreAddons

- Returnera typerna stringlist och boolean i KPluginMetaData::value
- DesktopFileParser: Ta hänsyn till fältet ServiceTypes

### KDBusAddons

- Lägg till Python-bindningar för KDBusAddons

### KDeclarative

- Introducera org.kde.kconfig QML-import med KAuthorized

### KDocTools

- kdoctools_install: Matcha programmets fullständiga sökväg (fel 374435)

### KGlobalAccel

- [runtime] Introducera miljövariabeln KGLOBALACCEL_TEST_MODE

### KDE GUI Addons

- Lägg till Python-bindningar för KGuiAddons

### KHTML

- Ställ in data för insticksprogram så att den inbäddningsbara bildvisningen fungerar

### KIconThemes

- Informera också QIconLoader när skrivbordets ikontema ändras (fel 365363)

### KInit

- Ta hänsyn till egenskapen X-KDE-RunOnDiscreteGpu när program startas med hjälp av klauncher

### KIO

- KIO::iconNameForUrl returnerar nu speciella ikoner för XDG-platser såsom användarens bildkatalog
- ForwardingSlaveBase: Rätta hur flaggan Overwrite skickas till kio_desktop (fel 360487)
- Förbättra och exportera klassen KPasswdServerClient, klientens programmeringsgränssnitt för kpasswdserver
- [KPropertiesDialog] Ta bort alternativet "Lägg till i systembricka"
- [KPropertiesDialog] Ändra inte "Namn" på "Länk" .desktop-filer om filnamnet är skrivskyddat
- [KFileWidget] Använd urlFromString istället för QUrl(QString) i setSelection (fel 369216)
- Lägg till alternativ för att köra ett program på ett diskret grafikkort i KPropertiesDialog
- Terminera Dropjob på ett riktigt sätt efter att ha släppt på ett körbart program
- Rätta användning av loggningskategori på Windows
- DropJob: Skicka att kopieringsjobbet har startat efter skapande
- Lägg till stöd för att anropa suspend() för ett kopieringsjobb innan det startas
- Lägg till stöd för att omedelbart viloläge för jobb, åtminstone för SimpleJob och FileCopyJob

### KItemModels

- Uppdatera proxy-värden för nyligen påkommen klass av fel (hantering av layoutChanged)
- Gör det möjligt för KConcatenateRowsProxyModel att fungera i QML
- KExtraColumnsProxyModel: Gör modellindex beständiga efter att ha skickat layoutChange, inte innan
- Rätta assert (i beginRemoveRows) när ett tomt underliggande objekt av ett markerat underliggande objekt avmarkeras i Korganizer

### KJobWidgets

- Fokusera inte förloppsfönster (fel 333934)

### KNewStuff

- [GHNS-knapp] Dölj när Kiosk-begränsningar gäller
- Rätta inställning av ::Engine när den skapas med en absolut sökväg till inställningsfilen

### KNotification

- Lägg till en manuell test för Unity-startprogram
- [KNotificationRestrictions] Låt användare kunna ange sträng med begränsningsorsak

### Ramverket KPackage

- [PackageLoader] Försök inte komma åt ogiltig KPluginMetadata (fel 374541)
- Rätta beskrivning av väljaren -t på manualsidan
- Förbättra felmeddelande
- Rätta hjälpmeddelandet för --type
- Förbättra installationsprocessen av packar i KPackage
- Installera filen kpackage-generic.desktop
- Undanta qmlc-filer från installation
- Lista inte Plasmoider separat från metadata.desktop och .json
- Ytterligare rättning av paket med olika typer men samma id
- Rätta fel i cmake när två paket med olika typ har samma id

### KParts

- Anropa det nya checkAmbiguousShortcuts() från MainWindow::createShellGUI

### KService

- KSycoca: Följ inte symboliska länkar till kataloger, det skapar risk för rekursion

### KTextEditor

- Rättning: Att dra text framåt ger felaktig markering (fel 374163)

### Ramverket KWallet

- Ange minimal version av GpgME++ som krävs, 1.7.0
- Återställ "Om Gpgmepp inte hittas, försök använda KF5Gpgmepp"

### KWidgetsAddons

- Lägg till Python-bindningar
- Lägg till KToolTipWidget, ett verktygstips som innehåller en annan grafisk komponent
- Rätta kontroller i KDateComboBox för giltiga inmatade datum
- KMessageWidget: Använd mörkare röd färg när typen är Fel (fel 357210)

### KXMLGUI

- Varna för tvetydiga genvägar vid start (med undantag för Skift+Delete)
- MSWin och Mac har liknande beteende när fönsterstorlek sparas automatiskt
- Visa programversion i Om-dialogrutans namnlist (fel 372367)

### Oxygen-ikoner

- Synkronisera med Breeze-ikoner

### Plasma ramverk

- Rätta egenskapen renderType för diverse komponenter
- [ToolTipDialog] Använd KWindowSystem::isPlatformX11() som är lagrad i cache
- [Icon Item] Rätta uppdatering av implicit storlek när ikonstorlek ändras
- [Dialog] Använd setPosition och setSize istället för att ställa in allting individuellt
- [Units] Gör egenskapen iconSizes konstant
- Nu finns en global instans av "Units", vilket reducerar minnesåtgång och skapelsetid för SVG-objekt
- [Icon Item] Stöd ikoner som inte är fyrkantiga (fel 355592)
- Sök och ersätt gamla hårdkodade typer från plasmapkg2 (fel 374463)
- Rätta typerna X-Plasma-Drop* (fel 374418)
- [Plasma ScrollViewStyle] Visa bara rullningslistens bakgrund när musen hålls över den
- Rätta nr. 374127 felplacering av rutor från dockade fönster
- Avråd från användning av programmeringsgränssnittet Plasma::Package i PluginLoader
- Kontrollera igen vilken representation vi ska använda i setPreferredRepresentation
- [declarativeimports] Använd QtRendering för telefonenheter
- Betrakta alltid en tom panel som "miniprogram laddade" (fel 373836)
- Skicka toolTipMainTextChanged om det ändras som svar på en rubrikändring
- [TextField] Tillåt att knappen för att avslöja lösenord kan inaktiveras via Kiosk-begränsning
- [AppletQuickItem] Stöd felmeddelande vid start
- Rätta logik för hantering av pilar för höger-till-vänster landsinställningar (fel 373749)
- TextFieldStyle: Rätta värdet på implicitHeight så att textmarkören är centrerad

### Sonnet

- cmake: Leta också efter hunspell-1.6

### Syntaxfärgläggning

- Rätta färgläggning av Makefile.inc genom att lägga till prioritet i makefile.xml
- Färglägg SCXML-filer som XML
- makefile.xml: många förbättringar, för långa för att lista här
- python syntax: Tillägg av f-litteraler och förbättrad stränghantering

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
