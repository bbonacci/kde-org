---
aliases:
- ../../kde-frameworks-5.23.0
date: 2016-06-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Gör det faktiskt möjligt att avgöra leverantörer från angiven webbadress
- Tillhandahåll QDebug-hjälpfunktioner för vissa Attica-klasser
- Rätta omdirigering av absoluta webbadresser (fel 354748)

### Baloo

- Rätta användning av mellanslag i I/O-slav tags (fel 349118)

### Breeze-ikoner

- Lägg till CMake-alternativ för att bygga Qt-binärresurser från ikonkataloger
- Många nya och uppdaterade ikoner
- Uppdatera nerkopplad nätverksikon för större skillnad från ansluten (fel 353369)
- Uppdatera ikonerna montera och avmontera (fel 358925)
- Lägg till några avatarer från plasma-desktop/kcms/useraccount/pics/sources
- Ta bort chromium-ikon eftersom chromium-standardikon passar bra (fel 363595)
- Gör ikonerna för konsole ljusare (fel 355697)
- Lägg till e-postikoner för Thunderbird (fel 357334)
- Lägg till ikon för öppen nyckel (fel 361366)
- Ta bort process-working-kde eftersom Konquerors ikoner ska användas (fel 360304)
- Uppdatera ikon för Krusader enligt (fel 359863)
- Byt namn på ikoner för mikrofon enligt D1291 (fel D1291)
- Lägg till några Mime-typ ikoner för skript (fel 363040)
- Lägg till funktionalitet för av/på-skärmmeddelande för virtuellt tangentbord och tryckplatta

### Integrering med ramverk

- Ta bort oanvända beroenden och översättningshantering

### KActivities

- Lägg till egenskapen runningActivities i Consumer

### KDE Doxygen-verktyg

- Större omarbetning av dokumentationsgenerering av programmeringsgränssnittet

### KCMUtils

- Använd QQuickWidget för QML-inställningsmoduler (fel 359124)

### KConfig

- Undvik att leverera KAuthorized-kontroll

### KConfigWidgets

- Tillåt att ny typ av anslutningssyntax används med KStandardAction::create()

### KCoreAddons

- Skriv ut insticksprogram som misslyckas vid underrättelse om typkonverteringsvarning
- [kshareddatacache] Rätta felaktig användning av &amp; för att undvika ej anpassade läsningar
- Kdelibs4ConfigMigrator: Hoppa över omtolkning om ingenting konverterades
- krandom: Lägg till testfall för att upptäcka fel 362161 (misslyckat automatiskt frö)

### KCrash

- Kontrollera storlek hos Unix sökväg för domänuttag innan kopiering till det

### KDeclarative

- Stöd markerat tillstånd
- KCMShell import kan nu användas för att kontrollera om det faktiskt är tillåtet att öppna en inställningsmodul

### Stöd för KDELibs 4

- Varna för att KDateTimeParser::parseDateUnicode inte är implementerad
- K4TimeZoneWidget: Korrigera sökväg till flaggbilder

### KDocTools

- Lägg till ofta använda entiteter för tangenter i en/user.entities
- Uppdatera docbook-mallen för manual
- Uppdatera bok- och dokumentmallar och lägg till artikelmall
- Anropa bara kdoctools_create_handbook för index.docbook (fel 357428)

### KEmoticons

- Lägg till stöd för emojis i KEmoticon + Emoji One ikoner
- Lägg till stöd för egna smilisstorlekar

### KHTML

- Rätta potentiell minnesläcka rapporterad av Coverity och förenkla koden
- Antal lager bestäms av antal värden åtskilda med kommatecken i egenskapen ‘background-image’
- Rätta tolkning av bakgrundsposition i kortformsdeklaration
- Skapa inte ny fontFace om det inte finns någon giltig källa

### KIconThemes

- Låt inte KIconThemes bero på Oxygen (fel 360664)
- Markerat tillstånd koncept för ikoner
- Använd systemfärger för svartvita ikoner

### KInit

- Rätta kapplöpning där filen som innehåller X11-kakan har fel rättigheter under en kort stund
- Korrigera rättigheter för /tmp/xauth-xxx-_y

### KIO

- Ge tydligare felmeddelande när en webbadress utan schema skickas till KRun(URL) (fel 363337)
- Lägg till KProtocolInfo::archiveMimetypes()
- Använd markerat ikonläge i sidoraden i dialogrutan för att öppna en fil
- kshorturifilter: Rätta regression där mailto: inte läggs till i början när inget e-postprogram är installerat

### KJobWidgets

- Ange riktig "dialog"-flagga för dialogrutan med grafisk förloppskomponent

### KNewStuff

- Initiera inte KNS3::DownloadManager med fel kategorier
- Utöka öppet programmeringsgränssnitt för KNS3::Entry

### KNotification

- Använd QUrl::fromUserInput för att skapa ljudwebbadress (fel 337276)

### KNotifyConfig

- Använd QUrl::fromUserInput för att skapa ljudwebbadress (fel 337276)

### KService

- Rätta associerade program för Mime-typer med stora bokstäver
- Ändra uppslagningsnyckel för Mime-typer till små bokstäver för att göra den skiftlägesokänslig
- Rätta ksycoca-underrättelser när databasen inte ännu finns

### KTextEditor

- Rätta standardkodning till UTF-8 (fel 362604)
- Rätta färginställningsbarhet för standardstilen "Fel"
- Sök och ersätt: Rätta bakgrundsfärg för ersätt (regression som uppstod i v5.22) (fel 363441)
- Nytt färgschema "Breeze mörk", se https://kate-editor.org/?post=3745
- KateUndoManager::setUndoRedoCursorOfLastGroup(): Skicka Cursor som konstantreferens
- Förbättra syntaxfärgläggning i sql-postgresql.xml genom att ignorera funktionsimplementeringar med flera rader
- Lägg till syntaxfärgläggning för Elixir och Kotlin
- Syntaxfärgläggning för VHDL i ktexteditor: Lägg till stöd för funktioner inne i architecture-satser
- vimode: Krascha inte när ett intervall anges för ett icke existerande kommando (fel 360418)
- Ta bort sammansatta tecken på ett riktigt sätt när indiska landsinställningar används

### KUnitConversion

- Rätta nerladdning av valutakurser (fel 345750)

### Ramverket KWallet

- Konvertering av KWalletd: Rätta felhantering, förhindrar att konverteringen sker vid varje start.

### Kwayland

- [klient] Kontrollera inte resursversion för PlasmaWindow
- Lägg till händelse för ursprungligt tillstånd i Plasma fönsterprotokoll
- [server] Utlös fel om en transient begäran försöker bli sin egen överliggande begäran
- [server] Hantera fallet då ett PlasmaWindow avregistreras innan en klient binds till det
- [server] Hantera destruktor i SlideInterface på rätt sätt
- Lägg till stöd för beröringshändelser i protokollet och gränssnittet fakeinput
- [server] Standardisera hantering av destruktor-begäran för resurser
- Implementera gränssnitten wl_text_input och zwp_text_input_v2
- [server] Förhindra dubbel borttagning av återanropsresurser i SurfaceInterface
- [server] Lägg till kontroll av nollpekare till resurs i ShellSurfaceInterface
- [server] Jämför ClientConnection i stället för wl_client i SeatInterface
- [server] Förbättra hantering när klienter kopplar ner
- server/plasmawindowmanagement_interface.cpp: Rätta varningen -Wreorder
- [klient] Lägg till sammanhangspekare för anslutningar i PlasmaWindowModel
- Många rättningar relaterade till destruktion

### KWidgetsAddons

- Använd markerad ikoneffekt för nuvarande sida i KPageView

### KWindowSystem

- [platform xcb] Respektera begärd ikonstorlek (fel 362324)

### KXMLGUI

- Högerklick på ett programs menyrad tillåter nu inte längre förbipassering

### NetworkManagerQt

- Återställ "sluta stödja WiMAX för NM 1.2.0+" eftersom det förstör binärkompatibilitet

### Oxygen-ikoner

- Synkronisera väderikoner med Breeze
- Lägg till uppdateringsikoner

### Plasma ramverk

- Lägg till stöd för cantata systembricka (fel 363784)
- Markerat tillstånd för Plasma::Svg och IconItem
- DaysModel: Nollställ m_agendaNeedsUpdate när insticksprogram skickar nya händelser
- Uppdatera ljud- och nätverksikon för att få bättre kontrast (fel 356082)
- Avråd från downloadPath(const QString &amp;file) till förmån för downloadPath()
- [ikonminiatyrbild] Begäran av önskad ikonstorlek (fel 362324)
- Plasmoider kan nu avgöra om grafiska komponenter är låsta av användaren eller begränsningar av systemadministratören
- [ContainmentInterface] Försök inte visa tom QMenu
- Använd SAX för Plasma::Svg stilmallsersättning
- [DialogShadows] Lagra åtkomst till QX11Info::display()
- Återställ Luft Plasma temaikoner från KDE 4
- Läs in markerat färgschemat igen när färger ändras

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
