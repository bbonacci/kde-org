---
aliases:
- ../announce-applications-15.08-rc
date: 2015-08-06
description: KDE Ships Applications 15.08 Release Candidate.
layout: application
release: applications-15.07.90
title: KDE выпускает первую версию-кандидат к выпуску KDE Applications 15.08
---
6 августа 2015 года. Сегодня KDE выпустило версию-кандидат к выпуску KDE Applications. Теперь, когда программные зависимости и функциональность «заморожены», команда KDE сконцентрируется на исправлении ошибок и наведении красоты.

With the various applications being based on KDE Frameworks 5, the KDE Applications 15.08 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
